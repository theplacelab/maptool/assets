const router = require('express').Router();
var asset = require('../models/asset.js');
const { RESPONSE } = require('../constants.js');
const {
  requireInParams,
  requireToBeOwnerOrSuper
} = require('../models/validation.js');
const { display } = require('../modules/environment.js');

router.get('/:uuid', (req, res) => {
  requireInParams(['uuid'], req, res, () => {
    asset.read({
      knex: req.knex,
      uuid: req.params.uuid,
      cb: (data) => res.status(RESPONSE.OK).send(data)
    });
  });
});

router.delete('/:id', (req, res) => {
  asset.destroy(req.knex, req.params.id, (qRes) =>
    res.status(qRes === 0 ? 204 : 200).end()
  );
});

router.patch('/:id', (req, res) => {
  const { display_name, asset_type, asset_path, asset_metadata } = req.body;
  asset.update(
    req.knex,
    req.params.id,
    {
      display_name,
      asset_type,
      asset_path,
      asset_metadata,
      updated_at: Date.now()
    },
    (qRes) => res.status(RESPONSE.OK).send(qRes)
  );
});

router.get('/:id', (req, res) => {
  asset.read(req.knex, req.params.id, (qRes) =>
    res.status(RESPONSE.OK).send(qRes)
  );
});

module.exports = router;
