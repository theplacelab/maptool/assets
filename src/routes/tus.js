const router = require('express').Router();
const express = require('express');
const tusUpload = express();
const { Server, EVENTS } = require('@tus/server');
const { FileStore } = require('@tus/file-store');
const asset = require('../models/asset');

const tusServer = new Server({
  path: '/files',
  datastore: new FileStore({ directory: './files' }),
  respectForwardedHeaders: true, // Nginx support
  async onUploadCreate(req, res, upload) {
    // console.log(`Uploaded by: ${JSON.stringify(req.token)}`);
    //console.log(`>> CREATE: ${upload.id} | ${upload.metadata.filename}`);
    //console.log(upload);
    await asset.create({
      knex: req.knex,
      asset_type: '',
      asset_path: '',
      asset_metadata: upload,
      display_name: upload.metadata.filename,
      uuid: upload.id,
      created_by: req.token.uuid,
      asset_type: upload.metadata.filetype
    });
    return {
      res,
      metadata: { ...upload.metadata, dog: 'woof', token: req.token }
    };
  }
});

tusUpload.all('*', tusServer.handle.bind(tusServer));

// CREATE: Let TUS handle post and patch (used for upload)
router.post('/', (req, res) => tusUpload(req, res));
router.patch('/:id', (req, res) => tusUpload(req, res));
router.patch('/', (req, res) => tusUpload(req, res));

// READ: Special check to see if file is public or not
router.get('/:id', (req, res) => {
  // TODO: Check to see if user has access to req.params.id
  // Note, we've already validated jwt
  console.log(`>> Checking you can get: ${req.params.id}`);
  return tusUpload(req, res);
});

// DELETE
router.delete('/:id', (req, res) => {
  asset.destroy(req.knex, req.params.id, (qRes) =>
    res.status(qRes === 0 ? 204 : 200).end()
  );
});

module.exports = router;
