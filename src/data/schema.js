// prettier-ignore
module.exports = {
  tables:{
    assets: {
      uuid:           { type: "string",   maxlength: 36,    nullable: false, validations: { isUUID: true }},
      display_name:   { type: "text",     maxlength: 2000,  nullable: true },
      asset_path:     { type: "text",     maxlength: 2000,  nullable: true },
      asset_type:     { type: "text",     maxlength: 256,   nullable: true },
      asset_metadata: { type: "text",     maxlength: 65535, nullable: true },
      created_by:     { type: "string",   maxlength: 36,    nullable: false, validations: { isUUID: true } },
      created_at:     { type: "dateTime", nullable: false },
      updated_at:     { type: "dateTime", nullable: true },
      permission:     { type: "smallint",  default:0,  nullable: true }
    }
  }
};
