const { create } = require('../../models/asset.js');
const dbSchema = require('../schema.js');
const { createTable } = require('../util.js');

exports.up = function (knex) {
  return createTable(knex, 'assets');
};

exports.down = function (knex) {
  return knex.schema.dropTable('assets');
};
