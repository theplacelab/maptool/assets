const schema = require('./schema.js');
module.exports = {
  createTable: (knex, tableName) => {
    const tableDef = schema.tables[tableName];
    return knex.schema.createTable(tableName, (table) => {
      console.log(`DB: CREATING TABLE: ${tableName}`);
      Object.keys(tableDef).forEach((rowName) => {
        const row = tableDef[rowName];
        if (row.primary) table.primary(rowName);
        if (row.nullable) {
          table[row.type](rowName, row.maxlength).nullable();
        } else {
          table[row.type](rowName, row.maxlength).notNullable();
        }
      });
      table.increments();
    });
  }
};
