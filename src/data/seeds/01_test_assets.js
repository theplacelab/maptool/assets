const { v4: uuidv4 } = require('uuid');

exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('assets').del();
  await knex('assets').insert([
    {
      id: 1,
      uuid: uuidv4(),
      display_name: 'woof!',
      asset_path: 'woof.png',
      asset_type: 'image/png',
      asset_metadata: '{"tag":"test"}',
      created_by: uuidv4(),
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString()
    }
  ]);
};
