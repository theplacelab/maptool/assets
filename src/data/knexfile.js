// Builds knex config from .env, used by both knex-cli and app
// http://knexjs.org/#knexfile
const debug = require('../modules/debugLog.js');
const fs = require('fs');
require('dotenv').config({ path: require('find-config')('.env') });
const parse = require('pg-connection-string').parse;

let config;
if (process.env.PG_CONNECTION_STRING) {
  // Postgres
  const rejectUnauthorized =
    process.env.PG_SSL_STRICT?.toLowerCase() === 'false' ? false : true;
  config = {
    client: 'pg',
    connection: {
      ...parse(process.env.PG_CONNECTION_STRING),
      ssl: {
        rejectUnauthorized
      }
    }
  };
} else {
  // SQLite
  if (!fs.existsSync('data')) fs.mkdirSync('data');
  config = {
    client: 'sqlite3',
    connection: {
      filename: __dirname + '/sqlite/data.db'
    }
  };
}

// Append to both
module.exports = {
  ...config,
  useNullAsDefault: true,
  migrations: {
    tableName: 'knex_assets_migrations',
    directory: __dirname + '/migrations'
  },
  seeds: {
    directory: __dirname + '/seeds'
  }
};
console.log('\n');
debug.log(`⚡️ [${config.client}] connecting...`);
