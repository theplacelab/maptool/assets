'use strict';
require('dotenv').config({ path: require('find-config')('.env') });
const express = require('express');
const cors = require('cors');
const jwt = require('./modules/jwt');
const debug = require('./modules/debugLog.js');
const env = require('./modules/environment.js');
const bodyParser = require('body-parser');
const base = require('./routes/base');
const asset = require('./routes/asset.js');
const assets = require('./routes/assets.js');
const tus = require('./routes/tus.js');
const knexConfig = require('./data/knexfile.js');
const { RESPONSE } = require('./constants.js');

const app = express();

module.exports = {
  app,
  start: (cb, overrideKnex) => {
    if (env.validate()) {
      const knex = overrideKnex ? overrideKnex : require('knex')(knexConfig);
      knex.migrate.latest().then(() => {
        app.use(cors());
        app.use(bodyParser.json());
        app.use((err, _req, res, next) => {
          if (!err) next();
          console.error(err);
          res
            .status(RESPONSE.UNPROCESSABLE)
            .send({ error: 'FAILED: Invalid JSON' });
        });

        app.use((req, res, next) => {
          req.knex = knex;
          next();
        });

        app.use('/', debug);
        app.use('/', base);

        app.use('/assets', jwt.validate);
        app.use('/assets', assets);

        app.use('/asset', jwt.validate);
        app.use('/asset', asset);

        app.use('/files', jwt.validate);
        app.use('/files', tus);

        app.use((_req, res) => {
          debug.log(`REQ: 404 - No Route`);
          res.sendStatus(RESPONSE.NOT_FOUND);
        });

        env.display();
        if (typeof cb === 'function') cb(knex);
      });
    }
  }
};
