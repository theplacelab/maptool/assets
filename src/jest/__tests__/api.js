const request = require('supertest');
const { v4: uuidv4 } = require('uuid');
const server = require('../../app');
const parse = require('pg-connection-string').parse;
const fs = require('fs');
const path = require('path');
const os = require('os');

const tempDir = fs.mkdtempSync(path.join(os.tmpdir(), 'auth-service'));
const database = `${tempDir}/${uuidv4()}.db`;
const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: database
  },
  useNullAsDefault: true,
  migrations: {
    directory: __dirname + '/../../data/migrations'
  },
  seeds: {
    directory: __dirname + '/../../data/seeds'
  }
});

beforeAll((done) => {
  knex.migrate
    .latest()
    .then(() => {
      return knex.seed.run();
    })
    .then(() => done());
});

afterAll(() => {
  return knex.migrate.rollback().then(() =>
    knex.destroy().then(() => {
      fs.unlink(database, () => {});
    })
  );
});

describe(`API`, () => {
  test('Up should respond with 200', (done) => {
    server.start(() => {
      request(server.app)
        .get('/up')
        .then((response) => {
          try {
            expect(response.statusCode).toBe(RESPONSE.OK);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });

  test('Bogus endpoint should respond with 404', (done) => {
    server.start(() => {
      request(server.app)
        .get(`/${uuidv4()}`)
        .then((response) => {
          try {
            expect(response.statusCode).toBe(RESPONSE.NOT_FOUND);
            done();
          } catch (e) {
            done(e);
          }
        });
    }, knex);
  });
});
