const debug = require('./debugLog.js');
const parse = require('pg-connection-string').parse;

module.exports = {
  display: () => {
    debug.log(`🆙 listening on ${process.env.PORT || 8888}`);
    debug.log(`Debug: ON`);
    if (process.env.PG_CONNECTION_STRING) {
      const rejectUnauthorized =
        process.env.PG_SSL_STRICT?.toLowerCase() === 'false' ? false : true;
      debug.log('Postgres');
      debug.log(
        `DB: ${parse(process.env.PG_CONNECTION_STRING).database}@${
          parse(process.env.PG_CONNECTION_STRING).host
        }`
      );
      debug.log(`SSL: ${rejectUnauthorized ? 'strict' : 'permissive'}`);
    } else {
      debug.log('SQLite');
    }
  },
  validate: () => {
    if (
      !(
        process.env.AWS_ACCESS_KEY_ID &&
        process.env.AWS_SECRET_ACCESS_KEY &&
        process.env.AWS_BUCKET &&
        process.env.AWS_ENDPOINT &&
        process.env.AWS_REGION &&
        process.env.JWT_SECRET
      )
    ) {
      console.error('** FATAL: Server not listening, check configuration');
      return false;
    } else {
      return true;
    }
  }
};
