const fs = require('fs');
const schema = require('../data/schema');

module.exports = {
  init: async (knex) => {
    Object.keys(schema).forEach(async (tableName) => {
      const tableExists = await knex.schema.hasTable(tableName);
      if (!tableExists) {
        console.log(`INITIALIZING DB: CREATE TABLE: "${tableName}"`);
        const tableDef = schema[tableName];
        await knex.schema.createTable(tableName, (table) => {
          Object.keys(tableDef).forEach((rowName) => {
            const row = tableDef[rowName];
            if (row.primary) table.primary(rowName);
            if (row.nullable) {
              table[row.type](rowName, row.maxlength).nullable();
            } else {
              table[row.type](rowName, row.maxlength).notNullable();
            }
          });
          table.increments();
        });
      }
    });
  },
  destroy: (path) => {
    try {
      fs.unlinkSync(path);
    } catch (err) {
      console.error(err);
    }
  }
};
