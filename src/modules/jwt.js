let jwt = require('jsonwebtoken');
const { RESPONSE } = require('../constants.js');

module.exports.validate = function (req, res, next) {
  let bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          console.log(err);
          res.sendStatus(RESPONSE.NOT_AUTHORIZED);
        } else {
          req.token = decodedToken;
          next();
        }
      }
    );
  } else {
    res.sendStatus(RESPONSE.NOT_AUTHORIZED);
  }
};

module.exports.validateWithFallback = function (
  req,
  res,
  onValidate,
  onFallback
) {
  let bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearer = bearerHeader.split(' ');
    const bearerToken = bearer[1];
    jwt.verify(
      bearerToken,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err) => {
        if (err) {
          onFallback();
        } else {
          onValidate();
        }
      }
    );
  } else {
    onFallback();
  }
};
