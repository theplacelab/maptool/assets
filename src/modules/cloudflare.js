const axios = require("axios");
const apiUrl = "https://api.cloudflare.com/client/v4/zones";

module.exports = {
  purge: (files) => {
    const url = `${apiUrl}/${process.env.CLOUDFLARE_ZONE}/purge_cache`;
    axios
      .post(
        url,
        { files },
        {
          headers: {
            "X-Auth-Email": process.env.CLOUDFLARE_EMAIL,
            "X-Auth-Key": process.env.CLOUDFLARE_TOKEN,
            "Content-Type": "application/json; charset=utf-8",
          },
        }
      )
      .then((res) => {
        if (!res.data.success) console.log(res.data);
      })
      .catch((error) => {
        console.error(error);
      });
  },
  zones: (files) => {
    const url = `${apiUrl}/?per_page=100`;
    axios
      .post(
        url,
        { files },
        {
          headers: {
            "X-Auth-Email": process.env.CLOUDFLARE_EMAIL,
            "X-Auth-Key": process.env.CLOUDFLARE_TOKEN,
            "Content-Type": "application/json; charset=utf-8",
          },
        }
      )
      .then((res) => {
        if (!res.data.success) console.log(res.data);
      })
      .catch((error) => {
        console.error(error);
      });
  },
};
