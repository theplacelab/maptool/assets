let jwt = require('jsonwebtoken');
const { ROLE_RANK, RESPONSE } = require('../constants');
const debug = require('../modules/debugLog.js');

module.exports = {
  requireValidToken: (req, res, onValid, onInvalid) => {
    if (Object.keys(req?.headers).includes('authorization')) {
      let token = req.headers.authorization.split(' ')[1];
      if (!token) res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      jwt.verify(
        token,
        process.env.JWT_SECRET,
        { ignoreExpiration: false },
        (err, decodedToken) => {
          if (err) {
            if (typeof onInvalid === 'function') {
              onInvalid();
            } else {
              res.sendStatus(RESPONSE.NOT_AUTHORIZED);
            }
          } else {
            req.token = decodedToken;
            onValid({ token: decodedToken });
          }
        }
      );
    } else {
      if (typeof onInvalid === 'function') {
        onInvalid(null, null);
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    }
  },
  isSecureRequest: (req, res, onValid, onInvalid) =>
    module.exports.requireValidToken(req, res, onValid, onInvalid),
  requireMinimumRole: function (requiredRole, req, res, onValid) {
    module.exports.requireValidToken(req, res, ({ token }) => {
      if (ROLE_RANK[token.role] >= ROLE_RANK[requiredRole]) {
        onValid({ token });
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireMinimumRoleOrSelf: ({ uuid, requiredRole, req, res, onValid }) => {
    module.exports.requireValidToken(req, res, async ({ token }) => {
      const isSelf = uuid === token.uuid;
      if (ROLE_RANK[token.role] >= ROLE_RANK[requiredRole] || isSelf) {
        onValid({ token, isSelf });
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireSelf: function (id, req, res, onValid) {
    module.exports.requireValidToken(req, res, ({ token }) => {
      const tokenIdMatchesRequestedId = id === parseInt(tokenId, 10);
      if (tokenIdMatchesRequestedId) {
        onValid({ token });
      } else {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
      }
    });
  },
  requireValidRefreshToken: (req, res, onValid) => {
    if (Object.keys(req?.headers).includes('authorization')) {
      const token = req.headers.authorization.split(' ')[1];
      const decoded = jwt.decode(token, { complete: true })?.payload;
      if (!token || !decoded || !Object.keys(decoded)?.includes('uuid')) {
        res.sendStatus(RESPONSE.NOT_AUTHORIZED);
        return;
      }
      req.knex
        .select()
        .from('users')
        .where({ uuid: decoded.uuid })
        .then(([user]) => {
          jwt.verify(
            token,
            user?.refresh_secret,
            { ignoreExpiration: false },
            (err, decodedToken) => {
              if (err) {
                debug.log(`RT: ${err.message}`);
                res.sendStatus(RESPONSE.NOT_AUTHORIZED);
              } else {
                onValid({
                  ...user,
                  extend: decodedToken.extended
                });
              }
            }
          );
        });
    } else {
      res.sendStatus(RESPONSE.NOT_AUTHORIZED);
    }
  },
  requireInBody: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (
        !Object.keys(req.body).includes(param) ||
        req[param]?.trim().length === 0
      )
        failedOn = param;
    });
    if (failedOn)
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    onValid();
  },
  requireInParams: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (!req.params[param] || req.params[param]?.trim().length === 0)
        failedOn = param;
    });
    if (failedOn)
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    onValid();
  },
  requireIntInParams: (params, req, res, onValid) => {
    let failedOn;
    params.forEach((param) => {
      if (
        !req.params[param] ||
        req.params[param]?.trim().length === 0 ||
        isNaN(parseInt(req.params[param], 10))
      ) {
        failedOn = param;
      } else {
        req.params[param] = parseInt(req.params[param], 10);
      }
    });
    if (failedOn)
      return res
        .status(RESPONSE.BAD_REQUEST)
        .send({ error: `${failedOn} required` });
    onValid();
  },
  requireValidPostedValidationToken: (req, res, onValid) => {
    const { token } = req.body;
    if (token) {
      const decoded = jwt.decode(token, { complete: true }).payload;
      if (!decoded.email) {
        res.sendStatus(RESPONSE.GONE);
        return;
      }
      const { email } = decoded;
      req.knex
        .select()
        .from('users')
        .where({ email })
        .then(([user]) => {
          jwt.verify(
            token,
            user?.refresh_secret,
            { ignoreExpiration: false },
            (err, decodedToken) => {
              if (err) {
                res.sendStatus(RESPONSE.GONE);
              } else {
                onValid(email);
              }
            }
          );
        });
    } else {
      res.sendStatus(RESPONSE.GONE);
    }
  },
  decodeToken: (reqOrToken, onSuccess, onFail) => {
    let token;
    if (reqOrToken.headers) {
      let bearerHeader = reqOrToken.headers['authorization'];
      if (typeof bearerHeader !== 'undefined') {
        const bearer = bearerHeader.split(' ');
        token = bearer[1];
      }
    } else {
      token = reqOrToken;
    }
    jwt.verify(
      token,
      process.env.JWT_SECRET,
      { ignoreExpiration: false },
      (err, decodedToken) => {
        if (err) {
          if (typeof onFail === 'function') {
            onFail(err);
          } else {
            console.error(err);
          }
        } else {
          onSuccess(decodedToken);
        }
      }
    );
  },
  requireToBeOwnerOrSuper: async (req, res, onValid) => {
    if (token.role === ROLE_RANK.websuper) {
      return onValid();
    } else {
      const q = await req.knex
        .select('*')
        .from('assets')
        .where({ uuid: req.params.uuid, created_by: req.token.uuid });
      if (q.length) {
        return onValid();
      } else {
        return res
          .status(RESPONSE.NOT_AUTHORIZED)
          .send({ error: `Not owner or super` });
      }
    }
  }
};
