module.exports = {
  VOID_ROOM: 'VOID',
  SELF: 'SELF',
  MSG_TYPE: {
    SYS: 'SYS',
    SYS_CHAT: 'SYS_CHAT',
    CHAT: 'CHAT',
    ROOM_CHANGE: 'ROOM_CHANGE',
    USER_UPDATE: 'USER_UPDATE',
    INTRODUCE_SELF: 'INTRODUCE_SELF'
  },
  TARGET: {
    GLOBAL: 'GLOBAL',
    SELF: 'SELF',
    ROOM: 'ROOM'
  },
  EVENT: {
    USER: {
      CONNECTED: 'USER_CONNECTED',
      DISCONNECTED: 'USER_DISCONNECTED',
      ROOM_ENTER: 'ROOM_ENTER',
      ROOM_EXIT: 'ROOM_EXIT',
      ROOM_UPDATE: 'ROOM_UPDATE',
      INTRODUCTION: 'INTRODUCTION'
    },
    SERVER: {
      LEAVE_ME_ALONE: 'LEAVE_ME_ALONE',
      REASON: { DUPLICATE: 'DUPLICATE' }
    }
  },
  ROLE: {
    ANON: 'webanon',
    USER: 'webuser',
    SUPER: 'websuper',
    SERVICE: 'service'
  },
  ROLE_RANK: {
    webanon: 0,
    webuser: 1,
    websuper: 2,
    service: 3
  },
  RESPONSE: {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    NOT_AUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    CONFLICT: 409,
    GONE: 410,
    INTERNAL_SERVER_ERROR: 500,
    NOT_IMPLEMENTED: 501,
    INSUFFICIENT_STORAGE: 507,
    UNPROCESSABLE: 422
  }
};
