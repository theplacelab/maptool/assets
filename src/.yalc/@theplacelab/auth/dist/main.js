module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/defineProperty.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _defineProperty; });
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/extends.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/extends.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _extends; });
function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/esm/objectSpread2.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _objectSpread2; });
/* harmony import */ var _defineProperty_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./defineProperty.js */ "./node_modules/@babel/runtime/helpers/esm/defineProperty.js");


function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        Object(_defineProperty_js__WEBPACK_IMPORTED_MODULE_0__["default"])(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableRest.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/slicedToArray.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles.js */ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js");

var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit.js */ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableRest = __webpack_require__(/*! ./nonIterableRest.js */ "./node_modules/@babel/runtime/helpers/nonIterableRest.js");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/@redux-saga/core/dist/io-6de156f3.js":
/*!***********************************************************!*\
  !*** ./node_modules/@redux-saga/core/dist/io-6de156f3.js ***!
  \***********************************************************/
/*! exports provided: $, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, _, a, a0, a1, a2, a3, a4, a5, a6, a7, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "$", function() { return apply; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "A", function() { return ALL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "B", function() { return logError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "C", function() { return CALL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "D", function() { return wrapSagaDispatch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "E", function() { return identity; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "F", function() { return FORK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "G", function() { return GET_CONTEXT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "H", function() { return buffers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "I", function() { return detach; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "J", function() { return JOIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "K", function() { return take; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "L", function() { return fork; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "M", function() { return cancel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "N", function() { return call; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "O", function() { return actionChannel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P", function() { return PUT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Q", function() { return sliding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "R", function() { return RACE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S", function() { return SELECT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "T", function() { return TAKE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "U", function() { return delay; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "V", function() { return race; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "W", function() { return effectTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "X", function() { return takeMaybe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Y", function() { return put; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Z", function() { return putResolve; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_", function() { return all; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CPS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a0", function() { return cps; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a1", function() { return spawn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a2", function() { return join; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a3", function() { return select; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a4", function() { return cancelled; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a5", function() { return flush; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a6", function() { return getContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a7", function() { return setContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CANCEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return check; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return ACTION_CHANNEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return expanding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return CANCELLED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "g", function() { return FLUSH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "h", function() { return SET_CONTEXT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "i", function() { return internalErr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "j", function() { return getMetaInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "k", function() { return kTrue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "l", function() { return createAllStyleChildCallbacks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "m", function() { return createEmptyArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "n", function() { return none; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "o", function() { return once; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "p", function() { return assignWithSymbols; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "q", function() { return makeIterator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "r", function() { return remove; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "s", function() { return shouldComplete; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "t", function() { return noop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "u", function() { return flatMap; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "v", function() { return getLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "w", function() { return createSetContextWarning; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "x", function() { return asyncIteratorSymbol; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "y", function() { return shouldCancel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "z", function() { return shouldTerminate; });
/* harmony import */ var _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @redux-saga/symbols */ "./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js");
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @redux-saga/is */ "./node_modules/@redux-saga/is/dist/redux-saga-is.esm.js");
/* harmony import */ var _redux_saga_delay_p__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @redux-saga/delay-p */ "./node_modules/@redux-saga/delay-p/dist/redux-saga-delay-p.esm.js");





var konst = function konst(v) {
  return function () {
    return v;
  };
};
var kTrue =
/*#__PURE__*/
konst(true);

var noop = function noop() {};

if ( true && typeof Proxy !== 'undefined') {
  noop =
  /*#__PURE__*/
  new Proxy(noop, {
    set: function set() {
      throw internalErr('There was an attempt to assign a property to internal `noop` function.');
    }
  });
}
var identity = function identity(v) {
  return v;
};
var hasSymbol = typeof Symbol === 'function';
var asyncIteratorSymbol = hasSymbol && Symbol.asyncIterator ? Symbol.asyncIterator : '@@asyncIterator';
function check(value, predicate, error) {
  if (!predicate(value)) {
    throw new Error(error);
  }
}
var assignWithSymbols = function assignWithSymbols(target, source) {
  Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_1__["default"])(target, source);

  if (Object.getOwnPropertySymbols) {
    Object.getOwnPropertySymbols(source).forEach(function (s) {
      target[s] = source[s];
    });
  }
};
var flatMap = function flatMap(mapper, arr) {
  var _ref;

  return (_ref = []).concat.apply(_ref, arr.map(mapper));
};
function remove(array, item) {
  var index = array.indexOf(item);

  if (index >= 0) {
    array.splice(index, 1);
  }
}
function once(fn) {
  var called = false;
  return function () {
    if (called) {
      return;
    }

    called = true;
    fn();
  };
}

var kThrow = function kThrow(err) {
  throw err;
};

var kReturn = function kReturn(value) {
  return {
    value: value,
    done: true
  };
};

function makeIterator(next, thro, name) {
  if (thro === void 0) {
    thro = kThrow;
  }

  if (name === void 0) {
    name = 'iterator';
  }

  var iterator = {
    meta: {
      name: name
    },
    next: next,
    throw: thro,
    return: kReturn,
    isSagaIterator: true
  };

  if (typeof Symbol !== 'undefined') {
    iterator[Symbol.iterator] = function () {
      return iterator;
    };
  }

  return iterator;
}
function logError(error, _ref2) {
  var sagaStack = _ref2.sagaStack;

  /*eslint-disable no-console*/
  console.error(error);
  console.error(sagaStack);
}
var internalErr = function internalErr(err) {
  return new Error("\n  redux-saga: Error checking hooks detected an inconsistent state. This is likely a bug\n  in redux-saga code and not yours. Thanks for reporting this in the project's github repo.\n  Error: " + err + "\n");
};
var createSetContextWarning = function createSetContextWarning(ctx, props) {
  return (ctx ? ctx + '.' : '') + "setContext(props): argument " + props + " is not a plain object";
};
var FROZEN_ACTION_ERROR = "You can't put (a.k.a. dispatch from saga) frozen actions.\nWe have to define a special non-enumerable property on those actions for scheduling purposes.\nOtherwise you wouldn't be able to communicate properly between sagas & other subscribers (action ordering would become far less predictable).\nIf you are using redux and you care about this behaviour (frozen actions),\nthen you might want to switch to freezing actions in a middleware rather than in action creator.\nExample implementation:\n\nconst freezeActions = store => next => action => next(Object.freeze(action))\n"; // creates empty, but not-holey array

var createEmptyArray = function createEmptyArray(n) {
  return Array.apply(null, new Array(n));
};
var wrapSagaDispatch = function wrapSagaDispatch(dispatch) {
  return function (action) {
    if (true) {
      check(action, function (ac) {
        return !Object.isFrozen(ac);
      }, FROZEN_ACTION_ERROR);
    }

    return dispatch(Object.defineProperty(action, _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["SAGA_ACTION"], {
      value: true
    }));
  };
};
var shouldTerminate = function shouldTerminate(res) {
  return res === _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["TERMINATE"];
};
var shouldCancel = function shouldCancel(res) {
  return res === _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["TASK_CANCEL"];
};
var shouldComplete = function shouldComplete(res) {
  return shouldTerminate(res) || shouldCancel(res);
};
function createAllStyleChildCallbacks(shape, parentCallback) {
  var keys = Object.keys(shape);
  var totalCount = keys.length;

  if (true) {
    check(totalCount, function (c) {
      return c > 0;
    }, 'createAllStyleChildCallbacks: get an empty array or object');
  }

  var completedCount = 0;
  var completed;
  var results = Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["array"])(shape) ? createEmptyArray(totalCount) : {};
  var childCallbacks = {};

  function checkEnd() {
    if (completedCount === totalCount) {
      completed = true;
      parentCallback(results);
    }
  }

  keys.forEach(function (key) {
    var chCbAtKey = function chCbAtKey(res, isErr) {
      if (completed) {
        return;
      }

      if (isErr || shouldComplete(res)) {
        parentCallback.cancel();
        parentCallback(res, isErr);
      } else {
        results[key] = res;
        completedCount++;
        checkEnd();
      }
    };

    chCbAtKey.cancel = noop;
    childCallbacks[key] = chCbAtKey;
  });

  parentCallback.cancel = function () {
    if (!completed) {
      completed = true;
      keys.forEach(function (key) {
        return childCallbacks[key].cancel();
      });
    }
  };

  return childCallbacks;
}
function getMetaInfo(fn) {
  return {
    name: fn.name || 'anonymous',
    location: getLocation(fn)
  };
}
function getLocation(instrumented) {
  return instrumented[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["SAGA_LOCATION"]];
}

var BUFFER_OVERFLOW = "Channel's Buffer overflow!";
var ON_OVERFLOW_THROW = 1;
var ON_OVERFLOW_DROP = 2;
var ON_OVERFLOW_SLIDE = 3;
var ON_OVERFLOW_EXPAND = 4;
var zeroBuffer = {
  isEmpty: kTrue,
  put: noop,
  take: noop
};

function ringBuffer(limit, overflowAction) {
  if (limit === void 0) {
    limit = 10;
  }

  var arr = new Array(limit);
  var length = 0;
  var pushIndex = 0;
  var popIndex = 0;

  var push = function push(it) {
    arr[pushIndex] = it;
    pushIndex = (pushIndex + 1) % limit;
    length++;
  };

  var take = function take() {
    if (length != 0) {
      var it = arr[popIndex];
      arr[popIndex] = null;
      length--;
      popIndex = (popIndex + 1) % limit;
      return it;
    }
  };

  var flush = function flush() {
    var items = [];

    while (length) {
      items.push(take());
    }

    return items;
  };

  return {
    isEmpty: function isEmpty() {
      return length == 0;
    },
    put: function put(it) {
      if (length < limit) {
        push(it);
      } else {
        var doubledLimit;

        switch (overflowAction) {
          case ON_OVERFLOW_THROW:
            throw new Error(BUFFER_OVERFLOW);

          case ON_OVERFLOW_SLIDE:
            arr[pushIndex] = it;
            pushIndex = (pushIndex + 1) % limit;
            popIndex = pushIndex;
            break;

          case ON_OVERFLOW_EXPAND:
            doubledLimit = 2 * limit;
            arr = flush();
            length = arr.length;
            pushIndex = arr.length;
            popIndex = 0;
            arr.length = doubledLimit;
            limit = doubledLimit;
            push(it);
            break;

          default: // DROP

        }
      }
    },
    take: take,
    flush: flush
  };
}

var none = function none() {
  return zeroBuffer;
};
var fixed = function fixed(limit) {
  return ringBuffer(limit, ON_OVERFLOW_THROW);
};
var dropping = function dropping(limit) {
  return ringBuffer(limit, ON_OVERFLOW_DROP);
};
var sliding = function sliding(limit) {
  return ringBuffer(limit, ON_OVERFLOW_SLIDE);
};
var expanding = function expanding(initialSize) {
  return ringBuffer(initialSize, ON_OVERFLOW_EXPAND);
};

var buffers = /*#__PURE__*/Object.freeze({
  __proto__: null,
  none: none,
  fixed: fixed,
  dropping: dropping,
  sliding: sliding,
  expanding: expanding
});

var TAKE = 'TAKE';
var PUT = 'PUT';
var ALL = 'ALL';
var RACE = 'RACE';
var CALL = 'CALL';
var CPS = 'CPS';
var FORK = 'FORK';
var JOIN = 'JOIN';
var CANCEL = 'CANCEL';
var SELECT = 'SELECT';
var ACTION_CHANNEL = 'ACTION_CHANNEL';
var CANCELLED = 'CANCELLED';
var FLUSH = 'FLUSH';
var GET_CONTEXT = 'GET_CONTEXT';
var SET_CONTEXT = 'SET_CONTEXT';

var effectTypes = /*#__PURE__*/Object.freeze({
  __proto__: null,
  TAKE: TAKE,
  PUT: PUT,
  ALL: ALL,
  RACE: RACE,
  CALL: CALL,
  CPS: CPS,
  FORK: FORK,
  JOIN: JOIN,
  CANCEL: CANCEL,
  SELECT: SELECT,
  ACTION_CHANNEL: ACTION_CHANNEL,
  CANCELLED: CANCELLED,
  FLUSH: FLUSH,
  GET_CONTEXT: GET_CONTEXT,
  SET_CONTEXT: SET_CONTEXT
});

var TEST_HINT = '\n(HINT: if you are getting these errors in tests, consider using createMockTask from @redux-saga/testing-utils)';

var makeEffect = function makeEffect(type, payload) {
  var _ref;

  return _ref = {}, _ref[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["IO"]] = true, _ref.combinator = false, _ref.type = type, _ref.payload = payload, _ref;
};

var isForkEffect = function isForkEffect(eff) {
  return Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["effect"])(eff) && eff.type === FORK;
};

var detach = function detach(eff) {
  if (true) {
    check(eff, isForkEffect, 'detach(eff): argument must be a fork effect');
  }

  return makeEffect(FORK, Object(_babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_1__["default"])({}, eff.payload, {
    detached: true
  }));
};
function take(patternOrChannel, multicastPattern) {
  if (patternOrChannel === void 0) {
    patternOrChannel = '*';
  }

  if ( true && arguments.length) {
    check(arguments[0], _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'take(patternOrChannel): patternOrChannel is undefined');
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["pattern"])(patternOrChannel)) {
    return makeEffect(TAKE, {
      pattern: patternOrChannel
    });
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["multicast"])(patternOrChannel) && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"])(multicastPattern) && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["pattern"])(multicastPattern)) {
    return makeEffect(TAKE, {
      channel: patternOrChannel,
      pattern: multicastPattern
    });
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["channel"])(patternOrChannel)) {
    return makeEffect(TAKE, {
      channel: patternOrChannel
    });
  }

  if (true) {
    throw new Error("take(patternOrChannel): argument " + patternOrChannel + " is not valid channel or a valid pattern");
  }
}
var takeMaybe = function takeMaybe() {
  var eff = take.apply(void 0, arguments);
  eff.payload.maybe = true;
  return eff;
};
function put(channel$1, action) {
  if (true) {
    if (arguments.length > 1) {
      check(channel$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'put(channel, action): argument channel is undefined');
      check(channel$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["channel"], "put(channel, action): argument " + channel$1 + " is not a valid channel");
      check(action, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'put(channel, action): argument action is undefined');
    } else {
      check(channel$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'put(action): argument action is undefined');
    }
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["undef"])(action)) {
    action = channel$1; // `undefined` instead of `null` to make default parameter work

    channel$1 = undefined;
  }

  return makeEffect(PUT, {
    channel: channel$1,
    action: action
  });
}
var putResolve = function putResolve() {
  var eff = put.apply(void 0, arguments);
  eff.payload.resolve = true;
  return eff;
};
function all(effects) {
  var eff = makeEffect(ALL, effects);
  eff.combinator = true;
  return eff;
}
function race(effects) {
  var eff = makeEffect(RACE, effects);
  eff.combinator = true;
  return eff;
} // this match getFnCallDescriptor logic

var validateFnDescriptor = function validateFnDescriptor(effectName, fnDescriptor) {
  check(fnDescriptor, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], effectName + ": argument fn is undefined or null");

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"])(fnDescriptor)) {
    return;
  }

  var context = null;
  var fn;

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["array"])(fnDescriptor)) {
    context = fnDescriptor[0];
    fn = fnDescriptor[1];
    check(fn, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], effectName + ": argument of type [context, fn] has undefined or null `fn`");
  } else if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["object"])(fnDescriptor)) {
    context = fnDescriptor.context;
    fn = fnDescriptor.fn;
    check(fn, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], effectName + ": argument of type {context, fn} has undefined or null `fn`");
  } else {
    check(fnDescriptor, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"], effectName + ": argument fn is not function");
    return;
  }

  if (context && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["string"])(fn)) {
    check(context[fn], _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"], effectName + ": context arguments has no such method - \"" + fn + "\"");
    return;
  }

  check(fn, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"], effectName + ": unpacked fn argument (from [context, fn] or {context, fn}) is not a function");
};

function getFnCallDescriptor(fnDescriptor, args) {
  var context = null;
  var fn;

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"])(fnDescriptor)) {
    fn = fnDescriptor;
  } else {
    if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["array"])(fnDescriptor)) {
      context = fnDescriptor[0];
      fn = fnDescriptor[1];
    } else {
      context = fnDescriptor.context;
      fn = fnDescriptor.fn;
    }

    if (context && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["string"])(fn) && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"])(context[fn])) {
      fn = context[fn];
    }
  }

  return {
    context: context,
    fn: fn,
    args: args
  };
}

var isNotDelayEffect = function isNotDelayEffect(fn) {
  return fn !== delay;
};

function call(fnDescriptor) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }

  if (true) {
    var arg0 = typeof args[0] === 'number' ? args[0] : 'ms';
    check(fnDescriptor, isNotDelayEffect, "instead of writing `yield call(delay, " + arg0 + ")` where delay is an effect from `redux-saga/effects` you should write `yield delay(" + arg0 + ")`");
    validateFnDescriptor('call', fnDescriptor);
  }

  return makeEffect(CALL, getFnCallDescriptor(fnDescriptor, args));
}
function apply(context, fn, args) {
  if (args === void 0) {
    args = [];
  }

  var fnDescriptor = [context, fn];

  if (true) {
    validateFnDescriptor('apply', fnDescriptor);
  }

  return makeEffect(CALL, getFnCallDescriptor([context, fn], args));
}
function cps(fnDescriptor) {
  if (true) {
    validateFnDescriptor('cps', fnDescriptor);
  }

  for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
    args[_key2 - 1] = arguments[_key2];
  }

  return makeEffect(CPS, getFnCallDescriptor(fnDescriptor, args));
}
function fork(fnDescriptor) {
  if (true) {
    validateFnDescriptor('fork', fnDescriptor);
    check(fnDescriptor, function (arg) {
      return !Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["effect"])(arg);
    }, 'fork: argument must not be an effect');
  }

  for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
    args[_key3 - 1] = arguments[_key3];
  }

  return makeEffect(FORK, getFnCallDescriptor(fnDescriptor, args));
}
function spawn(fnDescriptor) {
  if (true) {
    validateFnDescriptor('spawn', fnDescriptor);
  }

  for (var _len4 = arguments.length, args = new Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
    args[_key4 - 1] = arguments[_key4];
  }

  return detach(fork.apply(void 0, [fnDescriptor].concat(args)));
}
function join(taskOrTasks) {
  if (true) {
    if (arguments.length > 1) {
      throw new Error('join(...tasks) is not supported any more. Please use join([...tasks]) to join multiple tasks.');
    }

    if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["array"])(taskOrTasks)) {
      taskOrTasks.forEach(function (t) {
        check(t, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["task"], "join([...tasks]): argument " + t + " is not a valid Task object " + TEST_HINT);
      });
    } else {
      check(taskOrTasks, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["task"], "join(task): argument " + taskOrTasks + " is not a valid Task object " + TEST_HINT);
    }
  }

  return makeEffect(JOIN, taskOrTasks);
}
function cancel(taskOrTasks) {
  if (taskOrTasks === void 0) {
    taskOrTasks = _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["SELF_CANCELLATION"];
  }

  if (true) {
    if (arguments.length > 1) {
      throw new Error('cancel(...tasks) is not supported any more. Please use cancel([...tasks]) to cancel multiple tasks.');
    }

    if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["array"])(taskOrTasks)) {
      taskOrTasks.forEach(function (t) {
        check(t, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["task"], "cancel([...tasks]): argument " + t + " is not a valid Task object " + TEST_HINT);
      });
    } else if (taskOrTasks !== _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["SELF_CANCELLATION"] && Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"])(taskOrTasks)) {
      check(taskOrTasks, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["task"], "cancel(task): argument " + taskOrTasks + " is not a valid Task object " + TEST_HINT);
    }
  }

  return makeEffect(CANCEL, taskOrTasks);
}
function select(selector) {
  if (selector === void 0) {
    selector = identity;
  }

  for (var _len5 = arguments.length, args = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
    args[_key5 - 1] = arguments[_key5];
  }

  if ( true && arguments.length) {
    check(arguments[0], _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'select(selector, [...]): argument selector is undefined');
    check(selector, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"], "select(selector, [...]): argument " + selector + " is not a function");
  }

  return makeEffect(SELECT, {
    selector: selector,
    args: args
  });
}
/**
  channel(pattern, [buffer])    => creates a proxy channel for store actions
**/

function actionChannel(pattern$1, buffer$1) {
  if (true) {
    check(pattern$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["pattern"], 'actionChannel(pattern,...): argument pattern is not valid');

    if (arguments.length > 1) {
      check(buffer$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'actionChannel(pattern, buffer): argument buffer is undefined');
      check(buffer$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["buffer"], "actionChannel(pattern, buffer): argument " + buffer$1 + " is not a valid buffer");
    }
  }

  return makeEffect(ACTION_CHANNEL, {
    pattern: pattern$1,
    buffer: buffer$1
  });
}
function cancelled() {
  return makeEffect(CANCELLED, {});
}
function flush(channel$1) {
  if (true) {
    check(channel$1, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["channel"], "flush(channel): argument " + channel$1 + " is not valid channel");
  }

  return makeEffect(FLUSH, channel$1);
}
function getContext(prop) {
  if (true) {
    check(prop, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["string"], "getContext(prop): argument " + prop + " is not a string");
  }

  return makeEffect(GET_CONTEXT, prop);
}
function setContext(props) {
  if (true) {
    check(props, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["object"], createSetContextWarning(null, props));
  }

  return makeEffect(SET_CONTEXT, props);
}
var delay =
/*#__PURE__*/
call.bind(null, _redux_saga_delay_p__WEBPACK_IMPORTED_MODULE_3__["default"]);




/***/ }),

/***/ "./node_modules/@redux-saga/core/dist/redux-saga-effects.esm.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@redux-saga/core/dist/redux-saga-effects.esm.js ***!
  \**********************************************************************/
/*! exports provided: actionChannel, all, apply, call, cancel, cancelled, cps, delay, effectTypes, flush, fork, getContext, join, put, putResolve, race, select, setContext, spawn, take, takeMaybe, debounce, retry, takeEvery, takeLatest, takeLeading, throttle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return debounce; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "retry", function() { return retry$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "takeEvery", function() { return takeEvery$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "takeLatest", function() { return takeLatest$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "takeLeading", function() { return takeLeading$1; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "throttle", function() { return throttle$1; });
/* harmony import */ var _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @redux-saga/symbols */ "./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js");
/* harmony import */ var _babel_runtime_helpers_esm_extends__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/esm/extends */ "./node_modules/@babel/runtime/helpers/esm/extends.js");
/* harmony import */ var _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @redux-saga/is */ "./node_modules/@redux-saga/is/dist/redux-saga-is.esm.js");
/* harmony import */ var _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./io-6de156f3.js */ "./node_modules/@redux-saga/core/dist/io-6de156f3.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "actionChannel", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["O"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "all", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["_"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "apply", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["$"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "call", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["N"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cancel", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["M"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cancelled", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a4"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cps", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a0"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "delay", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["U"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "effectTypes", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["W"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "flush", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a5"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fork", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getContext", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a6"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "join", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a2"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "put", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["Y"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "putResolve", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["Z"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "race", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["V"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "select", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a3"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a7"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spawn", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["a1"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "take", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeMaybe", function() { return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["X"]; });

/* harmony import */ var _redux_saga_delay_p__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @redux-saga/delay-p */ "./node_modules/@redux-saga/delay-p/dist/redux-saga-delay-p.esm.js");







var done = function done(value) {
  return {
    done: true,
    value: value
  };
};

var qEnd = {};
function safeName(patternOrChannel) {
  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["channel"])(patternOrChannel)) {
    return 'channel';
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["stringableFunc"])(patternOrChannel)) {
    return String(patternOrChannel);
  }

  if (Object(_redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["func"])(patternOrChannel)) {
    return patternOrChannel.name;
  }

  return String(patternOrChannel);
}
function fsmIterator(fsm, startState, name) {
  var stateUpdater,
      errorState,
      effect,
      nextState = startState;

  function next(arg, error) {
    if (nextState === qEnd) {
      return done(arg);
    }

    if (error && !errorState) {
      nextState = qEnd;
      throw error;
    } else {
      stateUpdater && stateUpdater(arg);
      var currentState = error ? fsm[errorState](error) : fsm[nextState]();
      nextState = currentState.nextState;
      effect = currentState.effect;
      stateUpdater = currentState.stateUpdater;
      errorState = currentState.errorState;
      return nextState === qEnd ? done(arg) : effect;
    }
  }

  return Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["q"])(next, function (error) {
    return next(null, error);
  }, name);
}

function takeEvery(patternOrChannel, worker) {
  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  var yTake = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(patternOrChannel)
  };

  var yFork = function yFork(ac) {
    return {
      done: false,
      value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [worker].concat(args, [ac]))
    };
  };

  var action,
      setAction = function setAction(ac) {
    return action = ac;
  };

  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yTake,
        stateUpdater: setAction
      };
    },
    q2: function q2() {
      return {
        nextState: 'q1',
        effect: yFork(action)
      };
    }
  }, 'q1', "takeEvery(" + safeName(patternOrChannel) + ", " + worker.name + ")");
}

function takeLatest(patternOrChannel, worker) {
  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  var yTake = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(patternOrChannel)
  };

  var yFork = function yFork(ac) {
    return {
      done: false,
      value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [worker].concat(args, [ac]))
    };
  };

  var yCancel = function yCancel(task) {
    return {
      done: false,
      value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["M"])(task)
    };
  };

  var task, action;

  var setTask = function setTask(t) {
    return task = t;
  };

  var setAction = function setAction(ac) {
    return action = ac;
  };

  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yTake,
        stateUpdater: setAction
      };
    },
    q2: function q2() {
      return task ? {
        nextState: 'q3',
        effect: yCancel(task)
      } : {
        nextState: 'q1',
        effect: yFork(action),
        stateUpdater: setTask
      };
    },
    q3: function q3() {
      return {
        nextState: 'q1',
        effect: yFork(action),
        stateUpdater: setTask
      };
    }
  }, 'q1', "takeLatest(" + safeName(patternOrChannel) + ", " + worker.name + ")");
}

function takeLeading(patternOrChannel, worker) {
  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  var yTake = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(patternOrChannel)
  };

  var yCall = function yCall(ac) {
    return {
      done: false,
      value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["N"].apply(void 0, [worker].concat(args, [ac]))
    };
  };

  var action;

  var setAction = function setAction(ac) {
    return action = ac;
  };

  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yTake,
        stateUpdater: setAction
      };
    },
    q2: function q2() {
      return {
        nextState: 'q1',
        effect: yCall(action)
      };
    }
  }, 'q1', "takeLeading(" + safeName(patternOrChannel) + ", " + worker.name + ")");
}

function throttle(delayLength, pattern, worker) {
  for (var _len = arguments.length, args = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    args[_key - 3] = arguments[_key];
  }

  var action, channel;
  var yActionChannel = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["O"])(pattern, Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["Q"])(1))
  };

  var yTake = function yTake() {
    return {
      done: false,
      value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(channel)
    };
  };

  var yFork = function yFork(ac) {
    return {
      done: false,
      value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [worker].concat(args, [ac]))
    };
  };

  var yDelay = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["U"])(delayLength)
  };

  var setAction = function setAction(ac) {
    return action = ac;
  };

  var setChannel = function setChannel(ch) {
    return channel = ch;
  };

  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yActionChannel,
        stateUpdater: setChannel
      };
    },
    q2: function q2() {
      return {
        nextState: 'q3',
        effect: yTake(),
        stateUpdater: setAction
      };
    },
    q3: function q3() {
      return {
        nextState: 'q4',
        effect: yFork(action)
      };
    },
    q4: function q4() {
      return {
        nextState: 'q2',
        effect: yDelay
      };
    }
  }, 'q1', "throttle(" + safeName(pattern) + ", " + worker.name + ")");
}

function retry(maxTries, delayLength, fn) {
  var counter = maxTries;

  for (var _len = arguments.length, args = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    args[_key - 3] = arguments[_key];
  }

  var yCall = {
    done: false,
    value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["N"].apply(void 0, [fn].concat(args))
  };
  var yDelay = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["U"])(delayLength)
  };
  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yCall,
        errorState: 'q10'
      };
    },
    q2: function q2() {
      return {
        nextState: qEnd
      };
    },
    q10: function q10(error) {
      counter -= 1;

      if (counter <= 0) {
        throw error;
      }

      return {
        nextState: 'q1',
        effect: yDelay
      };
    }
  }, 'q1', "retry(" + fn.name + ")");
}

function debounceHelper(delayLength, patternOrChannel, worker) {
  for (var _len = arguments.length, args = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    args[_key - 3] = arguments[_key];
  }

  var action, raceOutput;
  var yTake = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(patternOrChannel)
  };
  var yRace = {
    done: false,
    value: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["V"])({
      action: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["K"])(patternOrChannel),
      debounce: Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["U"])(delayLength)
    })
  };

  var yFork = function yFork(ac) {
    return {
      done: false,
      value: _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [worker].concat(args, [ac]))
    };
  };

  var yNoop = function yNoop(value) {
    return {
      done: false,
      value: value
    };
  };

  var setAction = function setAction(ac) {
    return action = ac;
  };

  var setRaceOutput = function setRaceOutput(ro) {
    return raceOutput = ro;
  };

  return fsmIterator({
    q1: function q1() {
      return {
        nextState: 'q2',
        effect: yTake,
        stateUpdater: setAction
      };
    },
    q2: function q2() {
      return {
        nextState: 'q3',
        effect: yRace,
        stateUpdater: setRaceOutput
      };
    },
    q3: function q3() {
      return raceOutput.debounce ? {
        nextState: 'q1',
        effect: yFork(action)
      } : {
        nextState: 'q2',
        effect: yNoop(raceOutput.action),
        stateUpdater: setAction
      };
    }
  }, 'q1', "debounce(" + safeName(patternOrChannel) + ", " + worker.name + ")");
}

var validateTakeEffect = function validateTakeEffect(fn, patternOrChannel, worker) {
  Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["c"])(patternOrChannel, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], fn.name + " requires a pattern or channel");
  Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["c"])(worker, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], fn.name + " requires a saga parameter");
};

function takeEvery$1(patternOrChannel, worker) {
  if (true) {
    validateTakeEffect(takeEvery$1, patternOrChannel, worker);
  }

  for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
    args[_key - 2] = arguments[_key];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [takeEvery, patternOrChannel, worker].concat(args));
}
function takeLatest$1(patternOrChannel, worker) {
  if (true) {
    validateTakeEffect(takeLatest$1, patternOrChannel, worker);
  }

  for (var _len2 = arguments.length, args = new Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
    args[_key2 - 2] = arguments[_key2];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [takeLatest, patternOrChannel, worker].concat(args));
}
function takeLeading$1(patternOrChannel, worker) {
  if (true) {
    validateTakeEffect(takeLeading$1, patternOrChannel, worker);
  }

  for (var _len3 = arguments.length, args = new Array(_len3 > 2 ? _len3 - 2 : 0), _key3 = 2; _key3 < _len3; _key3++) {
    args[_key3 - 2] = arguments[_key3];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [takeLeading, patternOrChannel, worker].concat(args));
}
function throttle$1(ms, pattern, worker) {
  if (true) {
    Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["c"])(pattern, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'throttle requires a pattern');
    Object(_io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["c"])(worker, _redux_saga_is__WEBPACK_IMPORTED_MODULE_2__["notUndef"], 'throttle requires a saga parameter');
  }

  for (var _len4 = arguments.length, args = new Array(_len4 > 3 ? _len4 - 3 : 0), _key4 = 3; _key4 < _len4; _key4++) {
    args[_key4 - 3] = arguments[_key4];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [throttle, ms, pattern, worker].concat(args));
}
function retry$1(maxTries, delayLength, worker) {
  for (var _len5 = arguments.length, args = new Array(_len5 > 3 ? _len5 - 3 : 0), _key5 = 3; _key5 < _len5; _key5++) {
    args[_key5 - 3] = arguments[_key5];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["N"].apply(void 0, [retry, maxTries, delayLength, worker].concat(args));
}
function debounce(delayLength, pattern, worker) {
  for (var _len6 = arguments.length, args = new Array(_len6 > 3 ? _len6 - 3 : 0), _key6 = 3; _key6 < _len6; _key6++) {
    args[_key6 - 3] = arguments[_key6];
  }

  return _io_6de156f3_js__WEBPACK_IMPORTED_MODULE_3__["L"].apply(void 0, [debounceHelper, delayLength, pattern, worker].concat(args));
}




/***/ }),

/***/ "./node_modules/@redux-saga/delay-p/dist/redux-saga-delay-p.esm.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@redux-saga/delay-p/dist/redux-saga-delay-p.esm.js ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @redux-saga/symbols */ "./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js");


function delayP(ms, val) {
  if (val === void 0) {
    val = true;
  }

  var timeoutId;
  var promise = new Promise(function (resolve) {
    timeoutId = setTimeout(resolve, ms, val);
  });

  promise[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["CANCEL"]] = function () {
    clearTimeout(timeoutId);
  };

  return promise;
}

/* harmony default export */ __webpack_exports__["default"] = (delayP);


/***/ }),

/***/ "./node_modules/@redux-saga/is/dist/redux-saga-is.esm.js":
/*!***************************************************************!*\
  !*** ./node_modules/@redux-saga/is/dist/redux-saga-is.esm.js ***!
  \***************************************************************/
/*! exports provided: array, buffer, channel, effect, func, iterable, iterator, multicast, notUndef, number, object, observable, pattern, promise, sagaAction, string, stringableFunc, symbol, task, undef */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "array", function() { return array; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "buffer", function() { return buffer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "channel", function() { return channel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "effect", function() { return effect; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "func", function() { return func; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "iterable", function() { return iterable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "iterator", function() { return iterator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "multicast", function() { return multicast; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notUndef", function() { return notUndef; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "number", function() { return number; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "object", function() { return object; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observable", function() { return observable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pattern", function() { return pattern; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "promise", function() { return promise; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sagaAction", function() { return sagaAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "string", function() { return string; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stringableFunc", function() { return stringableFunc; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "symbol", function() { return symbol; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "task", function() { return task; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "undef", function() { return undef; });
/* harmony import */ var _redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @redux-saga/symbols */ "./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js");


var undef = function undef(v) {
  return v === null || v === undefined;
};
var notUndef = function notUndef(v) {
  return v !== null && v !== undefined;
};
var func = function func(f) {
  return typeof f === 'function';
};
var number = function number(n) {
  return typeof n === 'number';
};
var string = function string(s) {
  return typeof s === 'string';
};
var array = Array.isArray;
var object = function object(obj) {
  return obj && !array(obj) && typeof obj === 'object';
};
var promise = function promise(p) {
  return p && func(p.then);
};
var iterator = function iterator(it) {
  return it && func(it.next) && func(it.throw);
};
var iterable = function iterable(it) {
  return it && func(Symbol) ? func(it[Symbol.iterator]) : array(it);
};
var task = function task(t) {
  return t && t[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["TASK"]];
};
var sagaAction = function sagaAction(a) {
  return Boolean(a && a[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["SAGA_ACTION"]]);
};
var observable = function observable(ob) {
  return ob && func(ob.subscribe);
};
var buffer = function buffer(buf) {
  return buf && func(buf.isEmpty) && func(buf.take) && func(buf.put);
};
var pattern = function pattern(pat) {
  return pat && (string(pat) || symbol(pat) || func(pat) || array(pat) && pat.every(pattern));
};
var channel = function channel(ch) {
  return ch && func(ch.take) && func(ch.close);
};
var stringableFunc = function stringableFunc(f) {
  return func(f) && f.hasOwnProperty('toString');
};
var symbol = function symbol(sym) {
  return Boolean(sym) && typeof Symbol === 'function' && sym.constructor === Symbol && sym !== Symbol.prototype;
};
var multicast = function multicast(ch) {
  return channel(ch) && ch[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["MULTICAST"]];
};
var effect = function effect(eff) {
  return eff && eff[_redux_saga_symbols__WEBPACK_IMPORTED_MODULE_0__["IO"]];
};




/***/ }),

/***/ "./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@redux-saga/symbols/dist/redux-saga-symbols.esm.js ***!
  \*************************************************************************/
/*! exports provided: CANCEL, CHANNEL_END_TYPE, IO, MATCH, MULTICAST, SAGA_ACTION, SAGA_LOCATION, SELF_CANCELLATION, TASK, TASK_CANCEL, TERMINATE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CANCEL", function() { return CANCEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHANNEL_END_TYPE", function() { return CHANNEL_END_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IO", function() { return IO; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MATCH", function() { return MATCH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MULTICAST", function() { return MULTICAST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SAGA_ACTION", function() { return SAGA_ACTION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SAGA_LOCATION", function() { return SAGA_LOCATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SELF_CANCELLATION", function() { return SELF_CANCELLATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TASK", function() { return TASK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TASK_CANCEL", function() { return TASK_CANCEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TERMINATE", function() { return TERMINATE; });
var createSymbol = function createSymbol(name) {
  return "@@redux-saga/" + name;
};

var CANCEL =
/*#__PURE__*/
createSymbol('CANCEL_PROMISE');
var CHANNEL_END_TYPE =
/*#__PURE__*/
createSymbol('CHANNEL_END');
var IO =
/*#__PURE__*/
createSymbol('IO');
var MATCH =
/*#__PURE__*/
createSymbol('MATCH');
var MULTICAST =
/*#__PURE__*/
createSymbol('MULTICAST');
var SAGA_ACTION =
/*#__PURE__*/
createSymbol('SAGA_ACTION');
var SELF_CANCELLATION =
/*#__PURE__*/
createSymbol('SELF_CANCELLATION');
var TASK =
/*#__PURE__*/
createSymbol('TASK');
var TASK_CANCEL =
/*#__PURE__*/
createSymbol('TASK_CANCEL');
var TERMINATE =
/*#__PURE__*/
createSymbol('TERMINATE');
var SAGA_LOCATION =
/*#__PURE__*/
createSymbol('LOCATION');




/***/ }),

/***/ "./node_modules/classnames/index.js":
/*!******************************************!*\
  !*** ./node_modules/classnames/index.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
  Copyright (c) 2018 Jed Watson.
  Licensed under the MIT License (MIT), see
  http://jedwatson.github.io/classnames
*/
/* global define */

(function () {
	'use strict';

	var hasOwn = {}.hasOwnProperty;

	function classNames() {
		var classes = [];

		for (var i = 0; i < arguments.length; i++) {
			var arg = arguments[i];
			if (!arg) continue;

			var argType = typeof arg;

			if (argType === 'string' || argType === 'number') {
				classes.push(arg);
			} else if (Array.isArray(arg)) {
				if (arg.length) {
					var inner = classNames.apply(null, arg);
					if (inner) {
						classes.push(inner);
					}
				}
			} else if (argType === 'object') {
				if (arg.toString === Object.prototype.toString) {
					for (var key in arg) {
						if (hasOwn.call(arg, key) && arg[key]) {
							classes.push(key);
						}
					}
				} else {
					classes.push(arg.toString());
				}
			}
		}

		return classes.join(' ');
	}

	if ( true && module.exports) {
		classNames.default = classNames;
		module.exports = classNames;
	} else if (true) {
		// register as 'classnames', consistent with npm package name
		!(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_RESULT__ = (function () {
			return classNames;
		}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	} else {}
}());


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Circle/style.module.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Circle/style.module.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._3TfwnqG9S3_0uKqFKmI6mK {\n    display: inline-block;\n    margin: 8px;\n    border-radius: 50%;\n    animation: _3TfwnqG9S3_0uKqFKmI6mK 2.4s cubic-bezier(0, 0.2, 0.8, 1) infinite;\n  }\n@keyframes _3TfwnqG9S3_0uKqFKmI6mK {\n    0%, 100% {\n      animation-timing-function: cubic-bezier(0.5, 0, 1, 0.5);\n    }\n    0% {\n      transform: rotateY(0deg);\n    }\n    50% {\n      transform: rotateY(1800deg);\n      animation-timing-function: cubic-bezier(0, 0.5, 0.5, 1);\n    }\n    100% {\n      transform: rotateY(3600deg);\n    }\n}", ""]);
// Exports
exports.locals = {
	"lds-circle": "_3TfwnqG9S3_0uKqFKmI6mK"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Default/style.module.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Default/style.module.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._29J4YbhnFiezdW9nPwGTLZ {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n._29J4YbhnFiezdW9nPwGTLZ div {\n  position: absolute;\n  width: 6px;\n  height: 6px;\n  background: #fff;\n  border-radius: 50%;\n  animation: _29J4YbhnFiezdW9nPwGTLZ 1.2s linear infinite;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(1) {\n  animation-delay: 0s;\n  top: 46.25%;\n  left: 82.5%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(2) {\n  animation-delay: -0.1s;\n  top: 27.5%;\n  left: 77.5%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(3) {\n  animation-delay: -0.2s;\n  top: 13.75%;\n  left: 65%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(4) {\n  animation-delay: -0.3s;\n  top: 8.75%;\n  left: 46.25%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(5) {\n  animation-delay: -0.4s;\n  top: 13.75%;\n  left: 27.5%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(6) {\n  animation-delay: -0.5s;\n  top: 27.5%;\n  left: 13.75%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(7) {\n  animation-delay: -0.6s;\n  top: 46.25%;\n  left: 8.75%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(8) {\n  animation-delay: -0.7s;\n  top: 65%;\n  left: 13.75%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(9) {\n  animation-delay: -0.8s;\n  top: 77.5%;\n  left: 27.5%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(10) {\n  animation-delay: -0.9s;\n  top: 82.5%;\n  left: 46.25%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(11) {\n  animation-delay: -1s;\n  top: 77.5%;\n  left: 65%;\n}\n._29J4YbhnFiezdW9nPwGTLZ div:nth-child(12) {\n  animation-delay: -1.1s;\n  top: 65%;\n  left: 77.5%;\n}\n@keyframes _29J4YbhnFiezdW9nPwGTLZ {\n  0%,\n  20%,\n  80%,\n  100% {\n    transform: scale(1);\n  }\n  50% {\n    transform: scale(1.5);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-default": "_29J4YbhnFiezdW9nPwGTLZ"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._5mQtfP7HcDVAQBaDFlvW3 {\n  display: inline-block;\n  width: 80px;\n  height: 80px;\n}\n.fDsxRdFXe4Nwq0G1AZ8Pw {\n  content: ' ';\n  display: block;\n  width: 64px;\n  height: 64px;\n  margin: 8px;\n  border-radius: 50%;\n  border: 6px solid #000;\n  border-color: #000 transparent #000 transparent;\n  animation: _5mQtfP7HcDVAQBaDFlvW3 1.2s linear infinite;\n}\n@keyframes _5mQtfP7HcDVAQBaDFlvW3 {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-dual-ring": "_5mQtfP7HcDVAQBaDFlvW3",
	"lds-dual-ring-after": "fDsxRdFXe4Nwq0G1AZ8Pw"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._2hfoBLWHipnQyXXCeG7Y5i {\n    display: inline-block;\n    position: relative;\n  }\n  ._2hfoBLWHipnQyXXCeG7Y5i div {\n    position: absolute;\n    top: 41.25%;\n    width: 16.25%;\n    height: 16.25%;\n    border-radius: 50%;\n    background: #fff;\n    animation-timing-function: cubic-bezier(0, 1, 1, 0);\n  }\n  ._2hfoBLWHipnQyXXCeG7Y5i div:nth-child(1) {\n    left: 10%;\n    animation: _2PvItpP5IbCE0nFXWmvz6F 0.6s infinite;\n  }\n  ._2hfoBLWHipnQyXXCeG7Y5i div:nth-child(2) {\n    left: 10%;\n    animation: _3jldTgOw96Lv4N0LEyJU2 0.6s infinite;\n  }\n  ._2hfoBLWHipnQyXXCeG7Y5i div:nth-child(3) {\n    left: 40%;\n    animation: _3jldTgOw96Lv4N0LEyJU2 0.6s infinite;\n  }\n  ._2hfoBLWHipnQyXXCeG7Y5i div:nth-child(4) {\n    left: 70%;\n    animation: cFUSZV-8WYMJ3AuMqmrOe 0.6s infinite;\n  }\n  @keyframes _2PvItpP5IbCE0nFXWmvz6F {\n    0% {\n      transform: scale(0);\n    }\n    100% {\n      transform: scale(1);\n    }\n  }\n  @keyframes cFUSZV-8WYMJ3AuMqmrOe {\n    0% {\n      transform: scale(1);\n    }\n    100% {\n      transform: scale(0);\n    }\n  }\n  @keyframes _3jldTgOw96Lv4N0LEyJU2 {\n    0% {\n      transform: translate(0, 0);\n    }\n    100% {\n      transform: translate(184.61%, 0);\n    }\n  }", ""]);
// Exports
exports.locals = {
	"lds-ellipsis": "_2hfoBLWHipnQyXXCeG7Y5i",
	"lds-ellipsis1": "_2PvItpP5IbCE0nFXWmvz6F",
	"lds-ellipsis2": "_3jldTgOw96Lv4N0LEyJU2",
	"lds-ellipsis3": "cFUSZV-8WYMJ3AuMqmrOe"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._2-fL0ZIn6icSZnAuoIAwr4 {\n    display: inline-block;\n    position: relative;\n    width: 80px;\n    height: 80px;\n  }\n  ._2-fL0ZIn6icSZnAuoIAwr4 div {\n    display: inline-block;\n    position: absolute;\n    left: 10%;\n    width: 20%;\n    background: #fff;\n    animation: _2-fL0ZIn6icSZnAuoIAwr4 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;\n  }\n  ._2-fL0ZIn6icSZnAuoIAwr4 div:nth-child(1) {\n    left: 10%;\n    animation-delay: -0.24s;\n  }\n  ._2-fL0ZIn6icSZnAuoIAwr4 div:nth-child(2) {\n    left: 40%;\n    animation-delay: -0.12s;\n  }\n  ._2-fL0ZIn6icSZnAuoIAwr4 div:nth-child(3) {\n    left: 70%;\n    animation-delay: 0;\n  }\n  @keyframes _2-fL0ZIn6icSZnAuoIAwr4 {\n    0% {\n      top: 10%;\n      height: 80%;\n    }\n    50%, 100% {\n      top: 30%;\n      height: 40%;\n    }\n  }\n", ""]);
// Exports
exports.locals = {
	"lds-facebook": "_2-fL0ZIn6icSZnAuoIAwr4"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Grid/style.module.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Grid/style.module.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._31p459qIlv9db0P_Ocm3Ip {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n._31p459qIlv9db0P_Ocm3Ip div {\n  position: absolute;\n  width: 20%;\n  height: 20%;\n  border-radius: 50%;\n  background: #fff;\n  animation: _31p459qIlv9db0P_Ocm3Ip 1.2s linear infinite;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(1) {\n  top: 10%;\n  left: 10%;\n  animation-delay: 0s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(2) {\n  top: 10%;\n  left: 40%;\n  animation-delay: -0.4s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(3) {\n  top: 10%;\n  left: 70%;\n  animation-delay: -0.8s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(4) {\n  top: 40%;\n  left: 10%;\n  animation-delay: -0.4s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(5) {\n  top: 40%;\n  left: 40%;\n  animation-delay: -0.8s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(6) {\n  top: 40%;\n  left: 70%;\n  animation-delay: -1.2s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(7) {\n  top: 70%;\n  left: 10%;\n  animation-delay: -0.8s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(8) {\n  top: 70%;\n  left: 40%;\n  animation-delay: -1.2s;\n}\n._31p459qIlv9db0P_Ocm3Ip div:nth-child(9) {\n  top: 70%;\n  left: 70%;\n  animation-delay: -1.6s;\n}\n@keyframes _31p459qIlv9db0P_Ocm3Ip {\n  0%,\n  100% {\n    opacity: 1;\n  }\n  50% {\n    opacity: 0.5;\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-grid": "_31p459qIlv9db0P_Ocm3Ip"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Heart/style.module.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Heart/style.module.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._1O2sOvxzXnm48r84qwO3fB {\n    display: inline-block;\n    position: relative;\n    width: 80px;\n    height: 80px;\n    transform: rotate(45deg);\n    transform-origin: 40px 40px;\n  }\n  ._1O2sOvxzXnm48r84qwO3fB > div {\n    top: 32px;\n    left: 32px;\n    position: absolute;\n    width: 32px;\n    height: 32px;\n    background: #fff;\n    animation: _1O2sOvxzXnm48r84qwO3fB 1.2s infinite cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  ._1O2sOvxzXnm48r84qwO3fB ._3NykDX6sm7RrNbkWfGDqoq,\n  ._1O2sOvxzXnm48r84qwO3fB ._1iSQEJFErhVQkyY9UOoeij {\n    content: \" \";\n    position: absolute;\n    display: block;\n    width: 32px;\n    height: 32px;\n    background: #fff;\n  }\n  ._1O2sOvxzXnm48r84qwO3fB ._1iSQEJFErhVQkyY9UOoeij {\n    left: -24px;\n    border-radius: 50% 0 0 50%;\n  }\n  ._1O2sOvxzXnm48r84qwO3fB ._3NykDX6sm7RrNbkWfGDqoq {\n    top: -24px;\n    border-radius: 50% 50% 0 0;\n  }\n  @keyframes _1O2sOvxzXnm48r84qwO3fB {\n    0% {\n      transform: scale(0.95);\n    }\n    5% {\n      transform: scale(1.1);\n    }\n    39% {\n      transform: scale(0.85);\n    }\n    45% {\n      transform: scale(1);\n    }\n    60% {\n      transform: scale(0.95);\n    }\n    100% {\n      transform: scale(0.9);\n    }\n  }", ""]);
// Exports
exports.locals = {
	"lds-heart": "_1O2sOvxzXnm48r84qwO3fB",
	"div-after": "_3NykDX6sm7RrNbkWfGDqoq",
	"div-before": "_1iSQEJFErhVQkyY9UOoeij"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css ***!
  \****************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._1u-TqT1YdFL0wR1c18QdX9 {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n.yt3wAWWN9RWsaefmuiPQh {\n  content: ' ';\n  display: block;\n  border-radius: 50%;\n  width: 0;\n  height: 0;\n  margin: 8px;\n  box-sizing: border-box;\n  border: 32px solid #fff;\n  border-color: #fff transparent #fff transparent;\n  animation: _1u-TqT1YdFL0wR1c18QdX9 1.2s infinite;\n}\n@keyframes _1u-TqT1YdFL0wR1c18QdX9 {\n  0% {\n    transform: rotate(0);\n    animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);\n  }\n  50% {\n    transform: rotate(900deg);\n    animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);\n  }\n  100% {\n    transform: rotate(1800deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-hourglass": "_1u-TqT1YdFL0wR1c18QdX9",
	"lds-hourglass-after": "yt3wAWWN9RWsaefmuiPQh"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._3J-Dolx6qVQRlSgkIOTEj9 {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n._3J-Dolx6qVQRlSgkIOTEj9 * {\n  --center: translate(-50%, -50%);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._2STl7T8zXX7SObdEkTdZGe {\n  position: absolute;\n  width: 15px;\n  height: 15px;\n  border-radius: 50%;\n  top: 50%;\n  left: 50%;\n  transform: var(--center);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._20JTK7W5gJhnZf62wVceA-,\n._3J-Dolx6qVQRlSgkIOTEj9 ._3U2K3Dr_inBbu5dNGpXFy_ {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._1eLDIJ463UuJjh1Cv_UcjB {\n  position: absolute;\n  width: 31px;\n  height: 31px;\n  border-radius: 50%;\n  border: 3px solid;\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._18ri_Rt7ycSdd-jHMtxMQl {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(45deg);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._2s10v1qbGdmLT9FffvL5DD {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(25deg);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 .Yis9GoDnitNHuSOMO78aj {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  transform: var(--center) translate(17px, 0);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 .wwc--Y8chY4MHv28YLVOl {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 8px;\n  height: 8px;\n  border-radius: 50%;\n  transform: var(--center) translate(-17px, 0);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._9CgiDswH0cOmgRrhxE90P {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(65deg) scale(-1, -1);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._1dN_G28dxqfrGQLTGIpFCO {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(45deg) scale(-1, -1);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._3oBEUPcK_FyG4-MLoXPcUL {\n  position: absolute;\n  width: 60px;\n  height: 60px;\n  border-radius: 50%;\n  border: 3px solid;\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._25JbdaftrjNEqa0sNtH6YT {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(65deg);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._3xbkAM7hF4yowAyIEJaxwV {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(45deg);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._2WqCbyIGTX-KmSVgmf1toj {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 9px;\n  height: 9px;\n  border-radius: 50%;\n  transform: var(--center) translate(32px, 0);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 .PzcYTkjA8LO7Cv-NIjjIk {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  width: 9px;\n  height: 9px;\n  border-radius: 50%;\n  transform: var(--center) translate(-32px, 0);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._3atyw_iCOwPn9l-fcgQD1F {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(65deg) scale(-1, -1);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._2XnLjcvFaJMB2-ysv031U8 {\n  border-color: transparent transparent transparent;\n  transform: var(--center) rotate(45deg) scale(-1, -1);\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._20JTK7W5gJhnZf62wVceA- {\n  animation: _3446DGDYg0UqutSkaBeNMK 4s linear infinite;\n}\n._3J-Dolx6qVQRlSgkIOTEj9 ._3U2K3Dr_inBbu5dNGpXFy_ {\n  animation: _3446DGDYg0UqutSkaBeNMK 3s linear infinite;\n}\n@keyframes _3446DGDYg0UqutSkaBeNMK {\n  100% {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-orbitals": "_3J-Dolx6qVQRlSgkIOTEj9",
	"center": "_2STl7T8zXX7SObdEkTdZGe",
	"outer-spin": "_20JTK7W5gJhnZf62wVceA-",
	"inner-spin": "_3U2K3Dr_inBbu5dNGpXFy_",
	"inner-arc": "_1eLDIJ463UuJjh1Cv_UcjB",
	"inner-arc_start-a": "_18ri_Rt7ycSdd-jHMtxMQl",
	"inner-arc_end-a": "_2s10v1qbGdmLT9FffvL5DD",
	"inner-moon-a": "Yis9GoDnitNHuSOMO78aj",
	"inner-moon-b": "wwc--Y8chY4MHv28YLVOl",
	"inner-arc_start-b": "_9CgiDswH0cOmgRrhxE90P",
	"inner-arc_end-b": "_1dN_G28dxqfrGQLTGIpFCO",
	"outer-arc": "_3oBEUPcK_FyG4-MLoXPcUL",
	"outer-arc_start-a": "_25JbdaftrjNEqa0sNtH6YT",
	"outer-arc_end-a": "_3xbkAM7hF4yowAyIEJaxwV",
	"outer-moon-a": "_2WqCbyIGTX-KmSVgmf1toj",
	"outer-moon-b": "PzcYTkjA8LO7Cv-NIjjIk",
	"outer-arc_start-b": "_3atyw_iCOwPn9l-fcgQD1F",
	"outer-arc_end-b": "_2XnLjcvFaJMB2-ysv031U8",
	"spin": "_3446DGDYg0UqutSkaBeNMK"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css ***!
  \***************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._1jWUgyiFEM-M9iQUjS4Zqh {\n  position: relative;\n  display: inline-block;\n  height: 64px;\n  width: 64px;\n  margin: 0.5em;\n  border-radius: 50%;\n  overflow: hidden;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1) inset, 0 0 25px rgba(0, 0, 255, 0.075);\n}\n\n._1jWUgyiFEM-M9iQUjS4Zqh:after {\n  content: '';\n  position: relative;\n  top: 15%;\n  left: 15%;\n  display: block;\n  height: 70%;\n  width: 70%;\n  background: none repeat scroll 0 0 #f2f2f2;\n  border-radius: 50%;\n  box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\n}\n._1jWUgyiFEM-M9iQUjS4Zqh > span {\n  position: absolute;\n  height: 100%;\n  width: 50%;\n  overflow: hidden;\n}\n._1jWUgyiFEM-M9iQUjS4Zqh > ._1bCK3TxGolAWqIVUDdw4lz {\n  left: 0;\n}\n._1jWUgyiFEM-M9iQUjS4Zqh > .qmzHv02AmUTuzZtIJbAIs {\n  left: 50%;\n}\n\n._1jWUgyiFEM-M9iQUjS4Zqh > ._1bCK3TxGolAWqIVUDdw4lz > ._3EcK8HPwnhYJ38MpQ0w9Vj,\n._1jWUgyiFEM-M9iQUjS4Zqh > .qmzHv02AmUTuzZtIJbAIs > ._3EcK8HPwnhYJ38MpQ0w9Vj {\n  position: absolute;\n  left: 100%;\n  top: 0;\n  height: 100%;\n  width: 100%;\n  border-radius: 999px;\n  background: none repeat scroll 0 0 #508ec3;\n  animation: _3htQBvQX33orWJADEakjD8 3s infinite;\n  opacity: 0.8;\n  transform-origin: 0 50% 0;\n}\n._1jWUgyiFEM-M9iQUjS4Zqh > ._1bCK3TxGolAWqIVUDdw4lz > ._3EcK8HPwnhYJ38MpQ0w9Vj {\n  border-bottom-left-radius: 0;\n  border-top-left-radius: 0;\n}\n._1jWUgyiFEM-M9iQUjS4Zqh > .qmzHv02AmUTuzZtIJbAIs > ._3EcK8HPwnhYJ38MpQ0w9Vj {\n  border-bottom-right-radius: 0;\n  border-top-right-radius: 0;\n  left: -100%;\n  transform-origin: 100% 50% 0;\n}\n\n@keyframes _3htQBvQX33orWJADEakjD8 {\n  0% {\n    transform: rotate(0deg);\n  }\n  25% {\n    transform: rotate(0deg);\n  }\n  50% {\n    transform: rotate(180deg);\n  }\n  75% {\n    transform: rotate(180deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-ouroboro": "_1jWUgyiFEM-M9iQUjS4Zqh",
	"left": "_1bCK3TxGolAWqIVUDdw4lz",
	"right": "qmzHv02AmUTuzZtIJbAIs",
	"anim": "_3EcK8HPwnhYJ38MpQ0w9Vj",
	"lds-ouroboro-rotate": "_3htQBvQX33orWJADEakjD8"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ring/style.module.css":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Ring/style.module.css ***!
  \***********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._1evXIL5gZqPkMQrfbcOKzu {\n  display: inline-block;\n  position: relative;\n}\n._1evXIL5gZqPkMQrfbcOKzu div {\n  box-sizing: border-box;\n  display: block;\n  position: absolute;\n  border: 8px solid #fff;\n  border-radius: 50%;\n  animation: _1evXIL5gZqPkMQrfbcOKzu 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\n  border-color: #fff transparent transparent transparent;\n}\n._1evXIL5gZqPkMQrfbcOKzu div:nth-child(1) {\n  animation-delay: -0.45s;\n}\n._1evXIL5gZqPkMQrfbcOKzu div:nth-child(2) {\n  animation-delay: -0.3s;\n}\n._1evXIL5gZqPkMQrfbcOKzu div:nth-child(3) {\n  animation-delay: -0.15s;\n}\n@keyframes _1evXIL5gZqPkMQrfbcOKzu {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-ring": "_1evXIL5gZqPkMQrfbcOKzu"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._3kLRpLdDDkoyAQ9BJW9GCC {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n\n._3kLRpLdDDkoyAQ9BJW9GCC div {\n  position: absolute;\n  border: 4px solid #fff;\n  opacity: 1;\n  border-radius: 50%;\n  animation: _3kLRpLdDDkoyAQ9BJW9GCC 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;\n}\n\n._3kLRpLdDDkoyAQ9BJW9GCC div:nth-child(2) {\n  animation-delay: -0.5s;\n}\n\n@keyframes _3kLRpLdDDkoyAQ9BJW9GCC {\n  0% {\n    top: 45%;\n    left: 45%;\n    width: 0;\n    height: 0;\n    opacity: 1;\n  }\n\n  100% {\n    top: 0px;\n    left: 0px;\n    width: 90%;\n    height: 90%;\n    opacity: 0;\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-ripple": "_3kLRpLdDDkoyAQ9BJW9GCC"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Roller/style.module.css":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Roller/style.module.css ***!
  \*************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._2pzi0dQ60_yrFm-TjAkML1 {\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 > div {\n  animation: _2pzi0dQ60_yrFm-TjAkML1 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;\n  transform-origin: 40px 40px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div ._1_UH651_Ughv2KZKwNmPQI {\n  content: ' ';\n  display: block;\n  position: absolute;\n  width: 7px;\n  height: 7px;\n  border-radius: 50%;\n  background: #fff;\n  margin: -4px 0 0 -4px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(1) {\n  animation-delay: -0.036s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(1) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 63px;\n  left: 63px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(2) {\n  animation-delay: -0.072s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(2) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 68px;\n  left: 56px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(3) {\n  animation-delay: -0.108s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(3) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 71px;\n  left: 48px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(4) {\n  animation-delay: -0.144s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(4) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 72px;\n  left: 40px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(5) {\n  animation-delay: -0.18s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(5) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 71px;\n  left: 32px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(6) {\n  animation-delay: -0.216s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(6) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 68px;\n  left: 24px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(7) {\n  animation-delay: -0.252s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(7) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 63px;\n  left: 17px;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(8) {\n  animation-delay: -0.288s;\n}\n._2pzi0dQ60_yrFm-TjAkML1 div:nth-child(8) ._1_UH651_Ughv2KZKwNmPQI {\n  top: 56px;\n  left: 12px;\n}\n@keyframes _2pzi0dQ60_yrFm-TjAkML1 {\n  0% {\n    transform: rotate(0deg);\n  }\n  100% {\n    transform: rotate(360deg);\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-roller": "_2pzi0dQ60_yrFm-TjAkML1",
	"div-after": "_1_UH651_Ughv2KZKwNmPQI"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--5-1!./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "._1n7yHXLBVkCVENdrBUPHlv {\n  color: official;\n  display: inline-block;\n  position: relative;\n  width: 80px;\n  height: 80px;\n}\n._1n7yHXLBVkCVENdrBUPHlv div {\n  transform-origin: 40px 40px;\n  animation: _1n7yHXLBVkCVENdrBUPHlv 1.2s linear infinite;\n}\n._1n7yHXLBVkCVENdrBUPHlv div ._3NFVxrxg54QOqecjKB5qjd {\n  content: ' ';\n  display: block;\n  position: absolute;\n  top: 3px;\n  left: 37px;\n  width: 6px;\n  height: 18px;\n  border-radius: 20%;\n  background: #fff;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(1) {\n  transform: rotate(0deg);\n  animation-delay: -1.1s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(2) {\n  transform: rotate(30deg);\n  animation-delay: -1s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(3) {\n  transform: rotate(60deg);\n  animation-delay: -0.9s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(4) {\n  transform: rotate(90deg);\n  animation-delay: -0.8s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(5) {\n  transform: rotate(120deg);\n  animation-delay: -0.7s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(6) {\n  transform: rotate(150deg);\n  animation-delay: -0.6s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(7) {\n  transform: rotate(180deg);\n  animation-delay: -0.5s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(8) {\n  transform: rotate(210deg);\n  animation-delay: -0.4s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(9) {\n  transform: rotate(240deg);\n  animation-delay: -0.3s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(10) {\n  transform: rotate(270deg);\n  animation-delay: -0.2s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(11) {\n  transform: rotate(300deg);\n  animation-delay: -0.1s;\n}\n._1n7yHXLBVkCVENdrBUPHlv div:nth-child(12) {\n  transform: rotate(330deg);\n  animation-delay: 0s;\n}\n@keyframes _1n7yHXLBVkCVENdrBUPHlv {\n  0% {\n    opacity: 1;\n  }\n  100% {\n    opacity: 0;\n  }\n}\n", ""]);
// Exports
exports.locals = {
	"lds-spinner": "_1n7yHXLBVkCVENdrBUPHlv",
	"div-after": "_3NFVxrxg54QOqecjKB5qjd"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/object-assign/index.js":
/*!*********************************************!*\
  !*** ./node_modules/object-assign/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Circle/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Circle/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Circle; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Circle/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};



function Circle(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 64 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-circle'], className), style: __assign({ background: color, width: size, height: size }, style) }, rest), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Circle/style.module.css":
/*!*************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Circle/style.module.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Circle/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Default/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Default/index.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Default; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Default/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Default(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    var circles = __spreadArray([], Array(12)).map(function (_, index) { return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: { background: "" + color, width: size * 0.075, height: size * 0.075 } }, index)); });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-default'], className), style: __assign({ height: size, width: size }, style) }, rest, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Default/style.module.css":
/*!**************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Default/style.module.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Default/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/DualRing/index.js":
/*!*******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/DualRing/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return DualRing; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};



function DualRing(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-dual-ring'], className), style: __assign({ width: size, height: size }, style) }, rest, { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-dual-ring-after']), style: {
                borderColor: color + " transparent",
                borderWidth: size * 0.1,
                width: size * 0.7 - 6,
                height: size * 0.7 - 6,
            } }, void 0) }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css":
/*!***************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/DualRing/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ellipsis/index.js":
/*!*******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ellipsis/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ellipsis; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Ellipsis(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    var circles = __spreadArray([], Array(4)).map(function (_, index) { return Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: { background: "" + color } }, index); });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-ellipsis'], className), style: __assign(__assign({}, style), { width: size, height: size }) }, rest, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css":
/*!***************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ellipsis/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Facebook/index.js":
/*!*******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Facebook/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Facebook; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Facebook(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    var circles = __spreadArray([], Array(3)).map(function (_, index) { return Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: { background: "" + color } }, index); });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-facebook'], className), style: __assign({ width: size, height: size }, style) }, rest, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css":
/*!***************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Facebook/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Grid/index.js":
/*!***************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Grid/index.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Grid; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Grid/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Grid(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    var circles = __spreadArray([], Array(9)).map(function (_, index) { return Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: { background: "" + color } }, index); });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-grid'], className), style: __assign({ width: size, height: size }, style) }, rest, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Grid/style.module.css":
/*!***********************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Grid/style.module.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Grid/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Heart/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Heart/index.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Heart; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Heart/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (undefined && undefined.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};



function Heart(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style, rest = __rest(_a, ["color", "size", "className", "style"]);
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-heart'], className), style: __assign({ width: size, height: size }, style) }, rest, { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", __assign({ style: {
                background: color,
                width: size * 0.4,
                height: size * 0.4,
                left: size * 0.3,
                top: size * 0.3,
            } }, { children: [Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['div-before']), style: {
                        background: color,
                        width: size * 0.4,
                        height: size * 0.4,
                        left: -size * 0.3,
                    } }, void 0),
                Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['div-after']), style: {
                        background: color,
                        width: size * 0.4,
                        height: size * 0.4,
                        top: -size * 0.3,
                    } }, void 0)] }), void 0) }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Heart/style.module.css":
/*!************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Heart/style.module.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Heart/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Hourglass/index.js":
/*!********************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Hourglass/index.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Hourglass; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



function Hourglass(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 32 : _c, className = _a.className, style = _a.style;
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-hourglass'], className), style: __assign({}, style) }, { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-hourglass-after']), 
            //@ts-ignore
            style: { background: color, borderWidth: size, borderHeight: size } }, void 0) }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css":
/*!****************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Hourglass/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Orbitals/index.js":
/*!*******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Orbitals/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Orbitals; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



function Orbitals(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, className = _a.className, style = _a.style;
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-orbitals'], className), style: __assign({}, style) }, { children: [Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['center']), style: { background: color } }, void 0),
            Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-spin']) }, { children: [Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc_start-a']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc_end-a']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc_start-b']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-arc_end-b']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-moon-a']), style: { background: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['inner-moon-b']), style: { background: color } }, void 0)] }), void 0),
            Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-spin']) }, { children: [Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc_start-a']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc_end-a']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc_start-b']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc'], _style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-arc_end-b']), style: { borderColor: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-moon-a']), style: { background: color } }, void 0),
                    Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['outer-moon-b']), style: { background: color } }, void 0)] }), void 0)] }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css":
/*!***************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Orbitals/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ouroboro/index.js":
/*!*******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ouroboro/index.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ouroboro; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};



function Ouroboro(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, style = _a.style, className = _a.className;
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxs"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-ouroboro'], className), style: __assign({}, style) }, { children: [Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("span", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.left) }, { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("span", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.anim), style: { background: color } }, void 0) }), void 0),
            Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("span", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.right) }, { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("span", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a.anim), style: { background: color } }, void 0) }), void 0)] }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css":
/*!***************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ouroboro/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ring/index.js":
/*!***************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ring/index.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ring; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Ring/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Ring(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style;
    var circles = __spreadArray([], Array(4)).map(function (_, index) {
        return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: {
                borderColor: color + " transparent transparent transparent",
                width: size * 0.8,
                height: size * 0.8,
                margin: size * 0.1,
                borderWidth: size * 0.1,
            } }, index));
    });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-ring'], className), style: __assign({ width: size, height: size }, style) }, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ring/style.module.css":
/*!***********************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ring/style.module.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ring/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ripple/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ripple/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Ripple; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Ripple(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, _c = _a.size, size = _c === void 0 ? 80 : _c, className = _a.className, style = _a.style;
    var circles = __spreadArray([], Array(2)).map(function (_, index) { return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { style: {
            borderColor: "" + color,
            borderWidth: size * 0.05,
        } }, index)); });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-ripple'], className), style: __assign({ width: size, height: size }, style) }, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css":
/*!*************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Ripple/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Roller/index.js":
/*!*****************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Roller/index.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Roller; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Roller/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Roller(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, className = _a.className, style = _a.style;
    var circles = __spreadArray([], Array(8)).map(function (_, index) {
        return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['div-after']), style: { background: color } }, void 0) }, index));
    });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-roller'], className), style: __assign({}, style) }, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Roller/style.module.css":
/*!*************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Roller/style.module.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Roller/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Spinner/index.js":
/*!******************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Spinner/index.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Spinner; });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style.module.css */ "./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css");
/* harmony import */ var _style_module_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_style_module_css__WEBPACK_IMPORTED_MODULE_2__);
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (undefined && undefined.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};



function Spinner(_a) {
    var _b = _a.color, color = _b === void 0 ? '#7f58af' : _b, className = _a.className, style = _a.style;
    var circles = __spreadArray([], Array(12)).map(function (_, index) {
        return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { children: Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", { className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['div-after']), style: { background: color } }, void 0) }, index));
    });
    return (Object(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__["jsx"])("div", __assign({ className: classnames__WEBPACK_IMPORTED_MODULE_1___default()(_style_module_css__WEBPACK_IMPORTED_MODULE_2___default.a['lds-spinner'], className), style: __assign({}, style) }, { children: circles }), void 0));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css":
/*!**************************************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../css-loader/dist/cjs.js??ref--5-1!./style.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/react-spinners-css/lib/esm/Spinner/style.module.css");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./node_modules/react-spinners-css/lib/esm/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/react-spinners-css/lib/esm/index.js ***!
  \**********************************************************/
/*! exports provided: Circle, Default, DualRing, Ellipsis, Facebook, Grid, Heart, Hourglass, Orbitals, Ring, Ripple, Roller, Spinner, Ouroboro */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Circle__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Circle */ "./node_modules/react-spinners-css/lib/esm/Circle/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Circle", function() { return _Circle__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _Default__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Default */ "./node_modules/react-spinners-css/lib/esm/Default/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Default", function() { return _Default__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _DualRing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DualRing */ "./node_modules/react-spinners-css/lib/esm/DualRing/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DualRing", function() { return _DualRing__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _Ellipsis__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Ellipsis */ "./node_modules/react-spinners-css/lib/esm/Ellipsis/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ellipsis", function() { return _Ellipsis__WEBPACK_IMPORTED_MODULE_3__["default"]; });

/* harmony import */ var _Facebook__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Facebook */ "./node_modules/react-spinners-css/lib/esm/Facebook/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Facebook", function() { return _Facebook__WEBPACK_IMPORTED_MODULE_4__["default"]; });

/* harmony import */ var _Grid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Grid */ "./node_modules/react-spinners-css/lib/esm/Grid/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Grid", function() { return _Grid__WEBPACK_IMPORTED_MODULE_5__["default"]; });

/* harmony import */ var _Heart__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Heart */ "./node_modules/react-spinners-css/lib/esm/Heart/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Heart", function() { return _Heart__WEBPACK_IMPORTED_MODULE_6__["default"]; });

/* harmony import */ var _Hourglass__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Hourglass */ "./node_modules/react-spinners-css/lib/esm/Hourglass/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hourglass", function() { return _Hourglass__WEBPACK_IMPORTED_MODULE_7__["default"]; });

/* harmony import */ var _Orbitals__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Orbitals */ "./node_modules/react-spinners-css/lib/esm/Orbitals/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Orbitals", function() { return _Orbitals__WEBPACK_IMPORTED_MODULE_8__["default"]; });

/* harmony import */ var _Ring__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./Ring */ "./node_modules/react-spinners-css/lib/esm/Ring/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ring", function() { return _Ring__WEBPACK_IMPORTED_MODULE_9__["default"]; });

/* harmony import */ var _Ripple__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Ripple */ "./node_modules/react-spinners-css/lib/esm/Ripple/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ripple", function() { return _Ripple__WEBPACK_IMPORTED_MODULE_10__["default"]; });

/* harmony import */ var _Roller__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Roller */ "./node_modules/react-spinners-css/lib/esm/Roller/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Roller", function() { return _Roller__WEBPACK_IMPORTED_MODULE_11__["default"]; });

/* harmony import */ var _Spinner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./Spinner */ "./node_modules/react-spinners-css/lib/esm/Spinner/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Spinner", function() { return _Spinner__WEBPACK_IMPORTED_MODULE_12__["default"]; });

/* harmony import */ var _Ouroboro__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./Ouroboro */ "./node_modules/react-spinners-css/lib/esm/Ouroboro/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Ouroboro", function() { return _Ouroboro__WEBPACK_IMPORTED_MODULE_13__["default"]; });
















//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/react/cjs/react-jsx-runtime.development.js":
/*!*****************************************************************!*\
  !*** ./node_modules/react/cjs/react-jsx-runtime.development.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/** @license React v17.0.2
 * react-jsx-runtime.development.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



if (true) {
  (function() {
'use strict';

var React = __webpack_require__(/*! react */ "react");
var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

// ATTENTION
// When adding new symbols to this file,
// Please consider also adding to 'react-devtools-shared/src/backend/ReactSymbols'
// The Symbol used to tag the ReactElement-like types. If there is no native Symbol
// nor polyfill, then a plain number is used for performance.
var REACT_ELEMENT_TYPE = 0xeac7;
var REACT_PORTAL_TYPE = 0xeaca;
exports.Fragment = 0xeacb;
var REACT_STRICT_MODE_TYPE = 0xeacc;
var REACT_PROFILER_TYPE = 0xead2;
var REACT_PROVIDER_TYPE = 0xeacd;
var REACT_CONTEXT_TYPE = 0xeace;
var REACT_FORWARD_REF_TYPE = 0xead0;
var REACT_SUSPENSE_TYPE = 0xead1;
var REACT_SUSPENSE_LIST_TYPE = 0xead8;
var REACT_MEMO_TYPE = 0xead3;
var REACT_LAZY_TYPE = 0xead4;
var REACT_BLOCK_TYPE = 0xead9;
var REACT_SERVER_BLOCK_TYPE = 0xeada;
var REACT_FUNDAMENTAL_TYPE = 0xead5;
var REACT_SCOPE_TYPE = 0xead7;
var REACT_OPAQUE_ID_TYPE = 0xeae0;
var REACT_DEBUG_TRACING_MODE_TYPE = 0xeae1;
var REACT_OFFSCREEN_TYPE = 0xeae2;
var REACT_LEGACY_HIDDEN_TYPE = 0xeae3;

if (typeof Symbol === 'function' && Symbol.for) {
  var symbolFor = Symbol.for;
  REACT_ELEMENT_TYPE = symbolFor('react.element');
  REACT_PORTAL_TYPE = symbolFor('react.portal');
  exports.Fragment = symbolFor('react.fragment');
  REACT_STRICT_MODE_TYPE = symbolFor('react.strict_mode');
  REACT_PROFILER_TYPE = symbolFor('react.profiler');
  REACT_PROVIDER_TYPE = symbolFor('react.provider');
  REACT_CONTEXT_TYPE = symbolFor('react.context');
  REACT_FORWARD_REF_TYPE = symbolFor('react.forward_ref');
  REACT_SUSPENSE_TYPE = symbolFor('react.suspense');
  REACT_SUSPENSE_LIST_TYPE = symbolFor('react.suspense_list');
  REACT_MEMO_TYPE = symbolFor('react.memo');
  REACT_LAZY_TYPE = symbolFor('react.lazy');
  REACT_BLOCK_TYPE = symbolFor('react.block');
  REACT_SERVER_BLOCK_TYPE = symbolFor('react.server.block');
  REACT_FUNDAMENTAL_TYPE = symbolFor('react.fundamental');
  REACT_SCOPE_TYPE = symbolFor('react.scope');
  REACT_OPAQUE_ID_TYPE = symbolFor('react.opaque.id');
  REACT_DEBUG_TRACING_MODE_TYPE = symbolFor('react.debug_trace_mode');
  REACT_OFFSCREEN_TYPE = symbolFor('react.offscreen');
  REACT_LEGACY_HIDDEN_TYPE = symbolFor('react.legacy_hidden');
}

var MAYBE_ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
var FAUX_ITERATOR_SYMBOL = '@@iterator';
function getIteratorFn(maybeIterable) {
  if (maybeIterable === null || typeof maybeIterable !== 'object') {
    return null;
  }

  var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];

  if (typeof maybeIterator === 'function') {
    return maybeIterator;
  }

  return null;
}

var ReactSharedInternals = React.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED;

function error(format) {
  {
    for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    printWarning('error', format, args);
  }
}

function printWarning(level, format, args) {
  // When changing this logic, you might want to also
  // update consoleWithStackDev.www.js as well.
  {
    var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;
    var stack = ReactDebugCurrentFrame.getStackAddendum();

    if (stack !== '') {
      format += '%s';
      args = args.concat([stack]);
    }

    var argsWithFormat = args.map(function (item) {
      return '' + item;
    }); // Careful: RN currently depends on this prefix

    argsWithFormat.unshift('Warning: ' + format); // We intentionally don't use spread (or .apply) directly because it
    // breaks IE9: https://github.com/facebook/react/issues/13610
    // eslint-disable-next-line react-internal/no-production-logging

    Function.prototype.apply.call(console[level], console, argsWithFormat);
  }
}

// Filter certain DOM attributes (e.g. src, href) if their values are empty strings.

var enableScopeAPI = false; // Experimental Create Event Handle API.

function isValidElementType(type) {
  if (typeof type === 'string' || typeof type === 'function') {
    return true;
  } // Note: typeof might be other than 'symbol' or 'number' (e.g. if it's a polyfill).


  if (type === exports.Fragment || type === REACT_PROFILER_TYPE || type === REACT_DEBUG_TRACING_MODE_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || type === REACT_LEGACY_HIDDEN_TYPE || enableScopeAPI ) {
    return true;
  }

  if (typeof type === 'object' && type !== null) {
    if (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_BLOCK_TYPE || type[0] === REACT_SERVER_BLOCK_TYPE) {
      return true;
    }
  }

  return false;
}

function getWrappedName(outerType, innerType, wrapperName) {
  var functionName = innerType.displayName || innerType.name || '';
  return outerType.displayName || (functionName !== '' ? wrapperName + "(" + functionName + ")" : wrapperName);
}

function getContextName(type) {
  return type.displayName || 'Context';
}

function getComponentName(type) {
  if (type == null) {
    // Host root, text node or just invalid type.
    return null;
  }

  {
    if (typeof type.tag === 'number') {
      error('Received an unexpected object in getComponentName(). ' + 'This is likely a bug in React. Please file an issue.');
    }
  }

  if (typeof type === 'function') {
    return type.displayName || type.name || null;
  }

  if (typeof type === 'string') {
    return type;
  }

  switch (type) {
    case exports.Fragment:
      return 'Fragment';

    case REACT_PORTAL_TYPE:
      return 'Portal';

    case REACT_PROFILER_TYPE:
      return 'Profiler';

    case REACT_STRICT_MODE_TYPE:
      return 'StrictMode';

    case REACT_SUSPENSE_TYPE:
      return 'Suspense';

    case REACT_SUSPENSE_LIST_TYPE:
      return 'SuspenseList';
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_CONTEXT_TYPE:
        var context = type;
        return getContextName(context) + '.Consumer';

      case REACT_PROVIDER_TYPE:
        var provider = type;
        return getContextName(provider._context) + '.Provider';

      case REACT_FORWARD_REF_TYPE:
        return getWrappedName(type, type.render, 'ForwardRef');

      case REACT_MEMO_TYPE:
        return getComponentName(type.type);

      case REACT_BLOCK_TYPE:
        return getComponentName(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            return getComponentName(init(payload));
          } catch (x) {
            return null;
          }
        }
    }
  }

  return null;
}

// Helpers to patch console.logs to avoid logging during side-effect free
// replaying on render function. This currently only patches the object
// lazily which won't cover if the log function was extracted eagerly.
// We could also eagerly patch the method.
var disabledDepth = 0;
var prevLog;
var prevInfo;
var prevWarn;
var prevError;
var prevGroup;
var prevGroupCollapsed;
var prevGroupEnd;

function disabledLog() {}

disabledLog.__reactDisabledLog = true;
function disableLogs() {
  {
    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      prevLog = console.log;
      prevInfo = console.info;
      prevWarn = console.warn;
      prevError = console.error;
      prevGroup = console.group;
      prevGroupCollapsed = console.groupCollapsed;
      prevGroupEnd = console.groupEnd; // https://github.com/facebook/react/issues/19099

      var props = {
        configurable: true,
        enumerable: true,
        value: disabledLog,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        info: props,
        log: props,
        warn: props,
        error: props,
        group: props,
        groupCollapsed: props,
        groupEnd: props
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    disabledDepth++;
  }
}
function reenableLogs() {
  {
    disabledDepth--;

    if (disabledDepth === 0) {
      /* eslint-disable react-internal/no-production-logging */
      var props = {
        configurable: true,
        enumerable: true,
        writable: true
      }; // $FlowFixMe Flow thinks console is immutable.

      Object.defineProperties(console, {
        log: _assign({}, props, {
          value: prevLog
        }),
        info: _assign({}, props, {
          value: prevInfo
        }),
        warn: _assign({}, props, {
          value: prevWarn
        }),
        error: _assign({}, props, {
          value: prevError
        }),
        group: _assign({}, props, {
          value: prevGroup
        }),
        groupCollapsed: _assign({}, props, {
          value: prevGroupCollapsed
        }),
        groupEnd: _assign({}, props, {
          value: prevGroupEnd
        })
      });
      /* eslint-enable react-internal/no-production-logging */
    }

    if (disabledDepth < 0) {
      error('disabledDepth fell below zero. ' + 'This is a bug in React. Please file an issue.');
    }
  }
}

var ReactCurrentDispatcher = ReactSharedInternals.ReactCurrentDispatcher;
var prefix;
function describeBuiltInComponentFrame(name, source, ownerFn) {
  {
    if (prefix === undefined) {
      // Extract the VM specific prefix used by each line.
      try {
        throw Error();
      } catch (x) {
        var match = x.stack.trim().match(/\n( *(at )?)/);
        prefix = match && match[1] || '';
      }
    } // We use the prefix to ensure our stacks line up with native stack frames.


    return '\n' + prefix + name;
  }
}
var reentry = false;
var componentFrameCache;

{
  var PossiblyWeakMap = typeof WeakMap === 'function' ? WeakMap : Map;
  componentFrameCache = new PossiblyWeakMap();
}

function describeNativeComponentFrame(fn, construct) {
  // If something asked for a stack inside a fake render, it should get ignored.
  if (!fn || reentry) {
    return '';
  }

  {
    var frame = componentFrameCache.get(fn);

    if (frame !== undefined) {
      return frame;
    }
  }

  var control;
  reentry = true;
  var previousPrepareStackTrace = Error.prepareStackTrace; // $FlowFixMe It does accept undefined.

  Error.prepareStackTrace = undefined;
  var previousDispatcher;

  {
    previousDispatcher = ReactCurrentDispatcher.current; // Set the dispatcher in DEV because this might be call in the render function
    // for warnings.

    ReactCurrentDispatcher.current = null;
    disableLogs();
  }

  try {
    // This should throw.
    if (construct) {
      // Something should be setting the props in the constructor.
      var Fake = function () {
        throw Error();
      }; // $FlowFixMe


      Object.defineProperty(Fake.prototype, 'props', {
        set: function () {
          // We use a throwing setter instead of frozen or non-writable props
          // because that won't throw in a non-strict mode function.
          throw Error();
        }
      });

      if (typeof Reflect === 'object' && Reflect.construct) {
        // We construct a different control for this case to include any extra
        // frames added by the construct call.
        try {
          Reflect.construct(Fake, []);
        } catch (x) {
          control = x;
        }

        Reflect.construct(fn, [], Fake);
      } else {
        try {
          Fake.call();
        } catch (x) {
          control = x;
        }

        fn.call(Fake.prototype);
      }
    } else {
      try {
        throw Error();
      } catch (x) {
        control = x;
      }

      fn();
    }
  } catch (sample) {
    // This is inlined manually because closure doesn't do it for us.
    if (sample && control && typeof sample.stack === 'string') {
      // This extracts the first frame from the sample that isn't also in the control.
      // Skipping one frame that we assume is the frame that calls the two.
      var sampleLines = sample.stack.split('\n');
      var controlLines = control.stack.split('\n');
      var s = sampleLines.length - 1;
      var c = controlLines.length - 1;

      while (s >= 1 && c >= 0 && sampleLines[s] !== controlLines[c]) {
        // We expect at least one stack frame to be shared.
        // Typically this will be the root most one. However, stack frames may be
        // cut off due to maximum stack limits. In this case, one maybe cut off
        // earlier than the other. We assume that the sample is longer or the same
        // and there for cut off earlier. So we should find the root most frame in
        // the sample somewhere in the control.
        c--;
      }

      for (; s >= 1 && c >= 0; s--, c--) {
        // Next we find the first one that isn't the same which should be the
        // frame that called our sample function and the control.
        if (sampleLines[s] !== controlLines[c]) {
          // In V8, the first line is describing the message but other VMs don't.
          // If we're about to return the first line, and the control is also on the same
          // line, that's a pretty good indicator that our sample threw at same line as
          // the control. I.e. before we entered the sample frame. So we ignore this result.
          // This can happen if you passed a class to function component, or non-function.
          if (s !== 1 || c !== 1) {
            do {
              s--;
              c--; // We may still have similar intermediate frames from the construct call.
              // The next one that isn't the same should be our match though.

              if (c < 0 || sampleLines[s] !== controlLines[c]) {
                // V8 adds a "new" prefix for native classes. Let's remove it to make it prettier.
                var _frame = '\n' + sampleLines[s].replace(' at new ', ' at ');

                {
                  if (typeof fn === 'function') {
                    componentFrameCache.set(fn, _frame);
                  }
                } // Return the line we found.


                return _frame;
              }
            } while (s >= 1 && c >= 0);
          }

          break;
        }
      }
    }
  } finally {
    reentry = false;

    {
      ReactCurrentDispatcher.current = previousDispatcher;
      reenableLogs();
    }

    Error.prepareStackTrace = previousPrepareStackTrace;
  } // Fallback to just using the name if we couldn't make it throw.


  var name = fn ? fn.displayName || fn.name : '';
  var syntheticFrame = name ? describeBuiltInComponentFrame(name) : '';

  {
    if (typeof fn === 'function') {
      componentFrameCache.set(fn, syntheticFrame);
    }
  }

  return syntheticFrame;
}
function describeFunctionComponentFrame(fn, source, ownerFn) {
  {
    return describeNativeComponentFrame(fn, false);
  }
}

function shouldConstruct(Component) {
  var prototype = Component.prototype;
  return !!(prototype && prototype.isReactComponent);
}

function describeUnknownElementTypeFrameInDEV(type, source, ownerFn) {

  if (type == null) {
    return '';
  }

  if (typeof type === 'function') {
    {
      return describeNativeComponentFrame(type, shouldConstruct(type));
    }
  }

  if (typeof type === 'string') {
    return describeBuiltInComponentFrame(type);
  }

  switch (type) {
    case REACT_SUSPENSE_TYPE:
      return describeBuiltInComponentFrame('Suspense');

    case REACT_SUSPENSE_LIST_TYPE:
      return describeBuiltInComponentFrame('SuspenseList');
  }

  if (typeof type === 'object') {
    switch (type.$$typeof) {
      case REACT_FORWARD_REF_TYPE:
        return describeFunctionComponentFrame(type.render);

      case REACT_MEMO_TYPE:
        // Memo may contain any component type so we recursively resolve it.
        return describeUnknownElementTypeFrameInDEV(type.type, source, ownerFn);

      case REACT_BLOCK_TYPE:
        return describeFunctionComponentFrame(type._render);

      case REACT_LAZY_TYPE:
        {
          var lazyComponent = type;
          var payload = lazyComponent._payload;
          var init = lazyComponent._init;

          try {
            // Lazy may contain any component type so we recursively resolve it.
            return describeUnknownElementTypeFrameInDEV(init(payload), source, ownerFn);
          } catch (x) {}
        }
    }
  }

  return '';
}

var loggedTypeFailures = {};
var ReactDebugCurrentFrame = ReactSharedInternals.ReactDebugCurrentFrame;

function setCurrentlyValidatingElement(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      ReactDebugCurrentFrame.setExtraStackFrame(stack);
    } else {
      ReactDebugCurrentFrame.setExtraStackFrame(null);
    }
  }
}

function checkPropTypes(typeSpecs, values, location, componentName, element) {
  {
    // $FlowFixMe This is okay but Flow doesn't know it.
    var has = Function.call.bind(Object.prototype.hasOwnProperty);

    for (var typeSpecName in typeSpecs) {
      if (has(typeSpecs, typeSpecName)) {
        var error$1 = void 0; // Prop type validation may throw. In case they do, we don't want to
        // fail the render phase where it didn't fail before. So we log it.
        // After these have been cleaned up, we'll let them throw.

        try {
          // This is intentionally an invariant that gets caught. It's the same
          // behavior as without this statement except with a better message.
          if (typeof typeSpecs[typeSpecName] !== 'function') {
            var err = Error((componentName || 'React class') + ': ' + location + ' type `' + typeSpecName + '` is invalid; ' + 'it must be a function, usually from the `prop-types` package, but received `' + typeof typeSpecs[typeSpecName] + '`.' + 'This often happens because of typos such as `PropTypes.function` instead of `PropTypes.func`.');
            err.name = 'Invariant Violation';
            throw err;
          }

          error$1 = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, 'SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED');
        } catch (ex) {
          error$1 = ex;
        }

        if (error$1 && !(error$1 instanceof Error)) {
          setCurrentlyValidatingElement(element);

          error('%s: type specification of %s' + ' `%s` is invalid; the type checker ' + 'function must return `null` or an `Error` but returned a %s. ' + 'You may have forgotten to pass an argument to the type checker ' + 'creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and ' + 'shape all require an argument).', componentName || 'React class', location, typeSpecName, typeof error$1);

          setCurrentlyValidatingElement(null);
        }

        if (error$1 instanceof Error && !(error$1.message in loggedTypeFailures)) {
          // Only monitor this failure once because there tends to be a lot of the
          // same error.
          loggedTypeFailures[error$1.message] = true;
          setCurrentlyValidatingElement(element);

          error('Failed %s type: %s', location, error$1.message);

          setCurrentlyValidatingElement(null);
        }
      }
    }
  }
}

var ReactCurrentOwner = ReactSharedInternals.ReactCurrentOwner;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true
};
var specialPropKeyWarningShown;
var specialPropRefWarningShown;
var didWarnAboutStringRefs;

{
  didWarnAboutStringRefs = {};
}

function hasValidRef(config) {
  {
    if (hasOwnProperty.call(config, 'ref')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'ref').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.ref !== undefined;
}

function hasValidKey(config) {
  {
    if (hasOwnProperty.call(config, 'key')) {
      var getter = Object.getOwnPropertyDescriptor(config, 'key').get;

      if (getter && getter.isReactWarning) {
        return false;
      }
    }
  }

  return config.key !== undefined;
}

function warnIfStringRefCannotBeAutoConverted(config, self) {
  {
    if (typeof config.ref === 'string' && ReactCurrentOwner.current && self && ReactCurrentOwner.current.stateNode !== self) {
      var componentName = getComponentName(ReactCurrentOwner.current.type);

      if (!didWarnAboutStringRefs[componentName]) {
        error('Component "%s" contains the string ref "%s". ' + 'Support for string refs will be removed in a future major release. ' + 'This case cannot be automatically converted to an arrow function. ' + 'We ask you to manually fix this case by using useRef() or createRef() instead. ' + 'Learn more about using refs safely here: ' + 'https://reactjs.org/link/strict-mode-string-ref', getComponentName(ReactCurrentOwner.current.type), config.ref);

        didWarnAboutStringRefs[componentName] = true;
      }
    }
  }
}

function defineKeyPropWarningGetter(props, displayName) {
  {
    var warnAboutAccessingKey = function () {
      if (!specialPropKeyWarningShown) {
        specialPropKeyWarningShown = true;

        error('%s: `key` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    };

    warnAboutAccessingKey.isReactWarning = true;
    Object.defineProperty(props, 'key', {
      get: warnAboutAccessingKey,
      configurable: true
    });
  }
}

function defineRefPropWarningGetter(props, displayName) {
  {
    var warnAboutAccessingRef = function () {
      if (!specialPropRefWarningShown) {
        specialPropRefWarningShown = true;

        error('%s: `ref` is not a prop. Trying to access it will result ' + 'in `undefined` being returned. If you need to access the same ' + 'value within the child component, you should pass it as a different ' + 'prop. (https://reactjs.org/link/special-props)', displayName);
      }
    };

    warnAboutAccessingRef.isReactWarning = true;
    Object.defineProperty(props, 'ref', {
      get: warnAboutAccessingRef,
      configurable: true
    });
  }
}
/**
 * Factory method to create a new React element. This no longer adheres to
 * the class pattern, so do not use new to call it. Also, instanceof check
 * will not work. Instead test $$typeof field against Symbol.for('react.element') to check
 * if something is a React Element.
 *
 * @param {*} type
 * @param {*} props
 * @param {*} key
 * @param {string|object} ref
 * @param {*} owner
 * @param {*} self A *temporary* helper to detect places where `this` is
 * different from the `owner` when React.createElement is called, so that we
 * can warn. We want to get rid of owner and replace string `ref`s with arrow
 * functions, and as long as `this` and owner are the same, there will be no
 * change in behavior.
 * @param {*} source An annotation object (added by a transpiler or otherwise)
 * indicating filename, line number, and/or other information.
 * @internal
 */


var ReactElement = function (type, key, ref, self, source, owner, props) {
  var element = {
    // This tag allows us to uniquely identify this as a React Element
    $$typeof: REACT_ELEMENT_TYPE,
    // Built-in properties that belong on the element
    type: type,
    key: key,
    ref: ref,
    props: props,
    // Record the component responsible for creating this element.
    _owner: owner
  };

  {
    // The validation flag is currently mutative. We put it on
    // an external backing store so that we can freeze the whole object.
    // This can be replaced with a WeakMap once they are implemented in
    // commonly used development environments.
    element._store = {}; // To make comparing ReactElements easier for testing purposes, we make
    // the validation flag non-enumerable (where possible, which should
    // include every environment we run tests in), so the test framework
    // ignores it.

    Object.defineProperty(element._store, 'validated', {
      configurable: false,
      enumerable: false,
      writable: true,
      value: false
    }); // self and source are DEV only properties.

    Object.defineProperty(element, '_self', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: self
    }); // Two elements created in two different places should be considered
    // equal for testing purposes and therefore we hide it from enumeration.

    Object.defineProperty(element, '_source', {
      configurable: false,
      enumerable: false,
      writable: false,
      value: source
    });

    if (Object.freeze) {
      Object.freeze(element.props);
      Object.freeze(element);
    }
  }

  return element;
};
/**
 * https://github.com/reactjs/rfcs/pull/107
 * @param {*} type
 * @param {object} props
 * @param {string} key
 */

function jsxDEV(type, config, maybeKey, source, self) {
  {
    var propName; // Reserved names are extracted

    var props = {};
    var key = null;
    var ref = null; // Currently, key can be spread in as a prop. This causes a potential
    // issue if key is also explicitly declared (ie. <div {...props} key="Hi" />
    // or <div key="Hi" {...props} /> ). We want to deprecate key spread,
    // but as an intermediary step, we will use jsxDEV for everything except
    // <div {...props} key="Hi" />, because we aren't currently able to tell if
    // key is explicitly declared to be undefined or not.

    if (maybeKey !== undefined) {
      key = '' + maybeKey;
    }

    if (hasValidKey(config)) {
      key = '' + config.key;
    }

    if (hasValidRef(config)) {
      ref = config.ref;
      warnIfStringRefCannotBeAutoConverted(config, self);
    } // Remaining properties are added to a new props object


    for (propName in config) {
      if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
        props[propName] = config[propName];
      }
    } // Resolve default props


    if (type && type.defaultProps) {
      var defaultProps = type.defaultProps;

      for (propName in defaultProps) {
        if (props[propName] === undefined) {
          props[propName] = defaultProps[propName];
        }
      }
    }

    if (key || ref) {
      var displayName = typeof type === 'function' ? type.displayName || type.name || 'Unknown' : type;

      if (key) {
        defineKeyPropWarningGetter(props, displayName);
      }

      if (ref) {
        defineRefPropWarningGetter(props, displayName);
      }
    }

    return ReactElement(type, key, ref, self, source, ReactCurrentOwner.current, props);
  }
}

var ReactCurrentOwner$1 = ReactSharedInternals.ReactCurrentOwner;
var ReactDebugCurrentFrame$1 = ReactSharedInternals.ReactDebugCurrentFrame;

function setCurrentlyValidatingElement$1(element) {
  {
    if (element) {
      var owner = element._owner;
      var stack = describeUnknownElementTypeFrameInDEV(element.type, element._source, owner ? owner.type : null);
      ReactDebugCurrentFrame$1.setExtraStackFrame(stack);
    } else {
      ReactDebugCurrentFrame$1.setExtraStackFrame(null);
    }
  }
}

var propTypesMisspellWarningShown;

{
  propTypesMisspellWarningShown = false;
}
/**
 * Verifies the object is a ReactElement.
 * See https://reactjs.org/docs/react-api.html#isvalidelement
 * @param {?object} object
 * @return {boolean} True if `object` is a ReactElement.
 * @final
 */

function isValidElement(object) {
  {
    return typeof object === 'object' && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
  }
}

function getDeclarationErrorAddendum() {
  {
    if (ReactCurrentOwner$1.current) {
      var name = getComponentName(ReactCurrentOwner$1.current.type);

      if (name) {
        return '\n\nCheck the render method of `' + name + '`.';
      }
    }

    return '';
  }
}

function getSourceInfoErrorAddendum(source) {
  {
    if (source !== undefined) {
      var fileName = source.fileName.replace(/^.*[\\\/]/, '');
      var lineNumber = source.lineNumber;
      return '\n\nCheck your code at ' + fileName + ':' + lineNumber + '.';
    }

    return '';
  }
}
/**
 * Warn if there's no key explicitly set on dynamic arrays of children or
 * object keys are not valid. This allows us to keep track of children between
 * updates.
 */


var ownerHasKeyUseWarning = {};

function getCurrentComponentErrorInfo(parentType) {
  {
    var info = getDeclarationErrorAddendum();

    if (!info) {
      var parentName = typeof parentType === 'string' ? parentType : parentType.displayName || parentType.name;

      if (parentName) {
        info = "\n\nCheck the top-level render call using <" + parentName + ">.";
      }
    }

    return info;
  }
}
/**
 * Warn if the element doesn't have an explicit key assigned to it.
 * This element is in an array. The array could grow and shrink or be
 * reordered. All children that haven't already been validated are required to
 * have a "key" property assigned to it. Error statuses are cached so a warning
 * will only be shown once.
 *
 * @internal
 * @param {ReactElement} element Element that requires a key.
 * @param {*} parentType element's parent's type.
 */


function validateExplicitKey(element, parentType) {
  {
    if (!element._store || element._store.validated || element.key != null) {
      return;
    }

    element._store.validated = true;
    var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);

    if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
      return;
    }

    ownerHasKeyUseWarning[currentComponentErrorInfo] = true; // Usually the current owner is the offender, but if it accepts children as a
    // property, it may be the creator of the child that's responsible for
    // assigning it a key.

    var childOwner = '';

    if (element && element._owner && element._owner !== ReactCurrentOwner$1.current) {
      // Give the component that originally created this child.
      childOwner = " It was passed a child from " + getComponentName(element._owner.type) + ".";
    }

    setCurrentlyValidatingElement$1(element);

    error('Each child in a list should have a unique "key" prop.' + '%s%s See https://reactjs.org/link/warning-keys for more information.', currentComponentErrorInfo, childOwner);

    setCurrentlyValidatingElement$1(null);
  }
}
/**
 * Ensure that every element either is passed in a static location, in an
 * array with an explicit keys property defined, or in an object literal
 * with valid key property.
 *
 * @internal
 * @param {ReactNode} node Statically passed child of any type.
 * @param {*} parentType node's parent's type.
 */


function validateChildKeys(node, parentType) {
  {
    if (typeof node !== 'object') {
      return;
    }

    if (Array.isArray(node)) {
      for (var i = 0; i < node.length; i++) {
        var child = node[i];

        if (isValidElement(child)) {
          validateExplicitKey(child, parentType);
        }
      }
    } else if (isValidElement(node)) {
      // This element was passed in a valid location.
      if (node._store) {
        node._store.validated = true;
      }
    } else if (node) {
      var iteratorFn = getIteratorFn(node);

      if (typeof iteratorFn === 'function') {
        // Entry iterators used to provide implicit keys,
        // but now we print a separate warning for them later.
        if (iteratorFn !== node.entries) {
          var iterator = iteratorFn.call(node);
          var step;

          while (!(step = iterator.next()).done) {
            if (isValidElement(step.value)) {
              validateExplicitKey(step.value, parentType);
            }
          }
        }
      }
    }
  }
}
/**
 * Given an element, validate that its props follow the propTypes definition,
 * provided by the type.
 *
 * @param {ReactElement} element
 */


function validatePropTypes(element) {
  {
    var type = element.type;

    if (type === null || type === undefined || typeof type === 'string') {
      return;
    }

    var propTypes;

    if (typeof type === 'function') {
      propTypes = type.propTypes;
    } else if (typeof type === 'object' && (type.$$typeof === REACT_FORWARD_REF_TYPE || // Note: Memo only checks outer props here.
    // Inner props are checked in the reconciler.
    type.$$typeof === REACT_MEMO_TYPE)) {
      propTypes = type.propTypes;
    } else {
      return;
    }

    if (propTypes) {
      // Intentionally inside to avoid triggering lazy initializers:
      var name = getComponentName(type);
      checkPropTypes(propTypes, element.props, 'prop', name, element);
    } else if (type.PropTypes !== undefined && !propTypesMisspellWarningShown) {
      propTypesMisspellWarningShown = true; // Intentionally inside to avoid triggering lazy initializers:

      var _name = getComponentName(type);

      error('Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?', _name || 'Unknown');
    }

    if (typeof type.getDefaultProps === 'function' && !type.getDefaultProps.isReactClassApproved) {
      error('getDefaultProps is only used on classic React.createClass ' + 'definitions. Use a static property named `defaultProps` instead.');
    }
  }
}
/**
 * Given a fragment, validate that it can only be provided with fragment props
 * @param {ReactElement} fragment
 */


function validateFragmentProps(fragment) {
  {
    var keys = Object.keys(fragment.props);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];

      if (key !== 'children' && key !== 'key') {
        setCurrentlyValidatingElement$1(fragment);

        error('Invalid prop `%s` supplied to `React.Fragment`. ' + 'React.Fragment can only have `key` and `children` props.', key);

        setCurrentlyValidatingElement$1(null);
        break;
      }
    }

    if (fragment.ref !== null) {
      setCurrentlyValidatingElement$1(fragment);

      error('Invalid attribute `ref` supplied to `React.Fragment`.');

      setCurrentlyValidatingElement$1(null);
    }
  }
}

function jsxWithValidation(type, props, key, isStaticChildren, source, self) {
  {
    var validType = isValidElementType(type); // We warn in this case but don't throw. We expect the element creation to
    // succeed and there will likely be errors in render.

    if (!validType) {
      var info = '';

      if (type === undefined || typeof type === 'object' && type !== null && Object.keys(type).length === 0) {
        info += ' You likely forgot to export your component from the file ' + "it's defined in, or you might have mixed up default and named imports.";
      }

      var sourceInfo = getSourceInfoErrorAddendum(source);

      if (sourceInfo) {
        info += sourceInfo;
      } else {
        info += getDeclarationErrorAddendum();
      }

      var typeString;

      if (type === null) {
        typeString = 'null';
      } else if (Array.isArray(type)) {
        typeString = 'array';
      } else if (type !== undefined && type.$$typeof === REACT_ELEMENT_TYPE) {
        typeString = "<" + (getComponentName(type.type) || 'Unknown') + " />";
        info = ' Did you accidentally export a JSX literal instead of a component?';
      } else {
        typeString = typeof type;
      }

      error('React.jsx: type is invalid -- expected a string (for ' + 'built-in components) or a class/function (for composite ' + 'components) but got: %s.%s', typeString, info);
    }

    var element = jsxDEV(type, props, key, source, self); // The result can be nullish if a mock or a custom function is used.
    // TODO: Drop this when these are no longer allowed as the type argument.

    if (element == null) {
      return element;
    } // Skip key warning if the type isn't valid since our key validation logic
    // doesn't expect a non-string/function type and can throw confusing errors.
    // We don't want exception behavior to differ between dev and prod.
    // (Rendering will throw with a helpful message and as soon as the type is
    // fixed, the key warnings will appear.)


    if (validType) {
      var children = props.children;

      if (children !== undefined) {
        if (isStaticChildren) {
          if (Array.isArray(children)) {
            for (var i = 0; i < children.length; i++) {
              validateChildKeys(children[i], type);
            }

            if (Object.freeze) {
              Object.freeze(children);
            }
          } else {
            error('React.jsx: Static children should always be an array. ' + 'You are likely explicitly calling React.jsxs or React.jsxDEV. ' + 'Use the Babel transform instead.');
          }
        } else {
          validateChildKeys(children, type);
        }
      }
    }

    if (type === exports.Fragment) {
      validateFragmentProps(element);
    } else {
      validatePropTypes(element);
    }

    return element;
  }
} // These two functions exist to still get child warnings in dev
// even with the prod transform. This means that jsxDEV is purely
// opt-in behavior for better messages but that we won't stop
// giving you warnings if you use production apis.

function jsxWithValidationStatic(type, props, key) {
  {
    return jsxWithValidation(type, props, key, true);
  }
}
function jsxWithValidationDynamic(type, props, key) {
  {
    return jsxWithValidation(type, props, key, false);
  }
}

var jsx =  jsxWithValidationDynamic ; // we may want to special case jsxs internally to take advantage of static children.
// for now we can ship identical prod functions

var jsxs =  jsxWithValidationStatic ;

exports.jsx = jsx;
exports.jsxs = jsxs;
  })();
}


/***/ }),

/***/ "./node_modules/react/jsx-runtime.js":
/*!*******************************************!*\
  !*** ./node_modules/react/jsx-runtime.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


if (false) {} else {
  module.exports = __webpack_require__(/*! ./cjs/react-jsx-runtime.development.js */ "./node_modules/react/cjs/react-jsx-runtime.development.js");
}


/***/ }),

/***/ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js":
/*!**************************************************************************!*\
  !*** ./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js ***!
  \**************************************************************************/
/*! exports provided: actionChannel, all, apply, call, cancel, cancelled, cps, delay, effectTypes, flush, fork, getContext, join, put, putResolve, race, select, setContext, spawn, take, takeMaybe, debounce, retry, takeEvery, takeLatest, takeLeading, throttle */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @redux-saga/core/effects */ "./node_modules/@redux-saga/core/dist/redux-saga-effects.esm.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "actionChannel", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["actionChannel"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "all", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["all"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "apply", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["apply"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "call", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["call"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cancel", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["cancel"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cancelled", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["cancelled"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "cps", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["cps"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "delay", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["delay"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "effectTypes", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["effectTypes"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "flush", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["flush"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fork", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["fork"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getContext", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["getContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "join", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["join"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "put", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["put"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "putResolve", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["putResolve"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "race", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["race"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "select", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["select"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["setContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "spawn", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["spawn"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "take", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["take"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeMaybe", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["takeMaybe"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "debounce", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["debounce"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "retry", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["retry"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeEvery", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["takeEvery"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeLatest", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["takeLatest"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "takeLeading", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["takeLeading"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "throttle", function() { return _redux_saga_core_effects__WEBPACK_IMPORTED_MODULE_0__["throttle"]; });




/***/ }),

/***/ "./node_modules/redux/es/redux.js":
/*!****************************************!*\
  !*** ./node_modules/redux/es/redux.js ***!
  \****************************************/
/*! exports provided: __DO_NOT_USE__ActionTypes, applyMiddleware, bindActionCreators, combineReducers, compose, createStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__DO_NOT_USE__ActionTypes", function() { return ActionTypes; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyMiddleware", function() { return applyMiddleware; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bindActionCreators", function() { return bindActionCreators; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "combineReducers", function() { return combineReducers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "compose", function() { return compose; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createStore", function() { return createStore; });
/* harmony import */ var _babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/esm/objectSpread2 */ "./node_modules/@babel/runtime/helpers/esm/objectSpread2.js");


/**
 * Adapted from React: https://github.com/facebook/react/blob/master/packages/shared/formatProdErrorMessage.js
 *
 * Do not require this module directly! Use normal throw error calls. These messages will be replaced with error codes
 * during build.
 * @param {number} code
 */
function formatProdErrorMessage(code) {
  return "Minified Redux error #" + code + "; visit https://redux.js.org/Errors?code=" + code + " for the full message or " + 'use the non-minified dev environment for full errors. ';
}

// Inlined version of the `symbol-observable` polyfill
var $$observable = (function () {
  return typeof Symbol === 'function' && Symbol.observable || '@@observable';
})();

/**
 * These are private action types reserved by Redux.
 * For any unknown actions, you must return the current state.
 * If the current state is undefined, you must return the initial state.
 * Do not reference these action types directly in your code.
 */
var randomString = function randomString() {
  return Math.random().toString(36).substring(7).split('').join('.');
};

var ActionTypes = {
  INIT: "@@redux/INIT" + randomString(),
  REPLACE: "@@redux/REPLACE" + randomString(),
  PROBE_UNKNOWN_ACTION: function PROBE_UNKNOWN_ACTION() {
    return "@@redux/PROBE_UNKNOWN_ACTION" + randomString();
  }
};

/**
 * @param {any} obj The object to inspect.
 * @returns {boolean} True if the argument appears to be a plain object.
 */
function isPlainObject(obj) {
  if (typeof obj !== 'object' || obj === null) return false;
  var proto = obj;

  while (Object.getPrototypeOf(proto) !== null) {
    proto = Object.getPrototypeOf(proto);
  }

  return Object.getPrototypeOf(obj) === proto;
}

// Inlined / shortened version of `kindOf` from https://github.com/jonschlinkert/kind-of
function miniKindOf(val) {
  if (val === void 0) return 'undefined';
  if (val === null) return 'null';
  var type = typeof val;

  switch (type) {
    case 'boolean':
    case 'string':
    case 'number':
    case 'symbol':
    case 'function':
      {
        return type;
      }
  }

  if (Array.isArray(val)) return 'array';
  if (isDate(val)) return 'date';
  if (isError(val)) return 'error';
  var constructorName = ctorName(val);

  switch (constructorName) {
    case 'Symbol':
    case 'Promise':
    case 'WeakMap':
    case 'WeakSet':
    case 'Map':
    case 'Set':
      return constructorName;
  } // other


  return type.slice(8, -1).toLowerCase().replace(/\s/g, '');
}

function ctorName(val) {
  return typeof val.constructor === 'function' ? val.constructor.name : null;
}

function isError(val) {
  return val instanceof Error || typeof val.message === 'string' && val.constructor && typeof val.constructor.stackTraceLimit === 'number';
}

function isDate(val) {
  if (val instanceof Date) return true;
  return typeof val.toDateString === 'function' && typeof val.getDate === 'function' && typeof val.setDate === 'function';
}

function kindOf(val) {
  var typeOfVal = typeof val;

  if (true) {
    typeOfVal = miniKindOf(val);
  }

  return typeOfVal;
}

/**
 * Creates a Redux store that holds the state tree.
 * The only way to change the data in the store is to call `dispatch()` on it.
 *
 * There should only be a single store in your app. To specify how different
 * parts of the state tree respond to actions, you may combine several reducers
 * into a single reducer function by using `combineReducers`.
 *
 * @param {Function} reducer A function that returns the next state tree, given
 * the current state tree and the action to handle.
 *
 * @param {any} [preloadedState] The initial state. You may optionally specify it
 * to hydrate the state from the server in universal apps, or to restore a
 * previously serialized user session.
 * If you use `combineReducers` to produce the root reducer function, this must be
 * an object with the same shape as `combineReducers` keys.
 *
 * @param {Function} [enhancer] The store enhancer. You may optionally specify it
 * to enhance the store with third-party capabilities such as middleware,
 * time travel, persistence, etc. The only store enhancer that ships with Redux
 * is `applyMiddleware()`.
 *
 * @returns {Store} A Redux store that lets you read the state, dispatch actions
 * and subscribe to changes.
 */

function createStore(reducer, preloadedState, enhancer) {
  var _ref2;

  if (typeof preloadedState === 'function' && typeof enhancer === 'function' || typeof enhancer === 'function' && typeof arguments[3] === 'function') {
    throw new Error( false ? undefined : 'It looks like you are passing several store enhancers to ' + 'createStore(). This is not supported. Instead, compose them ' + 'together to a single function. See https://redux.js.org/tutorials/fundamentals/part-4-store#creating-a-store-with-enhancers for an example.');
  }

  if (typeof preloadedState === 'function' && typeof enhancer === 'undefined') {
    enhancer = preloadedState;
    preloadedState = undefined;
  }

  if (typeof enhancer !== 'undefined') {
    if (typeof enhancer !== 'function') {
      throw new Error( false ? undefined : "Expected the enhancer to be a function. Instead, received: '" + kindOf(enhancer) + "'");
    }

    return enhancer(createStore)(reducer, preloadedState);
  }

  if (typeof reducer !== 'function') {
    throw new Error( false ? undefined : "Expected the root reducer to be a function. Instead, received: '" + kindOf(reducer) + "'");
  }

  var currentReducer = reducer;
  var currentState = preloadedState;
  var currentListeners = [];
  var nextListeners = currentListeners;
  var isDispatching = false;
  /**
   * This makes a shallow copy of currentListeners so we can use
   * nextListeners as a temporary list while dispatching.
   *
   * This prevents any bugs around consumers calling
   * subscribe/unsubscribe in the middle of a dispatch.
   */

  function ensureCanMutateNextListeners() {
    if (nextListeners === currentListeners) {
      nextListeners = currentListeners.slice();
    }
  }
  /**
   * Reads the state tree managed by the store.
   *
   * @returns {any} The current state tree of your application.
   */


  function getState() {
    if (isDispatching) {
      throw new Error( false ? undefined : 'You may not call store.getState() while the reducer is executing. ' + 'The reducer has already received the state as an argument. ' + 'Pass it down from the top reducer instead of reading it from the store.');
    }

    return currentState;
  }
  /**
   * Adds a change listener. It will be called any time an action is dispatched,
   * and some part of the state tree may potentially have changed. You may then
   * call `getState()` to read the current state tree inside the callback.
   *
   * You may call `dispatch()` from a change listener, with the following
   * caveats:
   *
   * 1. The subscriptions are snapshotted just before every `dispatch()` call.
   * If you subscribe or unsubscribe while the listeners are being invoked, this
   * will not have any effect on the `dispatch()` that is currently in progress.
   * However, the next `dispatch()` call, whether nested or not, will use a more
   * recent snapshot of the subscription list.
   *
   * 2. The listener should not expect to see all state changes, as the state
   * might have been updated multiple times during a nested `dispatch()` before
   * the listener is called. It is, however, guaranteed that all subscribers
   * registered before the `dispatch()` started will be called with the latest
   * state by the time it exits.
   *
   * @param {Function} listener A callback to be invoked on every dispatch.
   * @returns {Function} A function to remove this change listener.
   */


  function subscribe(listener) {
    if (typeof listener !== 'function') {
      throw new Error( false ? undefined : "Expected the listener to be a function. Instead, received: '" + kindOf(listener) + "'");
    }

    if (isDispatching) {
      throw new Error( false ? undefined : 'You may not call store.subscribe() while the reducer is executing. ' + 'If you would like to be notified after the store has been updated, subscribe from a ' + 'component and invoke store.getState() in the callback to access the latest state. ' + 'See https://redux.js.org/api/store#subscribelistener for more details.');
    }

    var isSubscribed = true;
    ensureCanMutateNextListeners();
    nextListeners.push(listener);
    return function unsubscribe() {
      if (!isSubscribed) {
        return;
      }

      if (isDispatching) {
        throw new Error( false ? undefined : 'You may not unsubscribe from a store listener while the reducer is executing. ' + 'See https://redux.js.org/api/store#subscribelistener for more details.');
      }

      isSubscribed = false;
      ensureCanMutateNextListeners();
      var index = nextListeners.indexOf(listener);
      nextListeners.splice(index, 1);
      currentListeners = null;
    };
  }
  /**
   * Dispatches an action. It is the only way to trigger a state change.
   *
   * The `reducer` function, used to create the store, will be called with the
   * current state tree and the given `action`. Its return value will
   * be considered the **next** state of the tree, and the change listeners
   * will be notified.
   *
   * The base implementation only supports plain object actions. If you want to
   * dispatch a Promise, an Observable, a thunk, or something else, you need to
   * wrap your store creating function into the corresponding middleware. For
   * example, see the documentation for the `redux-thunk` package. Even the
   * middleware will eventually dispatch plain object actions using this method.
   *
   * @param {Object} action A plain object representing “what changed”. It is
   * a good idea to keep actions serializable so you can record and replay user
   * sessions, or use the time travelling `redux-devtools`. An action must have
   * a `type` property which may not be `undefined`. It is a good idea to use
   * string constants for action types.
   *
   * @returns {Object} For convenience, the same action object you dispatched.
   *
   * Note that, if you use a custom middleware, it may wrap `dispatch()` to
   * return something else (for example, a Promise you can await).
   */


  function dispatch(action) {
    if (!isPlainObject(action)) {
      throw new Error( false ? undefined : "Actions must be plain objects. Instead, the actual type was: '" + kindOf(action) + "'. You may need to add middleware to your store setup to handle dispatching other values, such as 'redux-thunk' to handle dispatching functions. See https://redux.js.org/tutorials/fundamentals/part-4-store#middleware and https://redux.js.org/tutorials/fundamentals/part-6-async-logic#using-the-redux-thunk-middleware for examples.");
    }

    if (typeof action.type === 'undefined') {
      throw new Error( false ? undefined : 'Actions may not have an undefined "type" property. You may have misspelled an action type string constant.');
    }

    if (isDispatching) {
      throw new Error( false ? undefined : 'Reducers may not dispatch actions.');
    }

    try {
      isDispatching = true;
      currentState = currentReducer(currentState, action);
    } finally {
      isDispatching = false;
    }

    var listeners = currentListeners = nextListeners;

    for (var i = 0; i < listeners.length; i++) {
      var listener = listeners[i];
      listener();
    }

    return action;
  }
  /**
   * Replaces the reducer currently used by the store to calculate the state.
   *
   * You might need this if your app implements code splitting and you want to
   * load some of the reducers dynamically. You might also need this if you
   * implement a hot reloading mechanism for Redux.
   *
   * @param {Function} nextReducer The reducer for the store to use instead.
   * @returns {void}
   */


  function replaceReducer(nextReducer) {
    if (typeof nextReducer !== 'function') {
      throw new Error( false ? undefined : "Expected the nextReducer to be a function. Instead, received: '" + kindOf(nextReducer));
    }

    currentReducer = nextReducer; // This action has a similiar effect to ActionTypes.INIT.
    // Any reducers that existed in both the new and old rootReducer
    // will receive the previous state. This effectively populates
    // the new state tree with any relevant data from the old one.

    dispatch({
      type: ActionTypes.REPLACE
    });
  }
  /**
   * Interoperability point for observable/reactive libraries.
   * @returns {observable} A minimal observable of state changes.
   * For more information, see the observable proposal:
   * https://github.com/tc39/proposal-observable
   */


  function observable() {
    var _ref;

    var outerSubscribe = subscribe;
    return _ref = {
      /**
       * The minimal observable subscription method.
       * @param {Object} observer Any object that can be used as an observer.
       * The observer object should have a `next` method.
       * @returns {subscription} An object with an `unsubscribe` method that can
       * be used to unsubscribe the observable from the store, and prevent further
       * emission of values from the observable.
       */
      subscribe: function subscribe(observer) {
        if (typeof observer !== 'object' || observer === null) {
          throw new Error( false ? undefined : "Expected the observer to be an object. Instead, received: '" + kindOf(observer) + "'");
        }

        function observeState() {
          if (observer.next) {
            observer.next(getState());
          }
        }

        observeState();
        var unsubscribe = outerSubscribe(observeState);
        return {
          unsubscribe: unsubscribe
        };
      }
    }, _ref[$$observable] = function () {
      return this;
    }, _ref;
  } // When a store is created, an "INIT" action is dispatched so that every
  // reducer returns their initial state. This effectively populates
  // the initial state tree.


  dispatch({
    type: ActionTypes.INIT
  });
  return _ref2 = {
    dispatch: dispatch,
    subscribe: subscribe,
    getState: getState,
    replaceReducer: replaceReducer
  }, _ref2[$$observable] = observable, _ref2;
}

/**
 * Prints a warning in the console if it exists.
 *
 * @param {String} message The warning message.
 * @returns {void}
 */
function warning(message) {
  /* eslint-disable no-console */
  if (typeof console !== 'undefined' && typeof console.error === 'function') {
    console.error(message);
  }
  /* eslint-enable no-console */


  try {
    // This error was thrown as a convenience so that if you enable
    // "break on all exceptions" in your console,
    // it would pause the execution at this line.
    throw new Error(message);
  } catch (e) {} // eslint-disable-line no-empty

}

function getUnexpectedStateShapeWarningMessage(inputState, reducers, action, unexpectedKeyCache) {
  var reducerKeys = Object.keys(reducers);
  var argumentName = action && action.type === ActionTypes.INIT ? 'preloadedState argument passed to createStore' : 'previous state received by the reducer';

  if (reducerKeys.length === 0) {
    return 'Store does not have a valid reducer. Make sure the argument passed ' + 'to combineReducers is an object whose values are reducers.';
  }

  if (!isPlainObject(inputState)) {
    return "The " + argumentName + " has unexpected type of \"" + kindOf(inputState) + "\". Expected argument to be an object with the following " + ("keys: \"" + reducerKeys.join('", "') + "\"");
  }

  var unexpectedKeys = Object.keys(inputState).filter(function (key) {
    return !reducers.hasOwnProperty(key) && !unexpectedKeyCache[key];
  });
  unexpectedKeys.forEach(function (key) {
    unexpectedKeyCache[key] = true;
  });
  if (action && action.type === ActionTypes.REPLACE) return;

  if (unexpectedKeys.length > 0) {
    return "Unexpected " + (unexpectedKeys.length > 1 ? 'keys' : 'key') + " " + ("\"" + unexpectedKeys.join('", "') + "\" found in " + argumentName + ". ") + "Expected to find one of the known reducer keys instead: " + ("\"" + reducerKeys.join('", "') + "\". Unexpected keys will be ignored.");
  }
}

function assertReducerShape(reducers) {
  Object.keys(reducers).forEach(function (key) {
    var reducer = reducers[key];
    var initialState = reducer(undefined, {
      type: ActionTypes.INIT
    });

    if (typeof initialState === 'undefined') {
      throw new Error( false ? undefined : "The slice reducer for key \"" + key + "\" returned undefined during initialization. " + "If the state passed to the reducer is undefined, you must " + "explicitly return the initial state. The initial state may " + "not be undefined. If you don't want to set a value for this reducer, " + "you can use null instead of undefined.");
    }

    if (typeof reducer(undefined, {
      type: ActionTypes.PROBE_UNKNOWN_ACTION()
    }) === 'undefined') {
      throw new Error( false ? undefined : "The slice reducer for key \"" + key + "\" returned undefined when probed with a random type. " + ("Don't try to handle '" + ActionTypes.INIT + "' or other actions in \"redux/*\" ") + "namespace. They are considered private. Instead, you must return the " + "current state for any unknown actions, unless it is undefined, " + "in which case you must return the initial state, regardless of the " + "action type. The initial state may not be undefined, but can be null.");
    }
  });
}
/**
 * Turns an object whose values are different reducer functions, into a single
 * reducer function. It will call every child reducer, and gather their results
 * into a single state object, whose keys correspond to the keys of the passed
 * reducer functions.
 *
 * @param {Object} reducers An object whose values correspond to different
 * reducer functions that need to be combined into one. One handy way to obtain
 * it is to use ES6 `import * as reducers` syntax. The reducers may never return
 * undefined for any action. Instead, they should return their initial state
 * if the state passed to them was undefined, and the current state for any
 * unrecognized action.
 *
 * @returns {Function} A reducer function that invokes every reducer inside the
 * passed object, and builds a state object with the same shape.
 */


function combineReducers(reducers) {
  var reducerKeys = Object.keys(reducers);
  var finalReducers = {};

  for (var i = 0; i < reducerKeys.length; i++) {
    var key = reducerKeys[i];

    if (true) {
      if (typeof reducers[key] === 'undefined') {
        warning("No reducer provided for key \"" + key + "\"");
      }
    }

    if (typeof reducers[key] === 'function') {
      finalReducers[key] = reducers[key];
    }
  }

  var finalReducerKeys = Object.keys(finalReducers); // This is used to make sure we don't warn about the same
  // keys multiple times.

  var unexpectedKeyCache;

  if (true) {
    unexpectedKeyCache = {};
  }

  var shapeAssertionError;

  try {
    assertReducerShape(finalReducers);
  } catch (e) {
    shapeAssertionError = e;
  }

  return function combination(state, action) {
    if (state === void 0) {
      state = {};
    }

    if (shapeAssertionError) {
      throw shapeAssertionError;
    }

    if (true) {
      var warningMessage = getUnexpectedStateShapeWarningMessage(state, finalReducers, action, unexpectedKeyCache);

      if (warningMessage) {
        warning(warningMessage);
      }
    }

    var hasChanged = false;
    var nextState = {};

    for (var _i = 0; _i < finalReducerKeys.length; _i++) {
      var _key = finalReducerKeys[_i];
      var reducer = finalReducers[_key];
      var previousStateForKey = state[_key];
      var nextStateForKey = reducer(previousStateForKey, action);

      if (typeof nextStateForKey === 'undefined') {
        var actionType = action && action.type;
        throw new Error( false ? undefined : "When called with an action of type " + (actionType ? "\"" + String(actionType) + "\"" : '(unknown type)') + ", the slice reducer for key \"" + _key + "\" returned undefined. " + "To ignore an action, you must explicitly return the previous state. " + "If you want this reducer to hold no value, you can return null instead of undefined.");
      }

      nextState[_key] = nextStateForKey;
      hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
    }

    hasChanged = hasChanged || finalReducerKeys.length !== Object.keys(state).length;
    return hasChanged ? nextState : state;
  };
}

function bindActionCreator(actionCreator, dispatch) {
  return function () {
    return dispatch(actionCreator.apply(this, arguments));
  };
}
/**
 * Turns an object whose values are action creators, into an object with the
 * same keys, but with every function wrapped into a `dispatch` call so they
 * may be invoked directly. This is just a convenience method, as you can call
 * `store.dispatch(MyActionCreators.doSomething())` yourself just fine.
 *
 * For convenience, you can also pass an action creator as the first argument,
 * and get a dispatch wrapped function in return.
 *
 * @param {Function|Object} actionCreators An object whose values are action
 * creator functions. One handy way to obtain it is to use ES6 `import * as`
 * syntax. You may also pass a single function.
 *
 * @param {Function} dispatch The `dispatch` function available on your Redux
 * store.
 *
 * @returns {Function|Object} The object mimicking the original object, but with
 * every action creator wrapped into the `dispatch` call. If you passed a
 * function as `actionCreators`, the return value will also be a single
 * function.
 */


function bindActionCreators(actionCreators, dispatch) {
  if (typeof actionCreators === 'function') {
    return bindActionCreator(actionCreators, dispatch);
  }

  if (typeof actionCreators !== 'object' || actionCreators === null) {
    throw new Error( false ? undefined : "bindActionCreators expected an object or a function, but instead received: '" + kindOf(actionCreators) + "'. " + "Did you write \"import ActionCreators from\" instead of \"import * as ActionCreators from\"?");
  }

  var boundActionCreators = {};

  for (var key in actionCreators) {
    var actionCreator = actionCreators[key];

    if (typeof actionCreator === 'function') {
      boundActionCreators[key] = bindActionCreator(actionCreator, dispatch);
    }
  }

  return boundActionCreators;
}

/**
 * Composes single-argument functions from right to left. The rightmost
 * function can take multiple arguments as it provides the signature for
 * the resulting composite function.
 *
 * @param {...Function} funcs The functions to compose.
 * @returns {Function} A function obtained by composing the argument functions
 * from right to left. For example, compose(f, g, h) is identical to doing
 * (...args) => f(g(h(...args))).
 */
function compose() {
  for (var _len = arguments.length, funcs = new Array(_len), _key = 0; _key < _len; _key++) {
    funcs[_key] = arguments[_key];
  }

  if (funcs.length === 0) {
    return function (arg) {
      return arg;
    };
  }

  if (funcs.length === 1) {
    return funcs[0];
  }

  return funcs.reduce(function (a, b) {
    return function () {
      return a(b.apply(void 0, arguments));
    };
  });
}

/**
 * Creates a store enhancer that applies middleware to the dispatch method
 * of the Redux store. This is handy for a variety of tasks, such as expressing
 * asynchronous actions in a concise manner, or logging every action payload.
 *
 * See `redux-thunk` package as an example of the Redux middleware.
 *
 * Because middleware is potentially asynchronous, this should be the first
 * store enhancer in the composition chain.
 *
 * Note that each middleware will be given the `dispatch` and `getState` functions
 * as named arguments.
 *
 * @param {...Function} middlewares The middleware chain to be applied.
 * @returns {Function} A store enhancer applying the middleware.
 */

function applyMiddleware() {
  for (var _len = arguments.length, middlewares = new Array(_len), _key = 0; _key < _len; _key++) {
    middlewares[_key] = arguments[_key];
  }

  return function (createStore) {
    return function () {
      var store = createStore.apply(void 0, arguments);

      var _dispatch = function dispatch() {
        throw new Error( false ? undefined : 'Dispatching while constructing your middleware is not allowed. ' + 'Other middleware would not be applied to this dispatch.');
      };

      var middlewareAPI = {
        getState: store.getState,
        dispatch: function dispatch() {
          return _dispatch.apply(void 0, arguments);
        }
      };
      var chain = middlewares.map(function (middleware) {
        return middleware(middlewareAPI);
      });
      _dispatch = compose.apply(void 0, chain)(store.dispatch);
      return Object(_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])(Object(_babel_runtime_helpers_esm_objectSpread2__WEBPACK_IMPORTED_MODULE_0__["default"])({}, store), {}, {
        dispatch: _dispatch
      });
    };
  };
}

/*
 * This is a dummy function to check if the function name has been altered by minification.
 * If the function has been minified and NODE_ENV !== 'production', warn the user.
 */

function isCrushed() {}

if ( true && typeof isCrushed.name === 'string' && isCrushed.name !== 'isCrushed') {
  warning('You are currently using minified code outside of NODE_ENV === "production". ' + 'This means that you are running a slower development build of Redux. ' + 'You can use loose-envify (https://github.com/zertosh/loose-envify) for browserify ' + 'or setting mode to production in webpack (https://webpack.js.org/concepts/mode/) ' + 'to ensure you have the correct code for your production build.');
}




/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}


/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && typeof btoa !== 'undefined') {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./src/actions.js":
/*!************************!*\
  !*** ./src/actions.js ***!
  \************************/
/*! exports provided: AUTH_API_UNREACHABLE, AUTH_API_REACHABLE, USER_LOGIN, USER_LOGIN_WITH_CHANGEPASS, USER_LOGIN_RESPONSE, USER_AVAILABILITY_CHECK, USER_AVAILABILITY_CHECK_RESPONSE, USER_LOGOUT, USER_REAUTH, USER_CREATE, USER_LOGIN_CONFIG, USER_LOGIN_CONFIG_COMPLETE, USER_REQUEST_VALIDATION, USER_VALIDATE, USER_VALIDATE_RESPONSE, usernameAvailable, userLoginConfig, userCreate, userLogin, userLogout, authUnreachable, userRequestValidation, userValidate, userLoginAndChangePassword */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AUTH_API_UNREACHABLE", function() { return AUTH_API_UNREACHABLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AUTH_API_REACHABLE", function() { return AUTH_API_REACHABLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGIN", function() { return USER_LOGIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGIN_WITH_CHANGEPASS", function() { return USER_LOGIN_WITH_CHANGEPASS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGIN_RESPONSE", function() { return USER_LOGIN_RESPONSE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_AVAILABILITY_CHECK", function() { return USER_AVAILABILITY_CHECK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_AVAILABILITY_CHECK_RESPONSE", function() { return USER_AVAILABILITY_CHECK_RESPONSE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGOUT", function() { return USER_LOGOUT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_REAUTH", function() { return USER_REAUTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_CREATE", function() { return USER_CREATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGIN_CONFIG", function() { return USER_LOGIN_CONFIG; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_LOGIN_CONFIG_COMPLETE", function() { return USER_LOGIN_CONFIG_COMPLETE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_REQUEST_VALIDATION", function() { return USER_REQUEST_VALIDATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_VALIDATE", function() { return USER_VALIDATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_VALIDATE_RESPONSE", function() { return USER_VALIDATE_RESPONSE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usernameAvailable", function() { return usernameAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userLoginConfig", function() { return userLoginConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userCreate", function() { return userCreate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userLogin", function() { return userLogin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userLogout", function() { return userLogout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "authUnreachable", function() { return authUnreachable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userRequestValidation", function() { return userRequestValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userValidate", function() { return userValidate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userLoginAndChangePassword", function() { return userLoginAndChangePassword; });
function action(type) {
  var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  return {
    type: type,
    payload: payload
  };
}

var AUTH_API_UNREACHABLE = "AUTH_API_UNREACHABLE";
var AUTH_API_REACHABLE = "AUTH_API_REACHABLE";
var USER_LOGIN = "USER_LOGIN";
var USER_LOGIN_WITH_CHANGEPASS = "USER_LOGIN_WITH_CHANGEPASS";
var USER_LOGIN_RESPONSE = "USER_LOGIN_RESPONSE";
var USER_AVAILABILITY_CHECK = "USER_AVAILABILITY_CHECK";
var USER_AVAILABILITY_CHECK_RESPONSE = "USER_AVAILABILITY_CHECK_RESPONSE";
var USER_LOGOUT = "USER_LOGOUT";
var USER_REAUTH = "USER_REAUTH";
var USER_CREATE = "USER_CREATE";
var USER_LOGIN_CONFIG = "USER_LOGIN_CONFIG";
var USER_LOGIN_CONFIG_COMPLETE = "USER_LOGIN_CONFIG_COMPLETE";
var USER_REQUEST_VALIDATION = "USER_REQUEST_VALIDATION";
var USER_VALIDATE = "USER_VALIDATE";
var USER_VALIDATE_RESPONSE = "USER_VALIDATE_RESPONSE";
var usernameAvailable = function usernameAvailable(payload) {
  return action(USER_AVAILABILITY_CHECK, payload);
};
var userLoginConfig = function userLoginConfig(payload) {
  return action(USER_LOGIN_CONFIG, payload);
};
var userCreate = function userCreate(payload) {
  return action(USER_CREATE, payload);
};
var userLogin = function userLogin(payload) {
  return action(USER_LOGIN, payload);
};
var userLogout = function userLogout() {
  return action(USER_LOGOUT);
};
var authUnreachable = function authUnreachable() {
  return action(AUTH_API_UNREACHABLE);
};
var userRequestValidation = function userRequestValidation(payload) {
  return action(USER_REQUEST_VALIDATION, payload);
};
var userValidate = function userValidate(payload) {
  return action(USER_VALIDATE, payload);
};
var userLoginAndChangePassword = function userLoginAndChangePassword(payload) {
  return action(USER_LOGIN_WITH_CHANGEPASS, payload);
};

/***/ }),

/***/ "./src/components/SignIn/Pages/AccountInvalid.js":
/*!*******************************************************!*\
  !*** ./src/components/SignIn/Pages/AccountInvalid.js ***!
  \*******************************************************/
/*! exports provided: AccountInvalid */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountInvalid", function() { return AccountInvalid; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var AccountInvalid = function AccountInvalid() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_2__["SignInContext"]);
  var styles = context.styles,
      onCloseModal = context.onCloseModal,
      onRevalidate = context.onRevalidate,
      onGoBack = context.onGoBack;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.title
  }, "Validate Your Account"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.goBack,
    onClick: onGoBack
  }, "Not your email?", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    style: {
      textDecoration: "underline"
    }
  }, "Go Back"), "."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.subtitle
  }, "To use this account you must validate it by clicking the link which has been emailed to you. If you have not received your validation email, please check your spam folder."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: _objectSpread(_objectSpread({}, styles.row), {}, {
      marginTop: "1rem"
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    style: styles.button,
    type: "button",
    value: "Re-Send Validation Email",
    onClick: onRevalidate
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
    style: styles.button,
    type: "button",
    value: "Ok",
    onClick: onCloseModal
  }))));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/AskForEmail.js":
/*!****************************************************!*\
  !*** ./src/components/SignIn/Pages/AskForEmail.js ***!
  \****************************************************/
/*! exports provided: AskForEmail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AskForEmail", function() { return AskForEmail; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");



var AskForEmail = function AskForEmail() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_2__["SignInContext"]);
  var styles = context.styles,
      onChange = context.onChange,
      onContinue = context.onContinue,
      formValues = context.formValues,
      helpLink = context.helpLink,
      isWorking = context.isWorking,
      hasCloseButton = context.hasCloseButton;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("form", {
    onSubmit: onContinue
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.title
  }, "Sign In To Continue"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.subtitle
  }, "Sign in or register with your email address"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__["FormElement"], {
    label: "Email Address:",
    style: styles,
    id: "email",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__["FIELDTYPE"].EMAIL,
    onChange: onChange
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    style: styles.button,
    type: "submit"
  }, "Continue")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.row
  }, helpLink)));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/ConfirmNewAccount.js":
/*!**********************************************************!*\
  !*** ./src/components/SignIn/Pages/ConfirmNewAccount.js ***!
  \**********************************************************/
/*! exports provided: ConfirmNewAccount */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmNewAccount", function() { return ConfirmNewAccount; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");



var ConfirmNewAccount = function ConfirmNewAccount() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_2__["SignInContext"]);
  var onCloseModal = context.onCloseModal;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      display: "flex"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      display: "flex",
      flexDirection: "column",
      height: "100%"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Your account has been created!"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, "Check your email to complete registration."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "button",
    value: "Ok",
    onClick: onCloseModal
  }))));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/NewSignup.js":
/*!**************************************************!*\
  !*** ./src/components/SignIn/Pages/NewSignup.js ***!
  \**************************************************/
/*! exports provided: NewSignup */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewSignup", function() { return NewSignup; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var NewSignup = function NewSignup() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_3__["SignInContext"]);
  var styles = context.styles,
      onChange = context.onChange,
      onContinue = context.onContinue,
      formValues = context.formValues,
      onGoBack = context.onGoBack;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    onSubmit: onContinue
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.title
  }, "Create your account"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.goBack,
    onClick: onGoBack
  }, "Not your email?", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    style: {
      textDecoration: "underline"
    }
  }, "Go Back"), "."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.label
  }, "Email:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, formValues.submitted_email)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.label
  }, "Name:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "name",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].TEXT,
    onChange: onChange
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: _objectSpread(_objectSpread({}, styles.row), {}, {
      marginTop: "2rem"
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    style: styles.button,
    type: "submit"
  }, "Register"))));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/ResetPass.js":
/*!**************************************************!*\
  !*** ./src/components/SignIn/Pages/ResetPass.js ***!
  \**************************************************/
/*! exports provided: ResetPass */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetPass", function() { return ResetPass; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var ResetPass = function ResetPass() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_3__["SignInContext"]);
  var styles = context.styles,
      onChange = context.onChange,
      onContinue = context.onContinue,
      formValues = context.formValues,
      helpLink = context.helpLink,
      badPassLimit = context.badPassLimit,
      passwordTries = context.passwordTries,
      isWorking = context.isWorking,
      onGoBack = context.onGoBack,
      onReset = context.onReset;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    onSubmit: onContinue
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.title
  }, "Welcome back ", formValues.submitted_email, "!"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.goBack,
    onClick: onGoBack
  }, "Not your email?", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    style: {
      textDecoration: "underline"
    }
  }, "Go Back"), "."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.subtitle
  }, "Please update your password to continue"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.col
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: badPassLimit < passwordTries && !isWorking ? styles.label_bad : styles.label
  }, "Current Password:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "password",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].PASSWORD,
    onChange: onChange
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, "New Password:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "new_password",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].PASSWORD,
    onChange: onChange
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, "New Password Again:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "new_password_confirm",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].PASSWORD,
    onChange: onChange
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: _objectSpread(_objectSpread({}, styles.row), {}, {
      marginTop: "1rem"
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.label
  }, "Remember Me:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "extendedSession",
    value: formValues["extendedSession"],
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].CHECKBOX,
    onChange: onChange
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    style: styles.button,
    type: "submit"
  }, "Sign In")), badPassLimit < passwordTries && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    onClick: onReset
  }, "reset account")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, helpLink)));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/SignOut.js":
/*!************************************************!*\
  !*** ./src/components/SignIn/Pages/SignOut.js ***!
  \************************************************/
/*! exports provided: SignOut */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignOut", function() { return SignOut; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _src_actions_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /src/actions.js */ "./src/actions.js");




var SignOut = function SignOut() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_1__["SignInContext"]);
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useDispatch"])();
  var styles = context.styles,
      onCloseModal = context.onCloseModal,
      helpLink = context.helpLink;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.title
  }, "Sign Out"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.subtitle
  }, "Do you really want to sign out?"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      marginRight: "1rem"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: function onClick() {
      return dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_3__["userLogout"])());
    },
    style: styles.button,
    type: "submit"
  }, "Sign Out")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: onCloseModal,
    style: styles.button,
    type: "submit"
  }, "Cancel"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: styles.row
  }, helpLink));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/WelcomeBack.js":
/*!****************************************************!*\
  !*** ./src/components/SignIn/Pages/WelcomeBack.js ***!
  \****************************************************/
/*! exports provided: WelcomeBack */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeBack", function() { return WelcomeBack; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _index_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.js */ "./src/components/SignIn/Pages/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var WelcomeBack = function WelcomeBack() {
  var context = Object(react__WEBPACK_IMPORTED_MODULE_1__["useContext"])(_index_js__WEBPACK_IMPORTED_MODULE_3__["SignInContext"]);
  var styles = context.styles,
      onChange = context.onChange,
      onContinue = context.onContinue,
      formValues = context.formValues,
      helpLink = context.helpLink,
      badPassLimit = context.badPassLimit,
      passwordTries = context.passwordTries,
      isWorking = context.isWorking,
      onGoBack = context.onGoBack,
      onReset = context.onReset;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("form", {
    onSubmit: onContinue
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.container
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.title
  }, "Welcome back ", formValues.submitted_email, "!"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.goBack,
    onClick: onGoBack
  }, "Not your email?", " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    style: {
      textDecoration: "underline"
    }
  }, "Go Back"), "."), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.subtitle
  }, "Great to see you again,", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " enter your password to continue"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: badPassLimit < passwordTries && !isWorking ? styles.label_bad : styles.label
  }, "Password:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "password",
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].PASSWORD,
    onChange: onChange
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: _objectSpread(_objectSpread({}, styles.row), {}, {
      marginTop: "1rem"
    })
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.label
  }, "Remember Me:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FormElement"], {
    id: "extendedSession",
    value: formValues["extendedSession"],
    type: _theplacelab_ui__WEBPACK_IMPORTED_MODULE_2__["FIELDTYPE"].CHECKBOX,
    onChange: onChange
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
    style: styles.button,
    type: "submit"
  }, "Sign In")), badPassLimit < passwordTries && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    onClick: onReset
  }, "reset account")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    style: styles.row
  }, helpLink)));
};

/***/ }),

/***/ "./src/components/SignIn/Pages/index.js":
/*!**********************************************!*\
  !*** ./src/components/SignIn/Pages/index.js ***!
  \**********************************************/
/*! exports provided: SignInContext, Pages, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInContext", function() { return SignInContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pages", function() { return Pages; });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _src_actions_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! /src/actions.js */ "./src/actions.js");
/* harmony import */ var _src_components_Spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! /src/components/Spinner */ "./src/components/Spinner.js");
/* harmony import */ var _NewSignup__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./NewSignup */ "./src/components/SignIn/Pages/NewSignup.js");
/* harmony import */ var _AskForEmail__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./AskForEmail */ "./src/components/SignIn/Pages/AskForEmail.js");
/* harmony import */ var _SignOut__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./SignOut */ "./src/components/SignIn/Pages/SignOut.js");
/* harmony import */ var _WelcomeBack__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./WelcomeBack */ "./src/components/SignIn/Pages/WelcomeBack.js");
/* harmony import */ var _AccountInvalid__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./AccountInvalid */ "./src/components/SignIn/Pages/AccountInvalid.js");
/* harmony import */ var _ResetPass__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./ResetPass */ "./src/components/SignIn/Pages/ResetPass.js");
/* harmony import */ var _ConfirmNewAccount__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./ConfirmNewAccount */ "./src/components/SignIn/Pages/ConfirmNewAccount.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }












var SignInContext = /*#__PURE__*/Object(react__WEBPACK_IMPORTED_MODULE_2__["createContext"])();
var Pages = function Pages(_ref) {
  var helpLink = _ref.helpLink,
      onCloseModal = _ref.onCloseModal,
      prefill = _ref.prefill,
      onStatus = _ref.onStatus,
      _ref$style = _ref.style,
      style = _ref$style === void 0 ? {} : _ref$style,
      isAuthenticated = _ref.isAuthenticated,
      hasCloseButton = _ref.hasCloseButton;
  var styles = {
    overlay: {
      position: "absolute",
      backgroundColor: "white",
      height: "100%",
      width: "100%",
      zIndex: 1,
      opacity: 1.0,
      display: "flex"
    },
    goBack: {
      margin: "1rem",
      opacity: "0.5"
    },
    container: _objectSpread({
      textAlign: "center",
      padding: "0rem",
      margin: "auto",
      position: "absolute",
      bottom: 0,
      top: 0,
      left: 0,
      right: 0
    }, style.container),
    label: _objectSpread({
      fontWeight: "900",
      marginRight: ".2rem"
    }, style.label),
    label_bad: _objectSpread({
      color: "red",
      fontWeight: "900"
    }, style.label_bad),
    row: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center"
    },
    title: _objectSpread({
      fontSize: "1.5rem",
      margin: "0 0 1rem 0"
    }, style.title),
    subtitle: _objectSpread({
      fontSize: "1rem",
      margin: "0 0 1rem 0"
    }, style.subtitle),
    button: _objectSpread({
      height: "2rem",
      border: "1px solid grey",
      borderRadius: "0.3rem"
    }, style.button)
  };
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useDispatch"])();
  var isWorking = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    return redux.isWorking;
  });
  var availableUsername = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    return redux.availableUsername;
  });

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])({
    extendedSession: true
  }),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
      formValues = _useState2[0],
      setFormValues = _useState2[1]; // How many password tries allowed


  var passwordTries = 3;

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(passwordTries),
      _useState4 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState3, 2),
      badPassLimit = _useState4[0],
      setBadPassLimit = _useState4[1];

  if (badPassLimit === 0) {
    setFormValues({});
    setBadPassLimit(passwordTries);
  } // Called indirectly via useEffect to avoid "update while render" hook issue


  var statusMessage;

  var onPostStatus = function onPostStatus(message) {
    if (typeof onStatus === "function") onStatus({
      icon: "envelope",
      backgroundColor: "#4c6d89",
      message: message
    });
  };

  Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
    if (statusMessage && typeof onStatus === "function") {
      onPostStatus(statusMessage);
      onCloseModal();
    }
  }); // Either prefill OR lookup

  var _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false),
      _useState6 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState5, 2),
      accountExists = _useState6[0],
      setAccountExists = _useState6[1];

  var _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false),
      _useState8 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState7, 2),
      accountValid = _useState8[0],
      setAccountValid = _useState8[1];

  var _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false),
      _useState10 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState9, 2),
      accountNeedsReset = _useState10[0],
      setAccountNeedsReset = _useState10[1];

  Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
    if (availableUsername) {
      setAccountExists(!availableUsername.available);
      setAccountValid(availableUsername.valid);
      setAccountNeedsReset(availableUsername.reset);
    }
  }, [availableUsername]);
  Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
    if (prefill) {
      setFormValues({
        email: prefill.email,
        submitted_email: prefill.email
      });
      setAccountExists(!prefill.available);
      setAccountValid(prefill.v);
      setAccountNeedsReset(prefill.r);
    }
  }, [prefill]); // Handles each page

  var _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(0),
      _useState12 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState11, 2),
      loginStage = _useState12[0],
      setLoginStage = _useState12[1];

  var onContinue = function onContinue(e) {
    var _newFormValues$submit, _newFormValues$submit2, _newFormValues$submit3;

    e === null || e === void 0 ? void 0 : e.preventDefault();

    var newFormValues = _objectSpread(_objectSpread({}, formValues), {}, {
      submitted_email: formValues["email"],
      submitted_password: formValues["password"],
      submitted_name: formValues["name"]
    });

    setFormValues(newFormValues);

    if (((_newFormValues$submit = newFormValues.submitted_email) === null || _newFormValues$submit === void 0 ? void 0 : _newFormValues$submit.length) > 0 && ((_newFormValues$submit2 = newFormValues.submitted_password) === null || _newFormValues$submit2 === void 0 ? void 0 : _newFormValues$submit2.length) > 0) {
      setBadPassLimit(badPassLimit - 1);
      setFormValues(_objectSpread(_objectSpread({}, newFormValues), {}, {
        submitted_password: ""
      }));

      if (accountNeedsReset) {
        if (formValues.new_password && formValues.new_password_confirm === formValues.new_password) {
          dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userLoginAndChangePassword"])({
            username: newFormValues.submitted_email,
            password: newFormValues.submitted_password,
            newpass: formValues.new_password,
            extendedSession: newFormValues.extendedSession
          }));
        } else {
          console.error("New passwords don't match");
        }
      } else {
        dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userLogin"])({
          username: newFormValues.submitted_email,
          password: newFormValues.submitted_password,
          extendedSession: newFormValues.extendedSession
        }));
      }
    } else if (!accountExists && newFormValues.submitted_email && newFormValues.submitted_name) {
      dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userCreate"])({
        username: newFormValues.submitted_email,
        name: newFormValues.submitted_name
      }));
    } else if (((_newFormValues$submit3 = newFormValues.submitted_email) === null || _newFormValues$submit3 === void 0 ? void 0 : _newFormValues$submit3.length) > 0) {
      dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["usernameAvailable"])({
        username: newFormValues.submitted_email
      }));
    }

    setLoginStage(loginStage + 1);
  };

  var onChange = function onChange(payload) {
    setFormValues(_objectSpread(_objectSpread({}, formValues), {}, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()({}, payload.id, payload.value)));
  };

  var onRevalidate = function onRevalidate(payload) {
    onCloseModal();
    dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userRequestValidation"])({
      username: formValues.submitted_email
    }));
    onPostStatus("Validation re-sent, check your inbox to complete registration!");
  };

  var onReset = function onReset(payload) {
    onCloseModal();
    dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userRequestValidation"])({
      username: formValues.submitted_email
    }));
    onPostStatus("Account reset, check your inbox to log back in");
  };

  var pageContent = isAuthenticated ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_SignOut__WEBPACK_IMPORTED_MODULE_8__["SignOut"], null) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_AskForEmail__WEBPACK_IMPORTED_MODULE_7__["AskForEmail"], null);

  if (!accountExists && formValues.submitted_email && formValues.submitted_name) {
    pageContent = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_ConfirmNewAccount__WEBPACK_IMPORTED_MODULE_12__["ConfirmNewAccount"], null);
    statusMessage = "Account created, check your inbox to complete registration!";
  } else if (formValues.submitted_email && !isWorking) {
    if (accountExists) {
      if (!accountValid) {
        pageContent = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_AccountInvalid__WEBPACK_IMPORTED_MODULE_10__["AccountInvalid"], null);
      } else if (accountNeedsReset) {
        pageContent = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_ResetPass__WEBPACK_IMPORTED_MODULE_11__["ResetPass"], null);
      } else {
        pageContent = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_WelcomeBack__WEBPACK_IMPORTED_MODULE_9__["WelcomeBack"], null);
      }
    } else {
      pageContent = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_NewSignup__WEBPACK_IMPORTED_MODULE_6__["NewSignup"], null);
    }
  }

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(SignInContext.Provider, {
    value: {
      styles: styles,
      onReset: onReset,
      onChange: onChange,
      onContinue: onContinue,
      onRevalidate: onRevalidate,
      onGoBack: function onGoBack() {
        return setFormValues({});
      },
      formValues: formValues,
      helpLink: helpLink,
      badPassLimit: badPassLimit,
      passwordTries: passwordTries,
      onCloseModal: onCloseModal,
      isWorking: isWorking,
      onStatus: onStatus,
      hasCloseButton: hasCloseButton
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_src_components_Spinner__WEBPACK_IMPORTED_MODULE_5__["Spinner"], null, pageContent));
};
/* harmony default export */ __webpack_exports__["default"] = (Pages);

/***/ }),

/***/ "./src/components/SignIn/index.js":
/*!****************************************!*\
  !*** ./src/components/SignIn/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _src_actions_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! /src/actions.js */ "./src/actions.js");
/* harmony import */ var _Pages__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Pages */ "./src/components/SignIn/Pages/index.js");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @theplacelab/ui */ "@theplacelab/ui");
/* harmony import */ var _theplacelab_ui__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_spinners_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-spinners-css */ "./node_modules/react-spinners-css/lib/esm/index.js");



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }








var SignIn = function SignIn(_ref) {
  var authService = _ref.authService,
      authHelper = _ref.authHelper,
      onLogin = _ref.onLogin,
      onLogout = _ref.onLogout,
      onStatus = _ref.onStatus,
      onAvatarClick = _ref.onAvatarClick,
      helpLink = _ref.helpLink,
      style = _ref.style,
      prefill = _ref.prefill,
      _ref$showUsername = _ref.showUsername,
      showUsername = _ref$showUsername === void 0 ? false : _ref$showUsername,
      _ref$sizeThreshold = _ref.sizeThreshold,
      sizeThreshold = _ref$sizeThreshold === void 0 ? 900 : _ref$sizeThreshold,
      _ref$hasCloseButton = _ref.hasCloseButton,
      hasCloseButton = _ref$hasCloseButton === void 0 ? true : _ref$hasCloseButton,
      _ref$loginText = _ref.loginText,
      loginText = _ref$loginText === void 0 ? "Sign In" : _ref$loginText,
      _ref$logoutText = _ref.logoutText,
      logoutText = _ref$logoutText === void 0 ? "Sign Out" : _ref$logoutText;

  var windowSize = function windowSize() {
    var _useState = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])({
      width: undefined,
      height: undefined,
      sizeThreshold: false
    }),
        _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1___default()(_useState, 2),
        windowSize = _useState2[0],
        setWindowSize = _useState2[1];

    Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
      function handleResize() {
        setWindowSize({
          width: window.innerWidth,
          height: window.innerHeight,
          threshold: window.innerWidth < sizeThreshold
        });
      }

      window.addEventListener("resize", handleResize);
      handleResize();
      return function () {
        return window.removeEventListener("resize", handleResize);
      };
    }, []);
    return windowSize;
  };

  var size = windowSize();
  var authServiceReachable = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    return redux.authServiceReachable;
  });
  var isAuthenticated = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    return redux.isAuthenticated;
  });
  var isInitializing = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    return redux.isInitializing;
  });

  var _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_2__["useState"])(false),
      _useState4 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_1___default()(_useState3, 2),
      modalVisible = _useState4[0],
      setModalVisible = _useState4[1];

  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useDispatch"])();
  var email = Object(react_redux__WEBPACK_IMPORTED_MODULE_3__["useSelector"])(function (redux) {
    var _redux$claims;

    return (_redux$claims = redux.claims) === null || _redux$claims === void 0 ? void 0 : _redux$claims.email;
  }); // Configure services

  Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
    dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_4__["userLoginConfig"])({
      authService: authService,
      authHelper: authHelper,
      onLogin: onLogin,
      onLogout: onLogout,
      onAvatarClick: onAvatarClick
    }));
  }, [authService, authHelper, onLogin, onLogout]);
  Object(react__WEBPACK_IMPORTED_MODULE_2__["useEffect"])(function () {
    setModalVisible(prefill ? true : false);
  }, [isAuthenticated]); // Prevents FOUC before we stabilize

  if (isInitializing) return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_spinners_css__WEBPACK_IMPORTED_MODULE_7__["Ring"], {
    color: "white",
    size: 20
  });

  var styles = _objectSpread(_objectSpread({}, style), {}, {
    container: _objectSpread({
      padding: 0,
      margin: 0
    }, style === null || style === void 0 ? void 0 : style.container),
    title: _objectSpread({
      margin: 0
    }, style === null || style === void 0 ? void 0 : style.title),
    subtitle: _objectSpread({
      margin: "1rem"
    }, style === null || style === void 0 ? void 0 : style.subtitle),
    footer: _objectSpread({
      display: "flex",
      textAlign: "center",
      background: "lightGrey",
      width: "100%",
      height: "4rem",
      position: "absolute",
      bottom: 0
    }, style === null || style === void 0 ? void 0 : style.footer),
    modal: size.threshold ? {
      height: "25rem",
      width: "25rem",
      maxWidth: "100vh",
      maxHeight: "100vh",
      minWidth: "100vw",
      minHeight: "100vh"
    } : {
      padding: 0,
      display: "flex",
      height: "25rem",
      width: "25rem",
      marginTop: "10rem",
      maxWidth: "40rem",
      maxHeight: "30rem"
    }
  });

  if (!authServiceReachable) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      className: "fas fa-exclamation-triangle"
    });
  } else {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_2___default.a.Fragment, null, !isAuthenticated && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      onClick: function onClick() {
        return setModalVisible(true);
      }
    }, loginText) || /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      onClick: function onClick() {
        return setModalVisible(true);
      }
    }, logoutText, " ", showUsername ? "(".concat(email, ")") : ""), modalVisible && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_theplacelab_ui__WEBPACK_IMPORTED_MODULE_6__["Modal"], {
      onOutsideClick: function onOutsideClick() {
        return setModalVisible(false);
      },
      style: styles.modal
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_Pages__WEBPACK_IMPORTED_MODULE_5__["default"], {
      isAuthenticated: isAuthenticated,
      hasCloseButton: hasCloseButton,
      onCloseModal: function onCloseModal() {
        return setModalVisible(false);
      },
      helpLink: helpLink,
      style: style,
      prefill: prefill,
      onStatus: onStatus
    }), hasCloseButton && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      style: styles.footer
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("div", {
      style: {
        margin: "auto"
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement("input", {
      style: styles.button,
      type: "button",
      value: "Cancel",
      onClick: function onClick() {
        return setModalVisible(false);
      }
    })))));
  }
};

/* harmony default export */ __webpack_exports__["default"] = (SignIn);

/***/ }),

/***/ "./src/components/Spinner.js":
/*!***********************************!*\
  !*** ./src/components/Spinner.js ***!
  \***********************************/
/*! exports provided: Spinner */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Spinner", function() { return Spinner; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_spinners_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-spinners-css */ "./node_modules/react-spinners-css/lib/esm/index.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);



var Spinner = function Spinner(_ref) {
  var children = _ref.children;
  var isWorking = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (redux) {
    return redux.isWorking;
  });
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, isWorking && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      position: "absolute",
      backgroundColor: "white",
      height: "100%",
      width: "100%",
      zIndex: 1,
      opacity: 1.0,
      display: "flex"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    style: {
      width: "100%",
      margin: "auto",
      textAlign: "center"
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_spinners_css__WEBPACK_IMPORTED_MODULE_1__["Ring"], {
    color: "#7a7676",
    size: 32
  }))), children);
};

/***/ }),

/***/ "./src/components/Validate/index.js":
/*!******************************************!*\
  !*** ./src/components/Validate/index.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _src_actions_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! /src/actions.js */ "./src/actions.js");
/* harmony import */ var react_spinners_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-spinners-css */ "./node_modules/react-spinners-css/lib/esm/index.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }







var Validate = function Validate(_ref) {
  var token = _ref.token,
      style = _ref.style,
      onComplete = _ref.onComplete,
      authService = _ref.authService,
      authHelper = _ref.authHelper,
      onLogin = _ref.onLogin,
      onLogout = _ref.onLogout,
      onAvatarClick = _ref.onAvatarClick;
  var dispatch = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useDispatch"])();
  var isWorking = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (redux) {
    return redux.isWorking;
  });
  var redux = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (redux) {
    return redux;
  });
  console.log(redux);
  var authServiceReachable = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (redux) {
    return redux.authServiceReachable;
  });
  var accountStatus = Object(react_redux__WEBPACK_IMPORTED_MODULE_2__["useSelector"])(function (redux) {
    return redux.validatedAccount;
  }); // Configure services

  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_3__["userLoginConfig"])({
      authService: authService,
      authHelper: authHelper,
      onLogin: onLogin,
      onLogout: onLogout,
      onAvatarClick: onAvatarClick
    }));
  }, [authService, authHelper, onLogin, onLogout]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (authServiceReachable) dispatch(Object(_src_actions_js__WEBPACK_IMPORTED_MODULE_3__["userValidate"])(token));
  }, [authServiceReachable, token]);
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
    if (!isWorking && accountStatus !== null && accountStatus !== void 0 && accountStatus.v && typeof onComplete === "function") onComplete(accountStatus);
  }, [isWorking, accountStatus]);
  if (!authServiceReachable) return null;

  var styles = _objectSpread({
    modalStyle: {
      height: "25rem",
      width: "25rem",
      marginTop: "10rem"
    }
  }, style);

  if (isWorking) {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
      style: {
        textAlign: "center"
      }
    }, "Validating...", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("br", null), " ", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_spinners_css__WEBPACK_IMPORTED_MODULE_4__["Ring"], {
      color: "#7a7676",
      size: 32
    }));
  } else {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, (accountStatus === null || accountStatus === void 0 ? void 0 : accountStatus.v) && /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, "Account Validated! Please sign in to continue") || /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", null, "Validation link is invalid or expired")));
  }
};

/* harmony default export */ __webpack_exports__["default"] = (Validate);

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: SignIn, Validate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignIn", function() { return SignIn; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Validate", function() { return Validate; });
/* harmony import */ var _components_Validate__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/Validate */ "./src/components/Validate/index.js");
/* harmony import */ var _components_SignIn__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/SignIn */ "./src/components/SignIn/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _store_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./store.js */ "./src/store.js");






var Validate = function Validate(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_3__["Provider"], {
    store: _store_js__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_Validate__WEBPACK_IMPORTED_MODULE_0__["default"], props));
};

var SignIn = function SignIn(props) {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_3__["Provider"], {
    store: _store_js__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(_components_SignIn__WEBPACK_IMPORTED_MODULE_1__["default"], props));
};



/***/ }),

/***/ "./src/reducer/index.js":
/*!******************************!*\
  !*** ./src/reducer/index.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../actions */ "./src/actions.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var user = function user() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    authServiceReachable: false,
    isInitializing: true,
    isAuthenticated: false,
    token: "",
    availableUsernames: [],
    validatedAccounts: []
  };
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_AVAILABILITY_CHECK"]:
    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_LOGIN"]:
    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_VALIDATE"]:
    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_LOGIN_WITH_CHANGEPASS"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        isWorking: true
      });

    case _actions__WEBPACK_IMPORTED_MODULE_1__["AUTH_API_UNREACHABLE"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        isInitializing: false,
        authServiceReachable: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_1__["AUTH_API_REACHABLE"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        isInitializing: false,
        authServiceReachable: true
      });

    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_LOGIN_CONFIG_COMPLETE"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        authServiceReachable: true
      }, action.payload);

    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_AVAILABILITY_CHECK_RESPONSE"]:
      return _objectSpread(_objectSpread({}, state), {}, {
        availableUsername: action.payload,
        isWorking: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_LOGIN_RESPONSE"]:
      return _objectSpread(_objectSpread(_objectSpread({}, state), action.payload), {}, {
        isWorking: false,
        isInitializing: false
      });

    case _actions__WEBPACK_IMPORTED_MODULE_1__["USER_VALIDATE_RESPONSE"]:
      console.log(action.payload);
      return _objectSpread(_objectSpread({}, state), {}, {
        validatedAccount: action.payload,
        isWorking: false
      });

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (user);

/***/ }),

/***/ "./src/saga.js":
/*!*********************!*\
  !*** ./src/saga.js ***!
  \*********************/
/*! exports provided: periodicallyReauthorize, reauthorize, changePassAndLogin, login, logout, userValidate, userRequestValidation, usernameAvailable, userCreate, validateConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "periodicallyReauthorize", function() { return periodicallyReauthorize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reauthorize", function() { return reauthorize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changePassAndLogin", function() { return changePassAndLogin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "login", function() { return login; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logout", function() { return logout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userValidate", function() { return userValidate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userRequestValidation", function() { return userRequestValidation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "usernameAvailable", function() { return usernameAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "userCreate", function() { return userCreate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateConfig", function() { return validateConfig; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var _actions_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./actions.js */ "./src/actions.js");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }





var decodeToken = function decodeToken(token) {
  try {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    return JSON.parse(window.atob(base64));
  } catch (_unused) {
    return null;
  }
};

var periodicallyReauthorize = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function periodicallyReauthorize() {
  var currentToken, reduxState, now, secondsUntilExpire;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function periodicallyReauthorize$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          currentToken = localStorage.getItem("refreshToken");

          if (!currentToken) {
            _context.next = 4;
            break;
          }

          _context.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(reauthorize);

        case 4:
          if (false) {}

          _context.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 7:
          reduxState = _context.sent;

          if (!reduxState.authServiceReachable) {
            _context.next = 20;
            break;
          }

          if (!reduxState.isAuthenticated) {
            _context.next = 17;
            break;
          }

          now = Date.now() / 1000;
          secondsUntilExpire = reduxState.claims.exp - now;

          if (!(secondsUntilExpire <= 120)) {
            _context.next = 15;
            break;
          }

          _context.next = 15;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(reauthorize);

        case 15:
          _context.next = 20;
          break;

        case 17:
          if (!currentToken) {
            _context.next = 20;
            break;
          }

          _context.next = 20;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(reauthorize);

        case 20:
          _context.next = 22;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["delay"])(60000);

        case 22:
          _context.next = 4;
          break;

        case 24:
        case "end":
          return _context.stop();
      }
    }
  }, periodicallyReauthorize);
});
var reauthorize = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function reauthorize(action) {
  var reduxState, refreshToken, response, _res, authToken, _refreshToken, payload;

  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function reauthorize$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 2:
          reduxState = _context2.sent;
          refreshToken = localStorage.getItem("refreshToken");

          if (!reduxState.authServiceReachable) {
            _context2.next = 30;
            break;
          }

          if (!refreshToken) {
            _context2.next = 28;
            break;
          }

          _context2.next = 8;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authService, "/rpc/reauthorize"),
            body: {
              token: refreshToken
            }
          });

        case 8:
          response = _context2.sent;

          if (!(response && response.ok)) {
            _context2.next = 22;
            break;
          }

          _context2.next = 12;
          return response.json();

        case 12:
          _res = _context2.sent;
          authToken = _res.auth;
          _refreshToken = _res.refresh;
          localStorage.setItem("refreshToken", _refreshToken);
          payload = _objectSpread(_objectSpread({}, _res), {}, {
            isAuthenticated: true,
            claims: decodeToken(authToken)
          });
          _context2.next = 19;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_LOGIN_RESPONSE"],
            payload: payload
          });

        case 19:
          if (typeof reduxState.onLogin === "function") reduxState.onLogin(payload);
          _context2.next = 26;
          break;

        case 22:
          localStorage.removeItem("refreshToken");
          console.warn("Reauthorization Failed");
          _context2.next = 26;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(logout);

        case 26:
          _context2.next = 30;
          break;

        case 28:
          _context2.next = 30;
          return logout();

        case 30:
        case "end":
          return _context2.stop();
      }
    }
  }, reauthorize);
});
var changePassAndLogin = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function changePassAndLogin(action) {
  var reduxState, response, _res2, authToken, refreshToken, payload;

  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function changePassAndLogin$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          localStorage.removeItem("refreshToken");
          _context3.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 3:
          reduxState = _context3.sent;

          if (!reduxState.authService) {
            _context3.next = 25;
            break;
          }

          _context3.next = 7;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authService, "/rpc/changepass"),
            body: {
              email: action.payload.username,
              pass: action.payload.password,
              newpass: action.payload.newpass,
              extend: action.payload.extendedSession ? action.payload.extendedSession : false
            }
          });

        case 7:
          response = _context3.sent;

          if (!(response && response.ok)) {
            _context3.next = 23;
            break;
          }

          _context3.next = 11;
          return response.json();

        case 11:
          _res2 = _context3.sent;
          authToken = _res2.auth;
          refreshToken = _res2.refresh;
          localStorage.setItem("refreshToken", refreshToken);
          payload = {
            isAuthenticated: true,
            authToken: authToken,
            claims: decodeToken(authToken)
          };
          _context3.next = 18;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_LOGIN_RESPONSE"],
            payload: payload
          });

        case 18:
          if (!(typeof reduxState.onLogin === "function")) {
            _context3.next = 21;
            break;
          }

          _context3.next = 21;
          return reduxState.onLogin(payload);

        case 21:
          _context3.next = 25;
          break;

        case 23:
          _context3.next = 25;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(logout);

        case 25:
        case "end":
          return _context3.stop();
      }
    }
  }, changePassAndLogin);
});
var login = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function login(action) {
  var reduxState, response, _res3, authToken, refreshToken, payload;

  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function login$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          localStorage.removeItem("refreshToken");
          _context4.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 3:
          reduxState = _context4.sent;

          if (!reduxState.authService) {
            _context4.next = 25;
            break;
          }

          _context4.next = 7;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authService, "/rpc/login"),
            body: {
              email: action.payload.username,
              pass: action.payload.password,
              extend: action.payload.extendedSession ? action.payload.extendedSession : false
            }
          });

        case 7:
          response = _context4.sent;

          if (!(response && response.ok)) {
            _context4.next = 23;
            break;
          }

          _context4.next = 11;
          return response.json();

        case 11:
          _res3 = _context4.sent;
          authToken = _res3.auth;
          refreshToken = _res3.refresh;
          localStorage.setItem("refreshToken", refreshToken);
          payload = _objectSpread(_objectSpread({}, _res3), {}, {
            isAuthenticated: true,
            claims: decodeToken(authToken)
          });
          _context4.next = 18;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_LOGIN_RESPONSE"],
            payload: payload
          });

        case 18:
          if (!(typeof reduxState.onLogin === "function")) {
            _context4.next = 21;
            break;
          }

          _context4.next = 21;
          return reduxState.onLogin(payload);

        case 21:
          _context4.next = 25;
          break;

        case 23:
          _context4.next = 25;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["call"])(logout);

        case 25:
        case "end":
          return _context4.stop();
      }
    }
  }, login);
});
var logout = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function logout(action) {
  var reduxState, payload;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function logout$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 2:
          reduxState = _context5.sent;
          localStorage.removeItem("refreshToken");
          payload = {
            isAuthenticated: false,
            authToken: "",
            claims: {}
          };
          _context5.next = 7;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_LOGIN_RESPONSE"],
            payload: payload
          });

        case 7:
          if (typeof reduxState.onLogout === "function") reduxState.onLogout(payload);

        case 8:
        case "end":
          return _context5.stop();
      }
    }
  }, logout);
});
var userValidate = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function userValidate(action) {
  var token, reduxState, decodedToken, checkToken, checkUser, userState;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function userValidate$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          token = action.payload;
          _context6.next = 3;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 3:
          reduxState = _context6.sent;
          _context6.next = 6;
          return console.log(reduxState);

        case 6:
          if (!reduxState.authServiceReachable) {
            _context6.next = 33;
            break;
          }

          decodedToken = decodeToken(token);
          _context6.next = 10;
          return console.log(decodedToken);

        case 10:
          if (!(decodedToken !== null && decodedToken !== void 0 && decodedToken.email)) {
            _context6.next = 31;
            break;
          }

          _context6.next = 13;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authHelper, "/user/validate"),
            body: {
              token: token
            }
          });

        case 13:
          checkToken = _context6.sent;
          _context6.next = 16;
          return console.log(checkToken);

        case 16:
          if (checkToken.ok) {
            _context6.next = 21;
            break;
          }

          _context6.next = 19;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_VALIDATE_RESPONSE"],
            payload: null
          });

        case 19:
          _context6.next = 29;
          break;

        case 21:
          _context6.next = 23;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authService, "/rpc/checkusername"),
            body: {
              username: decodedToken.email
            }
          });

        case 23:
          checkUser = _context6.sent;
          _context6.next = 26;
          return checkUser.json();

        case 26:
          userState = _context6.sent;
          _context6.next = 29;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_VALIDATE_RESPONSE"],
            payload: _objectSpread(_objectSpread({}, userState), {}, {
              email: decodedToken.email
            })
          });

        case 29:
          _context6.next = 33;
          break;

        case 31:
          _context6.next = 33;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_VALIDATE_RESPONSE"],
            payload: null
          });

        case 33:
        case "end":
          return _context6.stop();
      }
    }
  }, userValidate);
});
var userRequestValidation = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function userRequestValidation(action) {
  var reduxState, response;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function userRequestValidation$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 2:
          reduxState = _context7.sent;

          if (!reduxState.authServiceReachable) {
            _context7.next = 7;
            break;
          }

          _context7.next = 6;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authHelper, "/user/validate/reset"),
            body: {
              email: action.payload.username
            }
          });

        case 6:
          response = _context7.sent;

        case 7:
        case "end":
          return _context7.stop();
      }
    }
  }, userRequestValidation);
});
var usernameAvailable = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function usernameAvailable(action) {
  var reduxState, response, payload, _res4;

  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function usernameAvailable$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 2:
          reduxState = _context8.sent;

          if (!reduxState.authServiceReachable) {
            _context8.next = 17;
            break;
          }

          _context8.next = 6;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authService, "/rpc/checkusername"),
            body: {
              username: action.payload.username
            }
          });

        case 6:
          response = _context8.sent;

          if (!(response && response.ok)) {
            _context8.next = 14;
            break;
          }

          _context8.next = 10;
          return response.json();

        case 10:
          _res4 = _context8.sent;
          payload = {
            username: action.payload.username,
            available: _res4.available,
            valid: _res4.v,
            reset: _res4.r
          };
          _context8.next = 15;
          break;

        case 14:
          payload = {
            username: action.payload.username,
            available: false,
            valid: res.v,
            reset: res.r
          };

        case 15:
          _context8.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_AVAILABILITY_CHECK_RESPONSE"],
            payload: payload
          });

        case 17:
        case "end":
          return _context8.stop();
      }
    }
  }, usernameAvailable);
});
var userCreate = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function userCreate(action) {
  var reduxState;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function userCreate$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["select"])();

        case 2:
          reduxState = _context9.sent;

          if (!reduxState.authServiceReachable) {
            _context9.next = 6;
            break;
          }

          _context9.next = 6;
          return _makeRequest({
            method: "POST",
            url: "".concat(reduxState.authHelper, "/user"),
            body: {
              email: action.payload.username,
              name: action.payload.name
            }
          });

        case 6:
        case "end":
          return _context9.stop();
      }
    }
  }, userCreate);
});
var validateConfig = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function validateConfig(action) {
  var checkAuthService, checkAuthHelper;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function validateConfig$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          if (!(!action.payload.authService || !action.payload.authHelper)) {
            _context10.next = 6;
            break;
          }

          console.error("@theplacelab/user: Missing config. You must provide both authService and authHelper via <Login/>");
          _context10.next = 4;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["AUTH_API_UNREACHABLE"]
          });

        case 4:
          _context10.next = 17;
          break;

        case 6:
          _context10.next = 8;
          return _makeRequest({
            method: "GET",
            url: action.payload.authService
          });

        case 8:
          checkAuthService = _context10.sent;
          _context10.next = 11;
          return _makeRequest({
            method: "GET",
            url: action.payload.authHelper
          });

        case 11:
          checkAuthHelper = _context10.sent;

          if (!(checkAuthService.ok && checkAuthHelper.ok)) {
            _context10.next = 15;
            break;
          }

          _context10.next = 15;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_LOGIN_CONFIG_COMPLETE"],
            payload: action.payload
          });

        case 15:
          _context10.next = 17;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["USER_REAUTH"]
          });

        case 17:
        case "end":
          return _context10.stop();
      }
    }
  }, validateConfig);
});

var _makeRequest = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _makeRequest(_ref) {
  var method, url, body, request;
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _makeRequest$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          method = _ref.method, url = _ref.url, body = _ref.body;
          _context11.prev = 1;
          request = {
            method: method,
            headers: {
              Accept: "application/json, application/xml, text/plain, text/html, *.*",
              "Content-Type": "application/json; charset=utf-8"
            },
            body: body ? typeof body === "string" ? body : JSON.stringify(body).replace(/\\u0000/g, "") : null
          };
          if (!body || method === "GET") delete request.body;
          _context11.next = 6;
          return fetch(url, request);

        case 6:
          return _context11.abrupt("return", _context11.sent);

        case 9:
          _context11.prev = 9;
          _context11.t0 = _context11["catch"](1);
          _context11.next = 13;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_2__["put"])({
            type: _actions_js__WEBPACK_IMPORTED_MODULE_3__["AUTH_API_UNREACHABLE"]
          });

        case 13:
          console.error("FATAL: Cannot reach auth API endpoint(s)!");
          return _context11.abrupt("return", _context11.t0);

        case 15:
        case "end":
          return _context11.stop();
      }
    }
  }, _makeRequest, null, [[1, 9]]);
});

/***/ }),

/***/ "./src/store.js":
/*!**********************!*\
  !*** ./src/store.js ***!
  \**********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "./node_modules/redux-saga/dist/redux-saga-effects-npm-proxy.esm.js");
/* harmony import */ var _actions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./actions */ "./src/actions.js");
/* harmony import */ var _saga__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./saga */ "./src/saga.js");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! redux-saga */ "redux-saga");
/* harmony import */ var redux_saga__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(redux_saga__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _reducer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./reducer */ "./src/reducer/index.js");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");


var _marked = /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(rootSaga);








function rootSaga() {
  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function rootSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([Object(_saga__WEBPACK_IMPORTED_MODULE_3__["periodicallyReauthorize"])(), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_LOGIN"], _saga__WEBPACK_IMPORTED_MODULE_3__["login"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_LOGIN_WITH_CHANGEPASS"], _saga__WEBPACK_IMPORTED_MODULE_3__["changePassAndLogin"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_LOGOUT"], _saga__WEBPACK_IMPORTED_MODULE_3__["logout"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_REAUTH"], _saga__WEBPACK_IMPORTED_MODULE_3__["reauthorize"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_AVAILABILITY_CHECK"], _saga__WEBPACK_IMPORTED_MODULE_3__["usernameAvailable"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_CREATE"], _saga__WEBPACK_IMPORTED_MODULE_3__["userCreate"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_LOGIN_CONFIG"], _saga__WEBPACK_IMPORTED_MODULE_3__["validateConfig"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_REQUEST_VALIDATION"], _saga__WEBPACK_IMPORTED_MODULE_3__["userRequestValidation"]), Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(_actions__WEBPACK_IMPORTED_MODULE_2__["USER_VALIDATE"], _saga__WEBPACK_IMPORTED_MODULE_3__["userValidate"])]);

        case 2:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

var sagaMiddleware = redux_saga__WEBPACK_IMPORTED_MODULE_4___default()();

var store = function store() {
  var store = Object(redux__WEBPACK_IMPORTED_MODULE_6__["createStore"])(_reducer__WEBPACK_IMPORTED_MODULE_5__["default"], Object(redux__WEBPACK_IMPORTED_MODULE_6__["applyMiddleware"])(sagaMiddleware));
  sagaMiddleware.run(rootSaga);
  return store;
};

/* harmony default export */ __webpack_exports__["default"] = (store());

/***/ }),

/***/ "@theplacelab/ui":
/*!**********************************!*\
  !*** external "@theplacelab/ui" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@theplacelab/ui");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "redux-saga":
/*!*****************************!*\
  !*** external "redux-saga" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9pbmRleC93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2FycmF5TGlrZVRvQXJyYXkuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9hcnJheVdpdGhIb2xlcy5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvZXNtL2RlZmluZVByb3BlcnR5LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvZXNtL2V4dGVuZHMuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9lc20vb2JqZWN0U3ByZWFkMi5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2l0ZXJhYmxlVG9BcnJheUxpbWl0LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvbm9uSXRlcmFibGVSZXN0LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvc2xpY2VkVG9BcnJheS5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL3Vuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL3JlZ2VuZXJhdG9yL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0ByZWR1eC1zYWdhL2NvcmUvZGlzdC9pby02ZGUxNTZmMy5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AcmVkdXgtc2FnYS9jb3JlL2Rpc3QvcmVkdXgtc2FnYS1lZmZlY3RzLmVzbS5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AcmVkdXgtc2FnYS9kZWxheS1wL2Rpc3QvcmVkdXgtc2FnYS1kZWxheS1wLmVzbS5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9AcmVkdXgtc2FnYS9pcy9kaXN0L3JlZHV4LXNhZ2EtaXMuZXNtLmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL0ByZWR1eC1zYWdhL3N5bWJvbHMvZGlzdC9yZWR1eC1zYWdhLXN5bWJvbHMuZXNtLmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL2NsYXNzbmFtZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vQ2lyY2xlL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vRGVmYXVsdC9zdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0R1YWxSaW5nL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vRWxsaXBzaXMvc3R5bGUubW9kdWxlLmNzcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9GYWNlYm9vay9zdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0dyaWQvc3R5bGUubW9kdWxlLmNzcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9IZWFydC9zdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0hvdXJnbGFzcy9zdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL09yYml0YWxzL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vT3Vyb2Jvcm8vc3R5bGUubW9kdWxlLmNzcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9SaW5nL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vUmlwcGxlL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vUm9sbGVyL3N0eWxlLm1vZHVsZS5jc3MiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vU3Bpbm5lci9zdHlsZS5tb2R1bGUuY3NzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9vYmplY3QtYXNzaWduL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0NpcmNsZS9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9DaXJjbGUvc3R5bGUubW9kdWxlLmNzcz83ZGNiIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0RlZmF1bHQvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vRGVmYXVsdC9zdHlsZS5tb2R1bGUuY3NzPzJjZjUiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vRHVhbFJpbmcvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vRHVhbFJpbmcvc3R5bGUubW9kdWxlLmNzcz9iOGRmIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0VsbGlwc2lzL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0VsbGlwc2lzL3N0eWxlLm1vZHVsZS5jc3M/YjMzYiIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9GYWNlYm9vay9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9GYWNlYm9vay9zdHlsZS5tb2R1bGUuY3NzP2IyNWMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vR3JpZC9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9HcmlkL3N0eWxlLm1vZHVsZS5jc3M/YmY4YSIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9IZWFydC9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9IZWFydC9zdHlsZS5tb2R1bGUuY3NzPzAyMjciLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vSG91cmdsYXNzL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL0hvdXJnbGFzcy9zdHlsZS5tb2R1bGUuY3NzPzc1ZTAiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vT3JiaXRhbHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vT3JiaXRhbHMvc3R5bGUubW9kdWxlLmNzcz8zNzM5Iiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL091cm9ib3JvL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL091cm9ib3JvL3N0eWxlLm1vZHVsZS5jc3M/OTAzOSIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9SaW5nL2luZGV4LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL1Jpbmcvc3R5bGUubW9kdWxlLmNzcz9hZWM4Iiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL1JpcHBsZS9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9SaXBwbGUvc3R5bGUubW9kdWxlLmNzcz9kYjk0Iiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL1JvbGxlci9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWFjdC1zcGlubmVycy1jc3MvbGliL2VzbS9Sb2xsZXIvc3R5bGUubW9kdWxlLmNzcz8zMWVjIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0LXNwaW5uZXJzLWNzcy9saWIvZXNtL1NwaW5uZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vU3Bpbm5lci9zdHlsZS5tb2R1bGUuY3NzPzQyNTYiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3Qtc3Bpbm5lcnMtY3NzL2xpYi9lc20vaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvcmVhY3QvY2pzL3JlYWN0LWpzeC1ydW50aW1lLmRldmVsb3BtZW50LmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlYWN0L2pzeC1ydW50aW1lLmpzIiwid2VicGFjazovL2luZGV4Ly4vbm9kZV9tb2R1bGVzL3JlZHV4LXNhZ2EvZGlzdC9yZWR1eC1zYWdhLWVmZmVjdHMtbnBtLXByb3h5LmVzbS5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWR1eC9lcy9yZWR1eC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL25vZGVfbW9kdWxlcy9yZWdlbmVyYXRvci1ydW50aW1lL3J1bnRpbWUuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvYWN0aW9ucy5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL3NyYy9jb21wb25lbnRzL1NpZ25Jbi9QYWdlcy9BY2NvdW50SW52YWxpZC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL3NyYy9jb21wb25lbnRzL1NpZ25Jbi9QYWdlcy9Bc2tGb3JFbWFpbC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL3NyYy9jb21wb25lbnRzL1NpZ25Jbi9QYWdlcy9Db25maXJtTmV3QWNjb3VudC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL3NyYy9jb21wb25lbnRzL1NpZ25Jbi9QYWdlcy9OZXdTaWdudXAuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvY29tcG9uZW50cy9TaWduSW4vUGFnZXMvUmVzZXRQYXNzLmpzIiwid2VicGFjazovL2luZGV4Ly4vc3JjL2NvbXBvbmVudHMvU2lnbkluL1BhZ2VzL1NpZ25PdXQuanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvY29tcG9uZW50cy9TaWduSW4vUGFnZXMvV2VsY29tZUJhY2suanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvY29tcG9uZW50cy9TaWduSW4vUGFnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvY29tcG9uZW50cy9TaWduSW4vaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvY29tcG9uZW50cy9TcGlubmVyLmpzIiwid2VicGFjazovL2luZGV4Ly4vc3JjL2NvbXBvbmVudHMvVmFsaWRhdGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vaW5kZXgvLi9zcmMvcmVkdWNlci9pbmRleC5qcyIsIndlYnBhY2s6Ly9pbmRleC8uL3NyYy9zYWdhLmpzIiwid2VicGFjazovL2luZGV4Ly4vc3JjL3N0b3JlLmpzIiwid2VicGFjazovL2luZGV4L2V4dGVybmFsIFwiQHRoZXBsYWNlbGFiL3VpXCIiLCJ3ZWJwYWNrOi8vaW5kZXgvZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL2luZGV4L2V4dGVybmFsIFwicmVhY3QtcmVkdXhcIiIsIndlYnBhY2s6Ly9pbmRleC9leHRlcm5hbCBcInJlZHV4LXNhZ2FcIiJdLCJuYW1lcyI6WyJhY3Rpb24iLCJ0eXBlIiwicGF5bG9hZCIsIkFVVEhfQVBJX1VOUkVBQ0hBQkxFIiwiQVVUSF9BUElfUkVBQ0hBQkxFIiwiVVNFUl9MT0dJTiIsIlVTRVJfTE9HSU5fV0lUSF9DSEFOR0VQQVNTIiwiVVNFUl9MT0dJTl9SRVNQT05TRSIsIlVTRVJfQVZBSUxBQklMSVRZX0NIRUNLIiwiVVNFUl9BVkFJTEFCSUxJVFlfQ0hFQ0tfUkVTUE9OU0UiLCJVU0VSX0xPR09VVCIsIlVTRVJfUkVBVVRIIiwiVVNFUl9DUkVBVEUiLCJVU0VSX0xPR0lOX0NPTkZJRyIsIlVTRVJfTE9HSU5fQ09ORklHX0NPTVBMRVRFIiwiVVNFUl9SRVFVRVNUX1ZBTElEQVRJT04iLCJVU0VSX1ZBTElEQVRFIiwiVVNFUl9WQUxJREFURV9SRVNQT05TRSIsInVzZXJuYW1lQXZhaWxhYmxlIiwidXNlckxvZ2luQ29uZmlnIiwidXNlckNyZWF0ZSIsInVzZXJMb2dpbiIsInVzZXJMb2dvdXQiLCJhdXRoVW5yZWFjaGFibGUiLCJ1c2VyUmVxdWVzdFZhbGlkYXRpb24iLCJ1c2VyVmFsaWRhdGUiLCJ1c2VyTG9naW5BbmRDaGFuZ2VQYXNzd29yZCIsIkFjY291bnRJbnZhbGlkIiwiY29udGV4dCIsInVzZUNvbnRleHQiLCJTaWduSW5Db250ZXh0Iiwic3R5bGVzIiwib25DbG9zZU1vZGFsIiwib25SZXZhbGlkYXRlIiwib25Hb0JhY2siLCJjb250YWluZXIiLCJ0aXRsZSIsImdvQmFjayIsInRleHREZWNvcmF0aW9uIiwic3VidGl0bGUiLCJyb3ciLCJtYXJnaW5Ub3AiLCJidXR0b24iLCJBc2tGb3JFbWFpbCIsIm9uQ2hhbmdlIiwib25Db250aW51ZSIsImZvcm1WYWx1ZXMiLCJoZWxwTGluayIsImlzV29ya2luZyIsImhhc0Nsb3NlQnV0dG9uIiwiRklFTERUWVBFIiwiRU1BSUwiLCJDb25maXJtTmV3QWNjb3VudCIsImRpc3BsYXkiLCJmbGV4RGlyZWN0aW9uIiwiaGVpZ2h0IiwiTmV3U2lnbnVwIiwibGFiZWwiLCJzdWJtaXR0ZWRfZW1haWwiLCJURVhUIiwiUmVzZXRQYXNzIiwiYmFkUGFzc0xpbWl0IiwicGFzc3dvcmRUcmllcyIsIm9uUmVzZXQiLCJjb2wiLCJsYWJlbF9iYWQiLCJQQVNTV09SRCIsIkNIRUNLQk9YIiwiU2lnbk91dCIsImRpc3BhdGNoIiwidXNlRGlzcGF0Y2giLCJtYXJnaW5SaWdodCIsIldlbGNvbWVCYWNrIiwiY3JlYXRlQ29udGV4dCIsIlBhZ2VzIiwicHJlZmlsbCIsIm9uU3RhdHVzIiwic3R5bGUiLCJpc0F1dGhlbnRpY2F0ZWQiLCJvdmVybGF5IiwicG9zaXRpb24iLCJiYWNrZ3JvdW5kQ29sb3IiLCJ3aWR0aCIsInpJbmRleCIsIm9wYWNpdHkiLCJtYXJnaW4iLCJ0ZXh0QWxpZ24iLCJwYWRkaW5nIiwiYm90dG9tIiwidG9wIiwibGVmdCIsInJpZ2h0IiwiZm9udFdlaWdodCIsImNvbG9yIiwiYWxpZ25JdGVtcyIsImp1c3RpZnlDb250ZW50IiwiZm9udFNpemUiLCJib3JkZXIiLCJib3JkZXJSYWRpdXMiLCJ1c2VTZWxlY3RvciIsInJlZHV4IiwiYXZhaWxhYmxlVXNlcm5hbWUiLCJ1c2VTdGF0ZSIsImV4dGVuZGVkU2Vzc2lvbiIsInNldEZvcm1WYWx1ZXMiLCJzZXRCYWRQYXNzTGltaXQiLCJzdGF0dXNNZXNzYWdlIiwib25Qb3N0U3RhdHVzIiwibWVzc2FnZSIsImljb24iLCJ1c2VFZmZlY3QiLCJhY2NvdW50RXhpc3RzIiwic2V0QWNjb3VudEV4aXN0cyIsImFjY291bnRWYWxpZCIsInNldEFjY291bnRWYWxpZCIsImFjY291bnROZWVkc1Jlc2V0Iiwic2V0QWNjb3VudE5lZWRzUmVzZXQiLCJhdmFpbGFibGUiLCJ2YWxpZCIsInJlc2V0IiwiZW1haWwiLCJ2IiwiciIsImxvZ2luU3RhZ2UiLCJzZXRMb2dpblN0YWdlIiwiZSIsInByZXZlbnREZWZhdWx0IiwibmV3Rm9ybVZhbHVlcyIsInN1Ym1pdHRlZF9wYXNzd29yZCIsInN1Ym1pdHRlZF9uYW1lIiwibGVuZ3RoIiwibmV3X3Bhc3N3b3JkIiwibmV3X3Bhc3N3b3JkX2NvbmZpcm0iLCJ1c2VybmFtZSIsInBhc3N3b3JkIiwibmV3cGFzcyIsImNvbnNvbGUiLCJlcnJvciIsIm5hbWUiLCJpZCIsInZhbHVlIiwicGFnZUNvbnRlbnQiLCJTaWduSW4iLCJhdXRoU2VydmljZSIsImF1dGhIZWxwZXIiLCJvbkxvZ2luIiwib25Mb2dvdXQiLCJvbkF2YXRhckNsaWNrIiwic2hvd1VzZXJuYW1lIiwic2l6ZVRocmVzaG9sZCIsImxvZ2luVGV4dCIsImxvZ291dFRleHQiLCJ3aW5kb3dTaXplIiwidW5kZWZpbmVkIiwic2V0V2luZG93U2l6ZSIsImhhbmRsZVJlc2l6ZSIsIndpbmRvdyIsImlubmVyV2lkdGgiLCJpbm5lckhlaWdodCIsInRocmVzaG9sZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwic2l6ZSIsImF1dGhTZXJ2aWNlUmVhY2hhYmxlIiwiaXNJbml0aWFsaXppbmciLCJtb2RhbFZpc2libGUiLCJzZXRNb2RhbFZpc2libGUiLCJjbGFpbXMiLCJmb290ZXIiLCJiYWNrZ3JvdW5kIiwibW9kYWwiLCJtYXhXaWR0aCIsIm1heEhlaWdodCIsIm1pbldpZHRoIiwibWluSGVpZ2h0IiwiU3Bpbm5lciIsImNoaWxkcmVuIiwiVmFsaWRhdGUiLCJ0b2tlbiIsIm9uQ29tcGxldGUiLCJsb2ciLCJhY2NvdW50U3RhdHVzIiwidmFsaWRhdGVkQWNjb3VudCIsIm1vZGFsU3R5bGUiLCJwcm9wcyIsInN0b3JlIiwidXNlciIsInN0YXRlIiwiYXZhaWxhYmxlVXNlcm5hbWVzIiwidmFsaWRhdGVkQWNjb3VudHMiLCJhY3Rpb25zIiwiZGVjb2RlVG9rZW4iLCJiYXNlNjRVcmwiLCJzcGxpdCIsImJhc2U2NCIsInJlcGxhY2UiLCJKU09OIiwicGFyc2UiLCJhdG9iIiwicGVyaW9kaWNhbGx5UmVhdXRob3JpemUiLCJjdXJyZW50VG9rZW4iLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwiY2FsbCIsInJlYXV0aG9yaXplIiwic2VsZWN0IiwicmVkdXhTdGF0ZSIsIm5vdyIsIkRhdGUiLCJzZWNvbmRzVW50aWxFeHBpcmUiLCJleHAiLCJkZWxheSIsInJlZnJlc2hUb2tlbiIsIl9tYWtlUmVxdWVzdCIsIm1ldGhvZCIsInVybCIsImJvZHkiLCJyZXNwb25zZSIsIm9rIiwianNvbiIsInJlcyIsImF1dGhUb2tlbiIsImF1dGgiLCJyZWZyZXNoIiwic2V0SXRlbSIsInB1dCIsInJlbW92ZUl0ZW0iLCJ3YXJuIiwibG9nb3V0IiwiY2hhbmdlUGFzc0FuZExvZ2luIiwicGFzcyIsImV4dGVuZCIsImxvZ2luIiwiZGVjb2RlZFRva2VuIiwiY2hlY2tUb2tlbiIsImNoZWNrVXNlciIsInVzZXJTdGF0ZSIsInZhbGlkYXRlQ29uZmlnIiwiY2hlY2tBdXRoU2VydmljZSIsImNoZWNrQXV0aEhlbHBlciIsInJlcXVlc3QiLCJoZWFkZXJzIiwiQWNjZXB0Iiwic3RyaW5naWZ5IiwiZmV0Y2giLCJyb290U2FnYSIsImFsbCIsInRha2VMYXRlc3QiLCJzYWdhTWlkZGxld2FyZSIsImNyZWF0ZVNhZ2FNaWRkbGV3YXJlIiwiY3JlYXRlU3RvcmUiLCJyb290UmVkdWNlciIsImFwcGx5TWlkZGxld2FyZSIsInJ1biJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUNBOztBQUVBLHdDQUF3QyxTQUFTO0FBQ2pEO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLDZFOzs7Ozs7Ozs7OztBQ1hBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDZFOzs7Ozs7Ozs7OztBQ0xBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSw2RTs7Ozs7Ozs7Ozs7O0FDaEJBO0FBQUE7QUFBZTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ2JBO0FBQUE7QUFBZTtBQUNmO0FBQ0EsbUJBQW1CLHNCQUFzQjtBQUN6Qzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ2hCQTtBQUFBO0FBQUE7QUFBaUQ7O0FBRWpEO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVlO0FBQ2YsaUJBQWlCLHNCQUFzQjtBQUN2Qzs7QUFFQTtBQUNBO0FBQ0EsUUFBUSxrRUFBYztBQUN0QixPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBLEM7Ozs7Ozs7Ozs7O0FDdENBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSwyQkFBMkIsK0JBQStCO0FBQzFEOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0EsNkU7Ozs7Ozs7Ozs7O0FDL0JBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDZFOzs7Ozs7Ozs7OztBQ0xBLHFCQUFxQixtQkFBTyxDQUFDLG9GQUFxQjs7QUFFbEQsMkJBQTJCLG1CQUFPLENBQUMsZ0dBQTJCOztBQUU5RCxpQ0FBaUMsbUJBQU8sQ0FBQyw0R0FBaUM7O0FBRTFFLHNCQUFzQixtQkFBTyxDQUFDLHNGQUFzQjs7QUFFcEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsNkU7Ozs7Ozs7Ozs7O0FDYkEsdUJBQXVCLG1CQUFPLENBQUMsd0ZBQXVCOztBQUV0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsNkU7Ozs7Ozs7Ozs7O0FDWkEsaUJBQWlCLG1CQUFPLENBQUMsMEVBQXFCOzs7Ozs7Ozs7Ozs7O0FDQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWdIO0FBQ3REO0FBQ3VFO0FBQ3hGOztBQUV6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLElBQUksS0FBcUM7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUUsa0ZBQVE7O0FBRVY7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2bEJBQTZsQjs7QUFFN2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLElBQXFDO0FBQzdDO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUEsa0RBQWtELCtEQUFXO0FBQzdEO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQiw2REFBUztBQUMxQjtBQUNBO0FBQ0EsaUJBQWlCLCtEQUFXO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0EsZ0JBQWdCLDREQUFLO0FBQ3JCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IsaUVBQWE7QUFDbkM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTs7QUFFQSxrQkFBa0IsT0FBTyxzREFBRTtBQUMzQjs7QUFFQTtBQUNBLFNBQVMsNkRBQU07QUFDZjs7QUFFQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQSwwQkFBMEIsa0ZBQVEsR0FBRztBQUNyQztBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU0sS0FBcUM7QUFDM0Msd0JBQXdCLHVEQUFRO0FBQ2hDOztBQUVBLE1BQU0sOERBQU87QUFDYjtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBLE1BQU0sZ0VBQVMsc0JBQXNCLCtEQUFRLHNCQUFzQiw4REFBTztBQUMxRTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUEsTUFBTSw4REFBTztBQUNiO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUEsTUFBTSxJQUFxQztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0EsdUJBQXVCLHVEQUFRO0FBQy9CLHVCQUF1QixzREFBTztBQUM5QixvQkFBb0IsdURBQVE7QUFDNUIsS0FBSztBQUNMLHVCQUF1Qix1REFBUTtBQUMvQjtBQUNBOztBQUVBLE1BQU0sNERBQUs7QUFDWCx1QkFBdUI7O0FBRXZCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQSxzQkFBc0IsdURBQVE7O0FBRTlCLE1BQU0sMkRBQUk7QUFDVjtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsTUFBTSw0REFBSztBQUNYO0FBQ0E7QUFDQSxjQUFjLHVEQUFRO0FBQ3RCLEdBQUcsVUFBVSw2REFBTTtBQUNuQjtBQUNBO0FBQ0EsY0FBYyx1REFBUSxvQ0FBb0MsWUFBWTtBQUN0RSxHQUFHO0FBQ0gsd0JBQXdCLG1EQUFJO0FBQzVCO0FBQ0E7O0FBRUEsaUJBQWlCLDZEQUFNO0FBQ3ZCLHVCQUF1QixtREFBSTtBQUMzQjtBQUNBOztBQUVBLFlBQVksbURBQUksK0RBQStELFlBQVk7QUFDM0Y7O0FBRUE7QUFDQTtBQUNBOztBQUVBLE1BQU0sMkRBQUk7QUFDVjtBQUNBLEdBQUc7QUFDSCxRQUFRLDREQUFLO0FBQ2I7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUEsbUJBQW1CLDZEQUFNLFFBQVEsMkRBQUk7QUFDckM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx3RkFBd0YsYUFBYTtBQUNyRztBQUNBOztBQUVBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7O0FBRUEsNEZBQTRGLGVBQWU7QUFDM0c7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7QUFDQSxjQUFjLDZEQUFNO0FBQ3BCLEtBQUs7QUFDTDs7QUFFQSw0RkFBNEYsZUFBZTtBQUMzRztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQSw0RkFBNEYsZUFBZTtBQUMzRztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTtBQUNBOztBQUVBLFFBQVEsNERBQUs7QUFDYjtBQUNBLGlCQUFpQixtREFBSTtBQUNyQixPQUFPO0FBQ1AsS0FBSztBQUNMLHlCQUF5QixtREFBSTtBQUM3QjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0JBQWtCLHFFQUFpQjtBQUNuQzs7QUFFQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7QUFDQTs7QUFFQSxRQUFRLDREQUFLO0FBQ2I7QUFDQSxpQkFBaUIsbURBQUk7QUFDckIsT0FBTztBQUNQLEtBQUssMEJBQTBCLHFFQUFpQixJQUFJLCtEQUFRO0FBQzVELHlCQUF5QixtREFBSTtBQUM3QjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0RkFBNEYsZUFBZTtBQUMzRztBQUNBOztBQUVBLE1BQU0sS0FBcUM7QUFDM0Msd0JBQXdCLHVEQUFRO0FBQ2hDLG9CQUFvQixtREFBSTtBQUN4Qjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxNQUFNLElBQXFDO0FBQzNDLHFCQUFxQixzREFBTzs7QUFFNUI7QUFDQSxzQkFBc0IsdURBQVE7QUFDOUIsc0JBQXNCLHFEQUFNO0FBQzVCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0MscUJBQXFCLHNEQUFPO0FBQzVCOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0MsZ0JBQWdCLHFEQUFNO0FBQ3RCOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0MsaUJBQWlCLHFEQUFNO0FBQ3ZCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLDJEQUFNOztBQUUyNkI7Ozs7Ozs7Ozs7Ozs7QUN6cUJqOEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTZCO0FBQ2U7QUFDNkI7QUFDK0Y7QUFDMEo7QUFDclM7O0FBRTdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsTUFBTSw4REFBTztBQUNiO0FBQ0E7O0FBRUEsTUFBTSxxRUFBYztBQUNwQjtBQUNBOztBQUVBLE1BQU0sMkRBQUk7QUFDVjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUyx5REFBWTtBQUNyQjtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBLHdGQUF3RixhQUFhO0FBQ3JHO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBQVcseURBQUk7QUFDZjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlEQUFJO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0Esd0ZBQXdGLGFBQWE7QUFDckc7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FBVyx5REFBSTtBQUNmOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsaURBQUk7QUFDakI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLHlEQUFNO0FBQ25CO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBLHdGQUF3RixhQUFhO0FBQ3JHO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBQVcseURBQUk7QUFDZjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlEQUFJO0FBQ2pCO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBLHdGQUF3RixhQUFhO0FBQ3JHO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyx5REFBYSxVQUFVLHlEQUFPO0FBQ3pDOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEseURBQUk7QUFDakI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLGlEQUFJO0FBQ2pCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBQVcseURBQUs7QUFDaEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTs7QUFFQSx3RkFBd0YsYUFBYTtBQUNyRztBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXLGlEQUFJO0FBQ2Y7QUFDQTtBQUNBO0FBQ0EsV0FBVyx5REFBSztBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQSx3RkFBd0YsYUFBYTtBQUNyRztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcseURBQUk7QUFDZjtBQUNBO0FBQ0E7QUFDQSxXQUFXLHlEQUFJO0FBQ2YsY0FBYyx5REFBSTtBQUNsQixnQkFBZ0IseURBQUs7QUFDckIsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsaURBQUk7QUFDakI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBLEVBQUUseURBQUssbUJBQW1CLHVEQUFRO0FBQ2xDLEVBQUUseURBQUssU0FBUyx1REFBUTtBQUN4Qjs7QUFFQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQSx3RkFBd0YsYUFBYTtBQUNyRztBQUNBOztBQUVBLFNBQVMsaURBQUk7QUFDYjtBQUNBO0FBQ0EsTUFBTSxJQUFxQztBQUMzQztBQUNBOztBQUVBLDRGQUE0RixlQUFlO0FBQzNHO0FBQ0E7O0FBRUEsU0FBUyxpREFBSTtBQUNiO0FBQ0E7QUFDQSxNQUFNLElBQXFDO0FBQzNDO0FBQ0E7O0FBRUEsNEZBQTRGLGVBQWU7QUFDM0c7QUFDQTs7QUFFQSxTQUFTLGlEQUFJO0FBQ2I7QUFDQTtBQUNBLE1BQU0sSUFBcUM7QUFDM0MsSUFBSSx5REFBSyxVQUFVLHVEQUFRO0FBQzNCLElBQUkseURBQUssU0FBUyx1REFBUTtBQUMxQjs7QUFFQSw0RkFBNEYsZUFBZTtBQUMzRztBQUNBOztBQUVBLFNBQVMsaURBQUk7QUFDYjtBQUNBO0FBQ0EsNEZBQTRGLGVBQWU7QUFDM0c7QUFDQTs7QUFFQSxTQUFTLGlEQUFJO0FBQ2I7QUFDQTtBQUNBLDRGQUE0RixlQUFlO0FBQzNHO0FBQ0E7O0FBRUEsU0FBUyxpREFBSTtBQUNiOztBQUVrSjs7Ozs7Ozs7Ozs7OztBQzdibEo7QUFBQTtBQUE2Qzs7QUFFN0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSCxVQUFVLDBEQUFNO0FBQ2hCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFZSxxRUFBTSxFQUFDOzs7Ozs7Ozs7Ozs7O0FDbkJ0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF1RTs7QUFFdkU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQix3REFBSTtBQUNwQjtBQUNBO0FBQ0Esd0JBQXdCLCtEQUFXO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQkFBMkIsNkRBQVM7QUFDcEM7QUFDQTtBQUNBLG9CQUFvQixzREFBRTtBQUN0Qjs7QUFFZ007Ozs7Ozs7Ozs7Ozs7QUM3RGhNO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRXVJOzs7Ozs7Ozs7Ozs7QUN0Q3ZJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGdCQUFnQjs7QUFFaEI7QUFDQTs7QUFFQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsS0FBSyxLQUE2QjtBQUNsQztBQUNBO0FBQ0EsRUFBRSxVQUFVLElBQTRFO0FBQ3hGO0FBQ0EsRUFBRSxpQ0FBcUIsRUFBRSxtQ0FBRTtBQUMzQjtBQUNBLEdBQUc7QUFBQSxvR0FBQztBQUNKLEVBQUUsTUFBTSxFQUVOO0FBQ0YsQ0FBQzs7Ozs7Ozs7Ozs7O0FDekREO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDRCQUE0QixrQkFBa0IseUJBQXlCLG9GQUFvRixLQUFLLHNDQUFzQyxnQkFBZ0IsZ0VBQWdFLE9BQU8sVUFBVSxpQ0FBaUMsT0FBTyxXQUFXLG9DQUFvQyxnRUFBZ0UsT0FBTyxZQUFZLG9DQUFvQyxPQUFPLEdBQUc7QUFDbmpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDBCQUEwQix1QkFBdUIsZ0JBQWdCLGlCQUFpQixHQUFHLGdDQUFnQyx1QkFBdUIsZUFBZSxnQkFBZ0IscUJBQXFCLHVCQUF1Qiw0REFBNEQsR0FBRyw2Q0FBNkMsd0JBQXdCLGdCQUFnQixnQkFBZ0IsR0FBRyw2Q0FBNkMsMkJBQTJCLGVBQWUsZ0JBQWdCLEdBQUcsNkNBQTZDLDJCQUEyQixnQkFBZ0IsY0FBYyxHQUFHLDZDQUE2QywyQkFBMkIsZUFBZSxpQkFBaUIsR0FBRyw2Q0FBNkMsMkJBQTJCLGdCQUFnQixnQkFBZ0IsR0FBRyw2Q0FBNkMsMkJBQTJCLGVBQWUsaUJBQWlCLEdBQUcsNkNBQTZDLDJCQUEyQixnQkFBZ0IsZ0JBQWdCLEdBQUcsNkNBQTZDLDJCQUEyQixhQUFhLGlCQUFpQixHQUFHLDZDQUE2QywyQkFBMkIsZUFBZSxnQkFBZ0IsR0FBRyw4Q0FBOEMsMkJBQTJCLGVBQWUsaUJBQWlCLEdBQUcsOENBQThDLHlCQUF5QixlQUFlLGNBQWMsR0FBRyw4Q0FBOEMsMkJBQTJCLGFBQWEsZ0JBQWdCLEdBQUcsc0NBQXNDLGlDQUFpQywwQkFBMEIsS0FBSyxTQUFTLDRCQUE0QixLQUFLLEdBQUc7QUFDbnREO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNEJBQTRCLDBCQUEwQixnQkFBZ0IsaUJBQWlCLEdBQUcsMEJBQTBCLGlCQUFpQixtQkFBbUIsZ0JBQWdCLGlCQUFpQixnQkFBZ0IsdUJBQXVCLDJCQUEyQixvREFBb0QsMkRBQTJELEdBQUcscUNBQXFDLFFBQVEsOEJBQThCLEtBQUssVUFBVSxnQ0FBZ0MsS0FBSyxHQUFHO0FBQ3RnQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDRCQUE0Qix5QkFBeUIsS0FBSyxrQ0FBa0MseUJBQXlCLGtCQUFrQixvQkFBb0IscUJBQXFCLHlCQUF5Qix1QkFBdUIsMERBQTBELEtBQUssK0NBQStDLGdCQUFnQix1REFBdUQsS0FBSywrQ0FBK0MsZ0JBQWdCLHNEQUFzRCxLQUFLLCtDQUErQyxnQkFBZ0Isc0RBQXNELEtBQUssK0NBQStDLGdCQUFnQixxREFBcUQsS0FBSyx3Q0FBd0MsVUFBVSw0QkFBNEIsT0FBTyxZQUFZLDRCQUE0QixPQUFPLEtBQUssc0NBQXNDLFVBQVUsNEJBQTRCLE9BQU8sWUFBWSw0QkFBNEIsT0FBTyxLQUFLLHVDQUF1QyxVQUFVLG1DQUFtQyxPQUFPLFlBQVkseUNBQXlDLE9BQU8sS0FBSztBQUN2dUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDWkE7QUFDQSxrQ0FBa0MsbUJBQU8sQ0FBQyxpR0FBNEM7QUFDdEY7QUFDQTtBQUNBLGNBQWMsUUFBUyw2QkFBNkIsNEJBQTRCLHlCQUF5QixrQkFBa0IsbUJBQW1CLEtBQUssa0NBQWtDLDRCQUE0Qix5QkFBeUIsZ0JBQWdCLGlCQUFpQix1QkFBdUIsb0ZBQW9GLEtBQUssK0NBQStDLGdCQUFnQiw4QkFBOEIsS0FBSywrQ0FBK0MsZ0JBQWdCLDhCQUE4QixLQUFLLCtDQUErQyxnQkFBZ0IseUJBQXlCLEtBQUssd0NBQXdDLFVBQVUsaUJBQWlCLG9CQUFvQixPQUFPLGlCQUFpQixpQkFBaUIsb0JBQW9CLE9BQU8sS0FBSztBQUM1ekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDVEE7QUFDQSxrQ0FBa0MsbUJBQU8sQ0FBQyxpR0FBNEM7QUFDdEY7QUFDQTtBQUNBLGNBQWMsUUFBUyw2QkFBNkIsMEJBQTBCLHVCQUF1QixnQkFBZ0IsaUJBQWlCLEdBQUcsZ0NBQWdDLHVCQUF1QixlQUFlLGdCQUFnQix1QkFBdUIscUJBQXFCLDREQUE0RCxHQUFHLDZDQUE2QyxhQUFhLGNBQWMsd0JBQXdCLEdBQUcsNkNBQTZDLGFBQWEsY0FBYywyQkFBMkIsR0FBRyw2Q0FBNkMsYUFBYSxjQUFjLDJCQUEyQixHQUFHLDZDQUE2QyxhQUFhLGNBQWMsMkJBQTJCLEdBQUcsNkNBQTZDLGFBQWEsY0FBYywyQkFBMkIsR0FBRyw2Q0FBNkMsYUFBYSxjQUFjLDJCQUEyQixHQUFHLDZDQUE2QyxhQUFhLGNBQWMsMkJBQTJCLEdBQUcsNkNBQTZDLGFBQWEsY0FBYywyQkFBMkIsR0FBRyw2Q0FBNkMsYUFBYSxjQUFjLDJCQUEyQixHQUFHLHNDQUFzQyxpQkFBaUIsaUJBQWlCLEtBQUssU0FBUyxtQkFBbUIsS0FBSyxHQUFHO0FBQzkwQztBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNUQTtBQUNBLGtDQUFrQyxtQkFBTyxDQUFDLGlHQUE0QztBQUN0RjtBQUNBO0FBQ0EsY0FBYyxRQUFTLDZCQUE2Qiw0QkFBNEIseUJBQXlCLGtCQUFrQixtQkFBbUIsK0JBQStCLGtDQUFrQyxLQUFLLG9DQUFvQyxnQkFBZ0IsaUJBQWlCLHlCQUF5QixrQkFBa0IsbUJBQW1CLHVCQUF1QiwyRkFBMkYsS0FBSyw2R0FBNkcscUJBQXFCLHlCQUF5QixxQkFBcUIsa0JBQWtCLG1CQUFtQix1QkFBdUIsS0FBSyx1REFBdUQsa0JBQWtCLGlDQUFpQyxLQUFLLHVEQUF1RCxpQkFBaUIsaUNBQWlDLEtBQUssd0NBQXdDLFVBQVUsK0JBQStCLE9BQU8sVUFBVSw4QkFBOEIsT0FBTyxXQUFXLCtCQUErQixPQUFPLFdBQVcsNEJBQTRCLE9BQU8sV0FBVywrQkFBK0IsT0FBTyxZQUFZLDhCQUE4QixPQUFPLEtBQUs7QUFDenVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNYQTtBQUNBLGtDQUFrQyxtQkFBTyxDQUFDLGlHQUE0QztBQUN0RjtBQUNBO0FBQ0EsY0FBYyxRQUFTLDZCQUE2QiwwQkFBMEIsdUJBQXVCLGdCQUFnQixpQkFBaUIsR0FBRywwQkFBMEIsaUJBQWlCLG1CQUFtQix1QkFBdUIsYUFBYSxjQUFjLGdCQUFnQiwyQkFBMkIsNEJBQTRCLG9EQUFvRCxxREFBcUQsR0FBRyxzQ0FBc0MsUUFBUSwyQkFBMkIsd0VBQXdFLEtBQUssU0FBUyxnQ0FBZ0MscUVBQXFFLEtBQUssVUFBVSxpQ0FBaUMsS0FBSyxHQUFHO0FBQ3h1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1ZBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDBCQUEwQix1QkFBdUIsZ0JBQWdCLGlCQUFpQixHQUFHLDhCQUE4QixvQ0FBb0MsR0FBRyxxREFBcUQsdUJBQXVCLGdCQUFnQixpQkFBaUIsdUJBQXVCLGFBQWEsY0FBYyw2QkFBNkIsR0FBRyx5R0FBeUcsdUJBQXVCLGFBQWEsY0FBYyxHQUFHLHFEQUFxRCx1QkFBdUIsZ0JBQWdCLGlCQUFpQix1QkFBdUIsc0JBQXNCLEdBQUcscURBQXFELHNEQUFzRCwyQ0FBMkMsR0FBRyxxREFBcUQsc0RBQXNELDJDQUEyQyxHQUFHLG1EQUFtRCx1QkFBdUIsYUFBYSxjQUFjLGVBQWUsZ0JBQWdCLHVCQUF1QixnREFBZ0QsR0FBRyxtREFBbUQsdUJBQXVCLGFBQWEsY0FBYyxlQUFlLGdCQUFnQix1QkFBdUIsaURBQWlELEdBQUcsb0RBQW9ELHNEQUFzRCx5REFBeUQsR0FBRyxxREFBcUQsc0RBQXNELHlEQUF5RCxHQUFHLHFEQUFxRCx1QkFBdUIsZ0JBQWdCLGlCQUFpQix1QkFBdUIsc0JBQXNCLEdBQUcscURBQXFELHNEQUFzRCwyQ0FBMkMsR0FBRyxxREFBcUQsc0RBQXNELDJDQUEyQyxHQUFHLHFEQUFxRCx1QkFBdUIsYUFBYSxjQUFjLGVBQWUsZ0JBQWdCLHVCQUF1QixnREFBZ0QsR0FBRyxtREFBbUQsdUJBQXVCLGFBQWEsY0FBYyxlQUFlLGdCQUFnQix1QkFBdUIsaURBQWlELEdBQUcscURBQXFELHNEQUFzRCx5REFBeUQsR0FBRyxxREFBcUQsc0RBQXNELHlEQUF5RCxHQUFHLHFEQUFxRCwwREFBMEQsR0FBRyxxREFBcUQsMERBQTBELEdBQUcsc0NBQXNDLFVBQVUsZ0NBQWdDLEtBQUssR0FBRztBQUM1dEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7O0FDM0JBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLHVCQUF1QiwwQkFBMEIsaUJBQWlCLGdCQUFnQixrQkFBa0IsdUJBQXVCLHFCQUFxQixtRkFBbUYsR0FBRyxvQ0FBb0MsZ0JBQWdCLHVCQUF1QixhQUFhLGNBQWMsbUJBQW1CLGdCQUFnQixlQUFlLCtDQUErQyx1QkFBdUIsNENBQTRDLEdBQUcsbUNBQW1DLHVCQUF1QixpQkFBaUIsZUFBZSxxQkFBcUIsR0FBRyx1REFBdUQsWUFBWSxHQUFHLHFEQUFxRCxjQUFjLEdBQUcsbUtBQW1LLHVCQUF1QixlQUFlLFdBQVcsaUJBQWlCLGdCQUFnQix5QkFBeUIsK0NBQStDLG1EQUFtRCxpQkFBaUIsOEJBQThCLEdBQUcsa0ZBQWtGLGlDQUFpQyw4QkFBOEIsR0FBRyxnRkFBZ0Ysa0NBQWtDLCtCQUErQixnQkFBZ0IsaUNBQWlDLEdBQUcsd0NBQXdDLFFBQVEsOEJBQThCLEtBQUssU0FBUyw4QkFBOEIsS0FBSyxTQUFTLGdDQUFnQyxLQUFLLFNBQVMsZ0NBQWdDLEtBQUssVUFBVSxnQ0FBZ0MsS0FBSyxHQUFHO0FBQzd5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ2JBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDBCQUEwQix1QkFBdUIsR0FBRyxnQ0FBZ0MsMkJBQTJCLG1CQUFtQix1QkFBdUIsMkJBQTJCLHVCQUF1QixrRkFBa0YsMkRBQTJELEdBQUcsNkNBQTZDLDRCQUE0QixHQUFHLDZDQUE2QywyQkFBMkIsR0FBRyw2Q0FBNkMsNEJBQTRCLEdBQUcsc0NBQXNDLFFBQVEsOEJBQThCLEtBQUssVUFBVSxnQ0FBZ0MsS0FBSyxHQUFHO0FBQ3J2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNUQTtBQUNBLGtDQUFrQyxtQkFBTyxDQUFDLGlHQUE0QztBQUN0RjtBQUNBO0FBQ0EsY0FBYyxRQUFTLDZCQUE2QiwwQkFBMEIsdUJBQXVCLGdCQUFnQixpQkFBaUIsR0FBRyxrQ0FBa0MsdUJBQXVCLDJCQUEyQixlQUFlLHVCQUF1QixnRkFBZ0YsR0FBRywrQ0FBK0MsMkJBQTJCLEdBQUcsd0NBQXdDLFFBQVEsZUFBZSxnQkFBZ0IsZUFBZSxnQkFBZ0IsaUJBQWlCLEtBQUssWUFBWSxlQUFlLGdCQUFnQixpQkFBaUIsa0JBQWtCLGlCQUFpQixLQUFLLEdBQUc7QUFDOW9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ1RBO0FBQ0Esa0NBQWtDLG1CQUFPLENBQUMsaUdBQTRDO0FBQ3RGO0FBQ0E7QUFDQSxjQUFjLFFBQVMsNkJBQTZCLDBCQUEwQix1QkFBdUIsZ0JBQWdCLGlCQUFpQixHQUFHLGtDQUFrQyxrRkFBa0YsZ0NBQWdDLEdBQUcseURBQXlELGlCQUFpQixtQkFBbUIsdUJBQXVCLGVBQWUsZ0JBQWdCLHVCQUF1QixxQkFBcUIsMEJBQTBCLEdBQUcsNkNBQTZDLDZCQUE2QixHQUFHLHNFQUFzRSxjQUFjLGVBQWUsR0FBRyw2Q0FBNkMsNkJBQTZCLEdBQUcsc0VBQXNFLGNBQWMsZUFBZSxHQUFHLDZDQUE2Qyw2QkFBNkIsR0FBRyxzRUFBc0UsY0FBYyxlQUFlLEdBQUcsNkNBQTZDLDZCQUE2QixHQUFHLHNFQUFzRSxjQUFjLGVBQWUsR0FBRyw2Q0FBNkMsNEJBQTRCLEdBQUcsc0VBQXNFLGNBQWMsZUFBZSxHQUFHLDZDQUE2Qyw2QkFBNkIsR0FBRyxzRUFBc0UsY0FBYyxlQUFlLEdBQUcsNkNBQTZDLDZCQUE2QixHQUFHLHNFQUFzRSxjQUFjLGVBQWUsR0FBRyw2Q0FBNkMsNkJBQTZCLEdBQUcsc0VBQXNFLGNBQWMsZUFBZSxHQUFHLHNDQUFzQyxRQUFRLDhCQUE4QixLQUFLLFVBQVUsZ0NBQWdDLEtBQUssR0FBRztBQUN0aEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUNWQTtBQUNBLGtDQUFrQyxtQkFBTyxDQUFDLGlHQUE0QztBQUN0RjtBQUNBO0FBQ0EsY0FBYyxRQUFTLDZCQUE2QixvQkFBb0IsMEJBQTBCLHVCQUF1QixnQkFBZ0IsaUJBQWlCLEdBQUcsZ0NBQWdDLGdDQUFnQyw0REFBNEQsR0FBRyx5REFBeUQsaUJBQWlCLG1CQUFtQix1QkFBdUIsYUFBYSxlQUFlLGVBQWUsaUJBQWlCLHVCQUF1QixxQkFBcUIsR0FBRyw2Q0FBNkMsNEJBQTRCLDJCQUEyQixHQUFHLDZDQUE2Qyw2QkFBNkIseUJBQXlCLEdBQUcsNkNBQTZDLDZCQUE2QiwyQkFBMkIsR0FBRyw2Q0FBNkMsNkJBQTZCLDJCQUEyQixHQUFHLDZDQUE2Qyw4QkFBOEIsMkJBQTJCLEdBQUcsNkNBQTZDLDhCQUE4QiwyQkFBMkIsR0FBRyw2Q0FBNkMsOEJBQThCLDJCQUEyQixHQUFHLDZDQUE2Qyw4QkFBOEIsMkJBQTJCLEdBQUcsNkNBQTZDLDhCQUE4QiwyQkFBMkIsR0FBRyw4Q0FBOEMsOEJBQThCLDJCQUEyQixHQUFHLDhDQUE4Qyw4QkFBOEIsMkJBQTJCLEdBQUcsOENBQThDLDhCQUE4Qix3QkFBd0IsR0FBRyxzQ0FBc0MsUUFBUSxpQkFBaUIsS0FBSyxVQUFVLGlCQUFpQixLQUFLLEdBQUc7QUFDdjBEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7Ozs7OztBQ1ZhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCOztBQUVoQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw0Q0FBNEMscUJBQXFCO0FBQ2pFOztBQUVBO0FBQ0EsS0FBSztBQUNMLElBQUk7QUFDSjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLHFCQUFxQixpQkFBaUI7QUFDdEM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG9CQUFvQixxQkFBcUI7QUFDekM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBLDhCQUE4Qjs7QUFFOUI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOzs7QUFHRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsY0FBYztBQUNuRTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQzdGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsUUFBUTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsa0NBQWtDO0FBQ2xDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnQkFBZ0Isc0JBQXNCO0FBQ3RDOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGtCQUFrQixvQkFBb0I7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN6RkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLFNBQUksSUFBSSxTQUFJO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQTRELGNBQWM7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNnRDtBQUNaO0FBQ0k7QUFDekI7QUFDZjtBQUNBLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSw2Q0FBNkMsK0NBQStDLFVBQVU7QUFDcks7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUM3QkEsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxrTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLFNBQUksSUFBSSxTQUFJO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQTRELGNBQWM7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixTQUFJLElBQUksU0FBSTtBQUNqQyxvREFBb0QsUUFBUTtBQUM1RDtBQUNBO0FBQ0E7QUFDZ0Q7QUFDWjtBQUNJO0FBQ3pCO0FBQ2Y7QUFDQSx3RUFBd0UsU0FBUyw2REFBSSxTQUFTLFNBQVMsb0VBQW9FLEVBQUUsVUFBVSxFQUFFO0FBQ3pMLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSw4Q0FBOEMsNEJBQTRCLFVBQVUsU0FBUyxvQkFBb0I7QUFDaEw7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUNuQ0EsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxtTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLFNBQUksSUFBSSxTQUFJO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQTRELGNBQWM7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNnRDtBQUNaO0FBQ0k7QUFDekI7QUFDZjtBQUNBLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSxnREFBZ0QsNEJBQTRCLFVBQVUsU0FBUyxXQUFXLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNO0FBQ25OO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxFQUFFLFdBQVc7QUFDMUI7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUNsQ0EsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxvTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLFNBQUksSUFBSSxTQUFJO0FBQzFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNERBQTRELGNBQWM7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixTQUFJLElBQUksU0FBSTtBQUNqQyxvREFBb0QsUUFBUTtBQUM1RDtBQUNBO0FBQ0E7QUFDZ0Q7QUFDWjtBQUNJO0FBQ3pCO0FBQ2Y7QUFDQSx1RUFBdUUsUUFBUSw2REFBSSxTQUFTLFNBQVMseUJBQXlCLEVBQUUsU0FBUyxFQUFFO0FBQzNJLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSx5REFBeUQsV0FBVyw0QkFBNEIsR0FBRyxTQUFTLG9CQUFvQjtBQUMvTDtBQUNBLGlDOzs7Ozs7Ozs7OztBQ25DQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLG9MQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsU0FBSSxJQUFJLFNBQUk7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsY0FBYztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLFNBQUksSUFBSSxTQUFJO0FBQ2pDLG9EQUFvRCxRQUFRO0FBQzVEO0FBQ0E7QUFDQTtBQUNnRDtBQUNaO0FBQ0k7QUFDekI7QUFDZjtBQUNBLHVFQUF1RSxRQUFRLDZEQUFJLFNBQVMsU0FBUyx5QkFBeUIsRUFBRSxTQUFTLEVBQUU7QUFDM0ksWUFBWSw2REFBSSxrQkFBa0IsWUFBWSxpREFBVSxDQUFDLHdEQUFNLCtDQUErQyw0QkFBNEIsVUFBVSxTQUFTLG9CQUFvQjtBQUNqTDtBQUNBLGlDOzs7Ozs7Ozs7OztBQ25DQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLG9MQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsU0FBSSxJQUFJLFNBQUk7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsY0FBYztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLFNBQUksSUFBSSxTQUFJO0FBQ2pDLG9EQUFvRCxRQUFRO0FBQzVEO0FBQ0E7QUFDQTtBQUNnRDtBQUNaO0FBQ0k7QUFDekI7QUFDZjtBQUNBLHVFQUF1RSxRQUFRLDZEQUFJLFNBQVMsU0FBUyx5QkFBeUIsRUFBRSxTQUFTLEVBQUU7QUFDM0ksWUFBWSw2REFBSSxrQkFBa0IsWUFBWSxpREFBVSxDQUFDLHdEQUFNLDJDQUEyQyw0QkFBNEIsVUFBVSxTQUFTLG9CQUFvQjtBQUM3SztBQUNBLGlDOzs7Ozs7Ozs7OztBQ25DQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLGdMQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsU0FBSSxJQUFJLFNBQUk7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsY0FBYztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQytEO0FBQzNCO0FBQ0k7QUFDekI7QUFDZjtBQUNBLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSw0Q0FBNEMsNEJBQTRCLFVBQVUsU0FBUyxXQUFXLDhEQUFLLGtCQUFrQjtBQUM1TDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxFQUFFLEdBQUcsWUFBWSw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTTtBQUN4RTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixFQUFFO0FBQ3ZCLGdCQUFnQiw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTTtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixFQUFFLFlBQVksWUFBWTtBQUMvQztBQUNBLGlDOzs7Ozs7Ozs7OztBQzlDQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLGlMQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNnRDtBQUNaO0FBQ0k7QUFDekI7QUFDZjtBQUNBLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSxpREFBaUQsVUFBVSxHQUFHLFdBQVcsNkRBQUksU0FBUyxZQUFZLGlEQUFVLENBQUMsd0RBQU07QUFDbEw7QUFDQSxvQkFBb0IsMkRBQTJELEVBQUUsV0FBVztBQUM1RjtBQUNBLGlDOzs7Ozs7Ozs7OztBQ3BCQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLHFMQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUMrRDtBQUMzQjtBQUNJO0FBQ3pCO0FBQ2Y7QUFDQSxZQUFZLDhEQUFLLGtCQUFrQixZQUFZLGlEQUFVLENBQUMsd0RBQU0sZ0RBQWdELFVBQVUsR0FBRyxZQUFZLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNLHFCQUFxQixvQkFBb0IsRUFBRTtBQUM5TixZQUFZLDhEQUFLLGtCQUFrQixZQUFZLGlEQUFVLENBQUMsd0RBQU0saUJBQWlCLEdBQUcsWUFBWSw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSxlQUFlLHdEQUFNLGdDQUFnQyxxQkFBcUIsRUFBRTtBQUN0TixvQkFBb0IsNkRBQUksU0FBUyxZQUFZLGlEQUFVLENBQUMsd0RBQU0sZUFBZSx3REFBTSw4QkFBOEIscUJBQXFCLEVBQUU7QUFDeEksb0JBQW9CLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNLGVBQWUsd0RBQU0sZ0NBQWdDLHFCQUFxQixFQUFFO0FBQzFJLG9CQUFvQiw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSxlQUFlLHdEQUFNLDhCQUE4QixxQkFBcUIsRUFBRTtBQUN4SSxvQkFBb0IsNkRBQUksU0FBUyxZQUFZLGlEQUFVLENBQUMsd0RBQU0sMkJBQTJCLG9CQUFvQixFQUFFO0FBQy9HLG9CQUFvQiw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSwyQkFBMkIsb0JBQW9CLEVBQUUsWUFBWTtBQUMzSCxZQUFZLDhEQUFLLGtCQUFrQixZQUFZLGlEQUFVLENBQUMsd0RBQU0saUJBQWlCLEdBQUcsWUFBWSw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSxlQUFlLHdEQUFNLGdDQUFnQyxxQkFBcUIsRUFBRTtBQUN0TixvQkFBb0IsNkRBQUksU0FBUyxZQUFZLGlEQUFVLENBQUMsd0RBQU0sZUFBZSx3REFBTSw4QkFBOEIscUJBQXFCLEVBQUU7QUFDeEksb0JBQW9CLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNLGVBQWUsd0RBQU0sZ0NBQWdDLHFCQUFxQixFQUFFO0FBQzFJLG9CQUFvQiw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSxlQUFlLHdEQUFNLDhCQUE4QixxQkFBcUIsRUFBRTtBQUN4SSxvQkFBb0IsNkRBQUksU0FBUyxZQUFZLGlEQUFVLENBQUMsd0RBQU0sMkJBQTJCLG9CQUFvQixFQUFFO0FBQy9HLG9CQUFvQiw2REFBSSxTQUFTLFlBQVksaURBQVUsQ0FBQyx3REFBTSwyQkFBMkIsb0JBQW9CLEVBQUUsWUFBWSxhQUFhO0FBQ3hJO0FBQ0EsaUM7Ozs7Ozs7Ozs7O0FDOUJBLFVBQVUsbUJBQU8sQ0FBQywrSUFBb0U7QUFDdEYsMEJBQTBCLG1CQUFPLENBQUMsb0xBQW1FOztBQUVyRzs7QUFFQTtBQUNBLDBCQUEwQixRQUFTO0FBQ25DOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7Ozs7QUFJQSxzQzs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFBZ0IsU0FBSSxJQUFJLFNBQUk7QUFDNUI7QUFDQSxnREFBZ0QsT0FBTztBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQytEO0FBQzNCO0FBQ0k7QUFDekI7QUFDZjtBQUNBLFlBQVksOERBQUssa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSxnREFBZ0QsVUFBVSxHQUFHLFlBQVksNkRBQUksbUJBQW1CLFlBQVksaURBQVUsQ0FBQyx3REFBTSxRQUFRLEdBQUcsV0FBVyw2REFBSSxVQUFVLFlBQVksaURBQVUsQ0FBQyx3REFBTSxnQkFBZ0Isb0JBQW9CLEVBQUUsV0FBVztBQUMvUyxZQUFZLDZEQUFJLG1CQUFtQixZQUFZLGlEQUFVLENBQUMsd0RBQU0sU0FBUyxHQUFHLFdBQVcsNkRBQUksVUFBVSxZQUFZLGlEQUFVLENBQUMsd0RBQU0sZ0JBQWdCLG9CQUFvQixFQUFFLFdBQVcsYUFBYTtBQUNoTTtBQUNBLGlDOzs7Ozs7Ozs7OztBQ25CQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLG9MQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixTQUFJLElBQUksU0FBSTtBQUNqQyxvREFBb0QsUUFBUTtBQUM1RDtBQUNBO0FBQ0E7QUFDZ0Q7QUFDWjtBQUNJO0FBQ3pCO0FBQ2Y7QUFDQTtBQUNBLGdCQUFnQiw2REFBSSxTQUFTO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLEVBQUU7QUFDZixLQUFLO0FBQ0wsWUFBWSw2REFBSSxrQkFBa0IsWUFBWSxpREFBVSxDQUFDLHdEQUFNLDJDQUEyQyw0QkFBNEIsVUFBVSxHQUFHLG9CQUFvQjtBQUN2SztBQUNBLGlDOzs7Ozs7Ozs7OztBQ2hDQSxVQUFVLG1CQUFPLENBQUMsK0lBQW9FO0FBQ3RGLDBCQUEwQixtQkFBTyxDQUFDLGdMQUFtRTs7QUFFckc7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQWdCLFNBQUksSUFBSSxTQUFJO0FBQzVCO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixTQUFJLElBQUksU0FBSTtBQUNqQyxvREFBb0QsUUFBUTtBQUM1RDtBQUNBO0FBQ0E7QUFDZ0Q7QUFDWjtBQUNJO0FBQ3pCO0FBQ2Y7QUFDQSx1RUFBdUUsU0FBUyw2REFBSSxTQUFTO0FBQzdGO0FBQ0E7QUFDQSxTQUFTLEVBQUUsVUFBVSxFQUFFO0FBQ3ZCLFlBQVksNkRBQUksa0JBQWtCLFlBQVksaURBQVUsQ0FBQyx3REFBTSw2Q0FBNkMsNEJBQTRCLFVBQVUsR0FBRyxvQkFBb0I7QUFDeks7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUMzQkEsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxrTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsU0FBSSxJQUFJLFNBQUk7QUFDakMsb0RBQW9ELFFBQVE7QUFDNUQ7QUFDQTtBQUNBO0FBQ2dEO0FBQ1o7QUFDSTtBQUN6QjtBQUNmO0FBQ0E7QUFDQSxnQkFBZ0IsNkRBQUksU0FBUyxXQUFXLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNLHdCQUF3QixvQkFBb0IsRUFBRSxXQUFXO0FBQzNJLEtBQUs7QUFDTCxZQUFZLDZEQUFJLGtCQUFrQixZQUFZLGlEQUFVLENBQUMsd0RBQU0sOENBQThDLFVBQVUsR0FBRyxvQkFBb0I7QUFDOUk7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUMxQkEsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxrTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQUFnQixTQUFJLElBQUksU0FBSTtBQUM1QjtBQUNBLGdEQUFnRCxPQUFPO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsU0FBSSxJQUFJLFNBQUk7QUFDakMsb0RBQW9ELFFBQVE7QUFDNUQ7QUFDQTtBQUNBO0FBQ2dEO0FBQ1o7QUFDSTtBQUN6QjtBQUNmO0FBQ0E7QUFDQSxnQkFBZ0IsNkRBQUksU0FBUyxXQUFXLDZEQUFJLFNBQVMsWUFBWSxpREFBVSxDQUFDLHdEQUFNLHdCQUF3QixvQkFBb0IsRUFBRSxXQUFXO0FBQzNJLEtBQUs7QUFDTCxZQUFZLDZEQUFJLGtCQUFrQixZQUFZLGlEQUFVLENBQUMsd0RBQU0sK0NBQStDLFVBQVUsR0FBRyxvQkFBb0I7QUFDL0k7QUFDQSxpQzs7Ozs7Ozs7Ozs7QUMxQkEsVUFBVSxtQkFBTyxDQUFDLCtJQUFvRTtBQUN0RiwwQkFBMEIsbUJBQU8sQ0FBQyxtTEFBbUU7O0FBRXJHOztBQUVBO0FBQ0EsMEJBQTBCLFFBQVM7QUFDbkM7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7OztBQUlBLHNDOzs7Ozs7Ozs7Ozs7QUNsQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBOEI7QUFDRTtBQUNFO0FBQ0E7QUFDQTtBQUNSO0FBQ0U7QUFDUTtBQUNGO0FBQ1I7QUFDSTtBQUNBO0FBQ0U7QUFDRTtBQUNtRztBQUNySSxpQzs7Ozs7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFYTs7QUFFYixJQUFJLElBQXFDO0FBQ3pDO0FBQ0E7O0FBRUEsWUFBWSxtQkFBTyxDQUFDLG9CQUFPO0FBQzNCLGNBQWMsbUJBQU8sQ0FBQyw0REFBZTs7QUFFckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLDhGQUE4RixlQUFlO0FBQzdHO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSyxFQUFFOztBQUVQLGlEQUFpRDtBQUNqRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSwyQkFBMkI7O0FBRTNCO0FBQ0E7QUFDQTtBQUNBLEdBQUc7OztBQUdIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0NBQXNDOztBQUV0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUTs7QUFFUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7O0FBRVI7QUFDQSx1QkFBdUI7QUFDdkI7QUFDQSxTQUFTO0FBQ1Qsd0JBQXdCO0FBQ3hCO0FBQ0EsU0FBUztBQUNULHdCQUF3QjtBQUN4QjtBQUNBLFNBQVM7QUFDVCx5QkFBeUI7QUFDekI7QUFDQSxTQUFTO0FBQ1QseUJBQXlCO0FBQ3pCO0FBQ0EsU0FBUztBQUNULGtDQUFrQztBQUNsQztBQUNBLFNBQVM7QUFDVCw0QkFBNEI7QUFDNUI7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7OztBQUdMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSwwREFBMEQ7O0FBRTFEO0FBQ0E7O0FBRUE7QUFDQSx3REFBd0Q7QUFDeEQ7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVE7OztBQUdSO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBOztBQUVBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFlBQVksa0JBQWtCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCOzs7QUFHakI7QUFDQTtBQUNBLGFBQWE7QUFDYjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEdBQUc7OztBQUdIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwSEFBMEg7QUFDMUg7QUFDQTtBQUNBOztBQUVBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtRUFBbUU7O0FBRW5FO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2IsV0FBVyxFQUFFO0FBQ2IsV0FBVyxFQUFFO0FBQ2IsV0FBVyxjQUFjO0FBQ3pCLFdBQVcsRUFBRTtBQUNiLFdBQVcsRUFBRTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxFQUFFO0FBQ2I7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QjtBQUN4QjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssRUFBRTs7QUFFUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxFQUFFO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEI7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQiwyREFBMkQsU0FBUztBQUNwRSx5QkFBeUIsU0FBUztBQUNsQztBQUNBLGFBQWEsU0FBUztBQUN0Qjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7OztBQUdMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7O0FBR0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsUUFBUTtBQUNuQixZQUFZLFFBQVE7QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxhQUFhO0FBQ3hCLFdBQVcsRUFBRTtBQUNiOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLDREQUE0RDtBQUM1RDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQixXQUFXLEVBQUU7QUFDYjs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQixpQkFBaUI7QUFDdEM7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLGFBQWE7QUFDeEI7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCwyQ0FBMkM7O0FBRTNDOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLGFBQWE7QUFDeEI7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUIsaUJBQWlCO0FBQ3BDOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsNkNBQTZDO0FBQzdDOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlEQUF5RDtBQUN6RDs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixxQkFBcUI7QUFDaEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEscUNBQXFDO0FBQ3JDOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7Ozs7Ozs7Ozs7Ozs7QUNwc0NhOztBQUViLElBQUksS0FBcUMsRUFBRSxFQUUxQztBQUNELG1CQUFtQixtQkFBTyxDQUFDLHlHQUF3QztBQUNuRTs7Ozs7Ozs7Ozs7OztBQ05BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUM7Ozs7Ozs7Ozs7Ozs7QUNBekM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFxRTs7QUFFckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0EsNkNBQTZDO0FBQzdDOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsV0FBVyxJQUFJO0FBQ2YsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7OztBQUdIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBLFdBQVcsSUFBSTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE1BQU07QUFDbkI7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0Esb0JBQW9CLE1BQXFDLEdBQUcsU0FBeUI7QUFDckY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLHNCQUFzQixNQUFxQyxHQUFHLFNBQXlCO0FBQ3ZGOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxvQkFBb0IsTUFBcUMsR0FBRyxTQUF5QjtBQUNyRjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsSUFBSTtBQUNuQjs7O0FBR0E7QUFDQTtBQUNBLHNCQUFzQixNQUFxQyxHQUFHLFNBQXlCO0FBQ3ZGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxTQUFTO0FBQ3RCLGVBQWUsU0FBUztBQUN4Qjs7O0FBR0E7QUFDQTtBQUNBLHNCQUFzQixNQUFxQyxHQUFHLFNBQXlCO0FBQ3ZGOztBQUVBO0FBQ0Esc0JBQXNCLE1BQXFDLEdBQUcsU0FBeUI7QUFDdkY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx3QkFBd0IsTUFBcUMsR0FBRyxTQUF5QjtBQUN6Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLHNCQUFzQixNQUFxQyxHQUFHLFNBQXlCO0FBQ3ZGOztBQUVBO0FBQ0Esc0JBQXNCLE1BQXFDLEdBQUcsU0FBeUI7QUFDdkY7O0FBRUE7QUFDQSxzQkFBc0IsTUFBcUMsR0FBRyxTQUF5QjtBQUN2Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTs7QUFFQSxtQkFBbUIsc0JBQXNCO0FBQ3pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLFNBQVM7QUFDdEIsZUFBZTtBQUNmOzs7QUFHQTtBQUNBO0FBQ0Esc0JBQXNCLE1BQXFDLEdBQUcsU0FBMEI7QUFDeEY7O0FBRUEsaUNBQWlDO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLGVBQWUsV0FBVztBQUMxQjtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsT0FBTztBQUN4QjtBQUNBLG1CQUFtQixhQUFhO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsTUFBcUMsR0FBRyxTQUEwQjtBQUM1Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHLGFBQWE7O0FBRWhCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLHNCQUFzQixNQUFxQyxHQUFHLFNBQTBCO0FBQ3hGOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsc0JBQXNCLE1BQXFDLEdBQUcsU0FBMEI7QUFDeEY7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsd0JBQXdCO0FBQ3pDOztBQUVBLFFBQVEsSUFBcUM7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsb0RBQW9EO0FBQ3BEOztBQUVBOztBQUVBLE1BQU0sSUFBcUM7QUFDM0M7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLFFBQVEsSUFBcUM7QUFDN0M7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxvQkFBb0IsOEJBQThCO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx3QkFBd0IsTUFBcUMsR0FBRyxTQUEwQjtBQUMxRjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsZ0JBQWdCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBLFdBQVcsU0FBUztBQUNwQjtBQUNBO0FBQ0EsYUFBYSxnQkFBZ0I7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esb0JBQW9CLE1BQXFDLEdBQUcsU0FBMEI7QUFDdEY7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLFlBQVk7QUFDdkIsYUFBYSxTQUFTO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0VBQXNFLGFBQWE7QUFDbkY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsWUFBWTtBQUN2QixhQUFhLFNBQVM7QUFDdEI7O0FBRUE7QUFDQSw0RUFBNEUsYUFBYTtBQUN6RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHdCQUF3QixNQUFxQyxHQUFHLFNBQTBCO0FBQzFGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxhQUFhLHdGQUFhLENBQUMsd0ZBQWEsR0FBRyxZQUFZO0FBQ3ZEO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxJQUFJLEtBQXFDO0FBQ3pDO0FBQ0E7O0FBRWdJOzs7Ozs7Ozs7Ozs7QUMzcUJoSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxLQUFLO0FBQ0wsY0FBYztBQUNkO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5REFBeUQ7QUFDekQ7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0EsV0FBVztBQUNYOztBQUVBO0FBQ0E7QUFDQSx3Q0FBd0MsV0FBVztBQUNuRDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLFNBQVM7QUFDVDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esb0NBQW9DLGNBQWM7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUNBQWlDLGtCQUFrQjtBQUNuRDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLGlCQUFpQjtBQUN6QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLFlBQVk7QUFDWjtBQUNBOztBQUVBO0FBQ0EsWUFBWTtBQUNaOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOENBQThDLFFBQVE7QUFDdEQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBOztBQUVBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7O0FBRUEsV0FBVztBQUNYO0FBQ0E7QUFDQTs7QUFFQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0EsOENBQThDLFFBQVE7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSw4Q0FBOEMsUUFBUTtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSw4Q0FBOEMsUUFBUTtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUUsS0FBMEIsb0JBQW9CLFNBQUU7QUFDbEQ7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDanZCYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdURBQXVEOztBQUV2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTs7QUFFQSxpQkFBaUIsd0JBQXdCO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsaUJBQWlCLGlCQUFpQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsZ0JBQWdCLEtBQXdDLEdBQUcsc0JBQWlCLEdBQUcsU0FBSTs7QUFFbkY7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0EscUVBQXFFLHFCQUFxQixhQUFhOztBQUV2Rzs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0EseURBQXlEO0FBQ3pELEdBQUc7O0FBRUg7OztBQUdBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwwQkFBMEI7QUFDMUI7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxtQkFBbUIsNEJBQTRCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLG9CQUFvQiw2QkFBNkI7QUFDakQ7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7OztBQzVRQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFNBQVNBLE1BQVQsQ0FBZ0JDLElBQWhCLEVBQW9DO0FBQUEsTUFBZEMsT0FBYyx1RUFBSixFQUFJO0FBQ2xDLFNBQU87QUFBRUQsUUFBSSxFQUFKQSxJQUFGO0FBQVFDLFdBQU8sRUFBUEE7QUFBUixHQUFQO0FBQ0Q7O0FBRU0sSUFBTUMsb0JBQW9CLEdBQUcsc0JBQTdCO0FBQ0EsSUFBTUMsa0JBQWtCLEdBQUcsb0JBQTNCO0FBQ0EsSUFBTUMsVUFBVSxHQUFHLFlBQW5CO0FBQ0EsSUFBTUMsMEJBQTBCLEdBQUcsNEJBQW5DO0FBQ0EsSUFBTUMsbUJBQW1CLEdBQUcscUJBQTVCO0FBQ0EsSUFBTUMsdUJBQXVCLEdBQUcseUJBQWhDO0FBQ0EsSUFBTUMsZ0NBQWdDLEdBQzNDLGtDQURLO0FBRUEsSUFBTUMsV0FBVyxHQUFHLGFBQXBCO0FBQ0EsSUFBTUMsV0FBVyxHQUFHLGFBQXBCO0FBQ0EsSUFBTUMsV0FBVyxHQUFHLGFBQXBCO0FBQ0EsSUFBTUMsaUJBQWlCLEdBQUcsbUJBQTFCO0FBQ0EsSUFBTUMsMEJBQTBCLEdBQUcsNEJBQW5DO0FBQ0EsSUFBTUMsdUJBQXVCLEdBQUcseUJBQWhDO0FBQ0EsSUFBTUMsYUFBYSxHQUFHLGVBQXRCO0FBQ0EsSUFBTUMsc0JBQXNCLEdBQUcsd0JBQS9CO0FBRUEsSUFBTUMsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixDQUFDaEIsT0FBRDtBQUFBLFNBQy9CRixNQUFNLENBQUNRLHVCQUFELEVBQTBCTixPQUExQixDQUR5QjtBQUFBLENBQTFCO0FBRUEsSUFBTWlCLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBQ2pCLE9BQUQ7QUFBQSxTQUFhRixNQUFNLENBQUNhLGlCQUFELEVBQW9CWCxPQUFwQixDQUFuQjtBQUFBLENBQXhCO0FBQ0EsSUFBTWtCLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNsQixPQUFEO0FBQUEsU0FBYUYsTUFBTSxDQUFDWSxXQUFELEVBQWNWLE9BQWQsQ0FBbkI7QUFBQSxDQUFuQjtBQUNBLElBQU1tQixTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDbkIsT0FBRDtBQUFBLFNBQWFGLE1BQU0sQ0FBQ0ssVUFBRCxFQUFhSCxPQUFiLENBQW5CO0FBQUEsQ0FBbEI7QUFDQSxJQUFNb0IsVUFBVSxHQUFHLFNBQWJBLFVBQWE7QUFBQSxTQUFNdEIsTUFBTSxDQUFDVSxXQUFELENBQVo7QUFBQSxDQUFuQjtBQUNBLElBQU1hLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0I7QUFBQSxTQUFNdkIsTUFBTSxDQUFDRyxvQkFBRCxDQUFaO0FBQUEsQ0FBeEI7QUFDQSxJQUFNcUIscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDdEIsT0FBRDtBQUFBLFNBQ25DRixNQUFNLENBQUNlLHVCQUFELEVBQTBCYixPQUExQixDQUQ2QjtBQUFBLENBQTlCO0FBRUEsSUFBTXVCLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUN2QixPQUFEO0FBQUEsU0FBYUYsTUFBTSxDQUFDZ0IsYUFBRCxFQUFnQmQsT0FBaEIsQ0FBbkI7QUFBQSxDQUFyQjtBQUNBLElBQU13QiwwQkFBMEIsR0FBRyxTQUE3QkEsMEJBQTZCLENBQUN4QixPQUFEO0FBQUEsU0FDeENGLE1BQU0sQ0FBQ00sMEJBQUQsRUFBNkJKLE9BQTdCLENBRGtDO0FBQUEsQ0FBbkMsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CUDtBQUNBO0FBRU8sSUFBTXlCLGNBQWMsR0FBRyxTQUFqQkEsY0FBaUIsR0FBTTtBQUNsQyxNQUFNQyxPQUFPLEdBQUdDLHdEQUFVLENBQUNDLHVEQUFELENBQTFCO0FBQ0EsTUFBUUMsTUFBUixHQUF5REgsT0FBekQsQ0FBUUcsTUFBUjtBQUFBLE1BQWdCQyxZQUFoQixHQUF5REosT0FBekQsQ0FBZ0JJLFlBQWhCO0FBQUEsTUFBOEJDLFlBQTlCLEdBQXlETCxPQUF6RCxDQUE4QkssWUFBOUI7QUFBQSxNQUE0Q0MsUUFBNUMsR0FBeUROLE9BQXpELENBQTRDTSxRQUE1QztBQUVBLHNCQUNFO0FBQUssU0FBSyxFQUFFSCxNQUFNLENBQUNJO0FBQW5CLGtCQUNFO0FBQUssU0FBSyxFQUFFSixNQUFNLENBQUNLO0FBQW5CLDZCQURGLGVBRUU7QUFBSyxTQUFLLEVBQUVMLE1BQU0sQ0FBQ00sTUFBbkI7QUFBMkIsV0FBTyxFQUFFSDtBQUFwQyx3QkFDa0IsR0FEbEIsZUFFRTtBQUFNLFNBQUssRUFBRTtBQUFFSSxvQkFBYyxFQUFFO0FBQWxCO0FBQWIsZUFGRixNQUZGLGVBTUU7QUFBSyxTQUFLLEVBQUVQLE1BQU0sQ0FBQ1E7QUFBbkIsbUxBTkYsZUFXRTtBQUFLLFNBQUssa0NBQU9SLE1BQU0sQ0FBQ1MsR0FBZDtBQUFtQkMsZUFBUyxFQUFFO0FBQTlCO0FBQVYsa0JBQ0U7QUFDRSxTQUFLLEVBQUVWLE1BQU0sQ0FBQ1csTUFEaEI7QUFFRSxRQUFJLEVBQUMsUUFGUDtBQUdFLFNBQUssRUFBQywwQkFIUjtBQUlFLFdBQU8sRUFBRVQ7QUFKWCxJQURGLGVBT0UscUZBQ0U7QUFDRSxTQUFLLEVBQUVGLE1BQU0sQ0FBQ1csTUFEaEI7QUFFRSxRQUFJLEVBQUMsUUFGUDtBQUdFLFNBQUssRUFBQyxJQUhSO0FBSUUsV0FBTyxFQUFFVjtBQUpYLElBREYsQ0FQRixDQVhGLENBREY7QUE4QkQsQ0FsQ00sQzs7Ozs7Ozs7Ozs7O0FDSFA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFTyxJQUFNVyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUFNO0FBQy9CLE1BQU1mLE9BQU8sR0FBR0Msd0RBQVUsQ0FBQ0MsdURBQUQsQ0FBMUI7QUFDQSxNQUNFQyxNQURGLEdBUUlILE9BUkosQ0FDRUcsTUFERjtBQUFBLE1BRUVhLFFBRkYsR0FRSWhCLE9BUkosQ0FFRWdCLFFBRkY7QUFBQSxNQUdFQyxVQUhGLEdBUUlqQixPQVJKLENBR0VpQixVQUhGO0FBQUEsTUFJRUMsVUFKRixHQVFJbEIsT0FSSixDQUlFa0IsVUFKRjtBQUFBLE1BS0VDLFFBTEYsR0FRSW5CLE9BUkosQ0FLRW1CLFFBTEY7QUFBQSxNQU1FQyxTQU5GLEdBUUlwQixPQVJKLENBTUVvQixTQU5GO0FBQUEsTUFPRUMsY0FQRixHQVFJckIsT0FSSixDQU9FcUIsY0FQRjtBQVVBLHNCQUNFO0FBQU0sWUFBUSxFQUFFSjtBQUFoQixrQkFDRTtBQUFLLFNBQUssRUFBRWQsTUFBTSxDQUFDSTtBQUFuQixrQkFDRTtBQUFLLFNBQUssRUFBRUosTUFBTSxDQUFDSztBQUFuQiwyQkFERixlQUVFO0FBQUssU0FBSyxFQUFFTCxNQUFNLENBQUNRO0FBQW5CLG1EQUZGLGVBS0U7QUFBSyxTQUFLLEVBQUVSLE1BQU0sQ0FBQ1M7QUFBbkIsa0JBQ0UsMkRBQUMsMkRBQUQ7QUFDRSxTQUFLLEVBQUUsZ0JBRFQ7QUFFRSxTQUFLLEVBQUVULE1BRlQ7QUFHRSxNQUFFLEVBQUUsT0FITjtBQUlFLFFBQUksRUFBRW1CLHlEQUFTLENBQUNDLEtBSmxCO0FBS0UsWUFBUSxFQUFFUDtBQUxaLElBREYsQ0FMRixlQWNFO0FBQUssU0FBSyxFQUFFYixNQUFNLENBQUNTO0FBQW5CLGtCQUNFO0FBQVEsU0FBSyxFQUFFVCxNQUFNLENBQUNXLE1BQXRCO0FBQThCLFFBQUksRUFBQztBQUFuQyxnQkFERixDQWRGLGVBbUJFO0FBQUssU0FBSyxFQUFFWCxNQUFNLENBQUNTO0FBQW5CLEtBQXlCTyxRQUF6QixDQW5CRixDQURGLENBREY7QUF5QkQsQ0FyQ00sQzs7Ozs7Ozs7Ozs7O0FDSlA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFTyxJQUFNSyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLEdBQU07QUFDckMsTUFBTXhCLE9BQU8sR0FBR0Msd0RBQVUsQ0FBQ0MsdURBQUQsQ0FBMUI7QUFDQSxNQUFRRSxZQUFSLEdBQXlCSixPQUF6QixDQUFRSSxZQUFSO0FBRUEsc0JBQ0U7QUFBSyxTQUFLLEVBQUU7QUFBRXFCLGFBQU8sRUFBRTtBQUFYO0FBQVosa0JBQ0U7QUFBSyxTQUFLLEVBQUU7QUFBRUEsYUFBTyxFQUFFLE1BQVg7QUFBbUJDLG1CQUFhLEVBQUUsUUFBbEM7QUFBNENDLFlBQU0sRUFBRTtBQUFwRDtBQUFaLGtCQUNFLHlHQURGLGVBRUUscUhBRkYsZUFHRSxxRkFDRTtBQUFPLFFBQUksRUFBQyxRQUFaO0FBQXFCLFNBQUssRUFBQyxJQUEzQjtBQUFnQyxXQUFPLEVBQUV2QjtBQUF6QyxJQURGLENBSEYsQ0FERixDQURGO0FBV0QsQ0FmTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKUDtBQUNBO0FBQ0E7QUFFTyxJQUFNd0IsU0FBUyxHQUFHLFNBQVpBLFNBQVksR0FBTTtBQUM3QixNQUFNNUIsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyx1REFBRCxDQUExQjtBQUNBLE1BQVFDLE1BQVIsR0FBK0RILE9BQS9ELENBQVFHLE1BQVI7QUFBQSxNQUFnQmEsUUFBaEIsR0FBK0RoQixPQUEvRCxDQUFnQmdCLFFBQWhCO0FBQUEsTUFBMEJDLFVBQTFCLEdBQStEakIsT0FBL0QsQ0FBMEJpQixVQUExQjtBQUFBLE1BQXNDQyxVQUF0QyxHQUErRGxCLE9BQS9ELENBQXNDa0IsVUFBdEM7QUFBQSxNQUFrRFosUUFBbEQsR0FBK0ROLE9BQS9ELENBQWtETSxRQUFsRDtBQUVBLHNCQUNFO0FBQU0sWUFBUSxFQUFFVztBQUFoQixrQkFDRTtBQUFLLFNBQUssRUFBRWQsTUFBTSxDQUFDSTtBQUFuQixrQkFDRTtBQUFLLFNBQUssRUFBRUosTUFBTSxDQUFDSztBQUFuQiwyQkFERixlQUVFO0FBQUssU0FBSyxFQUFFTCxNQUFNLENBQUNNLE1BQW5CO0FBQTJCLFdBQU8sRUFBRUg7QUFBcEMsd0JBQ2tCLEdBRGxCLGVBRUU7QUFBTSxTQUFLLEVBQUU7QUFBRUksb0JBQWMsRUFBRTtBQUFsQjtBQUFiLGVBRkYsTUFGRixlQU1FO0FBQUssU0FBSyxFQUFFUCxNQUFNLENBQUNTO0FBQW5CLGtCQUNFO0FBQUssU0FBSyxFQUFFVCxNQUFNLENBQUMwQjtBQUFuQixjQURGLGVBRUUsd0VBQU1YLFVBQVUsQ0FBQ1ksZUFBakIsQ0FGRixDQU5GLGVBVUU7QUFBSyxTQUFLLEVBQUUzQixNQUFNLENBQUNTO0FBQW5CLGtCQUNFO0FBQUssU0FBSyxFQUFFVCxNQUFNLENBQUMwQjtBQUFuQixhQURGLGVBRUUscUZBQ0UsMkRBQUMsMkRBQUQ7QUFDRSxNQUFFLEVBQUUsTUFETjtBQUVFLFFBQUksRUFBRVAseURBQVMsQ0FBQ1MsSUFGbEI7QUFHRSxZQUFRLEVBQUVmO0FBSFosSUFERixDQUZGLENBVkYsZUFvQkU7QUFBSyxTQUFLLGtDQUFPYixNQUFNLENBQUNTLEdBQWQ7QUFBbUJDLGVBQVMsRUFBRTtBQUE5QjtBQUFWLGtCQUNFO0FBQVEsU0FBSyxFQUFFVixNQUFNLENBQUNXLE1BQXRCO0FBQThCLFFBQUksRUFBQztBQUFuQyxnQkFERixDQXBCRixDQURGLENBREY7QUE4QkQsQ0FsQ00sQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSlA7QUFDQTtBQUNBO0FBRU8sSUFBTWtCLFNBQVMsR0FBRyxTQUFaQSxTQUFZLEdBQU07QUFDN0IsTUFBTWhDLE9BQU8sR0FBR0Msd0RBQVUsQ0FBQ0MsdURBQUQsQ0FBMUI7QUFDQSxNQUNFQyxNQURGLEdBV0lILE9BWEosQ0FDRUcsTUFERjtBQUFBLE1BRUVhLFFBRkYsR0FXSWhCLE9BWEosQ0FFRWdCLFFBRkY7QUFBQSxNQUdFQyxVQUhGLEdBV0lqQixPQVhKLENBR0VpQixVQUhGO0FBQUEsTUFJRUMsVUFKRixHQVdJbEIsT0FYSixDQUlFa0IsVUFKRjtBQUFBLE1BS0VDLFFBTEYsR0FXSW5CLE9BWEosQ0FLRW1CLFFBTEY7QUFBQSxNQU1FYyxZQU5GLEdBV0lqQyxPQVhKLENBTUVpQyxZQU5GO0FBQUEsTUFPRUMsYUFQRixHQVdJbEMsT0FYSixDQU9Fa0MsYUFQRjtBQUFBLE1BUUVkLFNBUkYsR0FXSXBCLE9BWEosQ0FRRW9CLFNBUkY7QUFBQSxNQVNFZCxRQVRGLEdBV0lOLE9BWEosQ0FTRU0sUUFURjtBQUFBLE1BVUU2QixPQVZGLEdBV0luQyxPQVhKLENBVUVtQyxPQVZGO0FBYUEsc0JBQ0U7QUFBTSxZQUFRLEVBQUVsQjtBQUFoQixrQkFDRTtBQUFLLFNBQUssRUFBRWQsTUFBTSxDQUFDSTtBQUFuQixrQkFDRTtBQUFLLFNBQUssRUFBRUosTUFBTSxDQUFDSztBQUFuQixzQkFDZ0JVLFVBQVUsQ0FBQ1ksZUFEM0IsTUFERixlQUlFO0FBQUssU0FBSyxFQUFFM0IsTUFBTSxDQUFDTSxNQUFuQjtBQUEyQixXQUFPLEVBQUVIO0FBQXBDLHdCQUNrQixHQURsQixlQUVFO0FBQU0sU0FBSyxFQUFFO0FBQUVJLG9CQUFjLEVBQUU7QUFBbEI7QUFBYixlQUZGLE1BSkYsZUFRRTtBQUFLLFNBQUssRUFBRVAsTUFBTSxDQUFDUTtBQUFuQiwrQ0FSRixlQVdFO0FBQUssU0FBSyxFQUFFUixNQUFNLENBQUNTO0FBQW5CLGtCQUNFO0FBQUssU0FBSyxFQUFFVCxNQUFNLENBQUNpQztBQUFuQixrQkFDRTtBQUNFLFNBQUssRUFDSEgsWUFBWSxHQUFHQyxhQUFmLElBQWdDLENBQUNkLFNBQWpDLEdBQ0lqQixNQUFNLENBQUNrQyxTQURYLEdBRUlsQyxNQUFNLENBQUMwQjtBQUpmLHlCQURGLGVBVUUscUZBQ0UsMkRBQUMsMkRBQUQ7QUFDRSxNQUFFLEVBQUUsVUFETjtBQUVFLFFBQUksRUFBRVAseURBQVMsQ0FBQ2dCLFFBRmxCO0FBR0UsWUFBUSxFQUFFdEI7QUFIWixJQURGLENBVkYsZUFpQkUsd0ZBakJGLGVBa0JFLHFGQUNFLDJEQUFDLDJEQUFEO0FBQ0UsTUFBRSxFQUFFLGNBRE47QUFFRSxRQUFJLEVBQUVNLHlEQUFTLENBQUNnQixRQUZsQjtBQUdFLFlBQVEsRUFBRXRCO0FBSFosSUFERixDQWxCRixlQXlCRSw4RkF6QkYsZUEwQkUscUZBQ0UsMkRBQUMsMkRBQUQ7QUFDRSxNQUFFLEVBQUUsc0JBRE47QUFFRSxRQUFJLEVBQUVNLHlEQUFTLENBQUNnQixRQUZsQjtBQUdFLFlBQVEsRUFBRXRCO0FBSFosSUFERixDQTFCRixDQURGLENBWEYsZUErQ0U7QUFBSyxTQUFLLGtDQUFPYixNQUFNLENBQUNTLEdBQWQ7QUFBbUJDLGVBQVMsRUFBRTtBQUE5QjtBQUFWLGtCQUNFO0FBQUssU0FBSyxFQUFFVixNQUFNLENBQUMwQjtBQUFuQixvQkFERixlQUVFLHFGQUNFLDJEQUFDLDJEQUFEO0FBQ0UsTUFBRSxFQUFFLGlCQUROO0FBRUUsU0FBSyxFQUFFWCxVQUFVLENBQUMsaUJBQUQsQ0FGbkI7QUFHRSxRQUFJLEVBQUVJLHlEQUFTLENBQUNpQixRQUhsQjtBQUlFLFlBQVEsRUFBRXZCO0FBSlosSUFERixDQUZGLENBL0NGLGVBMERFO0FBQUssU0FBSyxFQUFFYixNQUFNLENBQUNTO0FBQW5CLGtCQUNFO0FBQVEsU0FBSyxFQUFFVCxNQUFNLENBQUNXLE1BQXRCO0FBQThCLFFBQUksRUFBQztBQUFuQyxlQURGLENBMURGLEVBK0RHbUIsWUFBWSxHQUFHQyxhQUFmLGlCQUNDLHFGQUNFO0FBQUcsV0FBTyxFQUFFQztBQUFaLHFCQURGLENBaEVKLGVBb0VFO0FBQUssU0FBSyxFQUFFaEMsTUFBTSxDQUFDUztBQUFuQixLQUF5Qk8sUUFBekIsQ0FwRUYsQ0FERixDQURGO0FBMEVELENBekZNLEM7Ozs7Ozs7Ozs7OztBQ0pQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUVPLElBQU1xQixPQUFPLEdBQUcsU0FBVkEsT0FBVSxHQUFNO0FBQzNCLE1BQU14QyxPQUFPLEdBQUdDLHdEQUFVLENBQUNDLHVEQUFELENBQTFCO0FBQ0EsTUFBTXVDLFFBQVEsR0FBR0MsK0RBQVcsRUFBNUI7QUFDQSxNQUFRdkMsTUFBUixHQUEyQ0gsT0FBM0MsQ0FBUUcsTUFBUjtBQUFBLE1BQWdCQyxZQUFoQixHQUEyQ0osT0FBM0MsQ0FBZ0JJLFlBQWhCO0FBQUEsTUFBOEJlLFFBQTlCLEdBQTJDbkIsT0FBM0MsQ0FBOEJtQixRQUE5QjtBQUVBLHNCQUNFO0FBQUssU0FBSyxFQUFFaEIsTUFBTSxDQUFDSTtBQUFuQixrQkFDRTtBQUFLLFNBQUssRUFBRUosTUFBTSxDQUFDSztBQUFuQixnQkFERixlQUVFO0FBQUssU0FBSyxFQUFFTCxNQUFNLENBQUNRO0FBQW5CLHVDQUZGLGVBSUU7QUFBSyxTQUFLLEVBQUVSLE1BQU0sQ0FBQ1M7QUFBbkIsa0JBQ0U7QUFBSyxTQUFLLEVBQUU7QUFBRStCLGlCQUFXLEVBQUU7QUFBZjtBQUFaLGtCQUNFO0FBQ0UsV0FBTyxFQUFFO0FBQUEsYUFBTUYsUUFBUSxDQUFDL0Msa0VBQVUsRUFBWCxDQUFkO0FBQUEsS0FEWDtBQUVFLFNBQUssRUFBRVMsTUFBTSxDQUFDVyxNQUZoQjtBQUdFLFFBQUksRUFBQztBQUhQLGdCQURGLENBREYsZUFVRSxxRkFDRTtBQUFRLFdBQU8sRUFBRVYsWUFBakI7QUFBK0IsU0FBSyxFQUFFRCxNQUFNLENBQUNXLE1BQTdDO0FBQXFELFFBQUksRUFBQztBQUExRCxjQURGLENBVkYsQ0FKRixlQW9CRTtBQUFLLFNBQUssRUFBRVgsTUFBTSxDQUFDUztBQUFuQixLQUF5Qk8sUUFBekIsQ0FwQkYsQ0FERjtBQXdCRCxDQTdCTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNMUDtBQUNBO0FBQ0E7QUFFTyxJQUFNeUIsV0FBVyxHQUFHLFNBQWRBLFdBQWMsR0FBTTtBQUMvQixNQUFNNUMsT0FBTyxHQUFHQyx3REFBVSxDQUFDQyx1REFBRCxDQUExQjtBQUNBLE1BQ0VDLE1BREYsR0FXSUgsT0FYSixDQUNFRyxNQURGO0FBQUEsTUFFRWEsUUFGRixHQVdJaEIsT0FYSixDQUVFZ0IsUUFGRjtBQUFBLE1BR0VDLFVBSEYsR0FXSWpCLE9BWEosQ0FHRWlCLFVBSEY7QUFBQSxNQUlFQyxVQUpGLEdBV0lsQixPQVhKLENBSUVrQixVQUpGO0FBQUEsTUFLRUMsUUFMRixHQVdJbkIsT0FYSixDQUtFbUIsUUFMRjtBQUFBLE1BTUVjLFlBTkYsR0FXSWpDLE9BWEosQ0FNRWlDLFlBTkY7QUFBQSxNQU9FQyxhQVBGLEdBV0lsQyxPQVhKLENBT0VrQyxhQVBGO0FBQUEsTUFRRWQsU0FSRixHQVdJcEIsT0FYSixDQVFFb0IsU0FSRjtBQUFBLE1BU0VkLFFBVEYsR0FXSU4sT0FYSixDQVNFTSxRQVRGO0FBQUEsTUFVRTZCLE9BVkYsR0FXSW5DLE9BWEosQ0FVRW1DLE9BVkY7QUFhQSxzQkFDRTtBQUFNLFlBQVEsRUFBRWxCO0FBQWhCLGtCQUNFO0FBQUssU0FBSyxFQUFFZCxNQUFNLENBQUNJO0FBQW5CLGtCQUNFO0FBQUssU0FBSyxFQUFFSixNQUFNLENBQUNLO0FBQW5CLHNCQUNnQlUsVUFBVSxDQUFDWSxlQUQzQixNQURGLGVBSUU7QUFBSyxTQUFLLEVBQUUzQixNQUFNLENBQUNNLE1BQW5CO0FBQTJCLFdBQU8sRUFBRUg7QUFBcEMsd0JBQ2tCLEdBRGxCLGVBRUU7QUFBTSxTQUFLLEVBQUU7QUFBRUksb0JBQWMsRUFBRTtBQUFsQjtBQUFiLGVBRkYsTUFKRixlQVFFO0FBQUssU0FBSyxFQUFFUCxNQUFNLENBQUNRO0FBQW5CLDZDQUVFLHNFQUZGLHFDQVJGLGVBWUU7QUFBSyxTQUFLLEVBQUVSLE1BQU0sQ0FBQ1M7QUFBbkIsa0JBQ0U7QUFDRSxTQUFLLEVBQ0hxQixZQUFZLEdBQUdDLGFBQWYsSUFBZ0MsQ0FBQ2QsU0FBakMsR0FDSWpCLE1BQU0sQ0FBQ2tDLFNBRFgsR0FFSWxDLE1BQU0sQ0FBQzBCO0FBSmYsaUJBREYsZUFVRSxxRkFDRSwyREFBQywyREFBRDtBQUNFLE1BQUUsRUFBRSxVQUROO0FBRUUsUUFBSSxFQUFFUCx5REFBUyxDQUFDZ0IsUUFGbEI7QUFHRSxZQUFRLEVBQUV0QjtBQUhaLElBREYsQ0FWRixDQVpGLGVBK0JFO0FBQUssU0FBSyxrQ0FBT2IsTUFBTSxDQUFDUyxHQUFkO0FBQW1CQyxlQUFTLEVBQUU7QUFBOUI7QUFBVixrQkFDRTtBQUFLLFNBQUssRUFBRVYsTUFBTSxDQUFDMEI7QUFBbkIsb0JBREYsZUFFRSxxRkFDRSwyREFBQywyREFBRDtBQUNFLE1BQUUsRUFBRSxpQkFETjtBQUVFLFNBQUssRUFBRVgsVUFBVSxDQUFDLGlCQUFELENBRm5CO0FBR0UsUUFBSSxFQUFFSSx5REFBUyxDQUFDaUIsUUFIbEI7QUFJRSxZQUFRLEVBQUV2QjtBQUpaLElBREYsQ0FGRixDQS9CRixlQTBDRTtBQUFLLFNBQUssRUFBRWIsTUFBTSxDQUFDUztBQUFuQixrQkFDRTtBQUFRLFNBQUssRUFBRVQsTUFBTSxDQUFDVyxNQUF0QjtBQUE4QixRQUFJLEVBQUM7QUFBbkMsZUFERixDQTFDRixFQWdER21CLFlBQVksR0FBR0MsYUFBZixpQkFDQyxxRkFDRTtBQUFHLFdBQU8sRUFBRUM7QUFBWixxQkFERixDQWpESixlQXFERTtBQUFLLFNBQUssRUFBRWhDLE1BQU0sQ0FBQ1M7QUFBbkIsS0FBeUJPLFFBQXpCLENBckRGLENBREYsQ0FERjtBQTJERCxDQTFFTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKUDtBQUNBO0FBQ0E7QUFPQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRU8sSUFBTWpCLGFBQWEsZ0JBQUcyQywyREFBYSxFQUFuQztBQUVBLElBQU1DLEtBQUssR0FBRyxTQUFSQSxLQUFRLE9BUWY7QUFBQSxNQVBKM0IsUUFPSSxRQVBKQSxRQU9JO0FBQUEsTUFOSmYsWUFNSSxRQU5KQSxZQU1JO0FBQUEsTUFMSjJDLE9BS0ksUUFMSkEsT0FLSTtBQUFBLE1BSkpDLFFBSUksUUFKSkEsUUFJSTtBQUFBLHdCQUhKQyxLQUdJO0FBQUEsTUFISkEsS0FHSSwyQkFISSxFQUdKO0FBQUEsTUFGSkMsZUFFSSxRQUZKQSxlQUVJO0FBQUEsTUFESjdCLGNBQ0ksUUFESkEsY0FDSTtBQUNKLE1BQU1sQixNQUFNLEdBQUc7QUFDYmdELFdBQU8sRUFBRTtBQUNQQyxjQUFRLEVBQUUsVUFESDtBQUVQQyxxQkFBZSxFQUFFLE9BRlY7QUFHUDFCLFlBQU0sRUFBRSxNQUhEO0FBSVAyQixXQUFLLEVBQUUsTUFKQTtBQUtQQyxZQUFNLEVBQUUsQ0FMRDtBQU1QQyxhQUFPLEVBQUUsR0FORjtBQU9QL0IsYUFBTyxFQUFFO0FBUEYsS0FESTtBQVViaEIsVUFBTSxFQUFFO0FBQ05nRCxZQUFNLEVBQUUsTUFERjtBQUVORCxhQUFPLEVBQUU7QUFGSCxLQVZLO0FBY2JqRCxhQUFTO0FBQ1BtRCxlQUFTLEVBQUUsUUFESjtBQUVQQyxhQUFPLEVBQUUsTUFGRjtBQUdQRixZQUFNLEVBQUUsTUFIRDtBQUlQTCxjQUFRLEVBQUUsVUFKSDtBQUtQUSxZQUFNLEVBQUUsQ0FMRDtBQU1QQyxTQUFHLEVBQUUsQ0FORTtBQU9QQyxVQUFJLEVBQUUsQ0FQQztBQVFQQyxXQUFLLEVBQUU7QUFSQSxPQVNKZCxLQUFLLENBQUMxQyxTQVRGLENBZEk7QUF5QmJzQixTQUFLO0FBQ0htQyxnQkFBVSxFQUFFLEtBRFQ7QUFFSHJCLGlCQUFXLEVBQUU7QUFGVixPQUdBTSxLQUFLLENBQUNwQixLQUhOLENBekJRO0FBOEJiUSxhQUFTO0FBQ1A0QixXQUFLLEVBQUUsS0FEQTtBQUVQRCxnQkFBVSxFQUFFO0FBRkwsT0FHSmYsS0FBSyxDQUFDWixTQUhGLENBOUJJO0FBbUNiekIsT0FBRyxFQUFFO0FBQ0hhLGFBQU8sRUFBRSxNQUROO0FBRUhDLG1CQUFhLEVBQUUsS0FGWjtBQUdId0MsZ0JBQVUsRUFBRSxRQUhUO0FBSUhDLG9CQUFjLEVBQUU7QUFKYixLQW5DUTtBQXlDYjNELFNBQUs7QUFDSDRELGNBQVEsRUFBRSxRQURQO0FBRUhYLFlBQU0sRUFBRTtBQUZMLE9BR0FSLEtBQUssQ0FBQ3pDLEtBSE4sQ0F6Q1E7QUE4Q2JHLFlBQVE7QUFDTnlELGNBQVEsRUFBRSxNQURKO0FBRU5YLFlBQU0sRUFBRTtBQUZGLE9BR0hSLEtBQUssQ0FBQ3RDLFFBSEgsQ0E5Q0s7QUFtRGJHLFVBQU07QUFDSmEsWUFBTSxFQUFFLE1BREo7QUFFSjBDLFlBQU0sRUFBRSxnQkFGSjtBQUdKQyxrQkFBWSxFQUFFO0FBSFYsT0FJRHJCLEtBQUssQ0FBQ25DLE1BSkw7QUFuRE8sR0FBZjtBQTBEQSxNQUFNMkIsUUFBUSxHQUFHQywrREFBVyxFQUE1QjtBQUNBLE1BQU10QixTQUFTLEdBQUdtRCwrREFBVyxDQUFDLFVBQUNDLEtBQUQ7QUFBQSxXQUFXQSxLQUFLLENBQUNwRCxTQUFqQjtBQUFBLEdBQUQsQ0FBN0I7QUFDQSxNQUFNcUQsaUJBQWlCLEdBQUdGLCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ0MsaUJBQWpCO0FBQUEsR0FBRCxDQUFyQzs7QUFDQSxrQkFBb0NDLHNEQUFRLENBQUM7QUFDM0NDLG1CQUFlLEVBQUU7QUFEMEIsR0FBRCxDQUE1QztBQUFBO0FBQUEsTUFBT3pELFVBQVA7QUFBQSxNQUFtQjBELGFBQW5CLGlCQTlESSxDQWtFSjs7O0FBQ0EsTUFBTTFDLGFBQWEsR0FBRyxDQUF0Qjs7QUFDQSxtQkFBd0N3QyxzREFBUSxDQUFDeEMsYUFBRCxDQUFoRDtBQUFBO0FBQUEsTUFBT0QsWUFBUDtBQUFBLE1BQXFCNEMsZUFBckI7O0FBQ0EsTUFBSTVDLFlBQVksS0FBSyxDQUFyQixFQUF3QjtBQUN0QjJDLGlCQUFhLENBQUMsRUFBRCxDQUFiO0FBQ0FDLG1CQUFlLENBQUMzQyxhQUFELENBQWY7QUFDRCxHQXhFRyxDQTBFSjs7O0FBQ0EsTUFBSTRDLGFBQUo7O0FBQ0EsTUFBTUMsWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsT0FBRCxFQUFhO0FBQ2hDLFFBQUksT0FBT2hDLFFBQVAsS0FBb0IsVUFBeEIsRUFDRUEsUUFBUSxDQUFDO0FBQ1BpQyxVQUFJLEVBQUUsVUFEQztBQUVQNUIscUJBQWUsRUFBRSxTQUZWO0FBR1AyQixhQUFPLEVBQVBBO0FBSE8sS0FBRCxDQUFSO0FBS0gsR0FQRDs7QUFRQUUseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSUosYUFBYSxJQUFJLE9BQU85QixRQUFQLEtBQW9CLFVBQXpDLEVBQXFEO0FBQ25EK0Isa0JBQVksQ0FBQ0QsYUFBRCxDQUFaO0FBQ0ExRSxrQkFBWTtBQUNiO0FBQ0YsR0FMUSxDQUFULENBcEZJLENBMkZKOztBQUNBLG1CQUEwQ3NFLHNEQUFRLENBQUMsS0FBRCxDQUFsRDtBQUFBO0FBQUEsTUFBT1MsYUFBUDtBQUFBLE1BQXNCQyxnQkFBdEI7O0FBQ0EsbUJBQXdDVixzREFBUSxDQUFDLEtBQUQsQ0FBaEQ7QUFBQTtBQUFBLE1BQU9XLFlBQVA7QUFBQSxNQUFxQkMsZUFBckI7O0FBQ0EsbUJBQWtEWixzREFBUSxDQUFDLEtBQUQsQ0FBMUQ7QUFBQTtBQUFBLE1BQU9hLGlCQUFQO0FBQUEsTUFBMEJDLG9CQUExQjs7QUFDQU4seURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSVQsaUJBQUosRUFBdUI7QUFDckJXLHNCQUFnQixDQUFDLENBQUNYLGlCQUFpQixDQUFDZ0IsU0FBcEIsQ0FBaEI7QUFDQUgscUJBQWUsQ0FBQ2IsaUJBQWlCLENBQUNpQixLQUFuQixDQUFmO0FBQ0FGLDBCQUFvQixDQUFDZixpQkFBaUIsQ0FBQ2tCLEtBQW5CLENBQXBCO0FBQ0Q7QUFDRixHQU5RLEVBTU4sQ0FBQ2xCLGlCQUFELENBTk0sQ0FBVDtBQU9BUyx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJbkMsT0FBSixFQUFhO0FBQ1g2QixtQkFBYSxDQUFDO0FBQUVnQixhQUFLLEVBQUU3QyxPQUFPLENBQUM2QyxLQUFqQjtBQUF3QjlELHVCQUFlLEVBQUVpQixPQUFPLENBQUM2QztBQUFqRCxPQUFELENBQWI7QUFDQVIsc0JBQWdCLENBQUMsQ0FBQ3JDLE9BQU8sQ0FBQzBDLFNBQVYsQ0FBaEI7QUFDQUgscUJBQWUsQ0FBQ3ZDLE9BQU8sQ0FBQzhDLENBQVQsQ0FBZjtBQUNBTCwwQkFBb0IsQ0FBQ3pDLE9BQU8sQ0FBQytDLENBQVQsQ0FBcEI7QUFDRDtBQUNGLEdBUFEsRUFPTixDQUFDL0MsT0FBRCxDQVBNLENBQVQsQ0F0R0ksQ0ErR0o7O0FBQ0Esb0JBQW9DMkIsc0RBQVEsQ0FBQyxDQUFELENBQTVDO0FBQUE7QUFBQSxNQUFPcUIsVUFBUDtBQUFBLE1BQW1CQyxhQUFuQjs7QUFDQSxNQUFNL0UsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ2dGLENBQUQsRUFBTztBQUFBOztBQUN4QkEsS0FBQyxTQUFELElBQUFBLENBQUMsV0FBRCxZQUFBQSxDQUFDLENBQUVDLGNBQUg7O0FBQ0EsUUFBTUMsYUFBYSxtQ0FDZGpGLFVBRGM7QUFFakJZLHFCQUFlLEVBQUVaLFVBQVUsQ0FBQyxPQUFELENBRlY7QUFHakJrRix3QkFBa0IsRUFBRWxGLFVBQVUsQ0FBQyxVQUFELENBSGI7QUFJakJtRixvQkFBYyxFQUFFbkYsVUFBVSxDQUFDLE1BQUQ7QUFKVCxNQUFuQjs7QUFNQTBELGlCQUFhLENBQUN1QixhQUFELENBQWI7O0FBQ0EsUUFDRSwwQkFBQUEsYUFBYSxDQUFDckUsZUFBZCxnRkFBK0J3RSxNQUEvQixJQUF3QyxDQUF4QyxJQUNBLDJCQUFBSCxhQUFhLENBQUNDLGtCQUFkLGtGQUFrQ0UsTUFBbEMsSUFBMkMsQ0FGN0MsRUFHRTtBQUNBekIscUJBQWUsQ0FBQzVDLFlBQVksR0FBRyxDQUFoQixDQUFmO0FBQ0EyQyxtQkFBYSxpQ0FBTXVCLGFBQU47QUFBcUJDLDBCQUFrQixFQUFFO0FBQXpDLFNBQWI7O0FBQ0EsVUFBSWIsaUJBQUosRUFBdUI7QUFDckIsWUFDRXJFLFVBQVUsQ0FBQ3FGLFlBQVgsSUFDQXJGLFVBQVUsQ0FBQ3NGLG9CQUFYLEtBQW9DdEYsVUFBVSxDQUFDcUYsWUFGakQsRUFHRTtBQUNBOUQsa0JBQVEsQ0FDTjNDLGtGQUEwQixDQUFDO0FBQ3pCMkcsb0JBQVEsRUFBRU4sYUFBYSxDQUFDckUsZUFEQztBQUV6QjRFLG9CQUFRLEVBQUVQLGFBQWEsQ0FBQ0Msa0JBRkM7QUFHekJPLG1CQUFPLEVBQUV6RixVQUFVLENBQUNxRixZQUhLO0FBSXpCNUIsMkJBQWUsRUFBRXdCLGFBQWEsQ0FBQ3hCO0FBSk4sV0FBRCxDQURwQixDQUFSO0FBUUQsU0FaRCxNQVlPO0FBQ0xpQyxpQkFBTyxDQUFDQyxLQUFSLENBQWMsMkJBQWQ7QUFDRDtBQUNGLE9BaEJELE1BZ0JPO0FBQ0xwRSxnQkFBUSxDQUNOaEQsaUVBQVMsQ0FBQztBQUNSZ0gsa0JBQVEsRUFBRU4sYUFBYSxDQUFDckUsZUFEaEI7QUFFUjRFLGtCQUFRLEVBQUVQLGFBQWEsQ0FBQ0Msa0JBRmhCO0FBR1J6Qix5QkFBZSxFQUFFd0IsYUFBYSxDQUFDeEI7QUFIdkIsU0FBRCxDQURILENBQVI7QUFPRDtBQUNGLEtBL0JELE1BK0JPLElBQ0wsQ0FBQ1EsYUFBRCxJQUNBZ0IsYUFBYSxDQUFDckUsZUFEZCxJQUVBcUUsYUFBYSxDQUFDRSxjQUhULEVBSUw7QUFDQTVELGNBQVEsQ0FDTmpELGtFQUFVLENBQUM7QUFDVGlILGdCQUFRLEVBQUVOLGFBQWEsQ0FBQ3JFLGVBRGY7QUFFVGdGLFlBQUksRUFBRVgsYUFBYSxDQUFDRTtBQUZYLE9BQUQsQ0FESixDQUFSO0FBTUQsS0FYTSxNQVdBLElBQUksMkJBQUFGLGFBQWEsQ0FBQ3JFLGVBQWQsa0ZBQStCd0UsTUFBL0IsSUFBd0MsQ0FBNUMsRUFBK0M7QUFDcEQ3RCxjQUFRLENBQUNuRCx5RUFBaUIsQ0FBQztBQUFFbUgsZ0JBQVEsRUFBRU4sYUFBYSxDQUFDckU7QUFBMUIsT0FBRCxDQUFsQixDQUFSO0FBQ0Q7O0FBQ0RrRSxpQkFBYSxDQUFDRCxVQUFVLEdBQUcsQ0FBZCxDQUFiO0FBQ0QsR0F2REQ7O0FBeURBLE1BQU0vRSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDMUMsT0FBRCxFQUFhO0FBQzVCc0csaUJBQWEsaUNBQU0xRCxVQUFOLHdGQUFtQjVDLE9BQU8sQ0FBQ3lJLEVBQTNCLEVBQWdDekksT0FBTyxDQUFDMEksS0FBeEMsR0FBYjtBQUNELEdBRkQ7O0FBSUEsTUFBTTNHLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUMvQixPQUFELEVBQWE7QUFDaEM4QixnQkFBWTtBQUNacUMsWUFBUSxDQUFDN0MsNkVBQXFCLENBQUM7QUFBRTZHLGNBQVEsRUFBRXZGLFVBQVUsQ0FBQ1k7QUFBdkIsS0FBRCxDQUF0QixDQUFSO0FBQ0FpRCxnQkFBWSxDQUNWLGdFQURVLENBQVo7QUFHRCxHQU5EOztBQVFBLE1BQU01QyxPQUFPLEdBQUcsU0FBVkEsT0FBVSxDQUFDN0QsT0FBRCxFQUFhO0FBQzNCOEIsZ0JBQVk7QUFDWnFDLFlBQVEsQ0FBQzdDLDZFQUFxQixDQUFDO0FBQUU2RyxjQUFRLEVBQUV2RixVQUFVLENBQUNZO0FBQXZCLEtBQUQsQ0FBdEIsQ0FBUjtBQUNBaUQsZ0JBQVksQ0FBQyxnREFBRCxDQUFaO0FBQ0QsR0FKRDs7QUFNQSxNQUFJa0MsV0FBVyxHQUFHL0QsZUFBZSxnQkFBRywyREFBQyxnREFBRCxPQUFILGdCQUFpQiwyREFBQyx3REFBRCxPQUFsRDs7QUFDQSxNQUNFLENBQUNpQyxhQUFELElBQ0FqRSxVQUFVLENBQUNZLGVBRFgsSUFFQVosVUFBVSxDQUFDbUYsY0FIYixFQUlFO0FBQ0FZLGVBQVcsZ0JBQUcsMkRBQUMscUVBQUQsT0FBZDtBQUNBbkMsaUJBQWEsR0FDWCw2REFERjtBQUVELEdBUkQsTUFRTyxJQUFJNUQsVUFBVSxDQUFDWSxlQUFYLElBQThCLENBQUNWLFNBQW5DLEVBQThDO0FBQ25ELFFBQUkrRCxhQUFKLEVBQW1CO0FBQ2pCLFVBQUksQ0FBQ0UsWUFBTCxFQUFtQjtBQUNqQjRCLG1CQUFXLGdCQUFHLDJEQUFDLCtEQUFELE9BQWQ7QUFDRCxPQUZELE1BRU8sSUFBSTFCLGlCQUFKLEVBQXVCO0FBQzVCMEIsbUJBQVcsZ0JBQUcsMkRBQUMscURBQUQsT0FBZDtBQUNELE9BRk0sTUFFQTtBQUNMQSxtQkFBVyxnQkFBRywyREFBQyx3REFBRCxPQUFkO0FBQ0Q7QUFDRixLQVJELE1BUU87QUFDTEEsaUJBQVcsZ0JBQUcsMkRBQUMsb0RBQUQsT0FBZDtBQUNEO0FBQ0Y7O0FBRUQsc0JBQ0UsMkRBQUMsYUFBRCxDQUFlLFFBQWY7QUFDRSxTQUFLLEVBQUU7QUFDTDlHLFlBQU0sRUFBTkEsTUFESztBQUVMZ0MsYUFBTyxFQUFQQSxPQUZLO0FBR0xuQixjQUFRLEVBQVJBLFFBSEs7QUFJTEMsZ0JBQVUsRUFBVkEsVUFKSztBQUtMWixrQkFBWSxFQUFaQSxZQUxLO0FBTUxDLGNBQVEsRUFBRTtBQUFBLGVBQU1zRSxhQUFhLENBQUMsRUFBRCxDQUFuQjtBQUFBLE9BTkw7QUFPTDFELGdCQUFVLEVBQVZBLFVBUEs7QUFRTEMsY0FBUSxFQUFSQSxRQVJLO0FBU0xjLGtCQUFZLEVBQVpBLFlBVEs7QUFVTEMsbUJBQWEsRUFBYkEsYUFWSztBQVdMOUIsa0JBQVksRUFBWkEsWUFYSztBQVlMZ0IsZUFBUyxFQUFUQSxTQVpLO0FBYUw0QixjQUFRLEVBQVJBLFFBYks7QUFjTDNCLG9CQUFjLEVBQWRBO0FBZEs7QUFEVCxrQkFrQkUsMkRBQUMsK0RBQUQsUUFBVTRGLFdBQVYsQ0FsQkYsQ0FERjtBQXNCRCxDQWpQTTtBQW1QUW5FLG9FQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTW9FLE1BQU0sR0FBRyxTQUFUQSxNQUFTLE9BZVQ7QUFBQSxNQWRKQyxXQWNJLFFBZEpBLFdBY0k7QUFBQSxNQWJKQyxVQWFJLFFBYkpBLFVBYUk7QUFBQSxNQVpKQyxPQVlJLFFBWkpBLE9BWUk7QUFBQSxNQVhKQyxRQVdJLFFBWEpBLFFBV0k7QUFBQSxNQVZKdEUsUUFVSSxRQVZKQSxRQVVJO0FBQUEsTUFUSnVFLGFBU0ksUUFUSkEsYUFTSTtBQUFBLE1BUkpwRyxRQVFJLFFBUkpBLFFBUUk7QUFBQSxNQVBKOEIsS0FPSSxRQVBKQSxLQU9JO0FBQUEsTUFOSkYsT0FNSSxRQU5KQSxPQU1JO0FBQUEsK0JBTEp5RSxZQUtJO0FBQUEsTUFMSkEsWUFLSSxrQ0FMVyxLQUtYO0FBQUEsZ0NBSkpDLGFBSUk7QUFBQSxNQUpKQSxhQUlJLG1DQUpZLEdBSVo7QUFBQSxpQ0FISnBHLGNBR0k7QUFBQSxNQUhKQSxjQUdJLG9DQUhhLElBR2I7QUFBQSw0QkFGSnFHLFNBRUk7QUFBQSxNQUZKQSxTQUVJLCtCQUZRLFNBRVI7QUFBQSw2QkFESkMsVUFDSTtBQUFBLE1BREpBLFVBQ0ksZ0NBRFMsVUFDVDs7QUFDSixNQUFNQyxVQUFVLEdBQUcsc0JBQU07QUFDdkIsb0JBQW9DbEQsc0RBQVEsQ0FBQztBQUMzQ3BCLFdBQUssRUFBRXVFLFNBRG9DO0FBRTNDbEcsWUFBTSxFQUFFa0csU0FGbUM7QUFHM0NKLG1CQUFhLEVBQUU7QUFINEIsS0FBRCxDQUE1QztBQUFBO0FBQUEsUUFBT0csVUFBUDtBQUFBLFFBQW1CRSxhQUFuQjs7QUFLQTVDLDJEQUFTLENBQUMsWUFBTTtBQUNkLGVBQVM2QyxZQUFULEdBQXdCO0FBQ3RCRCxxQkFBYSxDQUFDO0FBQ1p4RSxlQUFLLEVBQUUwRSxNQUFNLENBQUNDLFVBREY7QUFFWnRHLGdCQUFNLEVBQUVxRyxNQUFNLENBQUNFLFdBRkg7QUFHWkMsbUJBQVMsRUFBRUgsTUFBTSxDQUFDQyxVQUFQLEdBQW9CUjtBQUhuQixTQUFELENBQWI7QUFLRDs7QUFDRE8sWUFBTSxDQUFDSSxnQkFBUCxDQUF3QixRQUF4QixFQUFrQ0wsWUFBbEM7QUFDQUEsa0JBQVk7QUFDWixhQUFPO0FBQUEsZUFBTUMsTUFBTSxDQUFDSyxtQkFBUCxDQUEyQixRQUEzQixFQUFxQ04sWUFBckMsQ0FBTjtBQUFBLE9BQVA7QUFDRCxLQVhRLEVBV04sRUFYTSxDQUFUO0FBWUEsV0FBT0gsVUFBUDtBQUNELEdBbkJEOztBQW9CQSxNQUFNVSxJQUFJLEdBQUdWLFVBQVUsRUFBdkI7QUFDQSxNQUFNVyxvQkFBb0IsR0FBR2hFLCtEQUFXLENBQ3RDLFVBQUNDLEtBQUQ7QUFBQSxXQUFXQSxLQUFLLENBQUMrRCxvQkFBakI7QUFBQSxHQURzQyxDQUF4QztBQUdBLE1BQU1yRixlQUFlLEdBQUdxQiwrREFBVyxDQUFDLFVBQUNDLEtBQUQ7QUFBQSxXQUFXQSxLQUFLLENBQUN0QixlQUFqQjtBQUFBLEdBQUQsQ0FBbkM7QUFDQSxNQUFNc0YsY0FBYyxHQUFHakUsK0RBQVcsQ0FBQyxVQUFDQyxLQUFEO0FBQUEsV0FBV0EsS0FBSyxDQUFDZ0UsY0FBakI7QUFBQSxHQUFELENBQWxDOztBQUNBLG1CQUF3QzlELHNEQUFRLENBQUMsS0FBRCxDQUFoRDtBQUFBO0FBQUEsTUFBTytELFlBQVA7QUFBQSxNQUFxQkMsZUFBckI7O0FBQ0EsTUFBTWpHLFFBQVEsR0FBR0MsK0RBQVcsRUFBNUI7QUFDQSxNQUFNa0QsS0FBSyxHQUFHckIsK0RBQVcsQ0FBQyxVQUFDQyxLQUFEO0FBQUE7O0FBQUEsNEJBQVdBLEtBQUssQ0FBQ21FLE1BQWpCLGtEQUFXLGNBQWMvQyxLQUF6QjtBQUFBLEdBQUQsQ0FBekIsQ0E3QkksQ0ErQko7O0FBQ0FWLHlEQUFTLENBQUMsWUFBTTtBQUNkekMsWUFBUSxDQUNObEQsdUVBQWUsQ0FBQztBQUNkNEgsaUJBQVcsRUFBWEEsV0FEYztBQUVkQyxnQkFBVSxFQUFWQSxVQUZjO0FBR2RDLGFBQU8sRUFBUEEsT0FIYztBQUlkQyxjQUFRLEVBQVJBLFFBSmM7QUFLZEMsbUJBQWEsRUFBYkE7QUFMYyxLQUFELENBRFQsQ0FBUjtBQVNELEdBVlEsRUFVTixDQUFDSixXQUFELEVBQWNDLFVBQWQsRUFBMEJDLE9BQTFCLEVBQW1DQyxRQUFuQyxDQVZNLENBQVQ7QUFZQXBDLHlEQUFTLENBQUMsWUFBTTtBQUNkd0QsbUJBQWUsQ0FBQzNGLE9BQU8sR0FBRyxJQUFILEdBQVUsS0FBbEIsQ0FBZjtBQUNELEdBRlEsRUFFTixDQUFDRyxlQUFELENBRk0sQ0FBVCxDQTVDSSxDQWdESjs7QUFDQSxNQUFJc0YsY0FBSixFQUFvQixvQkFBTywyREFBQyx1REFBRDtBQUFNLFNBQUssRUFBQyxPQUFaO0FBQW9CLFFBQUksRUFBRTtBQUExQixJQUFQOztBQUNwQixNQUFNckksTUFBTSxtQ0FDUDhDLEtBRE87QUFFVjFDLGFBQVM7QUFDUG9ELGFBQU8sRUFBRSxDQURGO0FBRVBGLFlBQU0sRUFBRTtBQUZELE9BR0pSLEtBSEksYUFHSkEsS0FISSx1QkFHSkEsS0FBSyxDQUFFMUMsU0FISCxDQUZDO0FBT1ZDLFNBQUs7QUFDSGlELFlBQU0sRUFBRTtBQURMLE9BRUFSLEtBRkEsYUFFQUEsS0FGQSx1QkFFQUEsS0FBSyxDQUFFekMsS0FGUCxDQVBLO0FBV1ZHLFlBQVE7QUFDTjhDLFlBQU0sRUFBRTtBQURGLE9BRUhSLEtBRkcsYUFFSEEsS0FGRyx1QkFFSEEsS0FBSyxDQUFFdEMsUUFGSixDQVhFO0FBZVZpSSxVQUFNO0FBQ0puSCxhQUFPLEVBQUUsTUFETDtBQUVKaUMsZUFBUyxFQUFFLFFBRlA7QUFHSm1GLGdCQUFVLEVBQUUsV0FIUjtBQUlKdkYsV0FBSyxFQUFFLE1BSkg7QUFLSjNCLFlBQU0sRUFBRSxNQUxKO0FBTUp5QixjQUFRLEVBQUUsVUFOTjtBQU9KUSxZQUFNLEVBQUU7QUFQSixPQVFEWCxLQVJDLGFBUURBLEtBUkMsdUJBUURBLEtBQUssQ0FBRTJGLE1BUk4sQ0FmSTtBQXlCVkUsU0FBSyxFQUFFUixJQUFJLENBQUNILFNBQUwsR0FDSDtBQUNFeEcsWUFBTSxFQUFFLE9BRFY7QUFFRTJCLFdBQUssRUFBRSxPQUZUO0FBR0V5RixjQUFRLEVBQUUsT0FIWjtBQUlFQyxlQUFTLEVBQUUsT0FKYjtBQUtFQyxjQUFRLEVBQUUsT0FMWjtBQU1FQyxlQUFTLEVBQUU7QUFOYixLQURHLEdBU0g7QUFDRXZGLGFBQU8sRUFBRSxDQURYO0FBRUVsQyxhQUFPLEVBQUUsTUFGWDtBQUdFRSxZQUFNLEVBQUUsT0FIVjtBQUlFMkIsV0FBSyxFQUFFLE9BSlQ7QUFLRXpDLGVBQVMsRUFBRSxPQUxiO0FBTUVrSSxjQUFRLEVBQUUsT0FOWjtBQU9FQyxlQUFTLEVBQUU7QUFQYjtBQWxDTSxJQUFaOztBQTZDQSxNQUFJLENBQUNULG9CQUFMLEVBQTJCO0FBQ3pCLHdCQUFPO0FBQUssZUFBUyxFQUFFO0FBQWhCLE1BQVA7QUFDRCxHQUZELE1BRU87QUFDTCx3QkFDRSwyREFBQyw0Q0FBRCxDQUFPLFFBQVAsUUFDSSxDQUFDckYsZUFBRCxpQkFDQTtBQUFLLGFBQU8sRUFBRTtBQUFBLGVBQU13RixlQUFlLENBQUMsSUFBRCxDQUFyQjtBQUFBO0FBQWQsT0FBNENoQixTQUE1QyxDQURELGlCQUdDO0FBQUssYUFBTyxFQUFFO0FBQUEsZUFBTWdCLGVBQWUsQ0FBQyxJQUFELENBQXJCO0FBQUE7QUFBZCxPQUNHZixVQURILE9BQ2dCSCxZQUFZLGNBQU81QixLQUFQLFNBQWtCLEVBRDlDLENBSkosRUFRRzZDLFlBQVksaUJBQ1gsMkRBQUMscURBQUQ7QUFDRSxvQkFBYyxFQUFFO0FBQUEsZUFBTUMsZUFBZSxDQUFDLEtBQUQsQ0FBckI7QUFBQSxPQURsQjtBQUVFLFdBQUssRUFBRXZJLE1BQU0sQ0FBQzJJO0FBRmhCLG9CQUlFLDJEQUFDLDhDQUFEO0FBQ0UscUJBQWUsRUFBRTVGLGVBRG5CO0FBR0k3QixvQkFBYyxFQUFkQSxjQUhKO0FBSUlqQixrQkFBWSxFQUFFO0FBQUEsZUFBTXNJLGVBQWUsQ0FBQyxLQUFELENBQXJCO0FBQUEsT0FKbEI7QUFLSXZILGNBQVEsRUFBUkEsUUFMSjtBQU1JOEIsV0FBSyxFQUFMQSxLQU5KO0FBT0lGLGFBQU8sRUFBUEEsT0FQSjtBQVFJQyxjQUFRLEVBQVJBO0FBUkosTUFKRixFQWVHM0IsY0FBYyxpQkFDYjtBQUFLLFdBQUssRUFBRWxCLE1BQU0sQ0FBQ3lJO0FBQW5CLG9CQUNFO0FBQUssV0FBSyxFQUFFO0FBQUVuRixjQUFNLEVBQUU7QUFBVjtBQUFaLG9CQUNFO0FBQ0UsV0FBSyxFQUFFdEQsTUFBTSxDQUFDVyxNQURoQjtBQUVFLFVBQUksRUFBQyxRQUZQO0FBR0UsV0FBSyxFQUFFLFFBSFQ7QUFJRSxhQUFPLEVBQUU7QUFBQSxlQUFNNEgsZUFBZSxDQUFDLEtBQUQsQ0FBckI7QUFBQTtBQUpYLE1BREYsQ0FERixDQWhCSixDQVRKLENBREY7QUF5Q0Q7QUFDRixDQTNKRDs7QUE2SmV4QixxRUFBZixFOzs7Ozs7Ozs7Ozs7QUNwS0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDTyxJQUFNaUMsT0FBTyxHQUFHLFNBQVZBLE9BQVUsT0FBa0I7QUFBQSxNQUFmQyxRQUFlLFFBQWZBLFFBQWU7QUFDdkMsTUFBTWhJLFNBQVMsR0FBR21ELCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ3BELFNBQWpCO0FBQUEsR0FBRCxDQUE3QjtBQUNBLHNCQUNFLDJEQUFDLDRDQUFELENBQU8sUUFBUCxRQUNHQSxTQUFTLGlCQUNSO0FBQ0UsU0FBSyxFQUFFO0FBQ0xnQyxjQUFRLEVBQUUsVUFETDtBQUVMQyxxQkFBZSxFQUFFLE9BRlo7QUFHTDFCLFlBQU0sRUFBRSxNQUhIO0FBSUwyQixXQUFLLEVBQUUsTUFKRjtBQUtMQyxZQUFNLEVBQUUsQ0FMSDtBQU1MQyxhQUFPLEVBQUUsR0FOSjtBQU9ML0IsYUFBTyxFQUFFO0FBUEo7QUFEVCxrQkFXRTtBQUFLLFNBQUssRUFBRTtBQUFFNkIsV0FBSyxFQUFFLE1BQVQ7QUFBaUJHLFlBQU0sRUFBRSxNQUF6QjtBQUFpQ0MsZUFBUyxFQUFFO0FBQTVDO0FBQVosa0JBQ0UsMkRBQUMsdURBQUQ7QUFBTSxTQUFLLEVBQUMsU0FBWjtBQUFzQixRQUFJLEVBQUU7QUFBNUIsSUFERixDQVhGLENBRkosRUFrQkcwRixRQWxCSCxDQURGO0FBc0JELENBeEJNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0hQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsT0FTWDtBQUFBLE1BUkpDLEtBUUksUUFSSkEsS0FRSTtBQUFBLE1BUEpyRyxLQU9JLFFBUEpBLEtBT0k7QUFBQSxNQU5Kc0csVUFNSSxRQU5KQSxVQU1JO0FBQUEsTUFMSnBDLFdBS0ksUUFMSkEsV0FLSTtBQUFBLE1BSkpDLFVBSUksUUFKSkEsVUFJSTtBQUFBLE1BSEpDLE9BR0ksUUFISkEsT0FHSTtBQUFBLE1BRkpDLFFBRUksUUFGSkEsUUFFSTtBQUFBLE1BREpDLGFBQ0ksUUFESkEsYUFDSTtBQUNKLE1BQU05RSxRQUFRLEdBQUdDLCtEQUFXLEVBQTVCO0FBQ0EsTUFBTXRCLFNBQVMsR0FBR21ELCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ3BELFNBQWpCO0FBQUEsR0FBRCxDQUE3QjtBQUNBLE1BQU1vRCxLQUFLLEdBQUdELCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQVg7QUFBQSxHQUFELENBQXpCO0FBQ0FvQyxTQUFPLENBQUM0QyxHQUFSLENBQVloRixLQUFaO0FBQ0EsTUFBTStELG9CQUFvQixHQUFHaEUsK0RBQVcsQ0FDdEMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQytELG9CQUFqQjtBQUFBLEdBRHNDLENBQXhDO0FBR0EsTUFBTWtCLGFBQWEsR0FBR2xGLCtEQUFXLENBQUMsVUFBQ0MsS0FBRDtBQUFBLFdBQVdBLEtBQUssQ0FBQ2tGLGdCQUFqQjtBQUFBLEdBQUQsQ0FBakMsQ0FSSSxDQVVKOztBQUNBeEUseURBQVMsQ0FBQyxZQUFNO0FBQ2R6QyxZQUFRLENBQ05sRCx1RUFBZSxDQUFDO0FBQ2Q0SCxpQkFBVyxFQUFYQSxXQURjO0FBRWRDLGdCQUFVLEVBQVZBLFVBRmM7QUFHZEMsYUFBTyxFQUFQQSxPQUhjO0FBSWRDLGNBQVEsRUFBUkEsUUFKYztBQUtkQyxtQkFBYSxFQUFiQTtBQUxjLEtBQUQsQ0FEVCxDQUFSO0FBU0QsR0FWUSxFQVVOLENBQUNKLFdBQUQsRUFBY0MsVUFBZCxFQUEwQkMsT0FBMUIsRUFBbUNDLFFBQW5DLENBVk0sQ0FBVDtBQVlBcEMseURBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSXFELG9CQUFKLEVBQTBCOUYsUUFBUSxDQUFDNUMsb0VBQVksQ0FBQ3lKLEtBQUQsQ0FBYixDQUFSO0FBQzNCLEdBRlEsRUFFTixDQUFDZixvQkFBRCxFQUF1QmUsS0FBdkIsQ0FGTSxDQUFUO0FBSUFwRSx5REFBUyxDQUFDLFlBQU07QUFDZCxRQUFJLENBQUM5RCxTQUFELElBQWNxSSxhQUFkLGFBQWNBLGFBQWQsZUFBY0EsYUFBYSxDQUFFNUQsQ0FBN0IsSUFBa0MsT0FBTzBELFVBQVAsS0FBc0IsVUFBNUQsRUFDRUEsVUFBVSxDQUFDRSxhQUFELENBQVY7QUFDSCxHQUhRLEVBR04sQ0FBQ3JJLFNBQUQsRUFBWXFJLGFBQVosQ0FITSxDQUFUO0FBS0EsTUFBSSxDQUFDbEIsb0JBQUwsRUFBMkIsT0FBTyxJQUFQOztBQUMzQixNQUFNcEksTUFBTTtBQUNWd0osY0FBVSxFQUFFO0FBQ1ZoSSxZQUFNLEVBQUUsT0FERTtBQUVWMkIsV0FBSyxFQUFFLE9BRkc7QUFHVnpDLGVBQVMsRUFBRTtBQUhEO0FBREYsS0FNUG9DLEtBTk8sQ0FBWjs7QUFRQSxNQUFJN0IsU0FBSixFQUFlO0FBQ2Isd0JBQ0U7QUFBSyxXQUFLLEVBQUU7QUFBRXNDLGlCQUFTLEVBQUU7QUFBYjtBQUFaLHFDQUVFLHNFQUZGLG9CQUVTLDJEQUFDLHVEQUFEO0FBQU0sV0FBSyxFQUFDLFNBQVo7QUFBc0IsVUFBSSxFQUFFO0FBQTVCLE1BRlQsQ0FERjtBQU1ELEdBUEQsTUFPTztBQUNMLHdCQUNFLHdFQUNJLENBQUErRixhQUFhLFNBQWIsSUFBQUEsYUFBYSxXQUFiLFlBQUFBLGFBQWEsQ0FBRTVELENBQWYsa0JBQ0Esd0hBREQsaUJBR0MscUZBQ0UsZ0hBREYsQ0FKSixDQURGO0FBV0Q7QUFDRixDQXRFRDs7QUF1RWV3RCx1RUFBZixFOzs7Ozs7Ozs7Ozs7QUM3RUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLElBQU1BLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQUNPLEtBQUQsRUFBVztBQUMxQixzQkFDRSwyREFBQyxvREFBRDtBQUFVLFNBQUssRUFBRUMsaURBQUtBO0FBQXRCLGtCQUNFLDJEQUFDLDREQUFELEVBQXVCRCxLQUF2QixDQURGLENBREY7QUFLRCxDQU5EOztBQVFBLElBQU0xQyxNQUFNLEdBQUcsU0FBVEEsTUFBUyxDQUFDMEMsS0FBRCxFQUFXO0FBQ3hCLHNCQUNFLDJEQUFDLG9EQUFEO0FBQVUsU0FBSyxFQUFFQyxpREFBS0E7QUFBdEIsa0JBQ0UsMkRBQUMsMERBQUQsRUFBcUJELEtBQXJCLENBREYsQ0FERjtBQUtELENBTkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBLElBQU1FLElBQUksR0FBRyxTQUFQQSxJQUFPLEdBVVI7QUFBQSxNQVRIQyxLQVNHLHVFQVRLO0FBQ054Qix3QkFBb0IsRUFBRSxLQURoQjtBQUVOQyxrQkFBYyxFQUFFLElBRlY7QUFHTnRGLG1CQUFlLEVBQUUsS0FIWDtBQUlOb0csU0FBSyxFQUFFLEVBSkQ7QUFLTlUsc0JBQWtCLEVBQUUsRUFMZDtBQU1OQyxxQkFBaUIsRUFBRTtBQU5iLEdBU0w7QUFBQSxNQURIN0wsTUFDRzs7QUFDSCxVQUFRQSxNQUFNLENBQUNDLElBQWY7QUFDRSxTQUFLNkwsZ0VBQUw7QUFDQSxTQUFLQSxtREFBTDtBQUNBLFNBQUtBLHNEQUFMO0FBQ0EsU0FBS0EsbUVBQUw7QUFDRSw2Q0FDS0gsS0FETDtBQUVFM0ksaUJBQVMsRUFBRTtBQUZiOztBQUlGLFNBQUs4SSw2REFBTDtBQUNFLDZDQUFZSCxLQUFaO0FBQW1CdkIsc0JBQWMsRUFBRSxLQUFuQztBQUEwQ0QsNEJBQW9CLEVBQUU7QUFBaEU7O0FBQ0YsU0FBSzJCLDJEQUFMO0FBQ0UsNkNBQVlILEtBQVo7QUFBbUJ2QixzQkFBYyxFQUFFLEtBQW5DO0FBQTBDRCw0QkFBb0IsRUFBRTtBQUFoRTs7QUFDRixTQUFLMkIsbUVBQUw7QUFDRSw2Q0FDS0gsS0FETDtBQUVFeEIsNEJBQW9CLEVBQUU7QUFGeEIsU0FHS25LLE1BQU0sQ0FBQ0UsT0FIWjs7QUFLRixTQUFLNEwseUVBQUw7QUFDRSw2Q0FDS0gsS0FETDtBQUVFdEYseUJBQWlCLEVBQUVyRyxNQUFNLENBQUNFLE9BRjVCO0FBR0U4QyxpQkFBUyxFQUFFO0FBSGI7O0FBS0YsU0FBSzhJLDREQUFMO0FBQ0UsMkRBQ0tILEtBREwsR0FFSzNMLE1BQU0sQ0FBQ0UsT0FGWjtBQUdFOEMsaUJBQVMsRUFBRSxLQUhiO0FBSUVvSCxzQkFBYyxFQUFFO0FBSmxCOztBQU1GLFNBQUswQiwrREFBTDtBQUNFdEQsYUFBTyxDQUFDNEMsR0FBUixDQUFZcEwsTUFBTSxDQUFDRSxPQUFuQjtBQUNBLDZDQUNLeUwsS0FETDtBQUVFTCx3QkFBZ0IsRUFBRXRMLE1BQU0sQ0FBQ0UsT0FGM0I7QUFHRThDLGlCQUFTLEVBQUU7QUFIYjs7QUFLRjtBQUNFLGFBQU8ySSxLQUFQO0FBeENKO0FBMENELENBckREOztBQXVEZUQsbUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekRBO0FBQ0E7O0FBRUEsSUFBTUssV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBQ2IsS0FBRCxFQUFXO0FBQzdCLE1BQUk7QUFDRixRQUFJYyxTQUFTLEdBQUdkLEtBQUssQ0FBQ2UsS0FBTixDQUFZLEdBQVosRUFBaUIsQ0FBakIsQ0FBaEI7QUFDQSxRQUFJQyxNQUFNLEdBQUdGLFNBQVMsQ0FBQ0csT0FBVixDQUFrQixJQUFsQixFQUF3QixHQUF4QixFQUE2QkEsT0FBN0IsQ0FBcUMsSUFBckMsRUFBMkMsR0FBM0MsQ0FBYjtBQUNBLFdBQU9DLElBQUksQ0FBQ0MsS0FBTCxDQUFXekMsTUFBTSxDQUFDMEMsSUFBUCxDQUFZSixNQUFaLENBQVgsQ0FBUDtBQUNELEdBSkQsQ0FJRSxnQkFBTTtBQUNOLFdBQU8sSUFBUDtBQUNEO0FBQ0YsQ0FSRDs7QUFVTyxJQUFNSyx1QkFBdUIsdUZBQUcsU0FBMUJBLHVCQUEwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDL0JDLHNCQUQrQixHQUNoQkMsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGNBQXJCLENBRGdCOztBQUFBLGVBRWpDRixZQUZpQztBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUVuQixpQkFBTUcsK0RBQUksQ0FBQ0MsV0FBRCxDQUFWOztBQUZtQjtBQUFBOztBQUFBO0FBSWhCLGlCQUFNQyxpRUFBTSxFQUFaOztBQUpnQjtBQUk3QkMsb0JBSjZCOztBQUFBLGVBSy9CQSxVQUFVLENBQUMzQyxvQkFMb0I7QUFBQTtBQUFBO0FBQUE7O0FBQUEsZUFNN0IyQyxVQUFVLENBQUNoSSxlQU5rQjtBQUFBO0FBQUE7QUFBQTs7QUFPM0JpSSxhQVAyQixHQU9yQkMsSUFBSSxDQUFDRCxHQUFMLEtBQWEsSUFQUTtBQVEzQkUsNEJBUjJCLEdBUU5ILFVBQVUsQ0FBQ3ZDLE1BQVgsQ0FBa0IyQyxHQUFsQixHQUF3QkgsR0FSbEI7O0FBQUEsZ0JBUzNCRSxrQkFBa0IsSUFBSSxHQVRLO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBVTdCLGlCQUFNTiwrREFBSSxDQUFDQyxXQUFELENBQVY7O0FBVjZCO0FBQUE7QUFBQTs7QUFBQTtBQUFBLGVBYTNCSixZQWIyQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQWFiLGlCQUFNRywrREFBSSxDQUFDQyxXQUFELENBQVY7O0FBYmE7QUFBQTtBQWVuQyxpQkFBTU8sZ0VBQUssQ0FBQyxLQUFELENBQVg7O0FBZm1DO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBMUJaLHVCQUEwQjtBQUFBLENBQUgsQ0FBN0I7QUFtQkEsSUFBTUssV0FBVyx1RkFBRyxTQUFkQSxXQUFjLENBQVc1TSxNQUFYO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNOLGlCQUFNNk0saUVBQU0sRUFBWjs7QUFETTtBQUNuQkMsb0JBRG1CO0FBRW5CTSxzQkFGbUIsR0FFSlgsWUFBWSxDQUFDQyxPQUFiLENBQXFCLGNBQXJCLENBRkk7O0FBQUEsZUFHckJJLFVBQVUsQ0FBQzNDLG9CQUhVO0FBQUE7QUFBQTtBQUFBOztBQUFBLGVBSW5CaUQsWUFKbUI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFLSixpQkFBTUMsWUFBWSxDQUFDO0FBQ2xDQyxrQkFBTSxFQUFFLE1BRDBCO0FBRWxDQyxlQUFHLFlBQUtULFVBQVUsQ0FBQy9ELFdBQWhCLHFCQUYrQjtBQUdsQ3lFLGdCQUFJLEVBQUU7QUFBRXRDLG1CQUFLLEVBQUVrQztBQUFUO0FBSDRCLFdBQUQsQ0FBbEI7O0FBTEk7QUFLZkssa0JBTGU7O0FBQUEsZ0JBVWpCQSxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsRUFWSjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQVdQLGlCQUFNRCxRQUFRLENBQUNFLElBQVQsRUFBTjs7QUFYTztBQVdiQyxjQVhhO0FBWWJDLG1CQVphLEdBWURELElBQUcsQ0FBQ0UsSUFaSDtBQWFiVix1QkFiYSxHQWFFUSxJQUFHLENBQUNHLE9BYk47QUFjbkJ0QixzQkFBWSxDQUFDdUIsT0FBYixDQUFxQixjQUFyQixFQUFxQ1osYUFBckM7QUFDTWxOLGlCQWZhLG1DQWdCZDBOLElBaEJjO0FBaUJqQjlJLDJCQUFlLEVBQUUsSUFqQkE7QUFrQmpCeUYsa0JBQU0sRUFBRXdCLFdBQVcsQ0FBQzhCLFNBQUQ7QUFsQkY7QUFBQTtBQW9CbkIsaUJBQU1JLDhEQUFHLENBQUM7QUFDUmhPLGdCQUFJLEVBQUU2TCwrREFERTtBQUVSNUwsbUJBQU8sRUFBUEE7QUFGUSxXQUFELENBQVQ7O0FBcEJtQjtBQXdCbkIsY0FBSSxPQUFPNE0sVUFBVSxDQUFDN0QsT0FBbEIsS0FBOEIsVUFBbEMsRUFDRTZELFVBQVUsQ0FBQzdELE9BQVgsQ0FBbUIvSSxPQUFuQjtBQXpCaUI7QUFBQTs7QUFBQTtBQTJCbkJ1TSxzQkFBWSxDQUFDeUIsVUFBYixDQUF3QixjQUF4QjtBQUNBMUYsaUJBQU8sQ0FBQzJGLElBQVIsQ0FBYSx3QkFBYjtBQTVCbUI7QUE2Qm5CLGlCQUFNeEIsK0RBQUksQ0FBQ3lCLE1BQUQsQ0FBVjs7QUE3Qm1CO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBZ0NyQixpQkFBTUEsTUFBTSxFQUFaOztBQWhDcUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQWR4QixXQUFjO0FBQUEsQ0FBSCxDQUFqQjtBQXFDQSxJQUFNeUIsa0JBQWtCLHVGQUFHLFNBQXJCQSxrQkFBcUIsQ0FBV3JPLE1BQVg7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNoQ3lNLHNCQUFZLENBQUN5QixVQUFiLENBQXdCLGNBQXhCO0FBRGdDO0FBRWIsaUJBQU1yQixpRUFBTSxFQUFaOztBQUZhO0FBRTFCQyxvQkFGMEI7O0FBQUEsZUFHNUJBLFVBQVUsQ0FBQy9ELFdBSGlCO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBSWIsaUJBQU1zRSxZQUFZLENBQUM7QUFDbENDLGtCQUFNLEVBQUUsTUFEMEI7QUFFbENDLGVBQUcsWUFBS1QsVUFBVSxDQUFDL0QsV0FBaEIsb0JBRitCO0FBR2xDeUUsZ0JBQUksRUFBRTtBQUNKaEcsbUJBQUssRUFBRXhILE1BQU0sQ0FBQ0UsT0FBUCxDQUFlbUksUUFEbEI7QUFFSmlHLGtCQUFJLEVBQUV0TyxNQUFNLENBQUNFLE9BQVAsQ0FBZW9JLFFBRmpCO0FBR0pDLHFCQUFPLEVBQUV2SSxNQUFNLENBQUNFLE9BQVAsQ0FBZXFJLE9BSHBCO0FBSUpnRyxvQkFBTSxFQUFFdk8sTUFBTSxDQUFDRSxPQUFQLENBQWVxRyxlQUFmLEdBQ0p2RyxNQUFNLENBQUNFLE9BQVAsQ0FBZXFHLGVBRFgsR0FFSjtBQU5BO0FBSDRCLFdBQUQsQ0FBbEI7O0FBSmE7QUFJeEJrSCxrQkFKd0I7O0FBQUEsZ0JBaUIxQkEsUUFBUSxJQUFJQSxRQUFRLENBQUNDLEVBakJLO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBa0JoQixpQkFBTUQsUUFBUSxDQUFDRSxJQUFULEVBQU47O0FBbEJnQjtBQWtCdEJDLGVBbEJzQjtBQW1CdEJDLG1CQW5Cc0IsR0FtQlZELEtBQUcsQ0FBQ0UsSUFuQk07QUFvQnRCVixzQkFwQnNCLEdBb0JQUSxLQUFHLENBQUNHLE9BcEJHO0FBcUI1QnRCLHNCQUFZLENBQUN1QixPQUFiLENBQXFCLGNBQXJCLEVBQXFDWixZQUFyQztBQUNNbE4saUJBdEJzQixHQXNCWjtBQUNkNEUsMkJBQWUsRUFBRSxJQURIO0FBRWQrSSxxQkFBUyxFQUFUQSxTQUZjO0FBR2R0RCxrQkFBTSxFQUFFd0IsV0FBVyxDQUFDOEIsU0FBRDtBQUhMLFdBdEJZO0FBQUE7QUEyQjVCLGlCQUFNSSw4REFBRyxDQUFDO0FBQ1JoTyxnQkFBSSxFQUFFNkwsK0RBREU7QUFFUjVMLG1CQUFPLEVBQVBBO0FBRlEsV0FBRCxDQUFUOztBQTNCNEI7QUFBQSxnQkErQnhCLE9BQU80TSxVQUFVLENBQUM3RCxPQUFsQixLQUE4QixVQS9CTjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQWdDMUIsaUJBQU02RCxVQUFVLENBQUM3RCxPQUFYLENBQW1CL0ksT0FBbkIsQ0FBTjs7QUFoQzBCO0FBQUE7QUFBQTs7QUFBQTtBQUFBO0FBa0M1QixpQkFBTXlNLCtEQUFJLENBQUN5QixNQUFELENBQVY7O0FBbEM0QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBckJDLGtCQUFxQjtBQUFBLENBQUgsQ0FBeEI7QUF1Q0EsSUFBTUcsS0FBSyx1RkFBRyxTQUFSQSxLQUFRLENBQVd4TyxNQUFYO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDbkJ5TSxzQkFBWSxDQUFDeUIsVUFBYixDQUF3QixjQUF4QjtBQURtQjtBQUVBLGlCQUFNckIsaUVBQU0sRUFBWjs7QUFGQTtBQUViQyxvQkFGYTs7QUFBQSxlQUlmQSxVQUFVLENBQUMvRCxXQUpJO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBS0EsaUJBQU1zRSxZQUFZLENBQUM7QUFDbENDLGtCQUFNLEVBQUUsTUFEMEI7QUFFbENDLGVBQUcsWUFBS1QsVUFBVSxDQUFDL0QsV0FBaEIsZUFGK0I7QUFHbEN5RSxnQkFBSSxFQUFFO0FBQ0poRyxtQkFBSyxFQUFFeEgsTUFBTSxDQUFDRSxPQUFQLENBQWVtSSxRQURsQjtBQUVKaUcsa0JBQUksRUFBRXRPLE1BQU0sQ0FBQ0UsT0FBUCxDQUFlb0ksUUFGakI7QUFHSmlHLG9CQUFNLEVBQUV2TyxNQUFNLENBQUNFLE9BQVAsQ0FBZXFHLGVBQWYsR0FDSnZHLE1BQU0sQ0FBQ0UsT0FBUCxDQUFlcUcsZUFEWCxHQUVKO0FBTEE7QUFINEIsV0FBRCxDQUFsQjs7QUFMQTtBQUtYa0gsa0JBTFc7O0FBQUEsZ0JBaUJiQSxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsRUFqQlI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFrQkgsaUJBQU1ELFFBQVEsQ0FBQ0UsSUFBVCxFQUFOOztBQWxCRztBQWtCVEMsZUFsQlM7QUFtQlRDLG1CQW5CUyxHQW1CR0QsS0FBRyxDQUFDRSxJQW5CUDtBQW9CVFYsc0JBcEJTLEdBb0JNUSxLQUFHLENBQUNHLE9BcEJWO0FBcUJmdEIsc0JBQVksQ0FBQ3VCLE9BQWIsQ0FBcUIsY0FBckIsRUFBcUNaLFlBQXJDO0FBQ01sTixpQkF0QlMsbUNBdUJWME4sS0F2QlU7QUF3QmI5SSwyQkFBZSxFQUFFLElBeEJKO0FBeUJieUYsa0JBQU0sRUFBRXdCLFdBQVcsQ0FBQzhCLFNBQUQ7QUF6Qk47QUFBQTtBQTJCZixpQkFBTUksOERBQUcsQ0FBQztBQUNSaE8sZ0JBQUksRUFBRTZMLCtEQURFO0FBRVI1TCxtQkFBTyxFQUFQQTtBQUZRLFdBQUQsQ0FBVDs7QUEzQmU7QUFBQSxnQkErQlgsT0FBTzRNLFVBQVUsQ0FBQzdELE9BQWxCLEtBQThCLFVBL0JuQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQWdDYixpQkFBTTZELFVBQVUsQ0FBQzdELE9BQVgsQ0FBbUIvSSxPQUFuQixDQUFOOztBQWhDYTtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQWtDZixpQkFBTXlNLCtEQUFJLENBQUN5QixNQUFELENBQVY7O0FBbENlO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFSSSxLQUFRO0FBQUEsQ0FBSCxDQUFYO0FBdUNBLElBQU1KLE1BQU0sdUZBQUcsU0FBVEEsTUFBUyxDQUFXcE8sTUFBWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNELGlCQUFNNk0saUVBQU0sRUFBWjs7QUFEQztBQUNkQyxvQkFEYztBQUVwQkwsc0JBQVksQ0FBQ3lCLFVBQWIsQ0FBd0IsY0FBeEI7QUFDTWhPLGlCQUhjLEdBR0o7QUFDZDRFLDJCQUFlLEVBQUUsS0FESDtBQUVkK0kscUJBQVMsRUFBRSxFQUZHO0FBR2R0RCxrQkFBTSxFQUFFO0FBSE0sV0FISTtBQUFBO0FBUXBCLGlCQUFNMEQsOERBQUcsQ0FBQztBQUNSaE8sZ0JBQUksRUFBRTZMLCtEQURFO0FBRVI1TCxtQkFBTyxFQUFQQTtBQUZRLFdBQUQsQ0FBVDs7QUFSb0I7QUFZcEIsY0FBSSxPQUFPNE0sVUFBVSxDQUFDNUQsUUFBbEIsS0FBK0IsVUFBbkMsRUFBK0M0RCxVQUFVLENBQUM1RCxRQUFYLENBQW9CaEosT0FBcEI7O0FBWjNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFUa08sTUFBUztBQUFBLENBQUgsQ0FBWjtBQWVBLElBQU0zTSxZQUFZLHVGQUFHLFNBQWZBLFlBQWUsQ0FBV3pCLE1BQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ3BCa0wsZUFEb0IsR0FDWmxMLE1BQU0sQ0FBQ0UsT0FESztBQUFBO0FBRVAsaUJBQU0yTSxpRUFBTSxFQUFaOztBQUZPO0FBRXBCQyxvQkFGb0I7QUFBQTtBQUcxQixpQkFBTXRFLE9BQU8sQ0FBQzRDLEdBQVIsQ0FBWTBCLFVBQVosQ0FBTjs7QUFIMEI7QUFBQSxlQUl0QkEsVUFBVSxDQUFDM0Msb0JBSlc7QUFBQTtBQUFBO0FBQUE7O0FBS2xCc0Usc0JBTGtCLEdBS0gxQyxXQUFXLENBQUNiLEtBQUQsQ0FMUjtBQUFBO0FBTXhCLGlCQUFNMUMsT0FBTyxDQUFDNEMsR0FBUixDQUFZcUQsWUFBWixDQUFOOztBQU53QjtBQUFBLGdCQU9wQkEsWUFQb0IsYUFPcEJBLFlBUG9CLGVBT3BCQSxZQUFZLENBQUVqSCxLQVBNO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBUUgsaUJBQU02RixZQUFZLENBQUM7QUFDcENDLGtCQUFNLEVBQUUsTUFENEI7QUFFcENDLGVBQUcsWUFBS1QsVUFBVSxDQUFDOUQsVUFBaEIsbUJBRmlDO0FBR3BDd0UsZ0JBQUksRUFBRTtBQUFFdEMsbUJBQUssRUFBTEE7QUFBRjtBQUg4QixXQUFELENBQWxCOztBQVJHO0FBUWhCd0Qsb0JBUmdCO0FBQUE7QUFhdEIsaUJBQU1sRyxPQUFPLENBQUM0QyxHQUFSLENBQVlzRCxVQUFaLENBQU47O0FBYnNCO0FBQUEsY0FjakJBLFVBQVUsQ0FBQ2hCLEVBZE07QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFlcEIsaUJBQU1PLDhEQUFHLENBQUM7QUFDUmhPLGdCQUFJLEVBQUU2TCxrRUFERTtBQUVSNUwsbUJBQU8sRUFBRTtBQUZELFdBQUQsQ0FBVDs7QUFmb0I7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFvQkYsaUJBQU1tTixZQUFZLENBQUM7QUFDbkNDLGtCQUFNLEVBQUUsTUFEMkI7QUFFbkNDLGVBQUcsWUFBS1QsVUFBVSxDQUFDL0QsV0FBaEIsdUJBRmdDO0FBR25DeUUsZ0JBQUksRUFBRTtBQUFFbkYsc0JBQVEsRUFBRW9HLFlBQVksQ0FBQ2pIO0FBQXpCO0FBSDZCLFdBQUQsQ0FBbEI7O0FBcEJFO0FBb0JkbUgsbUJBcEJjO0FBQUE7QUF5QkYsaUJBQU1BLFNBQVMsQ0FBQ2hCLElBQVYsRUFBTjs7QUF6QkU7QUF5QmRpQixtQkF6QmM7QUFBQTtBQTBCcEIsaUJBQU1YLDhEQUFHLENBQUM7QUFDUmhPLGdCQUFJLEVBQUU2TCxrRUFERTtBQUVSNUwsbUJBQU8sa0NBQU8wTyxTQUFQO0FBQWtCcEgsbUJBQUssRUFBRWlILFlBQVksQ0FBQ2pIO0FBQXRDO0FBRkMsV0FBRCxDQUFUOztBQTFCb0I7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFnQ3RCLGlCQUFNeUcsOERBQUcsQ0FBQztBQUNSaE8sZ0JBQUksRUFBRTZMLGtFQURFO0FBRVI1TCxtQkFBTyxFQUFFO0FBRkQsV0FBRCxDQUFUOztBQWhDc0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQWZ1QixZQUFlO0FBQUEsQ0FBSCxDQUFsQjtBQXdDQSxJQUFNRCxxQkFBcUIsdUZBQUcsU0FBeEJBLHFCQUF3QixDQUFXeEIsTUFBWDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNoQixpQkFBTTZNLGlFQUFNLEVBQVo7O0FBRGdCO0FBQzdCQyxvQkFENkI7O0FBQUEsZUFFL0JBLFVBQVUsQ0FBQzNDLG9CQUZvQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUdoQixpQkFBTWtELFlBQVksQ0FBQztBQUNsQ0Msa0JBQU0sRUFBRSxNQUQwQjtBQUVsQ0MsZUFBRyxZQUFLVCxVQUFVLENBQUM5RCxVQUFoQix5QkFGK0I7QUFHbEN3RSxnQkFBSSxFQUFFO0FBQUVoRyxtQkFBSyxFQUFFeEgsTUFBTSxDQUFDRSxPQUFQLENBQWVtSTtBQUF4QjtBQUg0QixXQUFELENBQWxCOztBQUhnQjtBQUczQm9GLGtCQUgyQjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBeEJqTSxxQkFBd0I7QUFBQSxDQUFILENBQTNCO0FBV0EsSUFBTU4saUJBQWlCLHVGQUFHLFNBQXBCQSxpQkFBb0IsQ0FBV2xCLE1BQVg7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1osaUJBQU02TSxpRUFBTSxFQUFaOztBQURZO0FBQ3pCQyxvQkFEeUI7O0FBQUEsZUFFM0JBLFVBQVUsQ0FBQzNDLG9CQUZnQjtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUdaLGlCQUFNa0QsWUFBWSxDQUFDO0FBQ2xDQyxrQkFBTSxFQUFFLE1BRDBCO0FBRWxDQyxlQUFHLFlBQUtULFVBQVUsQ0FBQy9ELFdBQWhCLHVCQUYrQjtBQUdsQ3lFLGdCQUFJLEVBQUU7QUFBRW5GLHNCQUFRLEVBQUVySSxNQUFNLENBQUNFLE9BQVAsQ0FBZW1JO0FBQTNCO0FBSDRCLFdBQUQsQ0FBbEI7O0FBSFk7QUFHdkJvRixrQkFIdUI7O0FBQUEsZ0JBVXpCQSxRQUFRLElBQUlBLFFBQVEsQ0FBQ0MsRUFWSTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQVdmLGlCQUFNRCxRQUFRLENBQUNFLElBQVQsRUFBTjs7QUFYZTtBQVdyQkMsZUFYcUI7QUFZM0IxTixpQkFBTyxHQUFHO0FBQ1JtSSxvQkFBUSxFQUFFckksTUFBTSxDQUFDRSxPQUFQLENBQWVtSSxRQURqQjtBQUVSaEIscUJBQVMsRUFBRXVHLEtBQUcsQ0FBQ3ZHLFNBRlA7QUFHUkMsaUJBQUssRUFBRXNHLEtBQUcsQ0FBQ25HLENBSEg7QUFJUkYsaUJBQUssRUFBRXFHLEtBQUcsQ0FBQ2xHO0FBSkgsV0FBVjtBQVoyQjtBQUFBOztBQUFBO0FBbUIzQnhILGlCQUFPLEdBQUc7QUFDUm1JLG9CQUFRLEVBQUVySSxNQUFNLENBQUNFLE9BQVAsQ0FBZW1JLFFBRGpCO0FBRVJoQixxQkFBUyxFQUFFLEtBRkg7QUFHUkMsaUJBQUssRUFBRXNHLEdBQUcsQ0FBQ25HLENBSEg7QUFJUkYsaUJBQUssRUFBRXFHLEdBQUcsQ0FBQ2xHO0FBSkgsV0FBVjs7QUFuQjJCO0FBQUE7QUEwQjdCLGlCQUFNdUcsOERBQUcsQ0FBQztBQUNSaE8sZ0JBQUksRUFBRTZMLDRFQURFO0FBRVI1TCxtQkFBTyxFQUFQQTtBQUZRLFdBQUQsQ0FBVDs7QUExQjZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFwQmdCLGlCQUFvQjtBQUFBLENBQUgsQ0FBdkI7QUFpQ0EsSUFBTUUsVUFBVSx1RkFBRyxTQUFiQSxVQUFhLENBQVdwQixNQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0wsaUJBQU02TSxpRUFBTSxFQUFaOztBQURLO0FBQ2xCQyxvQkFEa0I7O0FBQUEsZUFFcEJBLFVBQVUsQ0FBQzNDLG9CQUZTO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBR3RCLGlCQUFNa0QsWUFBWSxDQUFDO0FBQ2pCQyxrQkFBTSxFQUFFLE1BRFM7QUFFakJDLGVBQUcsWUFBS1QsVUFBVSxDQUFDOUQsVUFBaEIsVUFGYztBQUdqQndFLGdCQUFJLEVBQUU7QUFBRWhHLG1CQUFLLEVBQUV4SCxNQUFNLENBQUNFLE9BQVAsQ0FBZW1JLFFBQXhCO0FBQWtDSyxrQkFBSSxFQUFFMUksTUFBTSxDQUFDRSxPQUFQLENBQWV3STtBQUF2RDtBQUhXLFdBQUQsQ0FBbEI7O0FBSHNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQUFidEgsVUFBYTtBQUFBLENBQUgsQ0FBaEI7QUFVQSxJQUFNeU4sY0FBYyx1RkFBRyxTQUFqQkEsY0FBaUIsQ0FBVzdPLE1BQVg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBQ3hCLENBQUNBLE1BQU0sQ0FBQ0UsT0FBUCxDQUFlNkksV0FBaEIsSUFBK0IsQ0FBQy9JLE1BQU0sQ0FBQ0UsT0FBUCxDQUFlOEksVUFEdkI7QUFBQTtBQUFBO0FBQUE7O0FBRTFCUixpQkFBTyxDQUFDQyxLQUFSLENBQ0Usa0dBREY7QUFGMEI7QUFLMUIsaUJBQU13Riw4REFBRyxDQUFDO0FBQUVoTyxnQkFBSSxFQUFFNkwsZ0VBQTRCM0w7QUFBcEMsV0FBRCxDQUFUOztBQUwwQjtBQUFBO0FBQUE7O0FBQUE7QUFBQTtBQU9ELGlCQUFNa04sWUFBWSxDQUFDO0FBQzFDQyxrQkFBTSxFQUFFLEtBRGtDO0FBRTFDQyxlQUFHLEVBQUV2TixNQUFNLENBQUNFLE9BQVAsQ0FBZTZJO0FBRnNCLFdBQUQsQ0FBbEI7O0FBUEM7QUFPcEIrRiwwQkFQb0I7QUFBQTtBQVdGLGlCQUFNekIsWUFBWSxDQUFDO0FBQ3pDQyxrQkFBTSxFQUFFLEtBRGlDO0FBRXpDQyxlQUFHLEVBQUV2TixNQUFNLENBQUNFLE9BQVAsQ0FBZThJO0FBRnFCLFdBQUQsQ0FBbEI7O0FBWEU7QUFXcEIrRix5QkFYb0I7O0FBQUEsZ0JBZXRCRCxnQkFBZ0IsQ0FBQ3BCLEVBQWpCLElBQXVCcUIsZUFBZSxDQUFDckIsRUFmakI7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFnQnhCLGlCQUFNTyw4REFBRyxDQUFDO0FBQ1JoTyxnQkFBSSxFQUFFNkwsc0VBREU7QUFFUjVMLG1CQUFPLEVBQUVGLE1BQU0sQ0FBQ0U7QUFGUixXQUFELENBQVQ7O0FBaEJ3QjtBQUFBO0FBb0IxQixpQkFBTStOLDhEQUFHLENBQUM7QUFDUmhPLGdCQUFJLEVBQUU2TCx1REFBbUJuTDtBQURqQixXQUFELENBQVQ7O0FBcEIwQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0FBakJrTyxjQUFpQjtBQUFBLENBQUgsQ0FBcEI7O0FBMEJQLElBQU14QixZQUFZLHVGQUFHLFNBQWZBLFlBQWU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWFDLGdCQUFiLFFBQWFBLE1BQWIsRUFBcUJDLEdBQXJCLFFBQXFCQSxHQUFyQixFQUEwQkMsSUFBMUIsUUFBMEJBLElBQTFCO0FBQUE7QUFFYndCLGlCQUZhLEdBRUg7QUFDWjFCLGtCQUFNLEVBQUVBLE1BREk7QUFFWjJCLG1CQUFPLEVBQUU7QUFDUEMsb0JBQU0sRUFBRSwrREFERDtBQUVQLDhCQUFnQjtBQUZULGFBRkc7QUFNWjFCLGdCQUFJLEVBQUVBLElBQUksR0FDTixPQUFPQSxJQUFQLEtBQWdCLFFBQWhCLEdBQ0VBLElBREYsR0FFRXBCLElBQUksQ0FBQytDLFNBQUwsQ0FBZTNCLElBQWYsRUFBcUJyQixPQUFyQixDQUE2QixVQUE3QixFQUF5QyxFQUF6QyxDQUhJLEdBSU47QUFWUSxXQUZHO0FBY2pCLGNBQUksQ0FBQ3FCLElBQUQsSUFBU0YsTUFBTSxLQUFLLEtBQXhCLEVBQStCLE9BQU8wQixPQUFPLENBQUN4QixJQUFmO0FBZGQ7QUFlVixpQkFBTTRCLEtBQUssQ0FBQzdCLEdBQUQsRUFBTXlCLE9BQU4sQ0FBWDs7QUFmVTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBaUJqQixpQkFBTWYsOERBQUcsQ0FBQztBQUFFaE8sZ0JBQUksRUFBRTZMLGdFQUE0QjNMO0FBQXBDLFdBQUQsQ0FBVDs7QUFqQmlCO0FBa0JqQnFJLGlCQUFPLENBQUNDLEtBQVIsQ0FBYywyQ0FBZDtBQWxCaUI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBQWY0RSxZQUFlO0FBQUEsQ0FBSCxDQUFsQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7a0dDeFFVZ0MsUTs7QUFsQlY7QUFDQTtBQUNBO0FBWUE7QUFDQTtBQUNBOztBQUVBLFNBQVVBLFFBQVY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0UsaUJBQU1DLDhEQUFHLENBQUMsQ0FDUi9DLHFFQUF1QixFQURmLEVBRVJnRCxxRUFBVSxDQUFDekQsbURBQUQsRUFBcUIwQywyQ0FBckIsQ0FGRixFQUdSZSxxRUFBVSxDQUFDekQsbUVBQUQsRUFBcUN1Qyx3REFBckMsQ0FIRixFQUlSa0IscUVBQVUsQ0FBQ3pELG9EQUFELEVBQXNCc0MsNENBQXRCLENBSkYsRUFLUm1CLHFFQUFVLENBQUN6RCxvREFBRCxFQUFzQmMsaURBQXRCLENBTEYsRUFNUjJDLHFFQUFVLENBQUN6RCxnRUFBRCxFQUFrQzVLLHVEQUFsQyxDQU5GLEVBT1JxTyxxRUFBVSxDQUFDekQsb0RBQUQsRUFBc0IxSyxnREFBdEIsQ0FQRixFQVFSbU8scUVBQVUsQ0FBQ3pELDBEQUFELEVBQTRCK0Msb0RBQTVCLENBUkYsRUFTUlUscUVBQVUsQ0FBQ3pELGdFQUFELEVBQWtDdEssMkRBQWxDLENBVEYsRUFVUitOLHFFQUFVLENBQUN6RCxzREFBRCxFQUF3QnJLLGtEQUF4QixDQVZGLENBQUQsQ0FBVDs7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFlQSxJQUFNK04sY0FBYyxHQUFHQyxpREFBb0IsRUFBM0M7O0FBQ0EsSUFBTWhFLEtBQUssR0FBRyxpQkFBTTtBQUNsQixNQUFJQSxLQUFLLEdBQUdpRSx5REFBVyxDQUFDQyxnREFBRCxFQUFjQyw2REFBZSxDQUFDSixjQUFELENBQTdCLENBQXZCO0FBQ0FBLGdCQUFjLENBQUNLLEdBQWYsQ0FBbUJSLFFBQW5CO0FBQ0EsU0FBTzVELEtBQVA7QUFDRCxDQUpEOztBQU1lQSxvRUFBSyxFQUFwQixFOzs7Ozs7Ozs7OztBQ3hDQSw0Qzs7Ozs7Ozs7Ozs7QUNBQSxrQzs7Ozs7Ozs7Ozs7QUNBQSx3Qzs7Ozs7Ozs7Ozs7QUNBQSx1QyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvaW5kZXguanNcIik7XG4iLCJmdW5jdGlvbiBfYXJyYXlMaWtlVG9BcnJheShhcnIsIGxlbikge1xuICBpZiAobGVuID09IG51bGwgfHwgbGVuID4gYXJyLmxlbmd0aCkgbGVuID0gYXJyLmxlbmd0aDtcblxuICBmb3IgKHZhciBpID0gMCwgYXJyMiA9IG5ldyBBcnJheShsZW4pOyBpIDwgbGVuOyBpKyspIHtcbiAgICBhcnIyW2ldID0gYXJyW2ldO1xuICB9XG5cbiAgcmV0dXJuIGFycjI7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2FycmF5TGlrZVRvQXJyYXk7XG5tb2R1bGUuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBtb2R1bGUuZXhwb3J0cywgbW9kdWxlLmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7IiwiZnVuY3Rpb24gX2FycmF5V2l0aEhvbGVzKGFycikge1xuICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSByZXR1cm4gYXJyO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9hcnJheVdpdGhIb2xlcztcbm1vZHVsZS5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IG1vZHVsZS5leHBvcnRzLCBtb2R1bGUuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTsiLCJmdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7XG4gIGlmIChrZXkgaW4gb2JqKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7XG4gICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgd3JpdGFibGU6IHRydWVcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICBvYmpba2V5XSA9IHZhbHVlO1xuICB9XG5cbiAgcmV0dXJuIG9iajtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmaW5lUHJvcGVydHk7XG5tb2R1bGUuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBtb2R1bGUuZXhwb3J0cywgbW9kdWxlLmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7IiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gX2RlZmluZVByb3BlcnR5KG9iaiwga2V5LCB2YWx1ZSkge1xuICBpZiAoa2V5IGluIG9iaikge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShvYmosIGtleSwge1xuICAgICAgdmFsdWU6IHZhbHVlLFxuICAgICAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZSxcbiAgICAgIHdyaXRhYmxlOiB0cnVlXG4gICAgfSk7XG4gIH0gZWxzZSB7XG4gICAgb2JqW2tleV0gPSB2YWx1ZTtcbiAgfVxuXG4gIHJldHVybiBvYmo7XG59IiwiZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gX2V4dGVuZHMoKSB7XG4gIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07XG5cbiAgICAgIGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHtcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHtcbiAgICAgICAgICB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRhcmdldDtcbiAgfTtcblxuICByZXR1cm4gX2V4dGVuZHMuYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn0iLCJpbXBvcnQgZGVmaW5lUHJvcGVydHkgZnJvbSBcIi4vZGVmaW5lUHJvcGVydHkuanNcIjtcblxuZnVuY3Rpb24gb3duS2V5cyhvYmplY3QsIGVudW1lcmFibGVPbmx5KSB7XG4gIHZhciBrZXlzID0gT2JqZWN0LmtleXMob2JqZWN0KTtcblxuICBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICAgIHZhciBzeW1ib2xzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhvYmplY3QpO1xuXG4gICAgaWYgKGVudW1lcmFibGVPbmx5KSB7XG4gICAgICBzeW1ib2xzID0gc3ltYm9scy5maWx0ZXIoZnVuY3Rpb24gKHN5bSkge1xuICAgICAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihvYmplY3QsIHN5bSkuZW51bWVyYWJsZTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGtleXMucHVzaC5hcHBseShrZXlzLCBzeW1ib2xzKTtcbiAgfVxuXG4gIHJldHVybiBrZXlzO1xufVxuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBfb2JqZWN0U3ByZWFkMih0YXJnZXQpIHtcbiAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldICE9IG51bGwgPyBhcmd1bWVudHNbaV0gOiB7fTtcblxuICAgIGlmIChpICUgMikge1xuICAgICAgb3duS2V5cyhPYmplY3Qoc291cmNlKSwgdHJ1ZSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgIGRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBzb3VyY2Vba2V5XSk7XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKSB7XG4gICAgICBPYmplY3QuZGVmaW5lUHJvcGVydGllcyh0YXJnZXQsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3JzKHNvdXJjZSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICBvd25LZXlzKE9iamVjdChzb3VyY2UpKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHNvdXJjZSwga2V5KSk7XG4gICAgICB9KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gdGFyZ2V0O1xufSIsImZ1bmN0aW9uIF9pdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsIGkpIHtcbiAgdmFyIF9pID0gYXJyID09IG51bGwgPyBudWxsIDogdHlwZW9mIFN5bWJvbCAhPT0gXCJ1bmRlZmluZWRcIiAmJiBhcnJbU3ltYm9sLml0ZXJhdG9yXSB8fCBhcnJbXCJAQGl0ZXJhdG9yXCJdO1xuXG4gIGlmIChfaSA9PSBudWxsKSByZXR1cm47XG4gIHZhciBfYXJyID0gW107XG4gIHZhciBfbiA9IHRydWU7XG4gIHZhciBfZCA9IGZhbHNlO1xuXG4gIHZhciBfcywgX2U7XG5cbiAgdHJ5IHtcbiAgICBmb3IgKF9pID0gX2kuY2FsbChhcnIpOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7XG4gICAgICBfYXJyLnB1c2goX3MudmFsdWUpO1xuXG4gICAgICBpZiAoaSAmJiBfYXJyLmxlbmd0aCA9PT0gaSkgYnJlYWs7XG4gICAgfVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICBfZCA9IHRydWU7XG4gICAgX2UgPSBlcnI7XG4gIH0gZmluYWxseSB7XG4gICAgdHJ5IHtcbiAgICAgIGlmICghX24gJiYgX2lbXCJyZXR1cm5cIl0gIT0gbnVsbCkgX2lbXCJyZXR1cm5cIl0oKTtcbiAgICB9IGZpbmFsbHkge1xuICAgICAgaWYgKF9kKSB0aHJvdyBfZTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gX2Fycjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfaXRlcmFibGVUb0FycmF5TGltaXQ7XG5tb2R1bGUuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBtb2R1bGUuZXhwb3J0cywgbW9kdWxlLmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7IiwiZnVuY3Rpb24gX25vbkl0ZXJhYmxlUmVzdCgpIHtcbiAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2UuXFxuSW4gb3JkZXIgdG8gYmUgaXRlcmFibGUsIG5vbi1hcnJheSBvYmplY3RzIG11c3QgaGF2ZSBhIFtTeW1ib2wuaXRlcmF0b3JdKCkgbWV0aG9kLlwiKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfbm9uSXRlcmFibGVSZXN0O1xubW9kdWxlLmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gbW9kdWxlLmV4cG9ydHMsIG1vZHVsZS5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlOyIsInZhciBhcnJheVdpdGhIb2xlcyA9IHJlcXVpcmUoXCIuL2FycmF5V2l0aEhvbGVzLmpzXCIpO1xuXG52YXIgaXRlcmFibGVUb0FycmF5TGltaXQgPSByZXF1aXJlKFwiLi9pdGVyYWJsZVRvQXJyYXlMaW1pdC5qc1wiKTtcblxudmFyIHVuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5ID0gcmVxdWlyZShcIi4vdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkuanNcIik7XG5cbnZhciBub25JdGVyYWJsZVJlc3QgPSByZXF1aXJlKFwiLi9ub25JdGVyYWJsZVJlc3QuanNcIik7XG5cbmZ1bmN0aW9uIF9zbGljZWRUb0FycmF5KGFyciwgaSkge1xuICByZXR1cm4gYXJyYXlXaXRoSG9sZXMoYXJyKSB8fCBpdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsIGkpIHx8IHVuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5KGFyciwgaSkgfHwgbm9uSXRlcmFibGVSZXN0KCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3NsaWNlZFRvQXJyYXk7XG5tb2R1bGUuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBtb2R1bGUuZXhwb3J0cywgbW9kdWxlLmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7IiwidmFyIGFycmF5TGlrZVRvQXJyYXkgPSByZXF1aXJlKFwiLi9hcnJheUxpa2VUb0FycmF5LmpzXCIpO1xuXG5mdW5jdGlvbiBfdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXkobywgbWluTGVuKSB7XG4gIGlmICghbykgcmV0dXJuO1xuICBpZiAodHlwZW9mIG8gPT09IFwic3RyaW5nXCIpIHJldHVybiBhcnJheUxpa2VUb0FycmF5KG8sIG1pbkxlbik7XG4gIHZhciBuID0gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKG8pLnNsaWNlKDgsIC0xKTtcbiAgaWYgKG4gPT09IFwiT2JqZWN0XCIgJiYgby5jb25zdHJ1Y3RvcikgbiA9IG8uY29uc3RydWN0b3IubmFtZTtcbiAgaWYgKG4gPT09IFwiTWFwXCIgfHwgbiA9PT0gXCJTZXRcIikgcmV0dXJuIEFycmF5LmZyb20obyk7XG4gIGlmIChuID09PSBcIkFyZ3VtZW50c1wiIHx8IC9eKD86VWl8SSludCg/Ojh8MTZ8MzIpKD86Q2xhbXBlZCk/QXJyYXkkLy50ZXN0KG4pKSByZXR1cm4gYXJyYXlMaWtlVG9BcnJheShvLCBtaW5MZW4pO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheTtcbm1vZHVsZS5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IG1vZHVsZS5leHBvcnRzLCBtb2R1bGUuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWdlbmVyYXRvci1ydW50aW1lXCIpO1xuIiwiaW1wb3J0IHsgVEFTS19DQU5DRUwsIFRFUk1JTkFURSwgU0FHQV9MT0NBVElPTiwgU0FHQV9BQ1RJT04sIElPLCBTRUxGX0NBTkNFTExBVElPTiB9IGZyb20gJ0ByZWR1eC1zYWdhL3N5bWJvbHMnO1xuaW1wb3J0IF9leHRlbmRzIGZyb20gJ0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvZXNtL2V4dGVuZHMnO1xuaW1wb3J0IHsgYXJyYXksIG5vdFVuZGVmLCBwYXR0ZXJuLCBtdWx0aWNhc3QsIGNoYW5uZWwsIHVuZGVmLCBlZmZlY3QsIHRhc2ssIGZ1bmMsIGJ1ZmZlciwgc3RyaW5nLCBvYmplY3QgfSBmcm9tICdAcmVkdXgtc2FnYS9pcyc7XG5pbXBvcnQgZGVsYXlQIGZyb20gJ0ByZWR1eC1zYWdhL2RlbGF5LXAnO1xuXG52YXIga29uc3QgPSBmdW5jdGlvbiBrb25zdCh2KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHY7XG4gIH07XG59O1xudmFyIGtUcnVlID1cbi8qI19fUFVSRV9fKi9cbmtvbnN0KHRydWUpO1xuXG52YXIgbm9vcCA9IGZ1bmN0aW9uIG5vb3AoKSB7fTtcblxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgdHlwZW9mIFByb3h5ICE9PSAndW5kZWZpbmVkJykge1xuICBub29wID1cbiAgLyojX19QVVJFX18qL1xuICBuZXcgUHJveHkobm9vcCwge1xuICAgIHNldDogZnVuY3Rpb24gc2V0KCkge1xuICAgICAgdGhyb3cgaW50ZXJuYWxFcnIoJ1RoZXJlIHdhcyBhbiBhdHRlbXB0IHRvIGFzc2lnbiBhIHByb3BlcnR5IHRvIGludGVybmFsIGBub29wYCBmdW5jdGlvbi4nKTtcbiAgICB9XG4gIH0pO1xufVxudmFyIGlkZW50aXR5ID0gZnVuY3Rpb24gaWRlbnRpdHkodikge1xuICByZXR1cm4gdjtcbn07XG52YXIgaGFzU3ltYm9sID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJztcbnZhciBhc3luY0l0ZXJhdG9yU3ltYm9sID0gaGFzU3ltYm9sICYmIFN5bWJvbC5hc3luY0l0ZXJhdG9yID8gU3ltYm9sLmFzeW5jSXRlcmF0b3IgOiAnQEBhc3luY0l0ZXJhdG9yJztcbmZ1bmN0aW9uIGNoZWNrKHZhbHVlLCBwcmVkaWNhdGUsIGVycm9yKSB7XG4gIGlmICghcHJlZGljYXRlKHZhbHVlKSkge1xuICAgIHRocm93IG5ldyBFcnJvcihlcnJvcik7XG4gIH1cbn1cbnZhciBhc3NpZ25XaXRoU3ltYm9scyA9IGZ1bmN0aW9uIGFzc2lnbldpdGhTeW1ib2xzKHRhcmdldCwgc291cmNlKSB7XG4gIF9leHRlbmRzKHRhcmdldCwgc291cmNlKTtcblxuICBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICAgIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMoc291cmNlKS5mb3JFYWNoKGZ1bmN0aW9uIChzKSB7XG4gICAgICB0YXJnZXRbc10gPSBzb3VyY2Vbc107XG4gICAgfSk7XG4gIH1cbn07XG52YXIgZmxhdE1hcCA9IGZ1bmN0aW9uIGZsYXRNYXAobWFwcGVyLCBhcnIpIHtcbiAgdmFyIF9yZWY7XG5cbiAgcmV0dXJuIChfcmVmID0gW10pLmNvbmNhdC5hcHBseShfcmVmLCBhcnIubWFwKG1hcHBlcikpO1xufTtcbmZ1bmN0aW9uIHJlbW92ZShhcnJheSwgaXRlbSkge1xuICB2YXIgaW5kZXggPSBhcnJheS5pbmRleE9mKGl0ZW0pO1xuXG4gIGlmIChpbmRleCA+PSAwKSB7XG4gICAgYXJyYXkuc3BsaWNlKGluZGV4LCAxKTtcbiAgfVxufVxuZnVuY3Rpb24gb25jZShmbikge1xuICB2YXIgY2FsbGVkID0gZmFsc2U7XG4gIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKGNhbGxlZCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNhbGxlZCA9IHRydWU7XG4gICAgZm4oKTtcbiAgfTtcbn1cblxudmFyIGtUaHJvdyA9IGZ1bmN0aW9uIGtUaHJvdyhlcnIpIHtcbiAgdGhyb3cgZXJyO1xufTtcblxudmFyIGtSZXR1cm4gPSBmdW5jdGlvbiBrUmV0dXJuKHZhbHVlKSB7XG4gIHJldHVybiB7XG4gICAgdmFsdWU6IHZhbHVlLFxuICAgIGRvbmU6IHRydWVcbiAgfTtcbn07XG5cbmZ1bmN0aW9uIG1ha2VJdGVyYXRvcihuZXh0LCB0aHJvLCBuYW1lKSB7XG4gIGlmICh0aHJvID09PSB2b2lkIDApIHtcbiAgICB0aHJvID0ga1Rocm93O1xuICB9XG5cbiAgaWYgKG5hbWUgPT09IHZvaWQgMCkge1xuICAgIG5hbWUgPSAnaXRlcmF0b3InO1xuICB9XG5cbiAgdmFyIGl0ZXJhdG9yID0ge1xuICAgIG1ldGE6IHtcbiAgICAgIG5hbWU6IG5hbWVcbiAgICB9LFxuICAgIG5leHQ6IG5leHQsXG4gICAgdGhyb3c6IHRocm8sXG4gICAgcmV0dXJuOiBrUmV0dXJuLFxuICAgIGlzU2FnYUl0ZXJhdG9yOiB0cnVlXG4gIH07XG5cbiAgaWYgKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgaXRlcmF0b3JbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBpdGVyYXRvcjtcbiAgICB9O1xuICB9XG5cbiAgcmV0dXJuIGl0ZXJhdG9yO1xufVxuZnVuY3Rpb24gbG9nRXJyb3IoZXJyb3IsIF9yZWYyKSB7XG4gIHZhciBzYWdhU3RhY2sgPSBfcmVmMi5zYWdhU3RhY2s7XG5cbiAgLyplc2xpbnQtZGlzYWJsZSBuby1jb25zb2xlKi9cbiAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gIGNvbnNvbGUuZXJyb3Ioc2FnYVN0YWNrKTtcbn1cbnZhciBpbnRlcm5hbEVyciA9IGZ1bmN0aW9uIGludGVybmFsRXJyKGVycikge1xuICByZXR1cm4gbmV3IEVycm9yKFwiXFxuICByZWR1eC1zYWdhOiBFcnJvciBjaGVja2luZyBob29rcyBkZXRlY3RlZCBhbiBpbmNvbnNpc3RlbnQgc3RhdGUuIFRoaXMgaXMgbGlrZWx5IGEgYnVnXFxuICBpbiByZWR1eC1zYWdhIGNvZGUgYW5kIG5vdCB5b3Vycy4gVGhhbmtzIGZvciByZXBvcnRpbmcgdGhpcyBpbiB0aGUgcHJvamVjdCdzIGdpdGh1YiByZXBvLlxcbiAgRXJyb3I6IFwiICsgZXJyICsgXCJcXG5cIik7XG59O1xudmFyIGNyZWF0ZVNldENvbnRleHRXYXJuaW5nID0gZnVuY3Rpb24gY3JlYXRlU2V0Q29udGV4dFdhcm5pbmcoY3R4LCBwcm9wcykge1xuICByZXR1cm4gKGN0eCA/IGN0eCArICcuJyA6ICcnKSArIFwic2V0Q29udGV4dChwcm9wcyk6IGFyZ3VtZW50IFwiICsgcHJvcHMgKyBcIiBpcyBub3QgYSBwbGFpbiBvYmplY3RcIjtcbn07XG52YXIgRlJPWkVOX0FDVElPTl9FUlJPUiA9IFwiWW91IGNhbid0IHB1dCAoYS5rLmEuIGRpc3BhdGNoIGZyb20gc2FnYSkgZnJvemVuIGFjdGlvbnMuXFxuV2UgaGF2ZSB0byBkZWZpbmUgYSBzcGVjaWFsIG5vbi1lbnVtZXJhYmxlIHByb3BlcnR5IG9uIHRob3NlIGFjdGlvbnMgZm9yIHNjaGVkdWxpbmcgcHVycG9zZXMuXFxuT3RoZXJ3aXNlIHlvdSB3b3VsZG4ndCBiZSBhYmxlIHRvIGNvbW11bmljYXRlIHByb3Blcmx5IGJldHdlZW4gc2FnYXMgJiBvdGhlciBzdWJzY3JpYmVycyAoYWN0aW9uIG9yZGVyaW5nIHdvdWxkIGJlY29tZSBmYXIgbGVzcyBwcmVkaWN0YWJsZSkuXFxuSWYgeW91IGFyZSB1c2luZyByZWR1eCBhbmQgeW91IGNhcmUgYWJvdXQgdGhpcyBiZWhhdmlvdXIgKGZyb3plbiBhY3Rpb25zKSxcXG50aGVuIHlvdSBtaWdodCB3YW50IHRvIHN3aXRjaCB0byBmcmVlemluZyBhY3Rpb25zIGluIGEgbWlkZGxld2FyZSByYXRoZXIgdGhhbiBpbiBhY3Rpb24gY3JlYXRvci5cXG5FeGFtcGxlIGltcGxlbWVudGF0aW9uOlxcblxcbmNvbnN0IGZyZWV6ZUFjdGlvbnMgPSBzdG9yZSA9PiBuZXh0ID0+IGFjdGlvbiA9PiBuZXh0KE9iamVjdC5mcmVlemUoYWN0aW9uKSlcXG5cIjsgLy8gY3JlYXRlcyBlbXB0eSwgYnV0IG5vdC1ob2xleSBhcnJheVxuXG52YXIgY3JlYXRlRW1wdHlBcnJheSA9IGZ1bmN0aW9uIGNyZWF0ZUVtcHR5QXJyYXkobikge1xuICByZXR1cm4gQXJyYXkuYXBwbHkobnVsbCwgbmV3IEFycmF5KG4pKTtcbn07XG52YXIgd3JhcFNhZ2FEaXNwYXRjaCA9IGZ1bmN0aW9uIHdyYXBTYWdhRGlzcGF0Y2goZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChhY3Rpb24pIHtcbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgY2hlY2soYWN0aW9uLCBmdW5jdGlvbiAoYWMpIHtcbiAgICAgICAgcmV0dXJuICFPYmplY3QuaXNGcm96ZW4oYWMpO1xuICAgICAgfSwgRlJPWkVOX0FDVElPTl9FUlJPUik7XG4gICAgfVxuXG4gICAgcmV0dXJuIGRpc3BhdGNoKE9iamVjdC5kZWZpbmVQcm9wZXJ0eShhY3Rpb24sIFNBR0FfQUNUSU9OLCB7XG4gICAgICB2YWx1ZTogdHJ1ZVxuICAgIH0pKTtcbiAgfTtcbn07XG52YXIgc2hvdWxkVGVybWluYXRlID0gZnVuY3Rpb24gc2hvdWxkVGVybWluYXRlKHJlcykge1xuICByZXR1cm4gcmVzID09PSBURVJNSU5BVEU7XG59O1xudmFyIHNob3VsZENhbmNlbCA9IGZ1bmN0aW9uIHNob3VsZENhbmNlbChyZXMpIHtcbiAgcmV0dXJuIHJlcyA9PT0gVEFTS19DQU5DRUw7XG59O1xudmFyIHNob3VsZENvbXBsZXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcGxldGUocmVzKSB7XG4gIHJldHVybiBzaG91bGRUZXJtaW5hdGUocmVzKSB8fCBzaG91bGRDYW5jZWwocmVzKTtcbn07XG5mdW5jdGlvbiBjcmVhdGVBbGxTdHlsZUNoaWxkQ2FsbGJhY2tzKHNoYXBlLCBwYXJlbnRDYWxsYmFjaykge1xuICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKHNoYXBlKTtcbiAgdmFyIHRvdGFsQ291bnQgPSBrZXlzLmxlbmd0aDtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIGNoZWNrKHRvdGFsQ291bnQsIGZ1bmN0aW9uIChjKSB7XG4gICAgICByZXR1cm4gYyA+IDA7XG4gICAgfSwgJ2NyZWF0ZUFsbFN0eWxlQ2hpbGRDYWxsYmFja3M6IGdldCBhbiBlbXB0eSBhcnJheSBvciBvYmplY3QnKTtcbiAgfVxuXG4gIHZhciBjb21wbGV0ZWRDb3VudCA9IDA7XG4gIHZhciBjb21wbGV0ZWQ7XG4gIHZhciByZXN1bHRzID0gYXJyYXkoc2hhcGUpID8gY3JlYXRlRW1wdHlBcnJheSh0b3RhbENvdW50KSA6IHt9O1xuICB2YXIgY2hpbGRDYWxsYmFja3MgPSB7fTtcblxuICBmdW5jdGlvbiBjaGVja0VuZCgpIHtcbiAgICBpZiAoY29tcGxldGVkQ291bnQgPT09IHRvdGFsQ291bnQpIHtcbiAgICAgIGNvbXBsZXRlZCA9IHRydWU7XG4gICAgICBwYXJlbnRDYWxsYmFjayhyZXN1bHRzKTtcbiAgICB9XG4gIH1cblxuICBrZXlzLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIHZhciBjaENiQXRLZXkgPSBmdW5jdGlvbiBjaENiQXRLZXkocmVzLCBpc0Vycikge1xuICAgICAgaWYgKGNvbXBsZXRlZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChpc0VyciB8fCBzaG91bGRDb21wbGV0ZShyZXMpKSB7XG4gICAgICAgIHBhcmVudENhbGxiYWNrLmNhbmNlbCgpO1xuICAgICAgICBwYXJlbnRDYWxsYmFjayhyZXMsIGlzRXJyKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc3VsdHNba2V5XSA9IHJlcztcbiAgICAgICAgY29tcGxldGVkQ291bnQrKztcbiAgICAgICAgY2hlY2tFbmQoKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgY2hDYkF0S2V5LmNhbmNlbCA9IG5vb3A7XG4gICAgY2hpbGRDYWxsYmFja3Nba2V5XSA9IGNoQ2JBdEtleTtcbiAgfSk7XG5cbiAgcGFyZW50Q2FsbGJhY2suY2FuY2VsID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICghY29tcGxldGVkKSB7XG4gICAgICBjb21wbGV0ZWQgPSB0cnVlO1xuICAgICAga2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgcmV0dXJuIGNoaWxkQ2FsbGJhY2tzW2tleV0uY2FuY2VsKCk7XG4gICAgICB9KTtcbiAgICB9XG4gIH07XG5cbiAgcmV0dXJuIGNoaWxkQ2FsbGJhY2tzO1xufVxuZnVuY3Rpb24gZ2V0TWV0YUluZm8oZm4pIHtcbiAgcmV0dXJuIHtcbiAgICBuYW1lOiBmbi5uYW1lIHx8ICdhbm9ueW1vdXMnLFxuICAgIGxvY2F0aW9uOiBnZXRMb2NhdGlvbihmbilcbiAgfTtcbn1cbmZ1bmN0aW9uIGdldExvY2F0aW9uKGluc3RydW1lbnRlZCkge1xuICByZXR1cm4gaW5zdHJ1bWVudGVkW1NBR0FfTE9DQVRJT05dO1xufVxuXG52YXIgQlVGRkVSX09WRVJGTE9XID0gXCJDaGFubmVsJ3MgQnVmZmVyIG92ZXJmbG93IVwiO1xudmFyIE9OX09WRVJGTE9XX1RIUk9XID0gMTtcbnZhciBPTl9PVkVSRkxPV19EUk9QID0gMjtcbnZhciBPTl9PVkVSRkxPV19TTElERSA9IDM7XG52YXIgT05fT1ZFUkZMT1dfRVhQQU5EID0gNDtcbnZhciB6ZXJvQnVmZmVyID0ge1xuICBpc0VtcHR5OiBrVHJ1ZSxcbiAgcHV0OiBub29wLFxuICB0YWtlOiBub29wXG59O1xuXG5mdW5jdGlvbiByaW5nQnVmZmVyKGxpbWl0LCBvdmVyZmxvd0FjdGlvbikge1xuICBpZiAobGltaXQgPT09IHZvaWQgMCkge1xuICAgIGxpbWl0ID0gMTA7XG4gIH1cblxuICB2YXIgYXJyID0gbmV3IEFycmF5KGxpbWl0KTtcbiAgdmFyIGxlbmd0aCA9IDA7XG4gIHZhciBwdXNoSW5kZXggPSAwO1xuICB2YXIgcG9wSW5kZXggPSAwO1xuXG4gIHZhciBwdXNoID0gZnVuY3Rpb24gcHVzaChpdCkge1xuICAgIGFycltwdXNoSW5kZXhdID0gaXQ7XG4gICAgcHVzaEluZGV4ID0gKHB1c2hJbmRleCArIDEpICUgbGltaXQ7XG4gICAgbGVuZ3RoKys7XG4gIH07XG5cbiAgdmFyIHRha2UgPSBmdW5jdGlvbiB0YWtlKCkge1xuICAgIGlmIChsZW5ndGggIT0gMCkge1xuICAgICAgdmFyIGl0ID0gYXJyW3BvcEluZGV4XTtcbiAgICAgIGFycltwb3BJbmRleF0gPSBudWxsO1xuICAgICAgbGVuZ3RoLS07XG4gICAgICBwb3BJbmRleCA9IChwb3BJbmRleCArIDEpICUgbGltaXQ7XG4gICAgICByZXR1cm4gaXQ7XG4gICAgfVxuICB9O1xuXG4gIHZhciBmbHVzaCA9IGZ1bmN0aW9uIGZsdXNoKCkge1xuICAgIHZhciBpdGVtcyA9IFtdO1xuXG4gICAgd2hpbGUgKGxlbmd0aCkge1xuICAgICAgaXRlbXMucHVzaCh0YWtlKCkpO1xuICAgIH1cblxuICAgIHJldHVybiBpdGVtcztcbiAgfTtcblxuICByZXR1cm4ge1xuICAgIGlzRW1wdHk6IGZ1bmN0aW9uIGlzRW1wdHkoKSB7XG4gICAgICByZXR1cm4gbGVuZ3RoID09IDA7XG4gICAgfSxcbiAgICBwdXQ6IGZ1bmN0aW9uIHB1dChpdCkge1xuICAgICAgaWYgKGxlbmd0aCA8IGxpbWl0KSB7XG4gICAgICAgIHB1c2goaXQpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdmFyIGRvdWJsZWRMaW1pdDtcblxuICAgICAgICBzd2l0Y2ggKG92ZXJmbG93QWN0aW9uKSB7XG4gICAgICAgICAgY2FzZSBPTl9PVkVSRkxPV19USFJPVzpcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihCVUZGRVJfT1ZFUkZMT1cpO1xuXG4gICAgICAgICAgY2FzZSBPTl9PVkVSRkxPV19TTElERTpcbiAgICAgICAgICAgIGFycltwdXNoSW5kZXhdID0gaXQ7XG4gICAgICAgICAgICBwdXNoSW5kZXggPSAocHVzaEluZGV4ICsgMSkgJSBsaW1pdDtcbiAgICAgICAgICAgIHBvcEluZGV4ID0gcHVzaEluZGV4O1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBjYXNlIE9OX09WRVJGTE9XX0VYUEFORDpcbiAgICAgICAgICAgIGRvdWJsZWRMaW1pdCA9IDIgKiBsaW1pdDtcbiAgICAgICAgICAgIGFyciA9IGZsdXNoKCk7XG4gICAgICAgICAgICBsZW5ndGggPSBhcnIubGVuZ3RoO1xuICAgICAgICAgICAgcHVzaEluZGV4ID0gYXJyLmxlbmd0aDtcbiAgICAgICAgICAgIHBvcEluZGV4ID0gMDtcbiAgICAgICAgICAgIGFyci5sZW5ndGggPSBkb3VibGVkTGltaXQ7XG4gICAgICAgICAgICBsaW1pdCA9IGRvdWJsZWRMaW1pdDtcbiAgICAgICAgICAgIHB1c2goaXQpO1xuICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICBkZWZhdWx0OiAvLyBEUk9QXG5cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG4gICAgdGFrZTogdGFrZSxcbiAgICBmbHVzaDogZmx1c2hcbiAgfTtcbn1cblxudmFyIG5vbmUgPSBmdW5jdGlvbiBub25lKCkge1xuICByZXR1cm4gemVyb0J1ZmZlcjtcbn07XG52YXIgZml4ZWQgPSBmdW5jdGlvbiBmaXhlZChsaW1pdCkge1xuICByZXR1cm4gcmluZ0J1ZmZlcihsaW1pdCwgT05fT1ZFUkZMT1dfVEhST1cpO1xufTtcbnZhciBkcm9wcGluZyA9IGZ1bmN0aW9uIGRyb3BwaW5nKGxpbWl0KSB7XG4gIHJldHVybiByaW5nQnVmZmVyKGxpbWl0LCBPTl9PVkVSRkxPV19EUk9QKTtcbn07XG52YXIgc2xpZGluZyA9IGZ1bmN0aW9uIHNsaWRpbmcobGltaXQpIHtcbiAgcmV0dXJuIHJpbmdCdWZmZXIobGltaXQsIE9OX09WRVJGTE9XX1NMSURFKTtcbn07XG52YXIgZXhwYW5kaW5nID0gZnVuY3Rpb24gZXhwYW5kaW5nKGluaXRpYWxTaXplKSB7XG4gIHJldHVybiByaW5nQnVmZmVyKGluaXRpYWxTaXplLCBPTl9PVkVSRkxPV19FWFBBTkQpO1xufTtcblxudmFyIGJ1ZmZlcnMgPSAvKiNfX1BVUkVfXyovT2JqZWN0LmZyZWV6ZSh7XG4gIF9fcHJvdG9fXzogbnVsbCxcbiAgbm9uZTogbm9uZSxcbiAgZml4ZWQ6IGZpeGVkLFxuICBkcm9wcGluZzogZHJvcHBpbmcsXG4gIHNsaWRpbmc6IHNsaWRpbmcsXG4gIGV4cGFuZGluZzogZXhwYW5kaW5nXG59KTtcblxudmFyIFRBS0UgPSAnVEFLRSc7XG52YXIgUFVUID0gJ1BVVCc7XG52YXIgQUxMID0gJ0FMTCc7XG52YXIgUkFDRSA9ICdSQUNFJztcbnZhciBDQUxMID0gJ0NBTEwnO1xudmFyIENQUyA9ICdDUFMnO1xudmFyIEZPUksgPSAnRk9SSyc7XG52YXIgSk9JTiA9ICdKT0lOJztcbnZhciBDQU5DRUwgPSAnQ0FOQ0VMJztcbnZhciBTRUxFQ1QgPSAnU0VMRUNUJztcbnZhciBBQ1RJT05fQ0hBTk5FTCA9ICdBQ1RJT05fQ0hBTk5FTCc7XG52YXIgQ0FOQ0VMTEVEID0gJ0NBTkNFTExFRCc7XG52YXIgRkxVU0ggPSAnRkxVU0gnO1xudmFyIEdFVF9DT05URVhUID0gJ0dFVF9DT05URVhUJztcbnZhciBTRVRfQ09OVEVYVCA9ICdTRVRfQ09OVEVYVCc7XG5cbnZhciBlZmZlY3RUeXBlcyA9IC8qI19fUFVSRV9fKi9PYmplY3QuZnJlZXplKHtcbiAgX19wcm90b19fOiBudWxsLFxuICBUQUtFOiBUQUtFLFxuICBQVVQ6IFBVVCxcbiAgQUxMOiBBTEwsXG4gIFJBQ0U6IFJBQ0UsXG4gIENBTEw6IENBTEwsXG4gIENQUzogQ1BTLFxuICBGT1JLOiBGT1JLLFxuICBKT0lOOiBKT0lOLFxuICBDQU5DRUw6IENBTkNFTCxcbiAgU0VMRUNUOiBTRUxFQ1QsXG4gIEFDVElPTl9DSEFOTkVMOiBBQ1RJT05fQ0hBTk5FTCxcbiAgQ0FOQ0VMTEVEOiBDQU5DRUxMRUQsXG4gIEZMVVNIOiBGTFVTSCxcbiAgR0VUX0NPTlRFWFQ6IEdFVF9DT05URVhULFxuICBTRVRfQ09OVEVYVDogU0VUX0NPTlRFWFRcbn0pO1xuXG52YXIgVEVTVF9ISU5UID0gJ1xcbihISU5UOiBpZiB5b3UgYXJlIGdldHRpbmcgdGhlc2UgZXJyb3JzIGluIHRlc3RzLCBjb25zaWRlciB1c2luZyBjcmVhdGVNb2NrVGFzayBmcm9tIEByZWR1eC1zYWdhL3Rlc3RpbmctdXRpbHMpJztcblxudmFyIG1ha2VFZmZlY3QgPSBmdW5jdGlvbiBtYWtlRWZmZWN0KHR5cGUsIHBheWxvYWQpIHtcbiAgdmFyIF9yZWY7XG5cbiAgcmV0dXJuIF9yZWYgPSB7fSwgX3JlZltJT10gPSB0cnVlLCBfcmVmLmNvbWJpbmF0b3IgPSBmYWxzZSwgX3JlZi50eXBlID0gdHlwZSwgX3JlZi5wYXlsb2FkID0gcGF5bG9hZCwgX3JlZjtcbn07XG5cbnZhciBpc0ZvcmtFZmZlY3QgPSBmdW5jdGlvbiBpc0ZvcmtFZmZlY3QoZWZmKSB7XG4gIHJldHVybiBlZmZlY3QoZWZmKSAmJiBlZmYudHlwZSA9PT0gRk9SSztcbn07XG5cbnZhciBkZXRhY2ggPSBmdW5jdGlvbiBkZXRhY2goZWZmKSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgY2hlY2soZWZmLCBpc0ZvcmtFZmZlY3QsICdkZXRhY2goZWZmKTogYXJndW1lbnQgbXVzdCBiZSBhIGZvcmsgZWZmZWN0Jyk7XG4gIH1cblxuICByZXR1cm4gbWFrZUVmZmVjdChGT1JLLCBfZXh0ZW5kcyh7fSwgZWZmLnBheWxvYWQsIHtcbiAgICBkZXRhY2hlZDogdHJ1ZVxuICB9KSk7XG59O1xuZnVuY3Rpb24gdGFrZShwYXR0ZXJuT3JDaGFubmVsLCBtdWx0aWNhc3RQYXR0ZXJuKSB7XG4gIGlmIChwYXR0ZXJuT3JDaGFubmVsID09PSB2b2lkIDApIHtcbiAgICBwYXR0ZXJuT3JDaGFubmVsID0gJyonO1xuICB9XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgYXJndW1lbnRzLmxlbmd0aCkge1xuICAgIGNoZWNrKGFyZ3VtZW50c1swXSwgbm90VW5kZWYsICd0YWtlKHBhdHRlcm5PckNoYW5uZWwpOiBwYXR0ZXJuT3JDaGFubmVsIGlzIHVuZGVmaW5lZCcpO1xuICB9XG5cbiAgaWYgKHBhdHRlcm4ocGF0dGVybk9yQ2hhbm5lbCkpIHtcbiAgICByZXR1cm4gbWFrZUVmZmVjdChUQUtFLCB7XG4gICAgICBwYXR0ZXJuOiBwYXR0ZXJuT3JDaGFubmVsXG4gICAgfSk7XG4gIH1cblxuICBpZiAobXVsdGljYXN0KHBhdHRlcm5PckNoYW5uZWwpICYmIG5vdFVuZGVmKG11bHRpY2FzdFBhdHRlcm4pICYmIHBhdHRlcm4obXVsdGljYXN0UGF0dGVybikpIHtcbiAgICByZXR1cm4gbWFrZUVmZmVjdChUQUtFLCB7XG4gICAgICBjaGFubmVsOiBwYXR0ZXJuT3JDaGFubmVsLFxuICAgICAgcGF0dGVybjogbXVsdGljYXN0UGF0dGVyblxuICAgIH0pO1xuICB9XG5cbiAgaWYgKGNoYW5uZWwocGF0dGVybk9yQ2hhbm5lbCkpIHtcbiAgICByZXR1cm4gbWFrZUVmZmVjdChUQUtFLCB7XG4gICAgICBjaGFubmVsOiBwYXR0ZXJuT3JDaGFubmVsXG4gICAgfSk7XG4gIH1cblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHRocm93IG5ldyBFcnJvcihcInRha2UocGF0dGVybk9yQ2hhbm5lbCk6IGFyZ3VtZW50IFwiICsgcGF0dGVybk9yQ2hhbm5lbCArIFwiIGlzIG5vdCB2YWxpZCBjaGFubmVsIG9yIGEgdmFsaWQgcGF0dGVyblwiKTtcbiAgfVxufVxudmFyIHRha2VNYXliZSA9IGZ1bmN0aW9uIHRha2VNYXliZSgpIHtcbiAgdmFyIGVmZiA9IHRha2UuYXBwbHkodm9pZCAwLCBhcmd1bWVudHMpO1xuICBlZmYucGF5bG9hZC5tYXliZSA9IHRydWU7XG4gIHJldHVybiBlZmY7XG59O1xuZnVuY3Rpb24gcHV0KGNoYW5uZWwkMSwgYWN0aW9uKSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPiAxKSB7XG4gICAgICBjaGVjayhjaGFubmVsJDEsIG5vdFVuZGVmLCAncHV0KGNoYW5uZWwsIGFjdGlvbik6IGFyZ3VtZW50IGNoYW5uZWwgaXMgdW5kZWZpbmVkJyk7XG4gICAgICBjaGVjayhjaGFubmVsJDEsIGNoYW5uZWwsIFwicHV0KGNoYW5uZWwsIGFjdGlvbik6IGFyZ3VtZW50IFwiICsgY2hhbm5lbCQxICsgXCIgaXMgbm90IGEgdmFsaWQgY2hhbm5lbFwiKTtcbiAgICAgIGNoZWNrKGFjdGlvbiwgbm90VW5kZWYsICdwdXQoY2hhbm5lbCwgYWN0aW9uKTogYXJndW1lbnQgYWN0aW9uIGlzIHVuZGVmaW5lZCcpO1xuICAgIH0gZWxzZSB7XG4gICAgICBjaGVjayhjaGFubmVsJDEsIG5vdFVuZGVmLCAncHV0KGFjdGlvbik6IGFyZ3VtZW50IGFjdGlvbiBpcyB1bmRlZmluZWQnKTtcbiAgICB9XG4gIH1cblxuICBpZiAodW5kZWYoYWN0aW9uKSkge1xuICAgIGFjdGlvbiA9IGNoYW5uZWwkMTsgLy8gYHVuZGVmaW5lZGAgaW5zdGVhZCBvZiBgbnVsbGAgdG8gbWFrZSBkZWZhdWx0IHBhcmFtZXRlciB3b3JrXG5cbiAgICBjaGFubmVsJDEgPSB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gbWFrZUVmZmVjdChQVVQsIHtcbiAgICBjaGFubmVsOiBjaGFubmVsJDEsXG4gICAgYWN0aW9uOiBhY3Rpb25cbiAgfSk7XG59XG52YXIgcHV0UmVzb2x2ZSA9IGZ1bmN0aW9uIHB1dFJlc29sdmUoKSB7XG4gIHZhciBlZmYgPSBwdXQuYXBwbHkodm9pZCAwLCBhcmd1bWVudHMpO1xuICBlZmYucGF5bG9hZC5yZXNvbHZlID0gdHJ1ZTtcbiAgcmV0dXJuIGVmZjtcbn07XG5mdW5jdGlvbiBhbGwoZWZmZWN0cykge1xuICB2YXIgZWZmID0gbWFrZUVmZmVjdChBTEwsIGVmZmVjdHMpO1xuICBlZmYuY29tYmluYXRvciA9IHRydWU7XG4gIHJldHVybiBlZmY7XG59XG5mdW5jdGlvbiByYWNlKGVmZmVjdHMpIHtcbiAgdmFyIGVmZiA9IG1ha2VFZmZlY3QoUkFDRSwgZWZmZWN0cyk7XG4gIGVmZi5jb21iaW5hdG9yID0gdHJ1ZTtcbiAgcmV0dXJuIGVmZjtcbn0gLy8gdGhpcyBtYXRjaCBnZXRGbkNhbGxEZXNjcmlwdG9yIGxvZ2ljXG5cbnZhciB2YWxpZGF0ZUZuRGVzY3JpcHRvciA9IGZ1bmN0aW9uIHZhbGlkYXRlRm5EZXNjcmlwdG9yKGVmZmVjdE5hbWUsIGZuRGVzY3JpcHRvcikge1xuICBjaGVjayhmbkRlc2NyaXB0b3IsIG5vdFVuZGVmLCBlZmZlY3ROYW1lICsgXCI6IGFyZ3VtZW50IGZuIGlzIHVuZGVmaW5lZCBvciBudWxsXCIpO1xuXG4gIGlmIChmdW5jKGZuRGVzY3JpcHRvcikpIHtcbiAgICByZXR1cm47XG4gIH1cblxuICB2YXIgY29udGV4dCA9IG51bGw7XG4gIHZhciBmbjtcblxuICBpZiAoYXJyYXkoZm5EZXNjcmlwdG9yKSkge1xuICAgIGNvbnRleHQgPSBmbkRlc2NyaXB0b3JbMF07XG4gICAgZm4gPSBmbkRlc2NyaXB0b3JbMV07XG4gICAgY2hlY2soZm4sIG5vdFVuZGVmLCBlZmZlY3ROYW1lICsgXCI6IGFyZ3VtZW50IG9mIHR5cGUgW2NvbnRleHQsIGZuXSBoYXMgdW5kZWZpbmVkIG9yIG51bGwgYGZuYFwiKTtcbiAgfSBlbHNlIGlmIChvYmplY3QoZm5EZXNjcmlwdG9yKSkge1xuICAgIGNvbnRleHQgPSBmbkRlc2NyaXB0b3IuY29udGV4dDtcbiAgICBmbiA9IGZuRGVzY3JpcHRvci5mbjtcbiAgICBjaGVjayhmbiwgbm90VW5kZWYsIGVmZmVjdE5hbWUgKyBcIjogYXJndW1lbnQgb2YgdHlwZSB7Y29udGV4dCwgZm59IGhhcyB1bmRlZmluZWQgb3IgbnVsbCBgZm5gXCIpO1xuICB9IGVsc2Uge1xuICAgIGNoZWNrKGZuRGVzY3JpcHRvciwgZnVuYywgZWZmZWN0TmFtZSArIFwiOiBhcmd1bWVudCBmbiBpcyBub3QgZnVuY3Rpb25cIik7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKGNvbnRleHQgJiYgc3RyaW5nKGZuKSkge1xuICAgIGNoZWNrKGNvbnRleHRbZm5dLCBmdW5jLCBlZmZlY3ROYW1lICsgXCI6IGNvbnRleHQgYXJndW1lbnRzIGhhcyBubyBzdWNoIG1ldGhvZCAtIFxcXCJcIiArIGZuICsgXCJcXFwiXCIpO1xuICAgIHJldHVybjtcbiAgfVxuXG4gIGNoZWNrKGZuLCBmdW5jLCBlZmZlY3ROYW1lICsgXCI6IHVucGFja2VkIGZuIGFyZ3VtZW50IChmcm9tIFtjb250ZXh0LCBmbl0gb3Ige2NvbnRleHQsIGZufSkgaXMgbm90IGEgZnVuY3Rpb25cIik7XG59O1xuXG5mdW5jdGlvbiBnZXRGbkNhbGxEZXNjcmlwdG9yKGZuRGVzY3JpcHRvciwgYXJncykge1xuICB2YXIgY29udGV4dCA9IG51bGw7XG4gIHZhciBmbjtcblxuICBpZiAoZnVuYyhmbkRlc2NyaXB0b3IpKSB7XG4gICAgZm4gPSBmbkRlc2NyaXB0b3I7XG4gIH0gZWxzZSB7XG4gICAgaWYgKGFycmF5KGZuRGVzY3JpcHRvcikpIHtcbiAgICAgIGNvbnRleHQgPSBmbkRlc2NyaXB0b3JbMF07XG4gICAgICBmbiA9IGZuRGVzY3JpcHRvclsxXTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29udGV4dCA9IGZuRGVzY3JpcHRvci5jb250ZXh0O1xuICAgICAgZm4gPSBmbkRlc2NyaXB0b3IuZm47XG4gICAgfVxuXG4gICAgaWYgKGNvbnRleHQgJiYgc3RyaW5nKGZuKSAmJiBmdW5jKGNvbnRleHRbZm5dKSkge1xuICAgICAgZm4gPSBjb250ZXh0W2ZuXTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4ge1xuICAgIGNvbnRleHQ6IGNvbnRleHQsXG4gICAgZm46IGZuLFxuICAgIGFyZ3M6IGFyZ3NcbiAgfTtcbn1cblxudmFyIGlzTm90RGVsYXlFZmZlY3QgPSBmdW5jdGlvbiBpc05vdERlbGF5RWZmZWN0KGZuKSB7XG4gIHJldHVybiBmbiAhPT0gZGVsYXk7XG59O1xuXG5mdW5jdGlvbiBjYWxsKGZuRGVzY3JpcHRvcikge1xuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IG5ldyBBcnJheShfbGVuID4gMSA/IF9sZW4gLSAxIDogMCksIF9rZXkgPSAxOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgYXJnc1tfa2V5IC0gMV0gPSBhcmd1bWVudHNbX2tleV07XG4gIH1cblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhciBhcmcwID0gdHlwZW9mIGFyZ3NbMF0gPT09ICdudW1iZXInID8gYXJnc1swXSA6ICdtcyc7XG4gICAgY2hlY2soZm5EZXNjcmlwdG9yLCBpc05vdERlbGF5RWZmZWN0LCBcImluc3RlYWQgb2Ygd3JpdGluZyBgeWllbGQgY2FsbChkZWxheSwgXCIgKyBhcmcwICsgXCIpYCB3aGVyZSBkZWxheSBpcyBhbiBlZmZlY3QgZnJvbSBgcmVkdXgtc2FnYS9lZmZlY3RzYCB5b3Ugc2hvdWxkIHdyaXRlIGB5aWVsZCBkZWxheShcIiArIGFyZzAgKyBcIilgXCIpO1xuICAgIHZhbGlkYXRlRm5EZXNjcmlwdG9yKCdjYWxsJywgZm5EZXNjcmlwdG9yKTtcbiAgfVxuXG4gIHJldHVybiBtYWtlRWZmZWN0KENBTEwsIGdldEZuQ2FsbERlc2NyaXB0b3IoZm5EZXNjcmlwdG9yLCBhcmdzKSk7XG59XG5mdW5jdGlvbiBhcHBseShjb250ZXh0LCBmbiwgYXJncykge1xuICBpZiAoYXJncyA9PT0gdm9pZCAwKSB7XG4gICAgYXJncyA9IFtdO1xuICB9XG5cbiAgdmFyIGZuRGVzY3JpcHRvciA9IFtjb250ZXh0LCBmbl07XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICB2YWxpZGF0ZUZuRGVzY3JpcHRvcignYXBwbHknLCBmbkRlc2NyaXB0b3IpO1xuICB9XG5cbiAgcmV0dXJuIG1ha2VFZmZlY3QoQ0FMTCwgZ2V0Rm5DYWxsRGVzY3JpcHRvcihbY29udGV4dCwgZm5dLCBhcmdzKSk7XG59XG5mdW5jdGlvbiBjcHMoZm5EZXNjcmlwdG9yKSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgdmFsaWRhdGVGbkRlc2NyaXB0b3IoJ2NwcycsIGZuRGVzY3JpcHRvcik7XG4gIH1cblxuICBmb3IgKHZhciBfbGVuMiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbjIgPiAxID8gX2xlbjIgLSAxIDogMCksIF9rZXkyID0gMTsgX2tleTIgPCBfbGVuMjsgX2tleTIrKykge1xuICAgIGFyZ3NbX2tleTIgLSAxXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gIH1cblxuICByZXR1cm4gbWFrZUVmZmVjdChDUFMsIGdldEZuQ2FsbERlc2NyaXB0b3IoZm5EZXNjcmlwdG9yLCBhcmdzKSk7XG59XG5mdW5jdGlvbiBmb3JrKGZuRGVzY3JpcHRvcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhbGlkYXRlRm5EZXNjcmlwdG9yKCdmb3JrJywgZm5EZXNjcmlwdG9yKTtcbiAgICBjaGVjayhmbkRlc2NyaXB0b3IsIGZ1bmN0aW9uIChhcmcpIHtcbiAgICAgIHJldHVybiAhZWZmZWN0KGFyZyk7XG4gICAgfSwgJ2Zvcms6IGFyZ3VtZW50IG11c3Qgbm90IGJlIGFuIGVmZmVjdCcpO1xuICB9XG5cbiAgZm9yICh2YXIgX2xlbjMgPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4zID4gMSA/IF9sZW4zIC0gMSA6IDApLCBfa2V5MyA9IDE7IF9rZXkzIDwgX2xlbjM7IF9rZXkzKyspIHtcbiAgICBhcmdzW19rZXkzIC0gMV0gPSBhcmd1bWVudHNbX2tleTNdO1xuICB9XG5cbiAgcmV0dXJuIG1ha2VFZmZlY3QoRk9SSywgZ2V0Rm5DYWxsRGVzY3JpcHRvcihmbkRlc2NyaXB0b3IsIGFyZ3MpKTtcbn1cbmZ1bmN0aW9uIHNwYXduKGZuRGVzY3JpcHRvcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhbGlkYXRlRm5EZXNjcmlwdG9yKCdzcGF3bicsIGZuRGVzY3JpcHRvcik7XG4gIH1cblxuICBmb3IgKHZhciBfbGVuNCA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbjQgPiAxID8gX2xlbjQgLSAxIDogMCksIF9rZXk0ID0gMTsgX2tleTQgPCBfbGVuNDsgX2tleTQrKykge1xuICAgIGFyZ3NbX2tleTQgLSAxXSA9IGFyZ3VtZW50c1tfa2V5NF07XG4gIH1cblxuICByZXR1cm4gZGV0YWNoKGZvcmsuYXBwbHkodm9pZCAwLCBbZm5EZXNjcmlwdG9yXS5jb25jYXQoYXJncykpKTtcbn1cbmZ1bmN0aW9uIGpvaW4odGFza09yVGFza3MpIHtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA+IDEpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignam9pbiguLi50YXNrcykgaXMgbm90IHN1cHBvcnRlZCBhbnkgbW9yZS4gUGxlYXNlIHVzZSBqb2luKFsuLi50YXNrc10pIHRvIGpvaW4gbXVsdGlwbGUgdGFza3MuJyk7XG4gICAgfVxuXG4gICAgaWYgKGFycmF5KHRhc2tPclRhc2tzKSkge1xuICAgICAgdGFza09yVGFza3MuZm9yRWFjaChmdW5jdGlvbiAodCkge1xuICAgICAgICBjaGVjayh0LCB0YXNrLCBcImpvaW4oWy4uLnRhc2tzXSk6IGFyZ3VtZW50IFwiICsgdCArIFwiIGlzIG5vdCBhIHZhbGlkIFRhc2sgb2JqZWN0IFwiICsgVEVTVF9ISU5UKTtcbiAgICAgIH0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBjaGVjayh0YXNrT3JUYXNrcywgdGFzaywgXCJqb2luKHRhc2spOiBhcmd1bWVudCBcIiArIHRhc2tPclRhc2tzICsgXCIgaXMgbm90IGEgdmFsaWQgVGFzayBvYmplY3QgXCIgKyBURVNUX0hJTlQpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBtYWtlRWZmZWN0KEpPSU4sIHRhc2tPclRhc2tzKTtcbn1cbmZ1bmN0aW9uIGNhbmNlbCh0YXNrT3JUYXNrcykge1xuICBpZiAodGFza09yVGFza3MgPT09IHZvaWQgMCkge1xuICAgIHRhc2tPclRhc2tzID0gU0VMRl9DQU5DRUxMQVRJT047XG4gIH1cblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdjYW5jZWwoLi4udGFza3MpIGlzIG5vdCBzdXBwb3J0ZWQgYW55IG1vcmUuIFBsZWFzZSB1c2UgY2FuY2VsKFsuLi50YXNrc10pIHRvIGNhbmNlbCBtdWx0aXBsZSB0YXNrcy4nKTtcbiAgICB9XG5cbiAgICBpZiAoYXJyYXkodGFza09yVGFza3MpKSB7XG4gICAgICB0YXNrT3JUYXNrcy5mb3JFYWNoKGZ1bmN0aW9uICh0KSB7XG4gICAgICAgIGNoZWNrKHQsIHRhc2ssIFwiY2FuY2VsKFsuLi50YXNrc10pOiBhcmd1bWVudCBcIiArIHQgKyBcIiBpcyBub3QgYSB2YWxpZCBUYXNrIG9iamVjdCBcIiArIFRFU1RfSElOVCk7XG4gICAgICB9KTtcbiAgICB9IGVsc2UgaWYgKHRhc2tPclRhc2tzICE9PSBTRUxGX0NBTkNFTExBVElPTiAmJiBub3RVbmRlZih0YXNrT3JUYXNrcykpIHtcbiAgICAgIGNoZWNrKHRhc2tPclRhc2tzLCB0YXNrLCBcImNhbmNlbCh0YXNrKTogYXJndW1lbnQgXCIgKyB0YXNrT3JUYXNrcyArIFwiIGlzIG5vdCBhIHZhbGlkIFRhc2sgb2JqZWN0IFwiICsgVEVTVF9ISU5UKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWFrZUVmZmVjdChDQU5DRUwsIHRhc2tPclRhc2tzKTtcbn1cbmZ1bmN0aW9uIHNlbGVjdChzZWxlY3Rvcikge1xuICBpZiAoc2VsZWN0b3IgPT09IHZvaWQgMCkge1xuICAgIHNlbGVjdG9yID0gaWRlbnRpdHk7XG4gIH1cblxuICBmb3IgKHZhciBfbGVuNSA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbjUgPiAxID8gX2xlbjUgLSAxIDogMCksIF9rZXk1ID0gMTsgX2tleTUgPCBfbGVuNTsgX2tleTUrKykge1xuICAgIGFyZ3NbX2tleTUgLSAxXSA9IGFyZ3VtZW50c1tfa2V5NV07XG4gIH1cblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJyAmJiBhcmd1bWVudHMubGVuZ3RoKSB7XG4gICAgY2hlY2soYXJndW1lbnRzWzBdLCBub3RVbmRlZiwgJ3NlbGVjdChzZWxlY3RvciwgWy4uLl0pOiBhcmd1bWVudCBzZWxlY3RvciBpcyB1bmRlZmluZWQnKTtcbiAgICBjaGVjayhzZWxlY3RvciwgZnVuYywgXCJzZWxlY3Qoc2VsZWN0b3IsIFsuLi5dKTogYXJndW1lbnQgXCIgKyBzZWxlY3RvciArIFwiIGlzIG5vdCBhIGZ1bmN0aW9uXCIpO1xuICB9XG5cbiAgcmV0dXJuIG1ha2VFZmZlY3QoU0VMRUNULCB7XG4gICAgc2VsZWN0b3I6IHNlbGVjdG9yLFxuICAgIGFyZ3M6IGFyZ3NcbiAgfSk7XG59XG4vKipcbiAgY2hhbm5lbChwYXR0ZXJuLCBbYnVmZmVyXSkgICAgPT4gY3JlYXRlcyBhIHByb3h5IGNoYW5uZWwgZm9yIHN0b3JlIGFjdGlvbnNcbioqL1xuXG5mdW5jdGlvbiBhY3Rpb25DaGFubmVsKHBhdHRlcm4kMSwgYnVmZmVyJDEpIHtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBjaGVjayhwYXR0ZXJuJDEsIHBhdHRlcm4sICdhY3Rpb25DaGFubmVsKHBhdHRlcm4sLi4uKTogYXJndW1lbnQgcGF0dGVybiBpcyBub3QgdmFsaWQnKTtcblxuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgY2hlY2soYnVmZmVyJDEsIG5vdFVuZGVmLCAnYWN0aW9uQ2hhbm5lbChwYXR0ZXJuLCBidWZmZXIpOiBhcmd1bWVudCBidWZmZXIgaXMgdW5kZWZpbmVkJyk7XG4gICAgICBjaGVjayhidWZmZXIkMSwgYnVmZmVyLCBcImFjdGlvbkNoYW5uZWwocGF0dGVybiwgYnVmZmVyKTogYXJndW1lbnQgXCIgKyBidWZmZXIkMSArIFwiIGlzIG5vdCBhIHZhbGlkIGJ1ZmZlclwiKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWFrZUVmZmVjdChBQ1RJT05fQ0hBTk5FTCwge1xuICAgIHBhdHRlcm46IHBhdHRlcm4kMSxcbiAgICBidWZmZXI6IGJ1ZmZlciQxXG4gIH0pO1xufVxuZnVuY3Rpb24gY2FuY2VsbGVkKCkge1xuICByZXR1cm4gbWFrZUVmZmVjdChDQU5DRUxMRUQsIHt9KTtcbn1cbmZ1bmN0aW9uIGZsdXNoKGNoYW5uZWwkMSkge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIGNoZWNrKGNoYW5uZWwkMSwgY2hhbm5lbCwgXCJmbHVzaChjaGFubmVsKTogYXJndW1lbnQgXCIgKyBjaGFubmVsJDEgKyBcIiBpcyBub3QgdmFsaWQgY2hhbm5lbFwiKTtcbiAgfVxuXG4gIHJldHVybiBtYWtlRWZmZWN0KEZMVVNILCBjaGFubmVsJDEpO1xufVxuZnVuY3Rpb24gZ2V0Q29udGV4dChwcm9wKSB7XG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgY2hlY2socHJvcCwgc3RyaW5nLCBcImdldENvbnRleHQocHJvcCk6IGFyZ3VtZW50IFwiICsgcHJvcCArIFwiIGlzIG5vdCBhIHN0cmluZ1wiKTtcbiAgfVxuXG4gIHJldHVybiBtYWtlRWZmZWN0KEdFVF9DT05URVhULCBwcm9wKTtcbn1cbmZ1bmN0aW9uIHNldENvbnRleHQocHJvcHMpIHtcbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICBjaGVjayhwcm9wcywgb2JqZWN0LCBjcmVhdGVTZXRDb250ZXh0V2FybmluZyhudWxsLCBwcm9wcykpO1xuICB9XG5cbiAgcmV0dXJuIG1ha2VFZmZlY3QoU0VUX0NPTlRFWFQsIHByb3BzKTtcbn1cbnZhciBkZWxheSA9XG4vKiNfX1BVUkVfXyovXG5jYWxsLmJpbmQobnVsbCwgZGVsYXlQKTtcblxuZXhwb3J0IHsgYXBwbHkgYXMgJCwgQUxMIGFzIEEsIGxvZ0Vycm9yIGFzIEIsIENBTEwgYXMgQywgd3JhcFNhZ2FEaXNwYXRjaCBhcyBELCBpZGVudGl0eSBhcyBFLCBGT1JLIGFzIEYsIEdFVF9DT05URVhUIGFzIEcsIGJ1ZmZlcnMgYXMgSCwgZGV0YWNoIGFzIEksIEpPSU4gYXMgSiwgdGFrZSBhcyBLLCBmb3JrIGFzIEwsIGNhbmNlbCBhcyBNLCBjYWxsIGFzIE4sIGFjdGlvbkNoYW5uZWwgYXMgTywgUFVUIGFzIFAsIHNsaWRpbmcgYXMgUSwgUkFDRSBhcyBSLCBTRUxFQ1QgYXMgUywgVEFLRSBhcyBULCBkZWxheSBhcyBVLCByYWNlIGFzIFYsIGVmZmVjdFR5cGVzIGFzIFcsIHRha2VNYXliZSBhcyBYLCBwdXQgYXMgWSwgcHV0UmVzb2x2ZSBhcyBaLCBhbGwgYXMgXywgQ1BTIGFzIGEsIGNwcyBhcyBhMCwgc3Bhd24gYXMgYTEsIGpvaW4gYXMgYTIsIHNlbGVjdCBhcyBhMywgY2FuY2VsbGVkIGFzIGE0LCBmbHVzaCBhcyBhNSwgZ2V0Q29udGV4dCBhcyBhNiwgc2V0Q29udGV4dCBhcyBhNywgQ0FOQ0VMIGFzIGIsIGNoZWNrIGFzIGMsIEFDVElPTl9DSEFOTkVMIGFzIGQsIGV4cGFuZGluZyBhcyBlLCBDQU5DRUxMRUQgYXMgZiwgRkxVU0ggYXMgZywgU0VUX0NPTlRFWFQgYXMgaCwgaW50ZXJuYWxFcnIgYXMgaSwgZ2V0TWV0YUluZm8gYXMgaiwga1RydWUgYXMgaywgY3JlYXRlQWxsU3R5bGVDaGlsZENhbGxiYWNrcyBhcyBsLCBjcmVhdGVFbXB0eUFycmF5IGFzIG0sIG5vbmUgYXMgbiwgb25jZSBhcyBvLCBhc3NpZ25XaXRoU3ltYm9scyBhcyBwLCBtYWtlSXRlcmF0b3IgYXMgcSwgcmVtb3ZlIGFzIHIsIHNob3VsZENvbXBsZXRlIGFzIHMsIG5vb3AgYXMgdCwgZmxhdE1hcCBhcyB1LCBnZXRMb2NhdGlvbiBhcyB2LCBjcmVhdGVTZXRDb250ZXh0V2FybmluZyBhcyB3LCBhc3luY0l0ZXJhdG9yU3ltYm9sIGFzIHgsIHNob3VsZENhbmNlbCBhcyB5LCBzaG91bGRUZXJtaW5hdGUgYXMgeiB9O1xuIiwiaW1wb3J0ICdAcmVkdXgtc2FnYS9zeW1ib2xzJztcbmltcG9ydCAnQGJhYmVsL3J1bnRpbWUvaGVscGVycy9lc20vZXh0ZW5kcyc7XG5pbXBvcnQgeyBjaGFubmVsLCBzdHJpbmdhYmxlRnVuYywgZnVuYywgbm90VW5kZWYgfSBmcm9tICdAcmVkdXgtc2FnYS9pcyc7XG5pbXBvcnQgeyBxIGFzIG1ha2VJdGVyYXRvciwgSyBhcyB0YWtlLCBMIGFzIGZvcmssIE0gYXMgY2FuY2VsLCBOIGFzIGNhbGwsIE8gYXMgYWN0aW9uQ2hhbm5lbCwgUSBhcyBzbGlkaW5nLCBVIGFzIGRlbGF5LCBWIGFzIHJhY2UsIGMgYXMgY2hlY2sgfSBmcm9tICcuL2lvLTZkZTE1NmYzLmpzJztcbmV4cG9ydCB7IE8gYXMgYWN0aW9uQ2hhbm5lbCwgXyBhcyBhbGwsICQgYXMgYXBwbHksIE4gYXMgY2FsbCwgTSBhcyBjYW5jZWwsIGE0IGFzIGNhbmNlbGxlZCwgYTAgYXMgY3BzLCBVIGFzIGRlbGF5LCBXIGFzIGVmZmVjdFR5cGVzLCBhNSBhcyBmbHVzaCwgTCBhcyBmb3JrLCBhNiBhcyBnZXRDb250ZXh0LCBhMiBhcyBqb2luLCBZIGFzIHB1dCwgWiBhcyBwdXRSZXNvbHZlLCBWIGFzIHJhY2UsIGEzIGFzIHNlbGVjdCwgYTcgYXMgc2V0Q29udGV4dCwgYTEgYXMgc3Bhd24sIEsgYXMgdGFrZSwgWCBhcyB0YWtlTWF5YmUgfSBmcm9tICcuL2lvLTZkZTE1NmYzLmpzJztcbmltcG9ydCAnQHJlZHV4LXNhZ2EvZGVsYXktcCc7XG5cbnZhciBkb25lID0gZnVuY3Rpb24gZG9uZSh2YWx1ZSkge1xuICByZXR1cm4ge1xuICAgIGRvbmU6IHRydWUsXG4gICAgdmFsdWU6IHZhbHVlXG4gIH07XG59O1xuXG52YXIgcUVuZCA9IHt9O1xuZnVuY3Rpb24gc2FmZU5hbWUocGF0dGVybk9yQ2hhbm5lbCkge1xuICBpZiAoY2hhbm5lbChwYXR0ZXJuT3JDaGFubmVsKSkge1xuICAgIHJldHVybiAnY2hhbm5lbCc7XG4gIH1cblxuICBpZiAoc3RyaW5nYWJsZUZ1bmMocGF0dGVybk9yQ2hhbm5lbCkpIHtcbiAgICByZXR1cm4gU3RyaW5nKHBhdHRlcm5PckNoYW5uZWwpO1xuICB9XG5cbiAgaWYgKGZ1bmMocGF0dGVybk9yQ2hhbm5lbCkpIHtcbiAgICByZXR1cm4gcGF0dGVybk9yQ2hhbm5lbC5uYW1lO1xuICB9XG5cbiAgcmV0dXJuIFN0cmluZyhwYXR0ZXJuT3JDaGFubmVsKTtcbn1cbmZ1bmN0aW9uIGZzbUl0ZXJhdG9yKGZzbSwgc3RhcnRTdGF0ZSwgbmFtZSkge1xuICB2YXIgc3RhdGVVcGRhdGVyLFxuICAgICAgZXJyb3JTdGF0ZSxcbiAgICAgIGVmZmVjdCxcbiAgICAgIG5leHRTdGF0ZSA9IHN0YXJ0U3RhdGU7XG5cbiAgZnVuY3Rpb24gbmV4dChhcmcsIGVycm9yKSB7XG4gICAgaWYgKG5leHRTdGF0ZSA9PT0gcUVuZCkge1xuICAgICAgcmV0dXJuIGRvbmUoYXJnKTtcbiAgICB9XG5cbiAgICBpZiAoZXJyb3IgJiYgIWVycm9yU3RhdGUpIHtcbiAgICAgIG5leHRTdGF0ZSA9IHFFbmQ7XG4gICAgICB0aHJvdyBlcnJvcjtcbiAgICB9IGVsc2Uge1xuICAgICAgc3RhdGVVcGRhdGVyICYmIHN0YXRlVXBkYXRlcihhcmcpO1xuICAgICAgdmFyIGN1cnJlbnRTdGF0ZSA9IGVycm9yID8gZnNtW2Vycm9yU3RhdGVdKGVycm9yKSA6IGZzbVtuZXh0U3RhdGVdKCk7XG4gICAgICBuZXh0U3RhdGUgPSBjdXJyZW50U3RhdGUubmV4dFN0YXRlO1xuICAgICAgZWZmZWN0ID0gY3VycmVudFN0YXRlLmVmZmVjdDtcbiAgICAgIHN0YXRlVXBkYXRlciA9IGN1cnJlbnRTdGF0ZS5zdGF0ZVVwZGF0ZXI7XG4gICAgICBlcnJvclN0YXRlID0gY3VycmVudFN0YXRlLmVycm9yU3RhdGU7XG4gICAgICByZXR1cm4gbmV4dFN0YXRlID09PSBxRW5kID8gZG9uZShhcmcpIDogZWZmZWN0O1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBtYWtlSXRlcmF0b3IobmV4dCwgZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgcmV0dXJuIG5leHQobnVsbCwgZXJyb3IpO1xuICB9LCBuYW1lKTtcbn1cblxuZnVuY3Rpb24gdGFrZUV2ZXJ5KHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcikge1xuICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IG5ldyBBcnJheShfbGVuID4gMiA/IF9sZW4gLSAyIDogMCksIF9rZXkgPSAyOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgYXJnc1tfa2V5IC0gMl0gPSBhcmd1bWVudHNbX2tleV07XG4gIH1cblxuICB2YXIgeVRha2UgPSB7XG4gICAgZG9uZTogZmFsc2UsXG4gICAgdmFsdWU6IHRha2UocGF0dGVybk9yQ2hhbm5lbClcbiAgfTtcblxuICB2YXIgeUZvcmsgPSBmdW5jdGlvbiB5Rm9yayhhYykge1xuICAgIHJldHVybiB7XG4gICAgICBkb25lOiBmYWxzZSxcbiAgICAgIHZhbHVlOiBmb3JrLmFwcGx5KHZvaWQgMCwgW3dvcmtlcl0uY29uY2F0KGFyZ3MsIFthY10pKVxuICAgIH07XG4gIH07XG5cbiAgdmFyIGFjdGlvbixcbiAgICAgIHNldEFjdGlvbiA9IGZ1bmN0aW9uIHNldEFjdGlvbihhYykge1xuICAgIHJldHVybiBhY3Rpb24gPSBhYztcbiAgfTtcblxuICByZXR1cm4gZnNtSXRlcmF0b3Ioe1xuICAgIHExOiBmdW5jdGlvbiBxMSgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5leHRTdGF0ZTogJ3EyJyxcbiAgICAgICAgZWZmZWN0OiB5VGFrZSxcbiAgICAgICAgc3RhdGVVcGRhdGVyOiBzZXRBY3Rpb25cbiAgICAgIH07XG4gICAgfSxcbiAgICBxMjogZnVuY3Rpb24gcTIoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMScsXG4gICAgICAgIGVmZmVjdDogeUZvcmsoYWN0aW9uKVxuICAgICAgfTtcbiAgICB9XG4gIH0sICdxMScsIFwidGFrZUV2ZXJ5KFwiICsgc2FmZU5hbWUocGF0dGVybk9yQ2hhbm5lbCkgKyBcIiwgXCIgKyB3b3JrZXIubmFtZSArIFwiKVwiKTtcbn1cblxuZnVuY3Rpb24gdGFrZUxhdGVzdChwYXR0ZXJuT3JDaGFubmVsLCB3b3JrZXIpIHtcbiAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbiA+IDIgPyBfbGVuIC0gMiA6IDApLCBfa2V5ID0gMjsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIGFyZ3NbX2tleSAtIDJdID0gYXJndW1lbnRzW19rZXldO1xuICB9XG5cbiAgdmFyIHlUYWtlID0ge1xuICAgIGRvbmU6IGZhbHNlLFxuICAgIHZhbHVlOiB0YWtlKHBhdHRlcm5PckNoYW5uZWwpXG4gIH07XG5cbiAgdmFyIHlGb3JrID0gZnVuY3Rpb24geUZvcmsoYWMpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZG9uZTogZmFsc2UsXG4gICAgICB2YWx1ZTogZm9yay5hcHBseSh2b2lkIDAsIFt3b3JrZXJdLmNvbmNhdChhcmdzLCBbYWNdKSlcbiAgICB9O1xuICB9O1xuXG4gIHZhciB5Q2FuY2VsID0gZnVuY3Rpb24geUNhbmNlbCh0YXNrKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGRvbmU6IGZhbHNlLFxuICAgICAgdmFsdWU6IGNhbmNlbCh0YXNrKVxuICAgIH07XG4gIH07XG5cbiAgdmFyIHRhc2ssIGFjdGlvbjtcblxuICB2YXIgc2V0VGFzayA9IGZ1bmN0aW9uIHNldFRhc2sodCkge1xuICAgIHJldHVybiB0YXNrID0gdDtcbiAgfTtcblxuICB2YXIgc2V0QWN0aW9uID0gZnVuY3Rpb24gc2V0QWN0aW9uKGFjKSB7XG4gICAgcmV0dXJuIGFjdGlvbiA9IGFjO1xuICB9O1xuXG4gIHJldHVybiBmc21JdGVyYXRvcih7XG4gICAgcTE6IGZ1bmN0aW9uIHExKCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTInLFxuICAgICAgICBlZmZlY3Q6IHlUYWtlLFxuICAgICAgICBzdGF0ZVVwZGF0ZXI6IHNldEFjdGlvblxuICAgICAgfTtcbiAgICB9LFxuICAgIHEyOiBmdW5jdGlvbiBxMigpIHtcbiAgICAgIHJldHVybiB0YXNrID8ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMycsXG4gICAgICAgIGVmZmVjdDogeUNhbmNlbCh0YXNrKVxuICAgICAgfSA6IHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTEnLFxuICAgICAgICBlZmZlY3Q6IHlGb3JrKGFjdGlvbiksXG4gICAgICAgIHN0YXRlVXBkYXRlcjogc2V0VGFza1xuICAgICAgfTtcbiAgICB9LFxuICAgIHEzOiBmdW5jdGlvbiBxMygpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5leHRTdGF0ZTogJ3ExJyxcbiAgICAgICAgZWZmZWN0OiB5Rm9yayhhY3Rpb24pLFxuICAgICAgICBzdGF0ZVVwZGF0ZXI6IHNldFRhc2tcbiAgICAgIH07XG4gICAgfVxuICB9LCAncTEnLCBcInRha2VMYXRlc3QoXCIgKyBzYWZlTmFtZShwYXR0ZXJuT3JDaGFubmVsKSArIFwiLCBcIiArIHdvcmtlci5uYW1lICsgXCIpXCIpO1xufVxuXG5mdW5jdGlvbiB0YWtlTGVhZGluZyhwYXR0ZXJuT3JDaGFubmVsLCB3b3JrZXIpIHtcbiAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbiA+IDIgPyBfbGVuIC0gMiA6IDApLCBfa2V5ID0gMjsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgIGFyZ3NbX2tleSAtIDJdID0gYXJndW1lbnRzW19rZXldO1xuICB9XG5cbiAgdmFyIHlUYWtlID0ge1xuICAgIGRvbmU6IGZhbHNlLFxuICAgIHZhbHVlOiB0YWtlKHBhdHRlcm5PckNoYW5uZWwpXG4gIH07XG5cbiAgdmFyIHlDYWxsID0gZnVuY3Rpb24geUNhbGwoYWMpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZG9uZTogZmFsc2UsXG4gICAgICB2YWx1ZTogY2FsbC5hcHBseSh2b2lkIDAsIFt3b3JrZXJdLmNvbmNhdChhcmdzLCBbYWNdKSlcbiAgICB9O1xuICB9O1xuXG4gIHZhciBhY3Rpb247XG5cbiAgdmFyIHNldEFjdGlvbiA9IGZ1bmN0aW9uIHNldEFjdGlvbihhYykge1xuICAgIHJldHVybiBhY3Rpb24gPSBhYztcbiAgfTtcblxuICByZXR1cm4gZnNtSXRlcmF0b3Ioe1xuICAgIHExOiBmdW5jdGlvbiBxMSgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5leHRTdGF0ZTogJ3EyJyxcbiAgICAgICAgZWZmZWN0OiB5VGFrZSxcbiAgICAgICAgc3RhdGVVcGRhdGVyOiBzZXRBY3Rpb25cbiAgICAgIH07XG4gICAgfSxcbiAgICBxMjogZnVuY3Rpb24gcTIoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMScsXG4gICAgICAgIGVmZmVjdDogeUNhbGwoYWN0aW9uKVxuICAgICAgfTtcbiAgICB9XG4gIH0sICdxMScsIFwidGFrZUxlYWRpbmcoXCIgKyBzYWZlTmFtZShwYXR0ZXJuT3JDaGFubmVsKSArIFwiLCBcIiArIHdvcmtlci5uYW1lICsgXCIpXCIpO1xufVxuXG5mdW5jdGlvbiB0aHJvdHRsZShkZWxheUxlbmd0aCwgcGF0dGVybiwgd29ya2VyKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4gPiAzID8gX2xlbiAtIDMgOiAwKSwgX2tleSA9IDM7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBhcmdzW19rZXkgLSAzXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHZhciBhY3Rpb24sIGNoYW5uZWw7XG4gIHZhciB5QWN0aW9uQ2hhbm5lbCA9IHtcbiAgICBkb25lOiBmYWxzZSxcbiAgICB2YWx1ZTogYWN0aW9uQ2hhbm5lbChwYXR0ZXJuLCBzbGlkaW5nKDEpKVxuICB9O1xuXG4gIHZhciB5VGFrZSA9IGZ1bmN0aW9uIHlUYWtlKCkge1xuICAgIHJldHVybiB7XG4gICAgICBkb25lOiBmYWxzZSxcbiAgICAgIHZhbHVlOiB0YWtlKGNoYW5uZWwpXG4gICAgfTtcbiAgfTtcblxuICB2YXIgeUZvcmsgPSBmdW5jdGlvbiB5Rm9yayhhYykge1xuICAgIHJldHVybiB7XG4gICAgICBkb25lOiBmYWxzZSxcbiAgICAgIHZhbHVlOiBmb3JrLmFwcGx5KHZvaWQgMCwgW3dvcmtlcl0uY29uY2F0KGFyZ3MsIFthY10pKVxuICAgIH07XG4gIH07XG5cbiAgdmFyIHlEZWxheSA9IHtcbiAgICBkb25lOiBmYWxzZSxcbiAgICB2YWx1ZTogZGVsYXkoZGVsYXlMZW5ndGgpXG4gIH07XG5cbiAgdmFyIHNldEFjdGlvbiA9IGZ1bmN0aW9uIHNldEFjdGlvbihhYykge1xuICAgIHJldHVybiBhY3Rpb24gPSBhYztcbiAgfTtcblxuICB2YXIgc2V0Q2hhbm5lbCA9IGZ1bmN0aW9uIHNldENoYW5uZWwoY2gpIHtcbiAgICByZXR1cm4gY2hhbm5lbCA9IGNoO1xuICB9O1xuXG4gIHJldHVybiBmc21JdGVyYXRvcih7XG4gICAgcTE6IGZ1bmN0aW9uIHExKCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTInLFxuICAgICAgICBlZmZlY3Q6IHlBY3Rpb25DaGFubmVsLFxuICAgICAgICBzdGF0ZVVwZGF0ZXI6IHNldENoYW5uZWxcbiAgICAgIH07XG4gICAgfSxcbiAgICBxMjogZnVuY3Rpb24gcTIoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMycsXG4gICAgICAgIGVmZmVjdDogeVRha2UoKSxcbiAgICAgICAgc3RhdGVVcGRhdGVyOiBzZXRBY3Rpb25cbiAgICAgIH07XG4gICAgfSxcbiAgICBxMzogZnVuY3Rpb24gcTMoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxNCcsXG4gICAgICAgIGVmZmVjdDogeUZvcmsoYWN0aW9uKVxuICAgICAgfTtcbiAgICB9LFxuICAgIHE0OiBmdW5jdGlvbiBxNCgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIG5leHRTdGF0ZTogJ3EyJyxcbiAgICAgICAgZWZmZWN0OiB5RGVsYXlcbiAgICAgIH07XG4gICAgfVxuICB9LCAncTEnLCBcInRocm90dGxlKFwiICsgc2FmZU5hbWUocGF0dGVybikgKyBcIiwgXCIgKyB3b3JrZXIubmFtZSArIFwiKVwiKTtcbn1cblxuZnVuY3Rpb24gcmV0cnkobWF4VHJpZXMsIGRlbGF5TGVuZ3RoLCBmbikge1xuICB2YXIgY291bnRlciA9IG1heFRyaWVzO1xuXG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4gPiAzID8gX2xlbiAtIDMgOiAwKSwgX2tleSA9IDM7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBhcmdzW19rZXkgLSAzXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHZhciB5Q2FsbCA9IHtcbiAgICBkb25lOiBmYWxzZSxcbiAgICB2YWx1ZTogY2FsbC5hcHBseSh2b2lkIDAsIFtmbl0uY29uY2F0KGFyZ3MpKVxuICB9O1xuICB2YXIgeURlbGF5ID0ge1xuICAgIGRvbmU6IGZhbHNlLFxuICAgIHZhbHVlOiBkZWxheShkZWxheUxlbmd0aClcbiAgfTtcbiAgcmV0dXJuIGZzbUl0ZXJhdG9yKHtcbiAgICBxMTogZnVuY3Rpb24gcTEoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMicsXG4gICAgICAgIGVmZmVjdDogeUNhbGwsXG4gICAgICAgIGVycm9yU3RhdGU6ICdxMTAnXG4gICAgICB9O1xuICAgIH0sXG4gICAgcTI6IGZ1bmN0aW9uIHEyKCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbmV4dFN0YXRlOiBxRW5kXG4gICAgICB9O1xuICAgIH0sXG4gICAgcTEwOiBmdW5jdGlvbiBxMTAoZXJyb3IpIHtcbiAgICAgIGNvdW50ZXIgLT0gMTtcblxuICAgICAgaWYgKGNvdW50ZXIgPD0gMCkge1xuICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTEnLFxuICAgICAgICBlZmZlY3Q6IHlEZWxheVxuICAgICAgfTtcbiAgICB9XG4gIH0sICdxMScsIFwicmV0cnkoXCIgKyBmbi5uYW1lICsgXCIpXCIpO1xufVxuXG5mdW5jdGlvbiBkZWJvdW5jZUhlbHBlcihkZWxheUxlbmd0aCwgcGF0dGVybk9yQ2hhbm5lbCwgd29ya2VyKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4gPiAzID8gX2xlbiAtIDMgOiAwKSwgX2tleSA9IDM7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBhcmdzW19rZXkgLSAzXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHZhciBhY3Rpb24sIHJhY2VPdXRwdXQ7XG4gIHZhciB5VGFrZSA9IHtcbiAgICBkb25lOiBmYWxzZSxcbiAgICB2YWx1ZTogdGFrZShwYXR0ZXJuT3JDaGFubmVsKVxuICB9O1xuICB2YXIgeVJhY2UgPSB7XG4gICAgZG9uZTogZmFsc2UsXG4gICAgdmFsdWU6IHJhY2Uoe1xuICAgICAgYWN0aW9uOiB0YWtlKHBhdHRlcm5PckNoYW5uZWwpLFxuICAgICAgZGVib3VuY2U6IGRlbGF5KGRlbGF5TGVuZ3RoKVxuICAgIH0pXG4gIH07XG5cbiAgdmFyIHlGb3JrID0gZnVuY3Rpb24geUZvcmsoYWMpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZG9uZTogZmFsc2UsXG4gICAgICB2YWx1ZTogZm9yay5hcHBseSh2b2lkIDAsIFt3b3JrZXJdLmNvbmNhdChhcmdzLCBbYWNdKSlcbiAgICB9O1xuICB9O1xuXG4gIHZhciB5Tm9vcCA9IGZ1bmN0aW9uIHlOb29wKHZhbHVlKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGRvbmU6IGZhbHNlLFxuICAgICAgdmFsdWU6IHZhbHVlXG4gICAgfTtcbiAgfTtcblxuICB2YXIgc2V0QWN0aW9uID0gZnVuY3Rpb24gc2V0QWN0aW9uKGFjKSB7XG4gICAgcmV0dXJuIGFjdGlvbiA9IGFjO1xuICB9O1xuXG4gIHZhciBzZXRSYWNlT3V0cHV0ID0gZnVuY3Rpb24gc2V0UmFjZU91dHB1dChybykge1xuICAgIHJldHVybiByYWNlT3V0cHV0ID0gcm87XG4gIH07XG5cbiAgcmV0dXJuIGZzbUl0ZXJhdG9yKHtcbiAgICBxMTogZnVuY3Rpb24gcTEoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBuZXh0U3RhdGU6ICdxMicsXG4gICAgICAgIGVmZmVjdDogeVRha2UsXG4gICAgICAgIHN0YXRlVXBkYXRlcjogc2V0QWN0aW9uXG4gICAgICB9O1xuICAgIH0sXG4gICAgcTI6IGZ1bmN0aW9uIHEyKCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTMnLFxuICAgICAgICBlZmZlY3Q6IHlSYWNlLFxuICAgICAgICBzdGF0ZVVwZGF0ZXI6IHNldFJhY2VPdXRwdXRcbiAgICAgIH07XG4gICAgfSxcbiAgICBxMzogZnVuY3Rpb24gcTMoKSB7XG4gICAgICByZXR1cm4gcmFjZU91dHB1dC5kZWJvdW5jZSA/IHtcbiAgICAgICAgbmV4dFN0YXRlOiAncTEnLFxuICAgICAgICBlZmZlY3Q6IHlGb3JrKGFjdGlvbilcbiAgICAgIH0gOiB7XG4gICAgICAgIG5leHRTdGF0ZTogJ3EyJyxcbiAgICAgICAgZWZmZWN0OiB5Tm9vcChyYWNlT3V0cHV0LmFjdGlvbiksXG4gICAgICAgIHN0YXRlVXBkYXRlcjogc2V0QWN0aW9uXG4gICAgICB9O1xuICAgIH1cbiAgfSwgJ3ExJywgXCJkZWJvdW5jZShcIiArIHNhZmVOYW1lKHBhdHRlcm5PckNoYW5uZWwpICsgXCIsIFwiICsgd29ya2VyLm5hbWUgKyBcIilcIik7XG59XG5cbnZhciB2YWxpZGF0ZVRha2VFZmZlY3QgPSBmdW5jdGlvbiB2YWxpZGF0ZVRha2VFZmZlY3QoZm4sIHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcikge1xuICBjaGVjayhwYXR0ZXJuT3JDaGFubmVsLCBub3RVbmRlZiwgZm4ubmFtZSArIFwiIHJlcXVpcmVzIGEgcGF0dGVybiBvciBjaGFubmVsXCIpO1xuICBjaGVjayh3b3JrZXIsIG5vdFVuZGVmLCBmbi5uYW1lICsgXCIgcmVxdWlyZXMgYSBzYWdhIHBhcmFtZXRlclwiKTtcbn07XG5cbmZ1bmN0aW9uIHRha2VFdmVyeSQxKHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhbGlkYXRlVGFrZUVmZmVjdCh0YWtlRXZlcnkkMSwgcGF0dGVybk9yQ2hhbm5lbCwgd29ya2VyKTtcbiAgfVxuXG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4gPiAyID8gX2xlbiAtIDIgOiAwKSwgX2tleSA9IDI7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBhcmdzW19rZXkgLSAyXSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHJldHVybiBmb3JrLmFwcGx5KHZvaWQgMCwgW3Rha2VFdmVyeSwgcGF0dGVybk9yQ2hhbm5lbCwgd29ya2VyXS5jb25jYXQoYXJncykpO1xufVxuZnVuY3Rpb24gdGFrZUxhdGVzdCQxKHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhbGlkYXRlVGFrZUVmZmVjdCh0YWtlTGF0ZXN0JDEsIHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcik7XG4gIH1cblxuICBmb3IgKHZhciBfbGVuMiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbjIgPiAyID8gX2xlbjIgLSAyIDogMCksIF9rZXkyID0gMjsgX2tleTIgPCBfbGVuMjsgX2tleTIrKykge1xuICAgIGFyZ3NbX2tleTIgLSAyXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gIH1cblxuICByZXR1cm4gZm9yay5hcHBseSh2b2lkIDAsIFt0YWtlTGF0ZXN0LCBwYXR0ZXJuT3JDaGFubmVsLCB3b3JrZXJdLmNvbmNhdChhcmdzKSk7XG59XG5mdW5jdGlvbiB0YWtlTGVhZGluZyQxKHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHZhbGlkYXRlVGFrZUVmZmVjdCh0YWtlTGVhZGluZyQxLCBwYXR0ZXJuT3JDaGFubmVsLCB3b3JrZXIpO1xuICB9XG5cbiAgZm9yICh2YXIgX2xlbjMgPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4zID4gMiA/IF9sZW4zIC0gMiA6IDApLCBfa2V5MyA9IDI7IF9rZXkzIDwgX2xlbjM7IF9rZXkzKyspIHtcbiAgICBhcmdzW19rZXkzIC0gMl0gPSBhcmd1bWVudHNbX2tleTNdO1xuICB9XG5cbiAgcmV0dXJuIGZvcmsuYXBwbHkodm9pZCAwLCBbdGFrZUxlYWRpbmcsIHBhdHRlcm5PckNoYW5uZWwsIHdvcmtlcl0uY29uY2F0KGFyZ3MpKTtcbn1cbmZ1bmN0aW9uIHRocm90dGxlJDEobXMsIHBhdHRlcm4sIHdvcmtlcikge1xuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIGNoZWNrKHBhdHRlcm4sIG5vdFVuZGVmLCAndGhyb3R0bGUgcmVxdWlyZXMgYSBwYXR0ZXJuJyk7XG4gICAgY2hlY2sod29ya2VyLCBub3RVbmRlZiwgJ3Rocm90dGxlIHJlcXVpcmVzIGEgc2FnYSBwYXJhbWV0ZXInKTtcbiAgfVxuXG4gIGZvciAodmFyIF9sZW40ID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IG5ldyBBcnJheShfbGVuNCA+IDMgPyBfbGVuNCAtIDMgOiAwKSwgX2tleTQgPSAzOyBfa2V5NCA8IF9sZW40OyBfa2V5NCsrKSB7XG4gICAgYXJnc1tfa2V5NCAtIDNdID0gYXJndW1lbnRzW19rZXk0XTtcbiAgfVxuXG4gIHJldHVybiBmb3JrLmFwcGx5KHZvaWQgMCwgW3Rocm90dGxlLCBtcywgcGF0dGVybiwgd29ya2VyXS5jb25jYXQoYXJncykpO1xufVxuZnVuY3Rpb24gcmV0cnkkMShtYXhUcmllcywgZGVsYXlMZW5ndGgsIHdvcmtlcikge1xuICBmb3IgKHZhciBfbGVuNSA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBuZXcgQXJyYXkoX2xlbjUgPiAzID8gX2xlbjUgLSAzIDogMCksIF9rZXk1ID0gMzsgX2tleTUgPCBfbGVuNTsgX2tleTUrKykge1xuICAgIGFyZ3NbX2tleTUgLSAzXSA9IGFyZ3VtZW50c1tfa2V5NV07XG4gIH1cblxuICByZXR1cm4gY2FsbC5hcHBseSh2b2lkIDAsIFtyZXRyeSwgbWF4VHJpZXMsIGRlbGF5TGVuZ3RoLCB3b3JrZXJdLmNvbmNhdChhcmdzKSk7XG59XG5mdW5jdGlvbiBkZWJvdW5jZShkZWxheUxlbmd0aCwgcGF0dGVybiwgd29ya2VyKSB7XG4gIGZvciAodmFyIF9sZW42ID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IG5ldyBBcnJheShfbGVuNiA+IDMgPyBfbGVuNiAtIDMgOiAwKSwgX2tleTYgPSAzOyBfa2V5NiA8IF9sZW42OyBfa2V5NisrKSB7XG4gICAgYXJnc1tfa2V5NiAtIDNdID0gYXJndW1lbnRzW19rZXk2XTtcbiAgfVxuXG4gIHJldHVybiBmb3JrLmFwcGx5KHZvaWQgMCwgW2RlYm91bmNlSGVscGVyLCBkZWxheUxlbmd0aCwgcGF0dGVybiwgd29ya2VyXS5jb25jYXQoYXJncykpO1xufVxuXG5leHBvcnQgeyBkZWJvdW5jZSwgcmV0cnkkMSBhcyByZXRyeSwgdGFrZUV2ZXJ5JDEgYXMgdGFrZUV2ZXJ5LCB0YWtlTGF0ZXN0JDEgYXMgdGFrZUxhdGVzdCwgdGFrZUxlYWRpbmckMSBhcyB0YWtlTGVhZGluZywgdGhyb3R0bGUkMSBhcyB0aHJvdHRsZSB9O1xuIiwiaW1wb3J0IHsgQ0FOQ0VMIH0gZnJvbSAnQHJlZHV4LXNhZ2Evc3ltYm9scyc7XG5cbmZ1bmN0aW9uIGRlbGF5UChtcywgdmFsKSB7XG4gIGlmICh2YWwgPT09IHZvaWQgMCkge1xuICAgIHZhbCA9IHRydWU7XG4gIH1cblxuICB2YXIgdGltZW91dElkO1xuICB2YXIgcHJvbWlzZSA9IG5ldyBQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgdGltZW91dElkID0gc2V0VGltZW91dChyZXNvbHZlLCBtcywgdmFsKTtcbiAgfSk7XG5cbiAgcHJvbWlzZVtDQU5DRUxdID0gZnVuY3Rpb24gKCkge1xuICAgIGNsZWFyVGltZW91dCh0aW1lb3V0SWQpO1xuICB9O1xuXG4gIHJldHVybiBwcm9taXNlO1xufVxuXG5leHBvcnQgZGVmYXVsdCBkZWxheVA7XG4iLCJpbXBvcnQgeyBUQVNLLCBTQUdBX0FDVElPTiwgTVVMVElDQVNULCBJTyB9IGZyb20gJ0ByZWR1eC1zYWdhL3N5bWJvbHMnO1xuXG52YXIgdW5kZWYgPSBmdW5jdGlvbiB1bmRlZih2KSB7XG4gIHJldHVybiB2ID09PSBudWxsIHx8IHYgPT09IHVuZGVmaW5lZDtcbn07XG52YXIgbm90VW5kZWYgPSBmdW5jdGlvbiBub3RVbmRlZih2KSB7XG4gIHJldHVybiB2ICE9PSBudWxsICYmIHYgIT09IHVuZGVmaW5lZDtcbn07XG52YXIgZnVuYyA9IGZ1bmN0aW9uIGZ1bmMoZikge1xuICByZXR1cm4gdHlwZW9mIGYgPT09ICdmdW5jdGlvbic7XG59O1xudmFyIG51bWJlciA9IGZ1bmN0aW9uIG51bWJlcihuKSB7XG4gIHJldHVybiB0eXBlb2YgbiA9PT0gJ251bWJlcic7XG59O1xudmFyIHN0cmluZyA9IGZ1bmN0aW9uIHN0cmluZyhzKSB7XG4gIHJldHVybiB0eXBlb2YgcyA9PT0gJ3N0cmluZyc7XG59O1xudmFyIGFycmF5ID0gQXJyYXkuaXNBcnJheTtcbnZhciBvYmplY3QgPSBmdW5jdGlvbiBvYmplY3Qob2JqKSB7XG4gIHJldHVybiBvYmogJiYgIWFycmF5KG9iaikgJiYgdHlwZW9mIG9iaiA9PT0gJ29iamVjdCc7XG59O1xudmFyIHByb21pc2UgPSBmdW5jdGlvbiBwcm9taXNlKHApIHtcbiAgcmV0dXJuIHAgJiYgZnVuYyhwLnRoZW4pO1xufTtcbnZhciBpdGVyYXRvciA9IGZ1bmN0aW9uIGl0ZXJhdG9yKGl0KSB7XG4gIHJldHVybiBpdCAmJiBmdW5jKGl0Lm5leHQpICYmIGZ1bmMoaXQudGhyb3cpO1xufTtcbnZhciBpdGVyYWJsZSA9IGZ1bmN0aW9uIGl0ZXJhYmxlKGl0KSB7XG4gIHJldHVybiBpdCAmJiBmdW5jKFN5bWJvbCkgPyBmdW5jKGl0W1N5bWJvbC5pdGVyYXRvcl0pIDogYXJyYXkoaXQpO1xufTtcbnZhciB0YXNrID0gZnVuY3Rpb24gdGFzayh0KSB7XG4gIHJldHVybiB0ICYmIHRbVEFTS107XG59O1xudmFyIHNhZ2FBY3Rpb24gPSBmdW5jdGlvbiBzYWdhQWN0aW9uKGEpIHtcbiAgcmV0dXJuIEJvb2xlYW4oYSAmJiBhW1NBR0FfQUNUSU9OXSk7XG59O1xudmFyIG9ic2VydmFibGUgPSBmdW5jdGlvbiBvYnNlcnZhYmxlKG9iKSB7XG4gIHJldHVybiBvYiAmJiBmdW5jKG9iLnN1YnNjcmliZSk7XG59O1xudmFyIGJ1ZmZlciA9IGZ1bmN0aW9uIGJ1ZmZlcihidWYpIHtcbiAgcmV0dXJuIGJ1ZiAmJiBmdW5jKGJ1Zi5pc0VtcHR5KSAmJiBmdW5jKGJ1Zi50YWtlKSAmJiBmdW5jKGJ1Zi5wdXQpO1xufTtcbnZhciBwYXR0ZXJuID0gZnVuY3Rpb24gcGF0dGVybihwYXQpIHtcbiAgcmV0dXJuIHBhdCAmJiAoc3RyaW5nKHBhdCkgfHwgc3ltYm9sKHBhdCkgfHwgZnVuYyhwYXQpIHx8IGFycmF5KHBhdCkgJiYgcGF0LmV2ZXJ5KHBhdHRlcm4pKTtcbn07XG52YXIgY2hhbm5lbCA9IGZ1bmN0aW9uIGNoYW5uZWwoY2gpIHtcbiAgcmV0dXJuIGNoICYmIGZ1bmMoY2gudGFrZSkgJiYgZnVuYyhjaC5jbG9zZSk7XG59O1xudmFyIHN0cmluZ2FibGVGdW5jID0gZnVuY3Rpb24gc3RyaW5nYWJsZUZ1bmMoZikge1xuICByZXR1cm4gZnVuYyhmKSAmJiBmLmhhc093blByb3BlcnR5KCd0b1N0cmluZycpO1xufTtcbnZhciBzeW1ib2wgPSBmdW5jdGlvbiBzeW1ib2woc3ltKSB7XG4gIHJldHVybiBCb29sZWFuKHN5bSkgJiYgdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiBzeW0uY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBzeW0gIT09IFN5bWJvbC5wcm90b3R5cGU7XG59O1xudmFyIG11bHRpY2FzdCA9IGZ1bmN0aW9uIG11bHRpY2FzdChjaCkge1xuICByZXR1cm4gY2hhbm5lbChjaCkgJiYgY2hbTVVMVElDQVNUXTtcbn07XG52YXIgZWZmZWN0ID0gZnVuY3Rpb24gZWZmZWN0KGVmZikge1xuICByZXR1cm4gZWZmICYmIGVmZltJT107XG59O1xuXG5leHBvcnQgeyBhcnJheSwgYnVmZmVyLCBjaGFubmVsLCBlZmZlY3QsIGZ1bmMsIGl0ZXJhYmxlLCBpdGVyYXRvciwgbXVsdGljYXN0LCBub3RVbmRlZiwgbnVtYmVyLCBvYmplY3QsIG9ic2VydmFibGUsIHBhdHRlcm4sIHByb21pc2UsIHNhZ2FBY3Rpb24sIHN0cmluZywgc3RyaW5nYWJsZUZ1bmMsIHN5bWJvbCwgdGFzaywgdW5kZWYgfTtcbiIsInZhciBjcmVhdGVTeW1ib2wgPSBmdW5jdGlvbiBjcmVhdGVTeW1ib2wobmFtZSkge1xuICByZXR1cm4gXCJAQHJlZHV4LXNhZ2EvXCIgKyBuYW1lO1xufTtcblxudmFyIENBTkNFTCA9XG4vKiNfX1BVUkVfXyovXG5jcmVhdGVTeW1ib2woJ0NBTkNFTF9QUk9NSVNFJyk7XG52YXIgQ0hBTk5FTF9FTkRfVFlQRSA9XG4vKiNfX1BVUkVfXyovXG5jcmVhdGVTeW1ib2woJ0NIQU5ORUxfRU5EJyk7XG52YXIgSU8gPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdJTycpO1xudmFyIE1BVENIID1cbi8qI19fUFVSRV9fKi9cbmNyZWF0ZVN5bWJvbCgnTUFUQ0gnKTtcbnZhciBNVUxUSUNBU1QgPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdNVUxUSUNBU1QnKTtcbnZhciBTQUdBX0FDVElPTiA9XG4vKiNfX1BVUkVfXyovXG5jcmVhdGVTeW1ib2woJ1NBR0FfQUNUSU9OJyk7XG52YXIgU0VMRl9DQU5DRUxMQVRJT04gPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdTRUxGX0NBTkNFTExBVElPTicpO1xudmFyIFRBU0sgPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdUQVNLJyk7XG52YXIgVEFTS19DQU5DRUwgPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdUQVNLX0NBTkNFTCcpO1xudmFyIFRFUk1JTkFURSA9XG4vKiNfX1BVUkVfXyovXG5jcmVhdGVTeW1ib2woJ1RFUk1JTkFURScpO1xudmFyIFNBR0FfTE9DQVRJT04gPVxuLyojX19QVVJFX18qL1xuY3JlYXRlU3ltYm9sKCdMT0NBVElPTicpO1xuXG5leHBvcnQgeyBDQU5DRUwsIENIQU5ORUxfRU5EX1RZUEUsIElPLCBNQVRDSCwgTVVMVElDQVNULCBTQUdBX0FDVElPTiwgU0FHQV9MT0NBVElPTiwgU0VMRl9DQU5DRUxMQVRJT04sIFRBU0ssIFRBU0tfQ0FOQ0VMLCBURVJNSU5BVEUgfTtcbiIsIi8qIVxuICBDb3B5cmlnaHQgKGMpIDIwMTggSmVkIFdhdHNvbi5cbiAgTGljZW5zZWQgdW5kZXIgdGhlIE1JVCBMaWNlbnNlIChNSVQpLCBzZWVcbiAgaHR0cDovL2plZHdhdHNvbi5naXRodWIuaW8vY2xhc3NuYW1lc1xuKi9cbi8qIGdsb2JhbCBkZWZpbmUgKi9cblxuKGZ1bmN0aW9uICgpIHtcblx0J3VzZSBzdHJpY3QnO1xuXG5cdHZhciBoYXNPd24gPSB7fS5oYXNPd25Qcm9wZXJ0eTtcblxuXHRmdW5jdGlvbiBjbGFzc05hbWVzKCkge1xuXHRcdHZhciBjbGFzc2VzID0gW107XG5cblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuXHRcdFx0dmFyIGFyZyA9IGFyZ3VtZW50c1tpXTtcblx0XHRcdGlmICghYXJnKSBjb250aW51ZTtcblxuXHRcdFx0dmFyIGFyZ1R5cGUgPSB0eXBlb2YgYXJnO1xuXG5cdFx0XHRpZiAoYXJnVHlwZSA9PT0gJ3N0cmluZycgfHwgYXJnVHlwZSA9PT0gJ251bWJlcicpIHtcblx0XHRcdFx0Y2xhc3Nlcy5wdXNoKGFyZyk7XG5cdFx0XHR9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkoYXJnKSkge1xuXHRcdFx0XHRpZiAoYXJnLmxlbmd0aCkge1xuXHRcdFx0XHRcdHZhciBpbm5lciA9IGNsYXNzTmFtZXMuYXBwbHkobnVsbCwgYXJnKTtcblx0XHRcdFx0XHRpZiAoaW5uZXIpIHtcblx0XHRcdFx0XHRcdGNsYXNzZXMucHVzaChpbm5lcik7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2UgaWYgKGFyZ1R5cGUgPT09ICdvYmplY3QnKSB7XG5cdFx0XHRcdGlmIChhcmcudG9TdHJpbmcgPT09IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcpIHtcblx0XHRcdFx0XHRmb3IgKHZhciBrZXkgaW4gYXJnKSB7XG5cdFx0XHRcdFx0XHRpZiAoaGFzT3duLmNhbGwoYXJnLCBrZXkpICYmIGFyZ1trZXldKSB7XG5cdFx0XHRcdFx0XHRcdGNsYXNzZXMucHVzaChrZXkpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRjbGFzc2VzLnB1c2goYXJnLnRvU3RyaW5nKCkpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0cmV0dXJuIGNsYXNzZXMuam9pbignICcpO1xuXHR9XG5cblx0aWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIG1vZHVsZS5leHBvcnRzKSB7XG5cdFx0Y2xhc3NOYW1lcy5kZWZhdWx0ID0gY2xhc3NOYW1lcztcblx0XHRtb2R1bGUuZXhwb3J0cyA9IGNsYXNzTmFtZXM7XG5cdH0gZWxzZSBpZiAodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgZGVmaW5lLmFtZCA9PT0gJ29iamVjdCcgJiYgZGVmaW5lLmFtZCkge1xuXHRcdC8vIHJlZ2lzdGVyIGFzICdjbGFzc25hbWVzJywgY29uc2lzdGVudCB3aXRoIG5wbSBwYWNrYWdlIG5hbWVcblx0XHRkZWZpbmUoJ2NsYXNzbmFtZXMnLCBbXSwgZnVuY3Rpb24gKCkge1xuXHRcdFx0cmV0dXJuIGNsYXNzTmFtZXM7XG5cdFx0fSk7XG5cdH0gZWxzZSB7XG5cdFx0d2luZG93LmNsYXNzTmFtZXMgPSBjbGFzc05hbWVzO1xuXHR9XG59KCkpO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8zVGZ3bnFHOVMzXzB1S3FGS21JNm1LIHtcXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgICBtYXJnaW46IDhweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgICBhbmltYXRpb246IF8zVGZ3bnFHOVMzXzB1S3FGS21JNm1LIDIuNHMgY3ViaWMtYmV6aWVyKDAsIDAuMiwgMC44LCAxKSBpbmZpbml0ZTtcXG4gIH1cXG5Aa2V5ZnJhbWVzIF8zVGZ3bnFHOVMzXzB1S3FGS21JNm1LIHtcXG4gICAgMCUsIDEwMCUge1xcbiAgICAgIGFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjUsIDAsIDEsIDAuNSk7XFxuICAgIH1cXG4gICAgMCUge1xcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWSgwZGVnKTtcXG4gICAgfVxcbiAgICA1MCUge1xcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWSgxODAwZGVnKTtcXG4gICAgICBhbmltYXRpb24tdGltaW5nLWZ1bmN0aW9uOiBjdWJpYy1iZXppZXIoMCwgMC41LCAwLjUsIDEpO1xcbiAgICB9XFxuICAgIDEwMCUge1xcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlWSgzNjAwZGVnKTtcXG4gICAgfVxcbn1cIiwgXCJcIl0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwibGRzLWNpcmNsZVwiOiBcIl8zVGZ3bnFHOVMzXzB1S3FGS21JNm1LXCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qc1wiKTtcbmV4cG9ydHMgPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18oZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuXzI5SjRZYmhuRmllemRXOW5Qd0dUTFoge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDgwcHg7XFxuICBoZWlnaHQ6IDgwcHg7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXYge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDZweDtcXG4gIGhlaWdodDogNnB4O1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGFuaW1hdGlvbjogXzI5SjRZYmhuRmllemRXOW5Qd0dUTFogMS4ycyBsaW5lYXIgaW5maW5pdGU7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXY6bnRoLWNoaWxkKDEpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogMHM7XFxuICB0b3A6IDQ2LjI1JTtcXG4gIGxlZnQ6IDgyLjUlO1xcbn1cXG4uXzI5SjRZYmhuRmllemRXOW5Qd0dUTFogZGl2Om50aC1jaGlsZCgyKSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjFzO1xcbiAgdG9wOiAyNy41JTtcXG4gIGxlZnQ6IDc3LjUlO1xcbn1cXG4uXzI5SjRZYmhuRmllemRXOW5Qd0dUTFogZGl2Om50aC1jaGlsZCgzKSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjJzO1xcbiAgdG9wOiAxMy43NSU7XFxuICBsZWZ0OiA2NSU7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXY6bnRoLWNoaWxkKDQpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuM3M7XFxuICB0b3A6IDguNzUlO1xcbiAgbGVmdDogNDYuMjUlO1xcbn1cXG4uXzI5SjRZYmhuRmllemRXOW5Qd0dUTFogZGl2Om50aC1jaGlsZCg1KSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjRzO1xcbiAgdG9wOiAxMy43NSU7XFxuICBsZWZ0OiAyNy41JTtcXG59XFxuLl8yOUo0WWJobkZpZXpkVzluUHdHVExaIGRpdjpudGgtY2hpbGQoNikge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcXG4gIHRvcDogMjcuNSU7XFxuICBsZWZ0OiAxMy43NSU7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXY6bnRoLWNoaWxkKDcpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNnM7XFxuICB0b3A6IDQ2LjI1JTtcXG4gIGxlZnQ6IDguNzUlO1xcbn1cXG4uXzI5SjRZYmhuRmllemRXOW5Qd0dUTFogZGl2Om50aC1jaGlsZCg4KSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjdzO1xcbiAgdG9wOiA2NSU7XFxuICBsZWZ0OiAxMy43NSU7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXY6bnRoLWNoaWxkKDkpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XFxuICB0b3A6IDc3LjUlO1xcbiAgbGVmdDogMjcuNSU7XFxufVxcbi5fMjlKNFliaG5GaWV6ZFc5blB3R1RMWiBkaXY6bnRoLWNoaWxkKDEwKSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjlzO1xcbiAgdG9wOiA4Mi41JTtcXG4gIGxlZnQ6IDQ2LjI1JTtcXG59XFxuLl8yOUo0WWJobkZpZXpkVzluUHdHVExaIGRpdjpudGgtY2hpbGQoMTEpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTFzO1xcbiAgdG9wOiA3Ny41JTtcXG4gIGxlZnQ6IDY1JTtcXG59XFxuLl8yOUo0WWJobkZpZXpkVzluUHdHVExaIGRpdjpudGgtY2hpbGQoMTIpIHtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTEuMXM7XFxuICB0b3A6IDY1JTtcXG4gIGxlZnQ6IDc3LjUlO1xcbn1cXG5Aa2V5ZnJhbWVzIF8yOUo0WWJobkZpZXpkVzluUHdHVExaIHtcXG4gIDAlLFxcbiAgMjAlLFxcbiAgODAlLFxcbiAgMTAwJSB7XFxuICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XFxuICB9XFxuICA1MCUge1xcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuNSk7XFxuICB9XFxufVxcblwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJsZHMtZGVmYXVsdFwiOiBcIl8yOUo0WWJobkZpZXpkVzluUHdHVExaXCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qc1wiKTtcbmV4cG9ydHMgPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18oZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuXzVtUXRmUDdIY0RWQVFCYURGbHZXMyB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICB3aWR0aDogODBweDtcXG4gIGhlaWdodDogODBweDtcXG59XFxuLmZEc3hSZEZYZTROd3EwRzFBWjhQdyB7XFxuICBjb250ZW50OiAnICc7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIHdpZHRoOiA2NHB4O1xcbiAgaGVpZ2h0OiA2NHB4O1xcbiAgbWFyZ2luOiA4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBib3JkZXI6IDZweCBzb2xpZCAjMDAwO1xcbiAgYm9yZGVyLWNvbG9yOiAjMDAwIHRyYW5zcGFyZW50ICMwMDAgdHJhbnNwYXJlbnQ7XFxuICBhbmltYXRpb246IF81bVF0ZlA3SGNEVkFRQmFERmx2VzMgMS4ycyBsaW5lYXIgaW5maW5pdGU7XFxufVxcbkBrZXlmcmFtZXMgXzVtUXRmUDdIY0RWQVFCYURGbHZXMyB7XFxuICAwJSB7XFxuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xcbiAgfVxcbiAgMTAwJSB7XFxuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XFxuICB9XFxufVxcblwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJsZHMtZHVhbC1yaW5nXCI6IFwiXzVtUXRmUDdIY0RWQVFCYURGbHZXM1wiLFxuXHRcImxkcy1kdWFsLXJpbmctYWZ0ZXJcIjogXCJmRHN4UmRGWGU0TndxMEcxQVo4UHdcIlxufTtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cztcbiIsIi8vIEltcG9ydHNcbnZhciBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpO1xuZXhwb3J0cyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyhmYWxzZSk7XG4vLyBNb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5fMmhmb0JMV0hpcG5ReVhYQ2VHN1k1aSB7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgfVxcbiAgLl8yaGZvQkxXSGlwblF5WFhDZUc3WTVpIGRpdiB7XFxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gICAgdG9wOiA0MS4yNSU7XFxuICAgIHdpZHRoOiAxNi4yNSU7XFxuICAgIGhlaWdodDogMTYuMjUlO1xcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICAgIGJhY2tncm91bmQ6ICNmZmY7XFxuICAgIGFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLCAxLCAxLCAwKTtcXG4gIH1cXG4gIC5fMmhmb0JMV0hpcG5ReVhYQ2VHN1k1aSBkaXY6bnRoLWNoaWxkKDEpIHtcXG4gICAgbGVmdDogMTAlO1xcbiAgICBhbmltYXRpb246IF8yUHZJdHBQNUliQ0UwbkZYV212ejZGIDAuNnMgaW5maW5pdGU7XFxuICB9XFxuICAuXzJoZm9CTFdIaXBuUXlYWENlRzdZNWkgZGl2Om50aC1jaGlsZCgyKSB7XFxuICAgIGxlZnQ6IDEwJTtcXG4gICAgYW5pbWF0aW9uOiBfM2psZFRnT3c5Nkx2NE4wTEV5SlUyIDAuNnMgaW5maW5pdGU7XFxuICB9XFxuICAuXzJoZm9CTFdIaXBuUXlYWENlRzdZNWkgZGl2Om50aC1jaGlsZCgzKSB7XFxuICAgIGxlZnQ6IDQwJTtcXG4gICAgYW5pbWF0aW9uOiBfM2psZFRnT3c5Nkx2NE4wTEV5SlUyIDAuNnMgaW5maW5pdGU7XFxuICB9XFxuICAuXzJoZm9CTFdIaXBuUXlYWENlRzdZNWkgZGl2Om50aC1jaGlsZCg0KSB7XFxuICAgIGxlZnQ6IDcwJTtcXG4gICAgYW5pbWF0aW9uOiBjRlVTWlYtOFdZTUozQXVNcW1yT2UgMC42cyBpbmZpbml0ZTtcXG4gIH1cXG4gIEBrZXlmcmFtZXMgXzJQdkl0cFA1SWJDRTBuRlhXbXZ6NkYge1xcbiAgICAwJSB7XFxuICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcXG4gICAgfVxcbiAgICAxMDAlIHtcXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xcbiAgICB9XFxuICB9XFxuICBAa2V5ZnJhbWVzIGNGVVNaVi04V1lNSjNBdU1xbXJPZSB7XFxuICAgIDAlIHtcXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xcbiAgICB9XFxuICAgIDEwMCUge1xcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUoMCk7XFxuICAgIH1cXG4gIH1cXG4gIEBrZXlmcmFtZXMgXzNqbGRUZ093OTZMdjROMExFeUpVMiB7XFxuICAgIDAlIHtcXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAwKTtcXG4gICAgfVxcbiAgICAxMDAlIHtcXG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxODQuNjElLCAwKTtcXG4gICAgfVxcbiAgfVwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJsZHMtZWxsaXBzaXNcIjogXCJfMmhmb0JMV0hpcG5ReVhYQ2VHN1k1aVwiLFxuXHRcImxkcy1lbGxpcHNpczFcIjogXCJfMlB2SXRwUDVJYkNFMG5GWFdtdno2RlwiLFxuXHRcImxkcy1lbGxpcHNpczJcIjogXCJfM2psZFRnT3c5Nkx2NE4wTEV5SlUyXCIsXG5cdFwibGRzLWVsbGlwc2lzM1wiOiBcImNGVVNaVi04V1lNSjNBdU1xbXJPZVwiXG59O1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8yLWZMMFpJbjZpY1NabkF1b0lBd3I0IHtcXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICAgIHdpZHRoOiA4MHB4O1xcbiAgICBoZWlnaHQ6IDgwcHg7XFxuICB9XFxuICAuXzItZkwwWkluNmljU1puQXVvSUF3cjQgZGl2IHtcXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICAgIGxlZnQ6IDEwJTtcXG4gICAgd2lkdGg6IDIwJTtcXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcXG4gICAgYW5pbWF0aW9uOiBfMi1mTDBaSW42aWNTWm5BdW9JQXdyNCAxLjJzIGN1YmljLWJlemllcigwLCAwLjUsIDAuNSwgMSkgaW5maW5pdGU7XFxuICB9XFxuICAuXzItZkwwWkluNmljU1puQXVvSUF3cjQgZGl2Om50aC1jaGlsZCgxKSB7XFxuICAgIGxlZnQ6IDEwJTtcXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMC4yNHM7XFxuICB9XFxuICAuXzItZkwwWkluNmljU1puQXVvSUF3cjQgZGl2Om50aC1jaGlsZCgyKSB7XFxuICAgIGxlZnQ6IDQwJTtcXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xMnM7XFxuICB9XFxuICAuXzItZkwwWkluNmljU1puQXVvSUF3cjQgZGl2Om50aC1jaGlsZCgzKSB7XFxuICAgIGxlZnQ6IDcwJTtcXG4gICAgYW5pbWF0aW9uLWRlbGF5OiAwO1xcbiAgfVxcbiAgQGtleWZyYW1lcyBfMi1mTDBaSW42aWNTWm5BdW9JQXdyNCB7XFxuICAgIDAlIHtcXG4gICAgICB0b3A6IDEwJTtcXG4gICAgICBoZWlnaHQ6IDgwJTtcXG4gICAgfVxcbiAgICA1MCUsIDEwMCUge1xcbiAgICAgIHRvcDogMzAlO1xcbiAgICAgIGhlaWdodDogNDAlO1xcbiAgICB9XFxuICB9XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1mYWNlYm9va1wiOiBcIl8yLWZMMFpJbjZpY1NabkF1b0lBd3I0XCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qc1wiKTtcbmV4cG9ydHMgPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18oZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDgwcHg7XFxuICBoZWlnaHQ6IDgwcHg7XFxufVxcbi5fMzFwNDU5cUlsdjlkYjBQX09jbTNJcCBkaXYge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDIwJTtcXG4gIGhlaWdodDogMjAlO1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIGFuaW1hdGlvbjogXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAgMS4ycyBsaW5lYXIgaW5maW5pdGU7XFxufVxcbi5fMzFwNDU5cUlsdjlkYjBQX09jbTNJcCBkaXY6bnRoLWNoaWxkKDEpIHtcXG4gIHRvcDogMTAlO1xcbiAgbGVmdDogMTAlO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAwcztcXG59XFxuLl8zMXA0NTlxSWx2OWRiMFBfT2NtM0lwIGRpdjpudGgtY2hpbGQoMikge1xcbiAgdG9wOiAxMCU7XFxuICBsZWZ0OiA0MCU7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjRzO1xcbn1cXG4uXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAgZGl2Om50aC1jaGlsZCgzKSB7XFxuICB0b3A6IDEwJTtcXG4gIGxlZnQ6IDcwJTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XFxufVxcbi5fMzFwNDU5cUlsdjlkYjBQX09jbTNJcCBkaXY6bnRoLWNoaWxkKDQpIHtcXG4gIHRvcDogNDAlO1xcbiAgbGVmdDogMTAlO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcXG59XFxuLl8zMXA0NTlxSWx2OWRiMFBfT2NtM0lwIGRpdjpudGgtY2hpbGQoNSkge1xcbiAgdG9wOiA0MCU7XFxuICBsZWZ0OiA0MCU7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjhzO1xcbn1cXG4uXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAgZGl2Om50aC1jaGlsZCg2KSB7XFxuICB0b3A6IDQwJTtcXG4gIGxlZnQ6IDcwJTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTEuMnM7XFxufVxcbi5fMzFwNDU5cUlsdjlkYjBQX09jbTNJcCBkaXY6bnRoLWNoaWxkKDcpIHtcXG4gIHRvcDogNzAlO1xcbiAgbGVmdDogMTAlO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC44cztcXG59XFxuLl8zMXA0NTlxSWx2OWRiMFBfT2NtM0lwIGRpdjpudGgtY2hpbGQoOCkge1xcbiAgdG9wOiA3MCU7XFxuICBsZWZ0OiA0MCU7XFxuICBhbmltYXRpb24tZGVsYXk6IC0xLjJzO1xcbn1cXG4uXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAgZGl2Om50aC1jaGlsZCg5KSB7XFxuICB0b3A6IDcwJTtcXG4gIGxlZnQ6IDcwJTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTEuNnM7XFxufVxcbkBrZXlmcmFtZXMgXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXAge1xcbiAgMCUsXFxuICAxMDAlIHtcXG4gICAgb3BhY2l0eTogMTtcXG4gIH1cXG4gIDUwJSB7XFxuICAgIG9wYWNpdHk6IDAuNTtcXG4gIH1cXG59XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1ncmlkXCI6IFwiXzMxcDQ1OXFJbHY5ZGIwUF9PY20zSXBcIlxufTtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cztcbiIsIi8vIEltcG9ydHNcbnZhciBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpO1xuZXhwb3J0cyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyhmYWxzZSk7XG4vLyBNb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5fMU8yc092eHpYbm00OHI4NHF3TzNmQiB7XFxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgICB3aWR0aDogODBweDtcXG4gICAgaGVpZ2h0OiA4MHB4O1xcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XFxuICAgIHRyYW5zZm9ybS1vcmlnaW46IDQwcHggNDBweDtcXG4gIH1cXG4gIC5fMU8yc092eHpYbm00OHI4NHF3TzNmQiA+IGRpdiB7XFxuICAgIHRvcDogMzJweDtcXG4gICAgbGVmdDogMzJweDtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICB3aWR0aDogMzJweDtcXG4gICAgaGVpZ2h0OiAzMnB4O1xcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xcbiAgICBhbmltYXRpb246IF8xTzJzT3Z4elhubTQ4cjg0cXdPM2ZCIDEuMnMgaW5maW5pdGUgY3ViaWMtYmV6aWVyKDAuMjE1LCAwLjYxLCAwLjM1NSwgMSk7XFxuICB9XFxuICAuXzFPMnNPdnh6WG5tNDhyODRxd08zZkIgLl8zTnlrRFg2c203UnJOYmtXZkdEcW9xLFxcbiAgLl8xTzJzT3Z4elhubTQ4cjg0cXdPM2ZCIC5fMWlTUUVKRkVyaFZRa3lZOVVPb2VpaiB7XFxuICAgIGNvbnRlbnQ6IFxcXCIgXFxcIjtcXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgICBkaXNwbGF5OiBibG9jaztcXG4gICAgd2lkdGg6IDMycHg7XFxuICAgIGhlaWdodDogMzJweDtcXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIH1cXG4gIC5fMU8yc092eHpYbm00OHI4NHF3TzNmQiAuXzFpU1FFSkZFcmhWUWt5WTlVT29laWoge1xcbiAgICBsZWZ0OiAtMjRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlIDAgMCA1MCU7XFxuICB9XFxuICAuXzFPMnNPdnh6WG5tNDhyODRxd08zZkIgLl8zTnlrRFg2c203UnJOYmtXZkdEcW9xIHtcXG4gICAgdG9wOiAtMjRweDtcXG4gICAgYm9yZGVyLXJhZGl1czogNTAlIDUwJSAwIDA7XFxuICB9XFxuICBAa2V5ZnJhbWVzIF8xTzJzT3Z4elhubTQ4cjg0cXdPM2ZCIHtcXG4gICAgMCUge1xcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC45NSk7XFxuICAgIH1cXG4gICAgNSUge1xcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUoMS4xKTtcXG4gICAgfVxcbiAgICAzOSUge1xcbiAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XFxuICAgIH1cXG4gICAgNDUlIHtcXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xcbiAgICB9XFxuICAgIDYwJSB7XFxuICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwLjk1KTtcXG4gICAgfVxcbiAgICAxMDAlIHtcXG4gICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XFxuICAgIH1cXG4gIH1cIiwgXCJcIl0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwibGRzLWhlYXJ0XCI6IFwiXzFPMnNPdnh6WG5tNDhyODRxd08zZkJcIixcblx0XCJkaXYtYWZ0ZXJcIjogXCJfM055a0RYNnNtN1JyTmJrV2ZHRHFvcVwiLFxuXHRcImRpdi1iZWZvcmVcIjogXCJfMWlTUUVKRkVyaFZRa3lZOVVPb2VpalwiXG59O1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8xdS1UcVQxWWRGTDB3UjFjMThRZFg5IHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiA4MHB4O1xcbiAgaGVpZ2h0OiA4MHB4O1xcbn1cXG4ueXQzd0FXV045UldzYWVmbXVpUFFoIHtcXG4gIGNvbnRlbnQ6ICcgJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgd2lkdGg6IDA7XFxuICBoZWlnaHQ6IDA7XFxuICBtYXJnaW46IDhweDtcXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxuICBib3JkZXI6IDMycHggc29saWQgI2ZmZjtcXG4gIGJvcmRlci1jb2xvcjogI2ZmZiB0cmFuc3BhcmVudCAjZmZmIHRyYW5zcGFyZW50O1xcbiAgYW5pbWF0aW9uOiBfMXUtVHFUMVlkRkwwd1IxYzE4UWRYOSAxLjJzIGluZmluaXRlO1xcbn1cXG5Aa2V5ZnJhbWVzIF8xdS1UcVQxWWRGTDB3UjFjMThRZFg5IHtcXG4gIDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMCk7XFxuICAgIGFuaW1hdGlvbi10aW1pbmctZnVuY3Rpb246IGN1YmljLWJlemllcigwLjU1LCAwLjA1NSwgMC42NzUsIDAuMTkpO1xcbiAgfVxcbiAgNTAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoOTAwZGVnKTtcXG4gICAgYW5pbWF0aW9uLXRpbWluZy1mdW5jdGlvbjogY3ViaWMtYmV6aWVyKDAuMjE1LCAwLjYxLCAwLjM1NSwgMSk7XFxuICB9XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwMGRlZyk7XFxuICB9XFxufVxcblwiLCBcIlwiXSk7XG4vLyBFeHBvcnRzXG5leHBvcnRzLmxvY2FscyA9IHtcblx0XCJsZHMtaG91cmdsYXNzXCI6IFwiXzF1LVRxVDFZZEZMMHdSMWMxOFFkWDlcIixcblx0XCJsZHMtaG91cmdsYXNzLWFmdGVyXCI6IFwieXQzd0FXV045UldzYWVmbXVpUFFoXCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qc1wiKTtcbmV4cG9ydHMgPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18oZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkge1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDgwcHg7XFxuICBoZWlnaHQ6IDgwcHg7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAqIHtcXG4gIC0tY2VudGVyOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAuXzJTVGw3VDh6WFg3U09iZEVrVGRaR2Uge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDE1cHg7XFxuICBoZWlnaHQ6IDE1cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB0b3A6IDUwJTtcXG4gIGxlZnQ6IDUwJTtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fMjBKVEs3VzVnSmhuWmY2MndWY2VBLSxcXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLl8zVTJLM0RyX2luQmJ1NWROR3BYRnlfIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogNTAlO1xcbiAgbGVmdDogNTAlO1xcbn1cXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLl8xZUxESUo0NjNVdUpqaDFDdl9VY2pCIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiAzMXB4O1xcbiAgaGVpZ2h0OiAzMXB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYm9yZGVyOiAzcHggc29saWQ7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAuXzE4cmlfUnQ3eWNTZGQtakhNdHhNUWwge1xcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCB0cmFuc3BhcmVudDtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKSByb3RhdGUoNDVkZWcpO1xcbn1cXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLl8yczEwdjFxYkdkbUxUOUZmZnZMNUREIHtcXG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQ7XFxuICB0cmFuc2Zvcm06IHZhcigtLWNlbnRlcikgcm90YXRlKDI1ZGVnKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5ZaXM5R29Ebml0Tkh1U09NTzc4YWoge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA1MCU7XFxuICBsZWZ0OiA1MCU7XFxuICB3aWR0aDogOHB4O1xcbiAgaGVpZ2h0OiA4cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB0cmFuc2Zvcm06IHZhcigtLWNlbnRlcikgdHJhbnNsYXRlKDE3cHgsIDApO1xcbn1cXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLnd3Yy0tWThjaFk0TUh2MjhZTFZPbCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDUwJTtcXG4gIGxlZnQ6IDUwJTtcXG4gIHdpZHRoOiA4cHg7XFxuICBoZWlnaHQ6IDhweDtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKSB0cmFuc2xhdGUoLTE3cHgsIDApO1xcbn1cXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLl85Q2dpRHN3SDBjT21nUnJoeEU5MFAge1xcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCB0cmFuc3BhcmVudDtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKSByb3RhdGUoNjVkZWcpIHNjYWxlKC0xLCAtMSk7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAuXzFkTl9HMjhkeHFmckdRTFRHSXBGQ08ge1xcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCB0cmFuc3BhcmVudDtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKSByb3RhdGUoNDVkZWcpIHNjYWxlKC0xLCAtMSk7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAuXzNvQkVVUGNLX0Z5RzQtTUxvWFBjVUwge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDYwcHg7XFxuICBoZWlnaHQ6IDYwcHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBib3JkZXI6IDNweCBzb2xpZDtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fMjVKYmRhZnRyak5FcWEwc050SDZZVCB7XFxuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50O1xcbiAgdHJhbnNmb3JtOiB2YXIoLS1jZW50ZXIpIHJvdGF0ZSg2NWRlZyk7XFxufVxcbi5fM0otRG9seDZxVlFSbFNna0lPVEVqOSAuXzN4YmtBTTdoRjR5b3dBeUlFSmF4d1Yge1xcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCB0cmFuc3BhcmVudCB0cmFuc3BhcmVudDtcXG4gIHRyYW5zZm9ybTogdmFyKC0tY2VudGVyKSByb3RhdGUoNDVkZWcpO1xcbn1cXG4uXzNKLURvbHg2cVZRUmxTZ2tJT1RFajkgLl8yV3FDYnlJR1RYLUttU1ZnbWYxdG9qIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogNTAlO1xcbiAgbGVmdDogNTAlO1xcbiAgd2lkdGg6IDlweDtcXG4gIGhlaWdodDogOXB4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgdHJhbnNmb3JtOiB2YXIoLS1jZW50ZXIpIHRyYW5zbGF0ZSgzMnB4LCAwKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5QemNZVGtqQThMTzdDdi1OSWpqSWsge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiA1MCU7XFxuICBsZWZ0OiA1MCU7XFxuICB3aWR0aDogOXB4O1xcbiAgaGVpZ2h0OiA5cHg7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICB0cmFuc2Zvcm06IHZhcigtLWNlbnRlcikgdHJhbnNsYXRlKC0zMnB4LCAwKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fM2F0eXdfaUNPd1BuOWwtZmNnUUQxRiB7XFxuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50O1xcbiAgdHJhbnNmb3JtOiB2YXIoLS1jZW50ZXIpIHJvdGF0ZSg2NWRlZykgc2NhbGUoLTEsIC0xKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fMlhuTGpjdkZhSk1CMi15c3YwMzFVOCB7XFxuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50O1xcbiAgdHJhbnNmb3JtOiB2YXIoLS1jZW50ZXIpIHJvdGF0ZSg0NWRlZykgc2NhbGUoLTEsIC0xKTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fMjBKVEs3VzVnSmhuWmY2MndWY2VBLSB7XFxuICBhbmltYXRpb246IF8zNDQ2REdEWWcwVXF1dFNrYUJlTk1LIDRzIGxpbmVhciBpbmZpbml0ZTtcXG59XFxuLl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5IC5fM1UySzNEcl9pbkJidTVkTkdwWEZ5XyB7XFxuICBhbmltYXRpb246IF8zNDQ2REdEWWcwVXF1dFNrYUJlTk1LIDNzIGxpbmVhciBpbmZpbml0ZTtcXG59XFxuQGtleWZyYW1lcyBfMzQ0NkRHRFlnMFVxdXRTa2FCZU5NSyB7XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcXG4gIH1cXG59XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1vcmJpdGFsc1wiOiBcIl8zSi1Eb2x4NnFWUVJsU2drSU9URWo5XCIsXG5cdFwiY2VudGVyXCI6IFwiXzJTVGw3VDh6WFg3U09iZEVrVGRaR2VcIixcblx0XCJvdXRlci1zcGluXCI6IFwiXzIwSlRLN1c1Z0poblpmNjJ3VmNlQS1cIixcblx0XCJpbm5lci1zcGluXCI6IFwiXzNVMkszRHJfaW5CYnU1ZE5HcFhGeV9cIixcblx0XCJpbm5lci1hcmNcIjogXCJfMWVMRElKNDYzVXVKamgxQ3ZfVWNqQlwiLFxuXHRcImlubmVyLWFyY19zdGFydC1hXCI6IFwiXzE4cmlfUnQ3eWNTZGQtakhNdHhNUWxcIixcblx0XCJpbm5lci1hcmNfZW5kLWFcIjogXCJfMnMxMHYxcWJHZG1MVDlGZmZ2TDVERFwiLFxuXHRcImlubmVyLW1vb24tYVwiOiBcIllpczlHb0RuaXROSHVTT01PNzhhalwiLFxuXHRcImlubmVyLW1vb24tYlwiOiBcInd3Yy0tWThjaFk0TUh2MjhZTFZPbFwiLFxuXHRcImlubmVyLWFyY19zdGFydC1iXCI6IFwiXzlDZ2lEc3dIMGNPbWdScmh4RTkwUFwiLFxuXHRcImlubmVyLWFyY19lbmQtYlwiOiBcIl8xZE5fRzI4ZHhxZnJHUUxUR0lwRkNPXCIsXG5cdFwib3V0ZXItYXJjXCI6IFwiXzNvQkVVUGNLX0Z5RzQtTUxvWFBjVUxcIixcblx0XCJvdXRlci1hcmNfc3RhcnQtYVwiOiBcIl8yNUpiZGFmdHJqTkVxYTBzTnRINllUXCIsXG5cdFwib3V0ZXItYXJjX2VuZC1hXCI6IFwiXzN4YmtBTTdoRjR5b3dBeUlFSmF4d1ZcIixcblx0XCJvdXRlci1tb29uLWFcIjogXCJfMldxQ2J5SUdUWC1LbVNWZ21mMXRvalwiLFxuXHRcIm91dGVyLW1vb24tYlwiOiBcIlB6Y1lUa2pBOExPN0N2LU5JampJa1wiLFxuXHRcIm91dGVyLWFyY19zdGFydC1iXCI6IFwiXzNhdHl3X2lDT3dQbjlsLWZjZ1FEMUZcIixcblx0XCJvdXRlci1hcmNfZW5kLWJcIjogXCJfMlhuTGpjdkZhSk1CMi15c3YwMzFVOFwiLFxuXHRcInNwaW5cIjogXCJfMzQ0NkRHRFlnMFVxdXRTa2FCZU5NS1wiXG59O1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8xaldVZ3lpRkVNLU05aVFValM0WnFoIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIGhlaWdodDogNjRweDtcXG4gIHdpZHRoOiA2NHB4O1xcbiAgbWFyZ2luOiAwLjVlbTtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxuICBib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIDAuMSkgaW5zZXQsIDAgMCAyNXB4IHJnYmEoMCwgMCwgMjU1LCAwLjA3NSk7XFxufVxcblxcbi5fMWpXVWd5aUZFTS1NOWlRVWpTNFpxaDphZnRlciB7XFxuICBjb250ZW50OiAnJztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHRvcDogMTUlO1xcbiAgbGVmdDogMTUlO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBoZWlnaHQ6IDcwJTtcXG4gIHdpZHRoOiA3MCU7XFxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICNmMmYyZjI7XFxuICBib3JkZXItcmFkaXVzOiA1MCU7XFxuICBib3gtc2hhZG93OiAwIDAgMTBweCByZ2JhKDAsIDAsIDAsIDAuMSk7XFxufVxcbi5fMWpXVWd5aUZFTS1NOWlRVWpTNFpxaCA+IHNwYW4ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDUwJTtcXG4gIG92ZXJmbG93OiBoaWRkZW47XFxufVxcbi5fMWpXVWd5aUZFTS1NOWlRVWpTNFpxaCA+IC5fMWJDSzNUeEdvbEFXcUlWVURkdzRseiB7XFxuICBsZWZ0OiAwO1xcbn1cXG4uXzFqV1VneWlGRU0tTTlpUVVqUzRacWggPiAucW16SHYwMkFtVVR1elp0SUpiQUlzIHtcXG4gIGxlZnQ6IDUwJTtcXG59XFxuXFxuLl8xaldVZ3lpRkVNLU05aVFValM0WnFoID4gLl8xYkNLM1R4R29sQVdxSVZVRGR3NGx6ID4gLl8zRWNLOEhQd25oWUozOE1wUTB3OVZqLFxcbi5fMWpXVWd5aUZFTS1NOWlRVWpTNFpxaCA+IC5xbXpIdjAyQW1VVHV6WnRJSmJBSXMgPiAuXzNFY0s4SFB3bmhZSjM4TXBRMHc5Vmoge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgbGVmdDogMTAwJTtcXG4gIHRvcDogMDtcXG4gIGhlaWdodDogMTAwJTtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgYm9yZGVyLXJhZGl1czogOTk5cHg7XFxuICBiYWNrZ3JvdW5kOiBub25lIHJlcGVhdCBzY3JvbGwgMCAwICM1MDhlYzM7XFxuICBhbmltYXRpb246IF8zaHRRQnZRWDMzb3JXSkFERWFrakQ4IDNzIGluZmluaXRlO1xcbiAgb3BhY2l0eTogMC44O1xcbiAgdHJhbnNmb3JtLW9yaWdpbjogMCA1MCUgMDtcXG59XFxuLl8xaldVZ3lpRkVNLU05aVFValM0WnFoID4gLl8xYkNLM1R4R29sQVdxSVZVRGR3NGx6ID4gLl8zRWNLOEhQd25oWUozOE1wUTB3OVZqIHtcXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDA7XFxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xcbn1cXG4uXzFqV1VneWlGRU0tTTlpUVVqUzRacWggPiAucW16SHYwMkFtVVR1elp0SUpiQUlzID4gLl8zRWNLOEhQd25oWUozOE1wUTB3OVZqIHtcXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1xcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XFxuICBsZWZ0OiAtMTAwJTtcXG4gIHRyYW5zZm9ybS1vcmlnaW46IDEwMCUgNTAlIDA7XFxufVxcblxcbkBrZXlmcmFtZXMgXzNodFFCdlFYMzNvcldKQURFYWtqRDgge1xcbiAgMCUge1xcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcXG4gIH1cXG4gIDI1JSB7XFxuICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xcbiAgfVxcbiAgNTAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMTgwZGVnKTtcXG4gIH1cXG4gIDc1JSB7XFxuICAgIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7XFxuICB9XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcXG4gIH1cXG59XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1vdXJvYm9yb1wiOiBcIl8xaldVZ3lpRkVNLU05aVFValM0WnFoXCIsXG5cdFwibGVmdFwiOiBcIl8xYkNLM1R4R29sQVdxSVZVRGR3NGx6XCIsXG5cdFwicmlnaHRcIjogXCJxbXpIdjAyQW1VVHV6WnRJSmJBSXNcIixcblx0XCJhbmltXCI6IFwiXzNFY0s4SFB3bmhZSjM4TXBRMHc5VmpcIixcblx0XCJsZHMtb3Vyb2Jvcm8tcm90YXRlXCI6IFwiXzNodFFCdlFYMzNvcldKQURFYWtqRDhcIlxufTtcbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0cztcbiIsIi8vIEltcG9ydHNcbnZhciBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCIpO1xuZXhwb3J0cyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyhmYWxzZSk7XG4vLyBNb2R1bGVcbmV4cG9ydHMucHVzaChbbW9kdWxlLmlkLCBcIi5fMWV2WElMNWdacVBrTVFyZmJjT0t6dSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxufVxcbi5fMWV2WElMNWdacVBrTVFyZmJjT0t6dSBkaXYge1xcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgYm9yZGVyOiA4cHggc29saWQgI2ZmZjtcXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcXG4gIGFuaW1hdGlvbjogXzFldlhJTDVnWnFQa01RcmZiY09LenUgMS4ycyBjdWJpYy1iZXppZXIoMC41LCAwLCAwLjUsIDEpIGluZmluaXRlO1xcbiAgYm9yZGVyLWNvbG9yOiAjZmZmIHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50O1xcbn1cXG4uXzFldlhJTDVnWnFQa01RcmZiY09LenUgZGl2Om50aC1jaGlsZCgxKSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjQ1cztcXG59XFxuLl8xZXZYSUw1Z1pxUGtNUXJmYmNPS3p1IGRpdjpudGgtY2hpbGQoMikge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4zcztcXG59XFxuLl8xZXZYSUw1Z1pxUGtNUXJmYmNPS3p1IGRpdjpudGgtY2hpbGQoMykge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xNXM7XFxufVxcbkBrZXlmcmFtZXMgXzFldlhJTDVnWnFQa01RcmZiY09LenUge1xcbiAgMCUge1xcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwZGVnKTtcXG4gIH1cXG4gIDEwMCUge1xcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xcbiAgfVxcbn1cXG5cIiwgXCJcIl0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwibGRzLXJpbmdcIjogXCJfMWV2WElMNWdacVBrTVFyZmJjT0t6dVwiXG59O1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8za0xScExkRERrb3lBUTlCSlc5R0NDIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiA4MHB4O1xcbiAgaGVpZ2h0OiA4MHB4O1xcbn1cXG5cXG4uXzNrTFJwTGRERGtveUFROUJKVzlHQ0MgZGl2IHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGJvcmRlcjogNHB4IHNvbGlkICNmZmY7XFxuICBvcGFjaXR5OiAxO1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYW5pbWF0aW9uOiBfM2tMUnBMZEREa295QVE5QkpXOUdDQyAxcyBjdWJpYy1iZXppZXIoMCwgMC4yLCAwLjgsIDEpIGluZmluaXRlO1xcbn1cXG5cXG4uXzNrTFJwTGRERGtveUFROUJKVzlHQ0MgZGl2Om50aC1jaGlsZCgyKSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjVzO1xcbn1cXG5cXG5Aa2V5ZnJhbWVzIF8za0xScExkRERrb3lBUTlCSlc5R0NDIHtcXG4gIDAlIHtcXG4gICAgdG9wOiA0NSU7XFxuICAgIGxlZnQ6IDQ1JTtcXG4gICAgd2lkdGg6IDA7XFxuICAgIGhlaWdodDogMDtcXG4gICAgb3BhY2l0eTogMTtcXG4gIH1cXG5cXG4gIDEwMCUge1xcbiAgICB0b3A6IDBweDtcXG4gICAgbGVmdDogMHB4O1xcbiAgICB3aWR0aDogOTAlO1xcbiAgICBoZWlnaHQ6IDkwJTtcXG4gICAgb3BhY2l0eTogMDtcXG4gIH1cXG59XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1yaXBwbGVcIjogXCJfM2tMUnBMZEREa295QVE5QkpXOUdDQ1wiXG59O1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzO1xuIiwiLy8gSW1wb3J0c1xudmFyIF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIik7XG5leHBvcnRzID0gX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fKGZhbHNlKTtcbi8vIE1vZHVsZVxuZXhwb3J0cy5wdXNoKFttb2R1bGUuaWQsIFwiLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIHtcXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHdpZHRoOiA4MHB4O1xcbiAgaGVpZ2h0OiA4MHB4O1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgPiBkaXYge1xcbiAgYW5pbWF0aW9uOiBfMnB6aTBkUTYwX3lyRm0tVGpBa01MMSAxLjJzIGN1YmljLWJlemllcigwLjUsIDAsIDAuNSwgMSkgaW5maW5pdGU7XFxuICB0cmFuc2Zvcm0tb3JpZ2luOiA0MHB4IDQwcHg7XFxufVxcbi5fMnB6aTBkUTYwX3lyRm0tVGpBa01MMSBkaXYgLl8xX1VINjUxX1VnaHYyS1pLd05tUFFJIHtcXG4gIGNvbnRlbnQ6ICcgJztcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgd2lkdGg6IDdweDtcXG4gIGhlaWdodDogN3B4O1xcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG4gIG1hcmdpbjogLTRweCAwIDAgLTRweDtcXG59XFxuLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIGRpdjpudGgtY2hpbGQoMSkge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4wMzZzO1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCgxKSAuXzFfVUg2NTFfVWdodjJLWkt3Tm1QUUkge1xcbiAgdG9wOiA2M3B4O1xcbiAgbGVmdDogNjNweDtcXG59XFxuLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIGRpdjpudGgtY2hpbGQoMikge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4wNzJzO1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCgyKSAuXzFfVUg2NTFfVWdodjJLWkt3Tm1QUUkge1xcbiAgdG9wOiA2OHB4O1xcbiAgbGVmdDogNTZweDtcXG59XFxuLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIGRpdjpudGgtY2hpbGQoMykge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xMDhzO1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCgzKSAuXzFfVUg2NTFfVWdodjJLWkt3Tm1QUUkge1xcbiAgdG9wOiA3MXB4O1xcbiAgbGVmdDogNDhweDtcXG59XFxuLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIGRpdjpudGgtY2hpbGQoNCkge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xNDRzO1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCg0KSAuXzFfVUg2NTFfVWdodjJLWkt3Tm1QUUkge1xcbiAgdG9wOiA3MnB4O1xcbiAgbGVmdDogNDBweDtcXG59XFxuLl8ycHppMGRRNjBfeXJGbS1UakFrTUwxIGRpdjpudGgtY2hpbGQoNSkge1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xOHM7XFxufVxcbi5fMnB6aTBkUTYwX3lyRm0tVGpBa01MMSBkaXY6bnRoLWNoaWxkKDUpIC5fMV9VSDY1MV9VZ2h2MktaS3dObVBRSSB7XFxuICB0b3A6IDcxcHg7XFxuICBsZWZ0OiAzMnB4O1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCg2KSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjIxNnM7XFxufVxcbi5fMnB6aTBkUTYwX3lyRm0tVGpBa01MMSBkaXY6bnRoLWNoaWxkKDYpIC5fMV9VSDY1MV9VZ2h2MktaS3dObVBRSSB7XFxuICB0b3A6IDY4cHg7XFxuICBsZWZ0OiAyNHB4O1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCg3KSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjI1MnM7XFxufVxcbi5fMnB6aTBkUTYwX3lyRm0tVGpBa01MMSBkaXY6bnRoLWNoaWxkKDcpIC5fMV9VSDY1MV9VZ2h2MktaS3dObVBRSSB7XFxuICB0b3A6IDYzcHg7XFxuICBsZWZ0OiAxN3B4O1xcbn1cXG4uXzJwemkwZFE2MF95ckZtLVRqQWtNTDEgZGl2Om50aC1jaGlsZCg4KSB7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjI4OHM7XFxufVxcbi5fMnB6aTBkUTYwX3lyRm0tVGpBa01MMSBkaXY6bnRoLWNoaWxkKDgpIC5fMV9VSDY1MV9VZ2h2MktaS3dObVBRSSB7XFxuICB0b3A6IDU2cHg7XFxuICBsZWZ0OiAxMnB4O1xcbn1cXG5Aa2V5ZnJhbWVzIF8ycHppMGRRNjBfeXJGbS1UakFrTUwxIHtcXG4gIDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XFxuICB9XFxuICAxMDAlIHtcXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcXG4gIH1cXG59XFxuXCIsIFwiXCJdKTtcbi8vIEV4cG9ydHNcbmV4cG9ydHMubG9jYWxzID0ge1xuXHRcImxkcy1yb2xsZXJcIjogXCJfMnB6aTBkUTYwX3lyRm0tVGpBa01MMVwiLFxuXHRcImRpdi1hZnRlclwiOiBcIl8xX1VINjUxX1VnaHYyS1pLd05tUFFJXCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCIvLyBJbXBvcnRzXG52YXIgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fID0gcmVxdWlyZShcIi4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9ydW50aW1lL2FwaS5qc1wiKTtcbmV4cG9ydHMgPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18oZmFsc2UpO1xuLy8gTW9kdWxlXG5leHBvcnRzLnB1c2goW21vZHVsZS5pZCwgXCIuXzFuN3lIWExCVmtDVkVOZHJCVVBIbHYge1xcbiAgY29sb3I6IG9mZmljaWFsO1xcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDgwcHg7XFxuICBoZWlnaHQ6IDgwcHg7XFxufVxcbi5fMW43eUhYTEJWa0NWRU5kckJVUEhsdiBkaXYge1xcbiAgdHJhbnNmb3JtLW9yaWdpbjogNDBweCA0MHB4O1xcbiAgYW5pbWF0aW9uOiBfMW43eUhYTEJWa0NWRU5kckJVUEhsdiAxLjJzIGxpbmVhciBpbmZpbml0ZTtcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdiAuXzNORlZ4cnhnNTRRT3FlY2pLQjVxamQge1xcbiAgY29udGVudDogJyAnO1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDNweDtcXG4gIGxlZnQ6IDM3cHg7XFxuICB3aWR0aDogNnB4O1xcbiAgaGVpZ2h0OiAxOHB4O1xcbiAgYm9yZGVyLXJhZGl1czogMjAlO1xcbiAgYmFja2dyb3VuZDogI2ZmZjtcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoMSkge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XFxuICBhbmltYXRpb24tZGVsYXk6IC0xLjFzO1xcbn1cXG4uXzFuN3lIWExCVmtDVkVOZHJCVVBIbHYgZGl2Om50aC1jaGlsZCgyKSB7XFxuICB0cmFuc2Zvcm06IHJvdGF0ZSgzMGRlZyk7XFxuICBhbmltYXRpb24tZGVsYXk6IC0xcztcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoMykge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoNjBkZWcpO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC45cztcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoNCkge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC44cztcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoNSkge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTIwZGVnKTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuN3M7XFxufVxcbi5fMW43eUhYTEJWa0NWRU5kckJVUEhsdiBkaXY6bnRoLWNoaWxkKDYpIHtcXG4gIHRyYW5zZm9ybTogcm90YXRlKDE1MGRlZyk7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjZzO1xcbn1cXG4uXzFuN3lIWExCVmtDVkVOZHJCVVBIbHYgZGl2Om50aC1jaGlsZCg3KSB7XFxuICB0cmFuc2Zvcm06IHJvdGF0ZSgxODBkZWcpO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC41cztcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoOCkge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjEwZGVnKTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNHM7XFxufVxcbi5fMW43eUhYTEJWa0NWRU5kckJVUEhsdiBkaXY6bnRoLWNoaWxkKDkpIHtcXG4gIHRyYW5zZm9ybTogcm90YXRlKDI0MGRlZyk7XFxuICBhbmltYXRpb24tZGVsYXk6IC0wLjNzO1xcbn1cXG4uXzFuN3lIWExCVmtDVkVOZHJCVVBIbHYgZGl2Om50aC1jaGlsZCgxMCkge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjcwZGVnKTtcXG4gIGFuaW1hdGlvbi1kZWxheTogLTAuMnM7XFxufVxcbi5fMW43eUhYTEJWa0NWRU5kckJVUEhsdiBkaXY6bnRoLWNoaWxkKDExKSB7XFxuICB0cmFuc2Zvcm06IHJvdGF0ZSgzMDBkZWcpO1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC4xcztcXG59XFxuLl8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IGRpdjpudGgtY2hpbGQoMTIpIHtcXG4gIHRyYW5zZm9ybTogcm90YXRlKDMzMGRlZyk7XFxuICBhbmltYXRpb24tZGVsYXk6IDBzO1xcbn1cXG5Aa2V5ZnJhbWVzIF8xbjd5SFhMQlZrQ1ZFTmRyQlVQSGx2IHtcXG4gIDAlIHtcXG4gICAgb3BhY2l0eTogMTtcXG4gIH1cXG4gIDEwMCUge1xcbiAgICBvcGFjaXR5OiAwO1xcbiAgfVxcbn1cXG5cIiwgXCJcIl0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0cy5sb2NhbHMgPSB7XG5cdFwibGRzLXNwaW5uZXJcIjogXCJfMW43eUhYTEJWa0NWRU5kckJVUEhsdlwiLFxuXHRcImRpdi1hZnRlclwiOiBcIl8zTkZWeHJ4ZzU0UU9xZWNqS0I1cWpkXCJcbn07XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHM7XG4iLCJcInVzZSBzdHJpY3RcIjtcblxuLypcbiAgTUlUIExpY2Vuc2UgaHR0cDovL3d3dy5vcGVuc291cmNlLm9yZy9saWNlbnNlcy9taXQtbGljZW5zZS5waHBcbiAgQXV0aG9yIFRvYmlhcyBLb3BwZXJzIEBzb2tyYVxuKi9cbi8vIGNzcyBiYXNlIGNvZGUsIGluamVjdGVkIGJ5IHRoZSBjc3MtbG9hZGVyXG4vLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAodXNlU291cmNlTWFwKSB7XG4gIHZhciBsaXN0ID0gW107IC8vIHJldHVybiB0aGUgbGlzdCBvZiBtb2R1bGVzIGFzIGNzcyBzdHJpbmdcblxuICBsaXN0LnRvU3RyaW5nID0gZnVuY3Rpb24gdG9TdHJpbmcoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICB2YXIgY29udGVudCA9IGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKTtcblxuICAgICAgaWYgKGl0ZW1bMl0pIHtcbiAgICAgICAgcmV0dXJuIFwiQG1lZGlhIFwiLmNvbmNhdChpdGVtWzJdLCBcIiB7XCIpLmNvbmNhdChjb250ZW50LCBcIn1cIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb250ZW50O1xuICAgIH0pLmpvaW4oJycpO1xuICB9OyAvLyBpbXBvcnQgYSBsaXN0IG9mIG1vZHVsZXMgaW50byB0aGUgbGlzdFxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgZnVuYy1uYW1lc1xuXG5cbiAgbGlzdC5pID0gZnVuY3Rpb24gKG1vZHVsZXMsIG1lZGlhUXVlcnksIGRlZHVwZSkge1xuICAgIGlmICh0eXBlb2YgbW9kdWxlcyA9PT0gJ3N0cmluZycpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICAgICAgbW9kdWxlcyA9IFtbbnVsbCwgbW9kdWxlcywgJyddXTtcbiAgICB9XG5cbiAgICB2YXIgYWxyZWFkeUltcG9ydGVkTW9kdWxlcyA9IHt9O1xuXG4gICAgaWYgKGRlZHVwZSkge1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBwcmVmZXItZGVzdHJ1Y3R1cmluZ1xuICAgICAgICB2YXIgaWQgPSB0aGlzW2ldWzBdO1xuXG4gICAgICAgIGlmIChpZCAhPSBudWxsKSB7XG4gICAgICAgICAgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpZF0gPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IG1vZHVsZXMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgaXRlbSA9IFtdLmNvbmNhdChtb2R1bGVzW19pXSk7XG5cbiAgICAgIGlmIChkZWR1cGUgJiYgYWxyZWFkeUltcG9ydGVkTW9kdWxlc1tpdGVtWzBdXSkge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tY29udGludWVcbiAgICAgICAgY29udGludWU7XG4gICAgICB9XG5cbiAgICAgIGlmIChtZWRpYVF1ZXJ5KSB7XG4gICAgICAgIGlmICghaXRlbVsyXSkge1xuICAgICAgICAgIGl0ZW1bMl0gPSBtZWRpYVF1ZXJ5O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGl0ZW1bMl0gPSBcIlwiLmNvbmNhdChtZWRpYVF1ZXJ5LCBcIiBhbmQgXCIpLmNvbmNhdChpdGVtWzJdKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBsaXN0LnB1c2goaXRlbSk7XG4gICAgfVxuICB9O1xuXG4gIHJldHVybiBsaXN0O1xufTtcblxuZnVuY3Rpb24gY3NzV2l0aE1hcHBpbmdUb1N0cmluZyhpdGVtLCB1c2VTb3VyY2VNYXApIHtcbiAgdmFyIGNvbnRlbnQgPSBpdGVtWzFdIHx8ICcnOyAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcblxuICB2YXIgY3NzTWFwcGluZyA9IGl0ZW1bM107XG5cbiAgaWYgKCFjc3NNYXBwaW5nKSB7XG4gICAgcmV0dXJuIGNvbnRlbnQ7XG4gIH1cblxuICBpZiAodXNlU291cmNlTWFwICYmIHR5cGVvZiBidG9hID09PSAnZnVuY3Rpb24nKSB7XG4gICAgdmFyIHNvdXJjZU1hcHBpbmcgPSB0b0NvbW1lbnQoY3NzTWFwcGluZyk7XG4gICAgdmFyIHNvdXJjZVVSTHMgPSBjc3NNYXBwaW5nLnNvdXJjZXMubWFwKGZ1bmN0aW9uIChzb3VyY2UpIHtcbiAgICAgIHJldHVybiBcIi8qIyBzb3VyY2VVUkw9XCIuY29uY2F0KGNzc01hcHBpbmcuc291cmNlUm9vdCB8fCAnJykuY29uY2F0KHNvdXJjZSwgXCIgKi9cIik7XG4gICAgfSk7XG4gICAgcmV0dXJuIFtjb250ZW50XS5jb25jYXQoc291cmNlVVJMcykuY29uY2F0KFtzb3VyY2VNYXBwaW5nXSkuam9pbignXFxuJyk7XG4gIH1cblxuICByZXR1cm4gW2NvbnRlbnRdLmpvaW4oJ1xcbicpO1xufSAvLyBBZGFwdGVkIGZyb20gY29udmVydC1zb3VyY2UtbWFwIChNSVQpXG5cblxuZnVuY3Rpb24gdG9Db21tZW50KHNvdXJjZU1hcCkge1xuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW5kZWZcbiAgdmFyIGJhc2U2NCA9IGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KHNvdXJjZU1hcCkpKSk7XG4gIHZhciBkYXRhID0gXCJzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtjaGFyc2V0PXV0Zi04O2Jhc2U2NCxcIi5jb25jYXQoYmFzZTY0KTtcbiAgcmV0dXJuIFwiLyojIFwiLmNvbmNhdChkYXRhLCBcIiAqL1wiKTtcbn0iLCIvKlxub2JqZWN0LWFzc2lnblxuKGMpIFNpbmRyZSBTb3JodXNcbkBsaWNlbnNlIE1JVFxuKi9cblxuJ3VzZSBzdHJpY3QnO1xuLyogZXNsaW50LWRpc2FibGUgbm8tdW51c2VkLXZhcnMgKi9cbnZhciBnZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzO1xudmFyIGhhc093blByb3BlcnR5ID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTtcbnZhciBwcm9wSXNFbnVtZXJhYmxlID0gT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZTtcblxuZnVuY3Rpb24gdG9PYmplY3QodmFsKSB7XG5cdGlmICh2YWwgPT09IG51bGwgfHwgdmFsID09PSB1bmRlZmluZWQpIHtcblx0XHR0aHJvdyBuZXcgVHlwZUVycm9yKCdPYmplY3QuYXNzaWduIGNhbm5vdCBiZSBjYWxsZWQgd2l0aCBudWxsIG9yIHVuZGVmaW5lZCcpO1xuXHR9XG5cblx0cmV0dXJuIE9iamVjdCh2YWwpO1xufVxuXG5mdW5jdGlvbiBzaG91bGRVc2VOYXRpdmUoKSB7XG5cdHRyeSB7XG5cdFx0aWYgKCFPYmplY3QuYXNzaWduKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdFx0Ly8gRGV0ZWN0IGJ1Z2d5IHByb3BlcnR5IGVudW1lcmF0aW9uIG9yZGVyIGluIG9sZGVyIFY4IHZlcnNpb25zLlxuXG5cdFx0Ly8gaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL3Y4L2lzc3Vlcy9kZXRhaWw/aWQ9NDExOFxuXHRcdHZhciB0ZXN0MSA9IG5ldyBTdHJpbmcoJ2FiYycpOyAgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1uZXctd3JhcHBlcnNcblx0XHR0ZXN0MVs1XSA9ICdkZSc7XG5cdFx0aWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRlc3QxKVswXSA9PT0gJzUnKSB7XG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdFx0Ly8gaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL3Y4L2lzc3Vlcy9kZXRhaWw/aWQ9MzA1NlxuXHRcdHZhciB0ZXN0MiA9IHt9O1xuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgMTA7IGkrKykge1xuXHRcdFx0dGVzdDJbJ18nICsgU3RyaW5nLmZyb21DaGFyQ29kZShpKV0gPSBpO1xuXHRcdH1cblx0XHR2YXIgb3JkZXIyID0gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGVzdDIpLm1hcChmdW5jdGlvbiAobikge1xuXHRcdFx0cmV0dXJuIHRlc3QyW25dO1xuXHRcdH0pO1xuXHRcdGlmIChvcmRlcjIuam9pbignJykgIT09ICcwMTIzNDU2Nzg5Jykge1xuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdH1cblxuXHRcdC8vIGh0dHBzOi8vYnVncy5jaHJvbWl1bS5vcmcvcC92OC9pc3N1ZXMvZGV0YWlsP2lkPTMwNTZcblx0XHR2YXIgdGVzdDMgPSB7fTtcblx0XHQnYWJjZGVmZ2hpamtsbW5vcHFyc3QnLnNwbGl0KCcnKS5mb3JFYWNoKGZ1bmN0aW9uIChsZXR0ZXIpIHtcblx0XHRcdHRlc3QzW2xldHRlcl0gPSBsZXR0ZXI7XG5cdFx0fSk7XG5cdFx0aWYgKE9iamVjdC5rZXlzKE9iamVjdC5hc3NpZ24oe30sIHRlc3QzKSkuam9pbignJykgIT09XG5cdFx0XHRcdCdhYmNkZWZnaGlqa2xtbm9wcXJzdCcpIHtcblx0XHRcdHJldHVybiBmYWxzZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gdHJ1ZTtcblx0fSBjYXRjaCAoZXJyKSB7XG5cdFx0Ly8gV2UgZG9uJ3QgZXhwZWN0IGFueSBvZiB0aGUgYWJvdmUgdG8gdGhyb3csIGJ1dCBiZXR0ZXIgdG8gYmUgc2FmZS5cblx0XHRyZXR1cm4gZmFsc2U7XG5cdH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBzaG91bGRVc2VOYXRpdmUoKSA/IE9iamVjdC5hc3NpZ24gOiBmdW5jdGlvbiAodGFyZ2V0LCBzb3VyY2UpIHtcblx0dmFyIGZyb207XG5cdHZhciB0byA9IHRvT2JqZWN0KHRhcmdldCk7XG5cdHZhciBzeW1ib2xzO1xuXG5cdGZvciAodmFyIHMgPSAxOyBzIDwgYXJndW1lbnRzLmxlbmd0aDsgcysrKSB7XG5cdFx0ZnJvbSA9IE9iamVjdChhcmd1bWVudHNbc10pO1xuXG5cdFx0Zm9yICh2YXIga2V5IGluIGZyb20pIHtcblx0XHRcdGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKGZyb20sIGtleSkpIHtcblx0XHRcdFx0dG9ba2V5XSA9IGZyb21ba2V5XTtcblx0XHRcdH1cblx0XHR9XG5cblx0XHRpZiAoZ2V0T3duUHJvcGVydHlTeW1ib2xzKSB7XG5cdFx0XHRzeW1ib2xzID0gZ2V0T3duUHJvcGVydHlTeW1ib2xzKGZyb20pO1xuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzeW1ib2xzLmxlbmd0aDsgaSsrKSB7XG5cdFx0XHRcdGlmIChwcm9wSXNFbnVtZXJhYmxlLmNhbGwoZnJvbSwgc3ltYm9sc1tpXSkpIHtcblx0XHRcdFx0XHR0b1tzeW1ib2xzW2ldXSA9IGZyb21bc3ltYm9sc1tpXV07XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9XG5cdH1cblxuXHRyZXR1cm4gdG87XG59O1xuIiwidmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbnZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbmltcG9ydCB7IGpzeCBhcyBfanN4IH0gZnJvbSBcInJlYWN0L2pzeC1ydW50aW1lXCI7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9zdHlsZS5tb2R1bGUuY3NzJztcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIENpcmNsZShfYSkge1xuICAgIHZhciBfYiA9IF9hLmNvbG9yLCBjb2xvciA9IF9iID09PSB2b2lkIDAgPyAnIzdmNThhZicgOiBfYiwgX2MgPSBfYS5zaXplLCBzaXplID0gX2MgPT09IHZvaWQgMCA/IDY0IDogX2MsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZSwgcmVzdCA9IF9fcmVzdChfYSwgW1wiY29sb3JcIiwgXCJzaXplXCIsIFwiY2xhc3NOYW1lXCIsIFwic3R5bGVcIl0pO1xuICAgIHJldHVybiAoX2pzeChcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2xkcy1jaXJjbGUnXSwgY2xhc3NOYW1lKSwgc3R5bGU6IF9fYXNzaWduKHsgYmFja2dyb3VuZDogY29sb3IsIHdpZHRoOiBzaXplLCBoZWlnaHQ6IHNpemUgfSwgc3R5bGUpIH0sIHJlc3QpLCB2b2lkIDApKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsInZhciBhcGkgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTUtMSEuL3N0eWxlLm1vZHVsZS5jc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsInZhciBfX2Fzc2lnbiA9ICh0aGlzICYmIHRoaXMuX19hc3NpZ24pIHx8IGZ1bmN0aW9uICgpIHtcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24odCkge1xuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpXG4gICAgICAgICAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfTtcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG52YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG52YXIgX19zcHJlYWRBcnJheSA9ICh0aGlzICYmIHRoaXMuX19zcHJlYWRBcnJheSkgfHwgZnVuY3Rpb24gKHRvLCBmcm9tKSB7XG4gICAgZm9yICh2YXIgaSA9IDAsIGlsID0gZnJvbS5sZW5ndGgsIGogPSB0by5sZW5ndGg7IGkgPCBpbDsgaSsrLCBqKyspXG4gICAgICAgIHRvW2pdID0gZnJvbVtpXTtcbiAgICByZXR1cm4gdG87XG59O1xuaW1wb3J0IHsganN4IGFzIF9qc3ggfSBmcm9tIFwicmVhY3QvanN4LXJ1bnRpbWVcIjtcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL3N0eWxlLm1vZHVsZS5jc3MnO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gRGVmYXVsdChfYSkge1xuICAgIHZhciBfYiA9IF9hLmNvbG9yLCBjb2xvciA9IF9iID09PSB2b2lkIDAgPyAnIzdmNThhZicgOiBfYiwgX2MgPSBfYS5zaXplLCBzaXplID0gX2MgPT09IHZvaWQgMCA/IDgwIDogX2MsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZSwgcmVzdCA9IF9fcmVzdChfYSwgW1wiY29sb3JcIiwgXCJzaXplXCIsIFwiY2xhc3NOYW1lXCIsIFwic3R5bGVcIl0pO1xuICAgIHZhciBjaXJjbGVzID0gX19zcHJlYWRBcnJheShbXSwgQXJyYXkoMTIpKS5tYXAoZnVuY3Rpb24gKF8sIGluZGV4KSB7IHJldHVybiAoX2pzeChcImRpdlwiLCB7IHN0eWxlOiB7IGJhY2tncm91bmQ6IFwiXCIgKyBjb2xvciwgd2lkdGg6IHNpemUgKiAwLjA3NSwgaGVpZ2h0OiBzaXplICogMC4wNzUgfSB9LCBpbmRleCkpOyB9KTtcbiAgICByZXR1cm4gKF9qc3goXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydsZHMtZGVmYXVsdCddLCBjbGFzc05hbWUpLCBzdHlsZTogX19hc3NpZ24oeyBoZWlnaHQ6IHNpemUsIHdpZHRoOiBzaXplIH0sIHN0eWxlKSB9LCByZXN0LCB7IGNoaWxkcmVuOiBjaXJjbGVzIH0pLCB2b2lkIDApKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsInZhciBhcGkgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTUtMSEuL3N0eWxlLm1vZHVsZS5jc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsInZhciBfX2Fzc2lnbiA9ICh0aGlzICYmIHRoaXMuX19hc3NpZ24pIHx8IGZ1bmN0aW9uICgpIHtcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24odCkge1xuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpXG4gICAgICAgICAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfTtcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG52YXIgX19yZXN0ID0gKHRoaXMgJiYgdGhpcy5fX3Jlc3QpIHx8IGZ1bmN0aW9uIChzLCBlKSB7XG4gICAgdmFyIHQgPSB7fTtcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcbiAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgaWYgKHMgIT0gbnVsbCAmJiB0eXBlb2YgT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICBpZiAoZS5pbmRleE9mKHBbaV0pIDwgMCAmJiBPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwocywgcFtpXSkpXG4gICAgICAgICAgICAgICAgdFtwW2ldXSA9IHNbcFtpXV07XG4gICAgICAgIH1cbiAgICByZXR1cm4gdDtcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBEdWFsUmluZyhfYSkge1xuICAgIHZhciBfYiA9IF9hLmNvbG9yLCBjb2xvciA9IF9iID09PSB2b2lkIDAgPyAnIzdmNThhZicgOiBfYiwgX2MgPSBfYS5zaXplLCBzaXplID0gX2MgPT09IHZvaWQgMCA/IDgwIDogX2MsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZSwgcmVzdCA9IF9fcmVzdChfYSwgW1wiY29sb3JcIiwgXCJzaXplXCIsIFwiY2xhc3NOYW1lXCIsIFwic3R5bGVcIl0pO1xuICAgIHJldHVybiAoX2pzeChcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2xkcy1kdWFsLXJpbmcnXSwgY2xhc3NOYW1lKSwgc3R5bGU6IF9fYXNzaWduKHsgd2lkdGg6IHNpemUsIGhlaWdodDogc2l6ZSB9LCBzdHlsZSkgfSwgcmVzdCwgeyBjaGlsZHJlbjogX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2xkcy1kdWFsLXJpbmctYWZ0ZXInXSksIHN0eWxlOiB7XG4gICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IGNvbG9yICsgXCIgdHJhbnNwYXJlbnRcIixcbiAgICAgICAgICAgICAgICBib3JkZXJXaWR0aDogc2l6ZSAqIDAuMSxcbiAgICAgICAgICAgICAgICB3aWR0aDogc2l6ZSAqIDAuNyAtIDYsXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiBzaXplICogMC43IC0gNixcbiAgICAgICAgICAgIH0gfSwgdm9pZCAwKSB9KSwgdm9pZCAwKSk7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qc1wiKTtcbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS01LTEhLi9zdHlsZS5tb2R1bGUuY3NzXCIpO1xuXG4gICAgICAgICAgICBjb250ZW50ID0gY29udGVudC5fX2VzTW9kdWxlID8gY29udGVudC5kZWZhdWx0IDogY29udGVudDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgICAgICB9XG5cbnZhciBvcHRpb25zID0ge307XG5cbm9wdGlvbnMuaW5zZXJ0ID0gXCJoZWFkXCI7XG5vcHRpb25zLnNpbmdsZXRvbiA9IGZhbHNlO1xuXG52YXIgdXBkYXRlID0gYXBpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTsiLCJ2YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xudmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xudmFyIF9fc3ByZWFkQXJyYXkgPSAodGhpcyAmJiB0aGlzLl9fc3ByZWFkQXJyYXkpIHx8IGZ1bmN0aW9uICh0bywgZnJvbSkge1xuICAgIGZvciAodmFyIGkgPSAwLCBpbCA9IGZyb20ubGVuZ3RoLCBqID0gdG8ubGVuZ3RoOyBpIDwgaWw7IGkrKywgaisrKVxuICAgICAgICB0b1tqXSA9IGZyb21baV07XG4gICAgcmV0dXJuIHRvO1xufTtcbmltcG9ydCB7IGpzeCBhcyBfanN4IH0gZnJvbSBcInJlYWN0L2pzeC1ydW50aW1lXCI7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9zdHlsZS5tb2R1bGUuY3NzJztcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEVsbGlwc2lzKF9hKSB7XG4gICAgdmFyIF9iID0gX2EuY29sb3IsIGNvbG9yID0gX2IgPT09IHZvaWQgMCA/ICcjN2Y1OGFmJyA6IF9iLCBfYyA9IF9hLnNpemUsIHNpemUgPSBfYyA9PT0gdm9pZCAwID8gODAgOiBfYywgY2xhc3NOYW1lID0gX2EuY2xhc3NOYW1lLCBzdHlsZSA9IF9hLnN0eWxlLCByZXN0ID0gX19yZXN0KF9hLCBbXCJjb2xvclwiLCBcInNpemVcIiwgXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiXSk7XG4gICAgdmFyIGNpcmNsZXMgPSBfX3NwcmVhZEFycmF5KFtdLCBBcnJheSg0KSkubWFwKGZ1bmN0aW9uIChfLCBpbmRleCkgeyByZXR1cm4gX2pzeChcImRpdlwiLCB7IHN0eWxlOiB7IGJhY2tncm91bmQ6IFwiXCIgKyBjb2xvciB9IH0sIGluZGV4KTsgfSk7XG4gICAgcmV0dXJuIChfanN4KFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLWVsbGlwc2lzJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbihfX2Fzc2lnbih7fSwgc3R5bGUpLCB7IHdpZHRoOiBzaXplLCBoZWlnaHQ6IHNpemUgfSkgfSwgcmVzdCwgeyBjaGlsZHJlbjogY2lyY2xlcyB9KSwgdm9pZCAwKSk7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qc1wiKTtcbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS01LTEhLi9zdHlsZS5tb2R1bGUuY3NzXCIpO1xuXG4gICAgICAgICAgICBjb250ZW50ID0gY29udGVudC5fX2VzTW9kdWxlID8gY29udGVudC5kZWZhdWx0IDogY29udGVudDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgICAgICB9XG5cbnZhciBvcHRpb25zID0ge307XG5cbm9wdGlvbnMuaW5zZXJ0ID0gXCJoZWFkXCI7XG5vcHRpb25zLnNpbmdsZXRvbiA9IGZhbHNlO1xuXG52YXIgdXBkYXRlID0gYXBpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTsiLCJ2YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xudmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xudmFyIF9fc3ByZWFkQXJyYXkgPSAodGhpcyAmJiB0aGlzLl9fc3ByZWFkQXJyYXkpIHx8IGZ1bmN0aW9uICh0bywgZnJvbSkge1xuICAgIGZvciAodmFyIGkgPSAwLCBpbCA9IGZyb20ubGVuZ3RoLCBqID0gdG8ubGVuZ3RoOyBpIDwgaWw7IGkrKywgaisrKVxuICAgICAgICB0b1tqXSA9IGZyb21baV07XG4gICAgcmV0dXJuIHRvO1xufTtcbmltcG9ydCB7IGpzeCBhcyBfanN4IH0gZnJvbSBcInJlYWN0L2pzeC1ydW50aW1lXCI7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9zdHlsZS5tb2R1bGUuY3NzJztcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEZhY2Vib29rKF9hKSB7XG4gICAgdmFyIF9iID0gX2EuY29sb3IsIGNvbG9yID0gX2IgPT09IHZvaWQgMCA/ICcjN2Y1OGFmJyA6IF9iLCBfYyA9IF9hLnNpemUsIHNpemUgPSBfYyA9PT0gdm9pZCAwID8gODAgOiBfYywgY2xhc3NOYW1lID0gX2EuY2xhc3NOYW1lLCBzdHlsZSA9IF9hLnN0eWxlLCByZXN0ID0gX19yZXN0KF9hLCBbXCJjb2xvclwiLCBcInNpemVcIiwgXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiXSk7XG4gICAgdmFyIGNpcmNsZXMgPSBfX3NwcmVhZEFycmF5KFtdLCBBcnJheSgzKSkubWFwKGZ1bmN0aW9uIChfLCBpbmRleCkgeyByZXR1cm4gX2pzeChcImRpdlwiLCB7IHN0eWxlOiB7IGJhY2tncm91bmQ6IFwiXCIgKyBjb2xvciB9IH0sIGluZGV4KTsgfSk7XG4gICAgcmV0dXJuIChfanN4KFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLWZhY2Vib29rJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7IHdpZHRoOiBzaXplLCBoZWlnaHQ6IHNpemUgfSwgc3R5bGUpIH0sIHJlc3QsIHsgY2hpbGRyZW46IGNpcmNsZXMgfSksIHZvaWQgMCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNS0xIS4vc3R5bGUubW9kdWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307IiwidmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbnZhciBfX3Jlc3QgPSAodGhpcyAmJiB0aGlzLl9fcmVzdCkgfHwgZnVuY3Rpb24gKHMsIGUpIHtcbiAgICB2YXIgdCA9IHt9O1xuICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSAmJiBlLmluZGV4T2YocCkgPCAwKVxuICAgICAgICB0W3BdID0gc1twXTtcbiAgICBpZiAocyAhPSBudWxsICYmIHR5cGVvZiBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBwID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzKTsgaSA8IHAubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcbiAgICAgICAgICAgICAgICB0W3BbaV1dID0gc1twW2ldXTtcbiAgICAgICAgfVxuICAgIHJldHVybiB0O1xufTtcbnZhciBfX3NwcmVhZEFycmF5ID0gKHRoaXMgJiYgdGhpcy5fX3NwcmVhZEFycmF5KSB8fCBmdW5jdGlvbiAodG8sIGZyb20pIHtcbiAgICBmb3IgKHZhciBpID0gMCwgaWwgPSBmcm9tLmxlbmd0aCwgaiA9IHRvLmxlbmd0aDsgaSA8IGlsOyBpKyssIGorKylcbiAgICAgICAgdG9bal0gPSBmcm9tW2ldO1xuICAgIHJldHVybiB0bztcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBHcmlkKF9hKSB7XG4gICAgdmFyIF9iID0gX2EuY29sb3IsIGNvbG9yID0gX2IgPT09IHZvaWQgMCA/ICcjN2Y1OGFmJyA6IF9iLCBfYyA9IF9hLnNpemUsIHNpemUgPSBfYyA9PT0gdm9pZCAwID8gODAgOiBfYywgY2xhc3NOYW1lID0gX2EuY2xhc3NOYW1lLCBzdHlsZSA9IF9hLnN0eWxlLCByZXN0ID0gX19yZXN0KF9hLCBbXCJjb2xvclwiLCBcInNpemVcIiwgXCJjbGFzc05hbWVcIiwgXCJzdHlsZVwiXSk7XG4gICAgdmFyIGNpcmNsZXMgPSBfX3NwcmVhZEFycmF5KFtdLCBBcnJheSg5KSkubWFwKGZ1bmN0aW9uIChfLCBpbmRleCkgeyByZXR1cm4gX2pzeChcImRpdlwiLCB7IHN0eWxlOiB7IGJhY2tncm91bmQ6IFwiXCIgKyBjb2xvciB9IH0sIGluZGV4KTsgfSk7XG4gICAgcmV0dXJuIChfanN4KFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLWdyaWQnXSwgY2xhc3NOYW1lKSwgc3R5bGU6IF9fYXNzaWduKHsgd2lkdGg6IHNpemUsIGhlaWdodDogc2l6ZSB9LCBzdHlsZSkgfSwgcmVzdCwgeyBjaGlsZHJlbjogY2lyY2xlcyB9KSwgdm9pZCAwKSk7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qc1wiKTtcbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS01LTEhLi9zdHlsZS5tb2R1bGUuY3NzXCIpO1xuXG4gICAgICAgICAgICBjb250ZW50ID0gY29udGVudC5fX2VzTW9kdWxlID8gY29udGVudC5kZWZhdWx0IDogY29udGVudDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgICAgICB9XG5cbnZhciBvcHRpb25zID0ge307XG5cbm9wdGlvbnMuaW5zZXJ0ID0gXCJoZWFkXCI7XG5vcHRpb25zLnNpbmdsZXRvbiA9IGZhbHNlO1xuXG52YXIgdXBkYXRlID0gYXBpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTsiLCJ2YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xudmFyIF9fcmVzdCA9ICh0aGlzICYmIHRoaXMuX19yZXN0KSB8fCBmdW5jdGlvbiAocywgZSkge1xuICAgIHZhciB0ID0ge307XG4gICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApICYmIGUuaW5kZXhPZihwKSA8IDApXG4gICAgICAgIHRbcF0gPSBzW3BdO1xuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgZm9yICh2YXIgaSA9IDAsIHAgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHMpOyBpIDwgcC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKGUuaW5kZXhPZihwW2ldKSA8IDAgJiYgT2JqZWN0LnByb3RvdHlwZS5wcm9wZXJ0eUlzRW51bWVyYWJsZS5jYWxsKHMsIHBbaV0pKVxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xuICAgICAgICB9XG4gICAgcmV0dXJuIHQ7XG59O1xuaW1wb3J0IHsganN4IGFzIF9qc3gsIGpzeHMgYXMgX2pzeHMgfSBmcm9tIFwicmVhY3QvanN4LXJ1bnRpbWVcIjtcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL3N0eWxlLm1vZHVsZS5jc3MnO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSGVhcnQoX2EpIHtcbiAgICB2YXIgX2IgPSBfYS5jb2xvciwgY29sb3IgPSBfYiA9PT0gdm9pZCAwID8gJyM3ZjU4YWYnIDogX2IsIF9jID0gX2Euc2l6ZSwgc2l6ZSA9IF9jID09PSB2b2lkIDAgPyA4MCA6IF9jLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWUsIHN0eWxlID0gX2Euc3R5bGUsIHJlc3QgPSBfX3Jlc3QoX2EsIFtcImNvbG9yXCIsIFwic2l6ZVwiLCBcImNsYXNzTmFtZVwiLCBcInN0eWxlXCJdKTtcbiAgICByZXR1cm4gKF9qc3goXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydsZHMtaGVhcnQnXSwgY2xhc3NOYW1lKSwgc3R5bGU6IF9fYXNzaWduKHsgd2lkdGg6IHNpemUsIGhlaWdodDogc2l6ZSB9LCBzdHlsZSkgfSwgcmVzdCwgeyBjaGlsZHJlbjogX2pzeHMoXCJkaXZcIiwgX19hc3NpZ24oeyBzdHlsZToge1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IGNvbG9yLFxuICAgICAgICAgICAgICAgIHdpZHRoOiBzaXplICogMC40LFxuICAgICAgICAgICAgICAgIGhlaWdodDogc2l6ZSAqIDAuNCxcbiAgICAgICAgICAgICAgICBsZWZ0OiBzaXplICogMC4zLFxuICAgICAgICAgICAgICAgIHRvcDogc2l6ZSAqIDAuMyxcbiAgICAgICAgICAgIH0gfSwgeyBjaGlsZHJlbjogW19qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydkaXYtYmVmb3JlJ10pLCBzdHlsZToge1xuICAgICAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZDogY29sb3IsXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogc2l6ZSAqIDAuNCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogc2l6ZSAqIDAuNCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IC1zaXplICogMC4zLFxuICAgICAgICAgICAgICAgICAgICB9IH0sIHZvaWQgMCksXG4gICAgICAgICAgICAgICAgX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2Rpdi1hZnRlciddKSwgc3R5bGU6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6IGNvbG9yLFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHNpemUgKiAwLjQsXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IHNpemUgKiAwLjQsXG4gICAgICAgICAgICAgICAgICAgICAgICB0b3A6IC1zaXplICogMC4zLFxuICAgICAgICAgICAgICAgICAgICB9IH0sIHZvaWQgMCldIH0pLCB2b2lkIDApIH0pLCB2b2lkIDApKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsInZhciBhcGkgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTUtMSEuL3N0eWxlLm1vZHVsZS5jc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsInZhciBfX2Fzc2lnbiA9ICh0aGlzICYmIHRoaXMuX19hc3NpZ24pIHx8IGZ1bmN0aW9uICgpIHtcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24odCkge1xuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpXG4gICAgICAgICAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfTtcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb3VyZ2xhc3MoX2EpIHtcbiAgICB2YXIgX2IgPSBfYS5jb2xvciwgY29sb3IgPSBfYiA9PT0gdm9pZCAwID8gJyM3ZjU4YWYnIDogX2IsIF9jID0gX2Euc2l6ZSwgc2l6ZSA9IF9jID09PSB2b2lkIDAgPyAzMiA6IF9jLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWUsIHN0eWxlID0gX2Euc3R5bGU7XG4gICAgcmV0dXJuIChfanN4KFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLWhvdXJnbGFzcyddLCBjbGFzc05hbWUpLCBzdHlsZTogX19hc3NpZ24oe30sIHN0eWxlKSB9LCB7IGNoaWxkcmVuOiBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLWhvdXJnbGFzcy1hZnRlciddKSwgXG4gICAgICAgICAgICAvL0B0cy1pZ25vcmVcbiAgICAgICAgICAgIHN0eWxlOiB7IGJhY2tncm91bmQ6IGNvbG9yLCBib3JkZXJXaWR0aDogc2l6ZSwgYm9yZGVySGVpZ2h0OiBzaXplIH0gfSwgdm9pZCAwKSB9KSwgdm9pZCAwKSk7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9zdHlsZS1sb2FkZXIvZGlzdC9ydW50aW1lL2luamVjdFN0eWxlc0ludG9TdHlsZVRhZy5qc1wiKTtcbiAgICAgICAgICAgIHZhciBjb250ZW50ID0gcmVxdWlyZShcIiEhLi4vLi4vLi4vLi4vY3NzLWxvYWRlci9kaXN0L2Nqcy5qcz8/cmVmLS01LTEhLi9zdHlsZS5tb2R1bGUuY3NzXCIpO1xuXG4gICAgICAgICAgICBjb250ZW50ID0gY29udGVudC5fX2VzTW9kdWxlID8gY29udGVudC5kZWZhdWx0IDogY29udGVudDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgICAgICB9XG5cbnZhciBvcHRpb25zID0ge307XG5cbm9wdGlvbnMuaW5zZXJ0ID0gXCJoZWFkXCI7XG5vcHRpb25zLnNpbmdsZXRvbiA9IGZhbHNlO1xuXG52YXIgdXBkYXRlID0gYXBpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTsiLCJ2YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xuaW1wb3J0IHsganN4IGFzIF9qc3gsIGpzeHMgYXMgX2pzeHMgfSBmcm9tIFwicmVhY3QvanN4LXJ1bnRpbWVcIjtcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL3N0eWxlLm1vZHVsZS5jc3MnO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gT3JiaXRhbHMoX2EpIHtcbiAgICB2YXIgX2IgPSBfYS5jb2xvciwgY29sb3IgPSBfYiA9PT0gdm9pZCAwID8gJyM3ZjU4YWYnIDogX2IsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZTtcbiAgICByZXR1cm4gKF9qc3hzKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snbGRzLW9yYml0YWxzJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7fSwgc3R5bGUpIH0sIHsgY2hpbGRyZW46IFtfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snY2VudGVyJ10pLCBzdHlsZTogeyBiYWNrZ3JvdW5kOiBjb2xvciB9IH0sIHZvaWQgMCksXG4gICAgICAgICAgICBfanN4cyhcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2lubmVyLXNwaW4nXSkgfSwgeyBjaGlsZHJlbjogW19qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydpbm5lci1hcmMnXSwgc3R5bGVzWydpbm5lci1hcmNfc3RhcnQtYSddKSwgc3R5bGU6IHsgYm9yZGVyQ29sb3I6IGNvbG9yIH0gfSwgdm9pZCAwKSxcbiAgICAgICAgICAgICAgICAgICAgX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2lubmVyLWFyYyddLCBzdHlsZXNbJ2lubmVyLWFyY19lbmQtYSddKSwgc3R5bGU6IHsgYm9yZGVyQ29sb3I6IGNvbG9yIH0gfSwgdm9pZCAwKSxcbiAgICAgICAgICAgICAgICAgICAgX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2lubmVyLWFyYyddLCBzdHlsZXNbJ2lubmVyLWFyY19zdGFydC1iJ10pLCBzdHlsZTogeyBib3JkZXJDb2xvcjogY29sb3IgfSB9LCB2b2lkIDApLFxuICAgICAgICAgICAgICAgICAgICBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snaW5uZXItYXJjJ10sIHN0eWxlc1snaW5uZXItYXJjX2VuZC1iJ10pLCBzdHlsZTogeyBib3JkZXJDb2xvcjogY29sb3IgfSB9LCB2b2lkIDApLFxuICAgICAgICAgICAgICAgICAgICBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snaW5uZXItbW9vbi1hJ10pLCBzdHlsZTogeyBiYWNrZ3JvdW5kOiBjb2xvciB9IH0sIHZvaWQgMCksXG4gICAgICAgICAgICAgICAgICAgIF9qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydpbm5lci1tb29uLWInXSksIHN0eWxlOiB7IGJhY2tncm91bmQ6IGNvbG9yIH0gfSwgdm9pZCAwKV0gfSksIHZvaWQgMCksXG4gICAgICAgICAgICBfanN4cyhcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ291dGVyLXNwaW4nXSkgfSwgeyBjaGlsZHJlbjogW19qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydvdXRlci1hcmMnXSwgc3R5bGVzWydvdXRlci1hcmNfc3RhcnQtYSddKSwgc3R5bGU6IHsgYm9yZGVyQ29sb3I6IGNvbG9yIH0gfSwgdm9pZCAwKSxcbiAgICAgICAgICAgICAgICAgICAgX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ291dGVyLWFyYyddLCBzdHlsZXNbJ291dGVyLWFyY19lbmQtYSddKSwgc3R5bGU6IHsgYm9yZGVyQ29sb3I6IGNvbG9yIH0gfSwgdm9pZCAwKSxcbiAgICAgICAgICAgICAgICAgICAgX2pzeChcImRpdlwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ291dGVyLWFyYyddLCBzdHlsZXNbJ291dGVyLWFyY19zdGFydC1iJ10pLCBzdHlsZTogeyBib3JkZXJDb2xvcjogY29sb3IgfSB9LCB2b2lkIDApLFxuICAgICAgICAgICAgICAgICAgICBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snb3V0ZXItYXJjJ10sIHN0eWxlc1snb3V0ZXItYXJjX2VuZC1iJ10pLCBzdHlsZTogeyBib3JkZXJDb2xvcjogY29sb3IgfSB9LCB2b2lkIDApLFxuICAgICAgICAgICAgICAgICAgICBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snb3V0ZXItbW9vbi1hJ10pLCBzdHlsZTogeyBiYWNrZ3JvdW5kOiBjb2xvciB9IH0sIHZvaWQgMCksXG4gICAgICAgICAgICAgICAgICAgIF9qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydvdXRlci1tb29uLWInXSksIHN0eWxlOiB7IGJhY2tncm91bmQ6IGNvbG9yIH0gfSwgdm9pZCAwKV0gfSksIHZvaWQgMCldIH0pLCB2b2lkIDApKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsInZhciBhcGkgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTUtMSEuL3N0eWxlLm1vZHVsZS5jc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsInZhciBfX2Fzc2lnbiA9ICh0aGlzICYmIHRoaXMuX19hc3NpZ24pIHx8IGZ1bmN0aW9uICgpIHtcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24odCkge1xuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpXG4gICAgICAgICAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfTtcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCwganN4cyBhcyBfanN4cyB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBPdXJvYm9ybyhfYSkge1xuICAgIHZhciBfYiA9IF9hLmNvbG9yLCBjb2xvciA9IF9iID09PSB2b2lkIDAgPyAnIzdmNThhZicgOiBfYiwgc3R5bGUgPSBfYS5zdHlsZSwgY2xhc3NOYW1lID0gX2EuY2xhc3NOYW1lO1xuICAgIHJldHVybiAoX2pzeHMoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydsZHMtb3Vyb2Jvcm8nXSwgY2xhc3NOYW1lKSwgc3R5bGU6IF9fYXNzaWduKHt9LCBzdHlsZSkgfSwgeyBjaGlsZHJlbjogW19qc3goXCJzcGFuXCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlcy5sZWZ0KSB9LCB7IGNoaWxkcmVuOiBfanN4KFwic3BhblwiLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXMuYW5pbSksIHN0eWxlOiB7IGJhY2tncm91bmQ6IGNvbG9yIH0gfSwgdm9pZCAwKSB9KSwgdm9pZCAwKSxcbiAgICAgICAgICAgIF9qc3goXCJzcGFuXCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlcy5yaWdodCkgfSwgeyBjaGlsZHJlbjogX2pzeChcInNwYW5cIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzLmFuaW0pLCBzdHlsZTogeyBiYWNrZ3JvdW5kOiBjb2xvciB9IH0sIHZvaWQgMCkgfSksIHZvaWQgMCldIH0pLCB2b2lkIDApKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsInZhciBhcGkgPSByZXF1aXJlKFwiIS4uLy4uLy4uLy4uL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzPz9yZWYtLTUtMSEuL3N0eWxlLm1vZHVsZS5jc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsInZhciBfX2Fzc2lnbiA9ICh0aGlzICYmIHRoaXMuX19hc3NpZ24pIHx8IGZ1bmN0aW9uICgpIHtcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24odCkge1xuICAgICAgICBmb3IgKHZhciBzLCBpID0gMSwgbiA9IGFyZ3VtZW50cy5sZW5ndGg7IGkgPCBuOyBpKyspIHtcbiAgICAgICAgICAgIHMgPSBhcmd1bWVudHNbaV07XG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpXG4gICAgICAgICAgICAgICAgdFtwXSA9IHNbcF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHQ7XG4gICAgfTtcbiAgICByZXR1cm4gX19hc3NpZ24uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcbn07XG52YXIgX19zcHJlYWRBcnJheSA9ICh0aGlzICYmIHRoaXMuX19zcHJlYWRBcnJheSkgfHwgZnVuY3Rpb24gKHRvLCBmcm9tKSB7XG4gICAgZm9yICh2YXIgaSA9IDAsIGlsID0gZnJvbS5sZW5ndGgsIGogPSB0by5sZW5ndGg7IGkgPCBpbDsgaSsrLCBqKyspXG4gICAgICAgIHRvW2pdID0gZnJvbVtpXTtcbiAgICByZXR1cm4gdG87XG59O1xuaW1wb3J0IHsganN4IGFzIF9qc3ggfSBmcm9tIFwicmVhY3QvanN4LXJ1bnRpbWVcIjtcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL3N0eWxlLm1vZHVsZS5jc3MnO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUmluZyhfYSkge1xuICAgIHZhciBfYiA9IF9hLmNvbG9yLCBjb2xvciA9IF9iID09PSB2b2lkIDAgPyAnIzdmNThhZicgOiBfYiwgX2MgPSBfYS5zaXplLCBzaXplID0gX2MgPT09IHZvaWQgMCA/IDgwIDogX2MsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZTtcbiAgICB2YXIgY2lyY2xlcyA9IF9fc3ByZWFkQXJyYXkoW10sIEFycmF5KDQpKS5tYXAoZnVuY3Rpb24gKF8sIGluZGV4KSB7XG4gICAgICAgIHJldHVybiAoX2pzeChcImRpdlwiLCB7IHN0eWxlOiB7XG4gICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6IGNvbG9yICsgXCIgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnRcIixcbiAgICAgICAgICAgICAgICB3aWR0aDogc2l6ZSAqIDAuOCxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IHNpemUgKiAwLjgsXG4gICAgICAgICAgICAgICAgbWFyZ2luOiBzaXplICogMC4xLFxuICAgICAgICAgICAgICAgIGJvcmRlcldpZHRoOiBzaXplICogMC4xLFxuICAgICAgICAgICAgfSB9LCBpbmRleCkpO1xuICAgIH0pO1xuICAgIHJldHVybiAoX2pzeChcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2xkcy1yaW5nJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7IHdpZHRoOiBzaXplLCBoZWlnaHQ6IHNpemUgfSwgc3R5bGUpIH0sIHsgY2hpbGRyZW46IGNpcmNsZXMgfSksIHZvaWQgMCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNS0xIS4vc3R5bGUubW9kdWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307IiwidmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbnZhciBfX3NwcmVhZEFycmF5ID0gKHRoaXMgJiYgdGhpcy5fX3NwcmVhZEFycmF5KSB8fCBmdW5jdGlvbiAodG8sIGZyb20pIHtcbiAgICBmb3IgKHZhciBpID0gMCwgaWwgPSBmcm9tLmxlbmd0aCwgaiA9IHRvLmxlbmd0aDsgaSA8IGlsOyBpKyssIGorKylcbiAgICAgICAgdG9bal0gPSBmcm9tW2ldO1xuICAgIHJldHVybiB0bztcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBSaXBwbGUoX2EpIHtcbiAgICB2YXIgX2IgPSBfYS5jb2xvciwgY29sb3IgPSBfYiA9PT0gdm9pZCAwID8gJyM3ZjU4YWYnIDogX2IsIF9jID0gX2Euc2l6ZSwgc2l6ZSA9IF9jID09PSB2b2lkIDAgPyA4MCA6IF9jLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWUsIHN0eWxlID0gX2Euc3R5bGU7XG4gICAgdmFyIGNpcmNsZXMgPSBfX3NwcmVhZEFycmF5KFtdLCBBcnJheSgyKSkubWFwKGZ1bmN0aW9uIChfLCBpbmRleCkgeyByZXR1cm4gKF9qc3goXCJkaXZcIiwgeyBzdHlsZToge1xuICAgICAgICAgICAgYm9yZGVyQ29sb3I6IFwiXCIgKyBjb2xvcixcbiAgICAgICAgICAgIGJvcmRlcldpZHRoOiBzaXplICogMC4wNSxcbiAgICAgICAgfSB9LCBpbmRleCkpOyB9KTtcbiAgICByZXR1cm4gKF9qc3goXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydsZHMtcmlwcGxlJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7IHdpZHRoOiBzaXplLCBoZWlnaHQ6IHNpemUgfSwgc3R5bGUpIH0sIHsgY2hpbGRyZW46IGNpcmNsZXMgfSksIHZvaWQgMCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNS0xIS4vc3R5bGUubW9kdWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307IiwidmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbnZhciBfX3NwcmVhZEFycmF5ID0gKHRoaXMgJiYgdGhpcy5fX3NwcmVhZEFycmF5KSB8fCBmdW5jdGlvbiAodG8sIGZyb20pIHtcbiAgICBmb3IgKHZhciBpID0gMCwgaWwgPSBmcm9tLmxlbmd0aCwgaiA9IHRvLmxlbmd0aDsgaSA8IGlsOyBpKyssIGorKylcbiAgICAgICAgdG9bal0gPSBmcm9tW2ldO1xuICAgIHJldHVybiB0bztcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBSb2xsZXIoX2EpIHtcbiAgICB2YXIgX2IgPSBfYS5jb2xvciwgY29sb3IgPSBfYiA9PT0gdm9pZCAwID8gJyM3ZjU4YWYnIDogX2IsIGNsYXNzTmFtZSA9IF9hLmNsYXNzTmFtZSwgc3R5bGUgPSBfYS5zdHlsZTtcbiAgICB2YXIgY2lyY2xlcyA9IF9fc3ByZWFkQXJyYXkoW10sIEFycmF5KDgpKS5tYXAoZnVuY3Rpb24gKF8sIGluZGV4KSB7XG4gICAgICAgIHJldHVybiAoX2pzeChcImRpdlwiLCB7IGNoaWxkcmVuOiBfanN4KFwiZGl2XCIsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWVzKHN0eWxlc1snZGl2LWFmdGVyJ10pLCBzdHlsZTogeyBiYWNrZ3JvdW5kOiBjb2xvciB9IH0sIHZvaWQgMCkgfSwgaW5kZXgpKTtcbiAgICB9KTtcbiAgICByZXR1cm4gKF9qc3goXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydsZHMtcm9sbGVyJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7fSwgc3R5bGUpIH0sIHsgY2hpbGRyZW46IGNpcmNsZXMgfSksIHZvaWQgMCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNS0xIS4vc3R5bGUubW9kdWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307IiwidmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbnZhciBfX3NwcmVhZEFycmF5ID0gKHRoaXMgJiYgdGhpcy5fX3NwcmVhZEFycmF5KSB8fCBmdW5jdGlvbiAodG8sIGZyb20pIHtcbiAgICBmb3IgKHZhciBpID0gMCwgaWwgPSBmcm9tLmxlbmd0aCwgaiA9IHRvLmxlbmd0aDsgaSA8IGlsOyBpKyssIGorKylcbiAgICAgICAgdG9bal0gPSBmcm9tW2ldO1xuICAgIHJldHVybiB0bztcbn07XG5pbXBvcnQgeyBqc3ggYXMgX2pzeCB9IGZyb20gXCJyZWFjdC9qc3gtcnVudGltZVwiO1xuaW1wb3J0IGNsYXNzTmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGUubW9kdWxlLmNzcyc7XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBTcGlubmVyKF9hKSB7XG4gICAgdmFyIF9iID0gX2EuY29sb3IsIGNvbG9yID0gX2IgPT09IHZvaWQgMCA/ICcjN2Y1OGFmJyA6IF9iLCBjbGFzc05hbWUgPSBfYS5jbGFzc05hbWUsIHN0eWxlID0gX2Euc3R5bGU7XG4gICAgdmFyIGNpcmNsZXMgPSBfX3NwcmVhZEFycmF5KFtdLCBBcnJheSgxMikpLm1hcChmdW5jdGlvbiAoXywgaW5kZXgpIHtcbiAgICAgICAgcmV0dXJuIChfanN4KFwiZGl2XCIsIHsgY2hpbGRyZW46IF9qc3goXCJkaXZcIiwgeyBjbGFzc05hbWU6IGNsYXNzTmFtZXMoc3R5bGVzWydkaXYtYWZ0ZXInXSksIHN0eWxlOiB7IGJhY2tncm91bmQ6IGNvbG9yIH0gfSwgdm9pZCAwKSB9LCBpbmRleCkpO1xuICAgIH0pO1xuICAgIHJldHVybiAoX2pzeChcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogY2xhc3NOYW1lcyhzdHlsZXNbJ2xkcy1zcGlubmVyJ10sIGNsYXNzTmFtZSksIHN0eWxlOiBfX2Fzc2lnbih7fSwgc3R5bGUpIH0sIHsgY2hpbGRyZW46IGNpcmNsZXMgfSksIHZvaWQgMCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL2Nzcy1sb2FkZXIvZGlzdC9janMuanM/P3JlZi0tNS0xIS4vc3R5bGUubW9kdWxlLmNzc1wiKTtcblxuICAgICAgICAgICAgY29udGVudCA9IGNvbnRlbnQuX19lc01vZHVsZSA/IGNvbnRlbnQuZGVmYXVsdCA6IGNvbnRlbnQ7XG5cbiAgICAgICAgICAgIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgICAgY29udGVudCA9IFtbbW9kdWxlLmlkLCBjb250ZW50LCAnJ11dO1xuICAgICAgICAgICAgfVxuXG52YXIgb3B0aW9ucyA9IHt9O1xuXG5vcHRpb25zLmluc2VydCA9IFwiaGVhZFwiO1xub3B0aW9ucy5zaW5nbGV0b24gPSBmYWxzZTtcblxudmFyIHVwZGF0ZSA9IGFwaShjb250ZW50LCBvcHRpb25zKTtcblxuXG5cbm1vZHVsZS5leHBvcnRzID0gY29udGVudC5sb2NhbHMgfHwge307IiwiaW1wb3J0IENpcmNsZSBmcm9tICcuL0NpcmNsZSc7XG5pbXBvcnQgRGVmYXVsdCBmcm9tICcuL0RlZmF1bHQnO1xuaW1wb3J0IER1YWxSaW5nIGZyb20gJy4vRHVhbFJpbmcnO1xuaW1wb3J0IEVsbGlwc2lzIGZyb20gJy4vRWxsaXBzaXMnO1xuaW1wb3J0IEZhY2Vib29rIGZyb20gJy4vRmFjZWJvb2snO1xuaW1wb3J0IEdyaWQgZnJvbSAnLi9HcmlkJztcbmltcG9ydCBIZWFydCBmcm9tICcuL0hlYXJ0JztcbmltcG9ydCBIb3VyZ2xhc3MgZnJvbSAnLi9Ib3VyZ2xhc3MnO1xuaW1wb3J0IE9yYml0YWxzIGZyb20gJy4vT3JiaXRhbHMnO1xuaW1wb3J0IFJpbmcgZnJvbSAnLi9SaW5nJztcbmltcG9ydCBSaXBwbGUgZnJvbSAnLi9SaXBwbGUnO1xuaW1wb3J0IFJvbGxlciBmcm9tICcuL1JvbGxlcic7XG5pbXBvcnQgU3Bpbm5lciBmcm9tICcuL1NwaW5uZXInO1xuaW1wb3J0IE91cm9ib3JvIGZyb20gJy4vT3Vyb2Jvcm8nO1xuZXhwb3J0IHsgQ2lyY2xlLCBEZWZhdWx0LCBEdWFsUmluZywgRWxsaXBzaXMsIEZhY2Vib29rLCBHcmlkLCBIZWFydCwgSG91cmdsYXNzLCBPcmJpdGFscywgUmluZywgUmlwcGxlLCBSb2xsZXIsIFNwaW5uZXIsIE91cm9ib3JvLCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiLyoqIEBsaWNlbnNlIFJlYWN0IHYxNy4wLjJcbiAqIHJlYWN0LWpzeC1ydW50aW1lLmRldmVsb3BtZW50LmpzXG4gKlxuICogQ29weXJpZ2h0IChjKSBGYWNlYm9vaywgSW5jLiBhbmQgaXRzIGFmZmlsaWF0ZXMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKi9cblxuJ3VzZSBzdHJpY3QnO1xuXG5pZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiKSB7XG4gIChmdW5jdGlvbigpIHtcbid1c2Ugc3RyaWN0JztcblxudmFyIFJlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcbnZhciBfYXNzaWduID0gcmVxdWlyZSgnb2JqZWN0LWFzc2lnbicpO1xuXG4vLyBBVFRFTlRJT05cbi8vIFdoZW4gYWRkaW5nIG5ldyBzeW1ib2xzIHRvIHRoaXMgZmlsZSxcbi8vIFBsZWFzZSBjb25zaWRlciBhbHNvIGFkZGluZyB0byAncmVhY3QtZGV2dG9vbHMtc2hhcmVkL3NyYy9iYWNrZW5kL1JlYWN0U3ltYm9scydcbi8vIFRoZSBTeW1ib2wgdXNlZCB0byB0YWcgdGhlIFJlYWN0RWxlbWVudC1saWtlIHR5cGVzLiBJZiB0aGVyZSBpcyBubyBuYXRpdmUgU3ltYm9sXG4vLyBub3IgcG9seWZpbGwsIHRoZW4gYSBwbGFpbiBudW1iZXIgaXMgdXNlZCBmb3IgcGVyZm9ybWFuY2UuXG52YXIgUkVBQ1RfRUxFTUVOVF9UWVBFID0gMHhlYWM3O1xudmFyIFJFQUNUX1BPUlRBTF9UWVBFID0gMHhlYWNhO1xuZXhwb3J0cy5GcmFnbWVudCA9IDB4ZWFjYjtcbnZhciBSRUFDVF9TVFJJQ1RfTU9ERV9UWVBFID0gMHhlYWNjO1xudmFyIFJFQUNUX1BST0ZJTEVSX1RZUEUgPSAweGVhZDI7XG52YXIgUkVBQ1RfUFJPVklERVJfVFlQRSA9IDB4ZWFjZDtcbnZhciBSRUFDVF9DT05URVhUX1RZUEUgPSAweGVhY2U7XG52YXIgUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRSA9IDB4ZWFkMDtcbnZhciBSRUFDVF9TVVNQRU5TRV9UWVBFID0gMHhlYWQxO1xudmFyIFJFQUNUX1NVU1BFTlNFX0xJU1RfVFlQRSA9IDB4ZWFkODtcbnZhciBSRUFDVF9NRU1PX1RZUEUgPSAweGVhZDM7XG52YXIgUkVBQ1RfTEFaWV9UWVBFID0gMHhlYWQ0O1xudmFyIFJFQUNUX0JMT0NLX1RZUEUgPSAweGVhZDk7XG52YXIgUkVBQ1RfU0VSVkVSX0JMT0NLX1RZUEUgPSAweGVhZGE7XG52YXIgUkVBQ1RfRlVOREFNRU5UQUxfVFlQRSA9IDB4ZWFkNTtcbnZhciBSRUFDVF9TQ09QRV9UWVBFID0gMHhlYWQ3O1xudmFyIFJFQUNUX09QQVFVRV9JRF9UWVBFID0gMHhlYWUwO1xudmFyIFJFQUNUX0RFQlVHX1RSQUNJTkdfTU9ERV9UWVBFID0gMHhlYWUxO1xudmFyIFJFQUNUX09GRlNDUkVFTl9UWVBFID0gMHhlYWUyO1xudmFyIFJFQUNUX0xFR0FDWV9ISURERU5fVFlQRSA9IDB4ZWFlMztcblxuaWYgKHR5cGVvZiBTeW1ib2wgPT09ICdmdW5jdGlvbicgJiYgU3ltYm9sLmZvcikge1xuICB2YXIgc3ltYm9sRm9yID0gU3ltYm9sLmZvcjtcbiAgUkVBQ1RfRUxFTUVOVF9UWVBFID0gc3ltYm9sRm9yKCdyZWFjdC5lbGVtZW50Jyk7XG4gIFJFQUNUX1BPUlRBTF9UWVBFID0gc3ltYm9sRm9yKCdyZWFjdC5wb3J0YWwnKTtcbiAgZXhwb3J0cy5GcmFnbWVudCA9IHN5bWJvbEZvcigncmVhY3QuZnJhZ21lbnQnKTtcbiAgUkVBQ1RfU1RSSUNUX01PREVfVFlQRSA9IHN5bWJvbEZvcigncmVhY3Quc3RyaWN0X21vZGUnKTtcbiAgUkVBQ1RfUFJPRklMRVJfVFlQRSA9IHN5bWJvbEZvcigncmVhY3QucHJvZmlsZXInKTtcbiAgUkVBQ1RfUFJPVklERVJfVFlQRSA9IHN5bWJvbEZvcigncmVhY3QucHJvdmlkZXInKTtcbiAgUkVBQ1RfQ09OVEVYVF9UWVBFID0gc3ltYm9sRm9yKCdyZWFjdC5jb250ZXh0Jyk7XG4gIFJFQUNUX0ZPUldBUkRfUkVGX1RZUEUgPSBzeW1ib2xGb3IoJ3JlYWN0LmZvcndhcmRfcmVmJyk7XG4gIFJFQUNUX1NVU1BFTlNFX1RZUEUgPSBzeW1ib2xGb3IoJ3JlYWN0LnN1c3BlbnNlJyk7XG4gIFJFQUNUX1NVU1BFTlNFX0xJU1RfVFlQRSA9IHN5bWJvbEZvcigncmVhY3Quc3VzcGVuc2VfbGlzdCcpO1xuICBSRUFDVF9NRU1PX1RZUEUgPSBzeW1ib2xGb3IoJ3JlYWN0Lm1lbW8nKTtcbiAgUkVBQ1RfTEFaWV9UWVBFID0gc3ltYm9sRm9yKCdyZWFjdC5sYXp5Jyk7XG4gIFJFQUNUX0JMT0NLX1RZUEUgPSBzeW1ib2xGb3IoJ3JlYWN0LmJsb2NrJyk7XG4gIFJFQUNUX1NFUlZFUl9CTE9DS19UWVBFID0gc3ltYm9sRm9yKCdyZWFjdC5zZXJ2ZXIuYmxvY2snKTtcbiAgUkVBQ1RfRlVOREFNRU5UQUxfVFlQRSA9IHN5bWJvbEZvcigncmVhY3QuZnVuZGFtZW50YWwnKTtcbiAgUkVBQ1RfU0NPUEVfVFlQRSA9IHN5bWJvbEZvcigncmVhY3Quc2NvcGUnKTtcbiAgUkVBQ1RfT1BBUVVFX0lEX1RZUEUgPSBzeW1ib2xGb3IoJ3JlYWN0Lm9wYXF1ZS5pZCcpO1xuICBSRUFDVF9ERUJVR19UUkFDSU5HX01PREVfVFlQRSA9IHN5bWJvbEZvcigncmVhY3QuZGVidWdfdHJhY2VfbW9kZScpO1xuICBSRUFDVF9PRkZTQ1JFRU5fVFlQRSA9IHN5bWJvbEZvcigncmVhY3Qub2Zmc2NyZWVuJyk7XG4gIFJFQUNUX0xFR0FDWV9ISURERU5fVFlQRSA9IHN5bWJvbEZvcigncmVhY3QubGVnYWN5X2hpZGRlbicpO1xufVxuXG52YXIgTUFZQkVfSVRFUkFUT1JfU1lNQk9MID0gdHlwZW9mIFN5bWJvbCA9PT0gJ2Z1bmN0aW9uJyAmJiBTeW1ib2wuaXRlcmF0b3I7XG52YXIgRkFVWF9JVEVSQVRPUl9TWU1CT0wgPSAnQEBpdGVyYXRvcic7XG5mdW5jdGlvbiBnZXRJdGVyYXRvckZuKG1heWJlSXRlcmFibGUpIHtcbiAgaWYgKG1heWJlSXRlcmFibGUgPT09IG51bGwgfHwgdHlwZW9mIG1heWJlSXRlcmFibGUgIT09ICdvYmplY3QnKSB7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICB2YXIgbWF5YmVJdGVyYXRvciA9IE1BWUJFX0lURVJBVE9SX1NZTUJPTCAmJiBtYXliZUl0ZXJhYmxlW01BWUJFX0lURVJBVE9SX1NZTUJPTF0gfHwgbWF5YmVJdGVyYWJsZVtGQVVYX0lURVJBVE9SX1NZTUJPTF07XG5cbiAgaWYgKHR5cGVvZiBtYXliZUl0ZXJhdG9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIG1heWJlSXRlcmF0b3I7XG4gIH1cblxuICByZXR1cm4gbnVsbDtcbn1cblxudmFyIFJlYWN0U2hhcmVkSW50ZXJuYWxzID0gUmVhY3QuX19TRUNSRVRfSU5URVJOQUxTX0RPX05PVF9VU0VfT1JfWU9VX1dJTExfQkVfRklSRUQ7XG5cbmZ1bmN0aW9uIGVycm9yKGZvcm1hdCkge1xuICB7XG4gICAgZm9yICh2YXIgX2xlbjIgPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gbmV3IEFycmF5KF9sZW4yID4gMSA/IF9sZW4yIC0gMSA6IDApLCBfa2V5MiA9IDE7IF9rZXkyIDwgX2xlbjI7IF9rZXkyKyspIHtcbiAgICAgIGFyZ3NbX2tleTIgLSAxXSA9IGFyZ3VtZW50c1tfa2V5Ml07XG4gICAgfVxuXG4gICAgcHJpbnRXYXJuaW5nKCdlcnJvcicsIGZvcm1hdCwgYXJncyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcHJpbnRXYXJuaW5nKGxldmVsLCBmb3JtYXQsIGFyZ3MpIHtcbiAgLy8gV2hlbiBjaGFuZ2luZyB0aGlzIGxvZ2ljLCB5b3UgbWlnaHQgd2FudCB0byBhbHNvXG4gIC8vIHVwZGF0ZSBjb25zb2xlV2l0aFN0YWNrRGV2Lnd3dy5qcyBhcyB3ZWxsLlxuICB7XG4gICAgdmFyIFJlYWN0RGVidWdDdXJyZW50RnJhbWUgPSBSZWFjdFNoYXJlZEludGVybmFscy5SZWFjdERlYnVnQ3VycmVudEZyYW1lO1xuICAgIHZhciBzdGFjayA9IFJlYWN0RGVidWdDdXJyZW50RnJhbWUuZ2V0U3RhY2tBZGRlbmR1bSgpO1xuXG4gICAgaWYgKHN0YWNrICE9PSAnJykge1xuICAgICAgZm9ybWF0ICs9ICclcyc7XG4gICAgICBhcmdzID0gYXJncy5jb25jYXQoW3N0YWNrXSk7XG4gICAgfVxuXG4gICAgdmFyIGFyZ3NXaXRoRm9ybWF0ID0gYXJncy5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgIHJldHVybiAnJyArIGl0ZW07XG4gICAgfSk7IC8vIENhcmVmdWw6IFJOIGN1cnJlbnRseSBkZXBlbmRzIG9uIHRoaXMgcHJlZml4XG5cbiAgICBhcmdzV2l0aEZvcm1hdC51bnNoaWZ0KCdXYXJuaW5nOiAnICsgZm9ybWF0KTsgLy8gV2UgaW50ZW50aW9uYWxseSBkb24ndCB1c2Ugc3ByZWFkIChvciAuYXBwbHkpIGRpcmVjdGx5IGJlY2F1c2UgaXRcbiAgICAvLyBicmVha3MgSUU5OiBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svcmVhY3QvaXNzdWVzLzEzNjEwXG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0LWludGVybmFsL25vLXByb2R1Y3Rpb24tbG9nZ2luZ1xuXG4gICAgRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwoY29uc29sZVtsZXZlbF0sIGNvbnNvbGUsIGFyZ3NXaXRoRm9ybWF0KTtcbiAgfVxufVxuXG4vLyBGaWx0ZXIgY2VydGFpbiBET00gYXR0cmlidXRlcyAoZS5nLiBzcmMsIGhyZWYpIGlmIHRoZWlyIHZhbHVlcyBhcmUgZW1wdHkgc3RyaW5ncy5cblxudmFyIGVuYWJsZVNjb3BlQVBJID0gZmFsc2U7IC8vIEV4cGVyaW1lbnRhbCBDcmVhdGUgRXZlbnQgSGFuZGxlIEFQSS5cblxuZnVuY3Rpb24gaXNWYWxpZEVsZW1lbnRUeXBlKHR5cGUpIHtcbiAgaWYgKHR5cGVvZiB0eXBlID09PSAnc3RyaW5nJyB8fCB0eXBlb2YgdHlwZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHJldHVybiB0cnVlO1xuICB9IC8vIE5vdGU6IHR5cGVvZiBtaWdodCBiZSBvdGhlciB0aGFuICdzeW1ib2wnIG9yICdudW1iZXInIChlLmcuIGlmIGl0J3MgYSBwb2x5ZmlsbCkuXG5cblxuICBpZiAodHlwZSA9PT0gZXhwb3J0cy5GcmFnbWVudCB8fCB0eXBlID09PSBSRUFDVF9QUk9GSUxFUl9UWVBFIHx8IHR5cGUgPT09IFJFQUNUX0RFQlVHX1RSQUNJTkdfTU9ERV9UWVBFIHx8IHR5cGUgPT09IFJFQUNUX1NUUklDVF9NT0RFX1RZUEUgfHwgdHlwZSA9PT0gUkVBQ1RfU1VTUEVOU0VfVFlQRSB8fCB0eXBlID09PSBSRUFDVF9TVVNQRU5TRV9MSVNUX1RZUEUgfHwgdHlwZSA9PT0gUkVBQ1RfTEVHQUNZX0hJRERFTl9UWVBFIHx8IGVuYWJsZVNjb3BlQVBJICkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgaWYgKHR5cGVvZiB0eXBlID09PSAnb2JqZWN0JyAmJiB0eXBlICE9PSBudWxsKSB7XG4gICAgaWYgKHR5cGUuJCR0eXBlb2YgPT09IFJFQUNUX0xBWllfVFlQRSB8fCB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9NRU1PX1RZUEUgfHwgdHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfUFJPVklERVJfVFlQRSB8fCB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9DT05URVhUX1RZUEUgfHwgdHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRSB8fCB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9GVU5EQU1FTlRBTF9UWVBFIHx8IHR5cGUuJCR0eXBlb2YgPT09IFJFQUNUX0JMT0NLX1RZUEUgfHwgdHlwZVswXSA9PT0gUkVBQ1RfU0VSVkVSX0JMT0NLX1RZUEUpIHtcbiAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxuZnVuY3Rpb24gZ2V0V3JhcHBlZE5hbWUob3V0ZXJUeXBlLCBpbm5lclR5cGUsIHdyYXBwZXJOYW1lKSB7XG4gIHZhciBmdW5jdGlvbk5hbWUgPSBpbm5lclR5cGUuZGlzcGxheU5hbWUgfHwgaW5uZXJUeXBlLm5hbWUgfHwgJyc7XG4gIHJldHVybiBvdXRlclR5cGUuZGlzcGxheU5hbWUgfHwgKGZ1bmN0aW9uTmFtZSAhPT0gJycgPyB3cmFwcGVyTmFtZSArIFwiKFwiICsgZnVuY3Rpb25OYW1lICsgXCIpXCIgOiB3cmFwcGVyTmFtZSk7XG59XG5cbmZ1bmN0aW9uIGdldENvbnRleHROYW1lKHR5cGUpIHtcbiAgcmV0dXJuIHR5cGUuZGlzcGxheU5hbWUgfHwgJ0NvbnRleHQnO1xufVxuXG5mdW5jdGlvbiBnZXRDb21wb25lbnROYW1lKHR5cGUpIHtcbiAgaWYgKHR5cGUgPT0gbnVsbCkge1xuICAgIC8vIEhvc3Qgcm9vdCwgdGV4dCBub2RlIG9yIGp1c3QgaW52YWxpZCB0eXBlLlxuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAge1xuICAgIGlmICh0eXBlb2YgdHlwZS50YWcgPT09ICdudW1iZXInKSB7XG4gICAgICBlcnJvcignUmVjZWl2ZWQgYW4gdW5leHBlY3RlZCBvYmplY3QgaW4gZ2V0Q29tcG9uZW50TmFtZSgpLiAnICsgJ1RoaXMgaXMgbGlrZWx5IGEgYnVnIGluIFJlYWN0LiBQbGVhc2UgZmlsZSBhbiBpc3N1ZS4nKTtcbiAgICB9XG4gIH1cblxuICBpZiAodHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm4gdHlwZS5kaXNwbGF5TmFtZSB8fCB0eXBlLm5hbWUgfHwgbnVsbDtcbiAgfVxuXG4gIGlmICh0eXBlb2YgdHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gdHlwZTtcbiAgfVxuXG4gIHN3aXRjaCAodHlwZSkge1xuICAgIGNhc2UgZXhwb3J0cy5GcmFnbWVudDpcbiAgICAgIHJldHVybiAnRnJhZ21lbnQnO1xuXG4gICAgY2FzZSBSRUFDVF9QT1JUQUxfVFlQRTpcbiAgICAgIHJldHVybiAnUG9ydGFsJztcblxuICAgIGNhc2UgUkVBQ1RfUFJPRklMRVJfVFlQRTpcbiAgICAgIHJldHVybiAnUHJvZmlsZXInO1xuXG4gICAgY2FzZSBSRUFDVF9TVFJJQ1RfTU9ERV9UWVBFOlxuICAgICAgcmV0dXJuICdTdHJpY3RNb2RlJztcblxuICAgIGNhc2UgUkVBQ1RfU1VTUEVOU0VfVFlQRTpcbiAgICAgIHJldHVybiAnU3VzcGVuc2UnO1xuXG4gICAgY2FzZSBSRUFDVF9TVVNQRU5TRV9MSVNUX1RZUEU6XG4gICAgICByZXR1cm4gJ1N1c3BlbnNlTGlzdCc7XG4gIH1cblxuICBpZiAodHlwZW9mIHR5cGUgPT09ICdvYmplY3QnKSB7XG4gICAgc3dpdGNoICh0eXBlLiQkdHlwZW9mKSB7XG4gICAgICBjYXNlIFJFQUNUX0NPTlRFWFRfVFlQRTpcbiAgICAgICAgdmFyIGNvbnRleHQgPSB0eXBlO1xuICAgICAgICByZXR1cm4gZ2V0Q29udGV4dE5hbWUoY29udGV4dCkgKyAnLkNvbnN1bWVyJztcblxuICAgICAgY2FzZSBSRUFDVF9QUk9WSURFUl9UWVBFOlxuICAgICAgICB2YXIgcHJvdmlkZXIgPSB0eXBlO1xuICAgICAgICByZXR1cm4gZ2V0Q29udGV4dE5hbWUocHJvdmlkZXIuX2NvbnRleHQpICsgJy5Qcm92aWRlcic7XG5cbiAgICAgIGNhc2UgUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRTpcbiAgICAgICAgcmV0dXJuIGdldFdyYXBwZWROYW1lKHR5cGUsIHR5cGUucmVuZGVyLCAnRm9yd2FyZFJlZicpO1xuXG4gICAgICBjYXNlIFJFQUNUX01FTU9fVFlQRTpcbiAgICAgICAgcmV0dXJuIGdldENvbXBvbmVudE5hbWUodHlwZS50eXBlKTtcblxuICAgICAgY2FzZSBSRUFDVF9CTE9DS19UWVBFOlxuICAgICAgICByZXR1cm4gZ2V0Q29tcG9uZW50TmFtZSh0eXBlLl9yZW5kZXIpO1xuXG4gICAgICBjYXNlIFJFQUNUX0xBWllfVFlQRTpcbiAgICAgICAge1xuICAgICAgICAgIHZhciBsYXp5Q29tcG9uZW50ID0gdHlwZTtcbiAgICAgICAgICB2YXIgcGF5bG9hZCA9IGxhenlDb21wb25lbnQuX3BheWxvYWQ7XG4gICAgICAgICAgdmFyIGluaXQgPSBsYXp5Q29tcG9uZW50Ll9pbml0O1xuXG4gICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHJldHVybiBnZXRDb21wb25lbnROYW1lKGluaXQocGF5bG9hZCkpO1xuICAgICAgICAgIH0gY2F0Y2ggKHgpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBudWxsO1xufVxuXG4vLyBIZWxwZXJzIHRvIHBhdGNoIGNvbnNvbGUubG9ncyB0byBhdm9pZCBsb2dnaW5nIGR1cmluZyBzaWRlLWVmZmVjdCBmcmVlXG4vLyByZXBsYXlpbmcgb24gcmVuZGVyIGZ1bmN0aW9uLiBUaGlzIGN1cnJlbnRseSBvbmx5IHBhdGNoZXMgdGhlIG9iamVjdFxuLy8gbGF6aWx5IHdoaWNoIHdvbid0IGNvdmVyIGlmIHRoZSBsb2cgZnVuY3Rpb24gd2FzIGV4dHJhY3RlZCBlYWdlcmx5LlxuLy8gV2UgY291bGQgYWxzbyBlYWdlcmx5IHBhdGNoIHRoZSBtZXRob2QuXG52YXIgZGlzYWJsZWREZXB0aCA9IDA7XG52YXIgcHJldkxvZztcbnZhciBwcmV2SW5mbztcbnZhciBwcmV2V2FybjtcbnZhciBwcmV2RXJyb3I7XG52YXIgcHJldkdyb3VwO1xudmFyIHByZXZHcm91cENvbGxhcHNlZDtcbnZhciBwcmV2R3JvdXBFbmQ7XG5cbmZ1bmN0aW9uIGRpc2FibGVkTG9nKCkge31cblxuZGlzYWJsZWRMb2cuX19yZWFjdERpc2FibGVkTG9nID0gdHJ1ZTtcbmZ1bmN0aW9uIGRpc2FibGVMb2dzKCkge1xuICB7XG4gICAgaWYgKGRpc2FibGVkRGVwdGggPT09IDApIHtcbiAgICAgIC8qIGVzbGludC1kaXNhYmxlIHJlYWN0LWludGVybmFsL25vLXByb2R1Y3Rpb24tbG9nZ2luZyAqL1xuICAgICAgcHJldkxvZyA9IGNvbnNvbGUubG9nO1xuICAgICAgcHJldkluZm8gPSBjb25zb2xlLmluZm87XG4gICAgICBwcmV2V2FybiA9IGNvbnNvbGUud2FybjtcbiAgICAgIHByZXZFcnJvciA9IGNvbnNvbGUuZXJyb3I7XG4gICAgICBwcmV2R3JvdXAgPSBjb25zb2xlLmdyb3VwO1xuICAgICAgcHJldkdyb3VwQ29sbGFwc2VkID0gY29uc29sZS5ncm91cENvbGxhcHNlZDtcbiAgICAgIHByZXZHcm91cEVuZCA9IGNvbnNvbGUuZ3JvdXBFbmQ7IC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC9pc3N1ZXMvMTkwOTlcblxuICAgICAgdmFyIHByb3BzID0ge1xuICAgICAgICBjb25maWd1cmFibGU6IHRydWUsXG4gICAgICAgIGVudW1lcmFibGU6IHRydWUsXG4gICAgICAgIHZhbHVlOiBkaXNhYmxlZExvZyxcbiAgICAgICAgd3JpdGFibGU6IHRydWVcbiAgICAgIH07IC8vICRGbG93Rml4TWUgRmxvdyB0aGlua3MgY29uc29sZSBpcyBpbW11dGFibGUuXG5cbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKGNvbnNvbGUsIHtcbiAgICAgICAgaW5mbzogcHJvcHMsXG4gICAgICAgIGxvZzogcHJvcHMsXG4gICAgICAgIHdhcm46IHByb3BzLFxuICAgICAgICBlcnJvcjogcHJvcHMsXG4gICAgICAgIGdyb3VwOiBwcm9wcyxcbiAgICAgICAgZ3JvdXBDb2xsYXBzZWQ6IHByb3BzLFxuICAgICAgICBncm91cEVuZDogcHJvcHNcbiAgICAgIH0pO1xuICAgICAgLyogZXNsaW50LWVuYWJsZSByZWFjdC1pbnRlcm5hbC9uby1wcm9kdWN0aW9uLWxvZ2dpbmcgKi9cbiAgICB9XG5cbiAgICBkaXNhYmxlZERlcHRoKys7XG4gIH1cbn1cbmZ1bmN0aW9uIHJlZW5hYmxlTG9ncygpIHtcbiAge1xuICAgIGRpc2FibGVkRGVwdGgtLTtcblxuICAgIGlmIChkaXNhYmxlZERlcHRoID09PSAwKSB7XG4gICAgICAvKiBlc2xpbnQtZGlzYWJsZSByZWFjdC1pbnRlcm5hbC9uby1wcm9kdWN0aW9uLWxvZ2dpbmcgKi9cbiAgICAgIHZhciBwcm9wcyA9IHtcbiAgICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgICB3cml0YWJsZTogdHJ1ZVxuICAgICAgfTsgLy8gJEZsb3dGaXhNZSBGbG93IHRoaW5rcyBjb25zb2xlIGlzIGltbXV0YWJsZS5cblxuICAgICAgT2JqZWN0LmRlZmluZVByb3BlcnRpZXMoY29uc29sZSwge1xuICAgICAgICBsb2c6IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZMb2dcbiAgICAgICAgfSksXG4gICAgICAgIGluZm86IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZJbmZvXG4gICAgICAgIH0pLFxuICAgICAgICB3YXJuOiBfYXNzaWduKHt9LCBwcm9wcywge1xuICAgICAgICAgIHZhbHVlOiBwcmV2V2FyblxuICAgICAgICB9KSxcbiAgICAgICAgZXJyb3I6IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZFcnJvclxuICAgICAgICB9KSxcbiAgICAgICAgZ3JvdXA6IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZHcm91cFxuICAgICAgICB9KSxcbiAgICAgICAgZ3JvdXBDb2xsYXBzZWQ6IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZHcm91cENvbGxhcHNlZFxuICAgICAgICB9KSxcbiAgICAgICAgZ3JvdXBFbmQ6IF9hc3NpZ24oe30sIHByb3BzLCB7XG4gICAgICAgICAgdmFsdWU6IHByZXZHcm91cEVuZFxuICAgICAgICB9KVxuICAgICAgfSk7XG4gICAgICAvKiBlc2xpbnQtZW5hYmxlIHJlYWN0LWludGVybmFsL25vLXByb2R1Y3Rpb24tbG9nZ2luZyAqL1xuICAgIH1cblxuICAgIGlmIChkaXNhYmxlZERlcHRoIDwgMCkge1xuICAgICAgZXJyb3IoJ2Rpc2FibGVkRGVwdGggZmVsbCBiZWxvdyB6ZXJvLiAnICsgJ1RoaXMgaXMgYSBidWcgaW4gUmVhY3QuIFBsZWFzZSBmaWxlIGFuIGlzc3VlLicpO1xuICAgIH1cbiAgfVxufVxuXG52YXIgUmVhY3RDdXJyZW50RGlzcGF0Y2hlciA9IFJlYWN0U2hhcmVkSW50ZXJuYWxzLlJlYWN0Q3VycmVudERpc3BhdGNoZXI7XG52YXIgcHJlZml4O1xuZnVuY3Rpb24gZGVzY3JpYmVCdWlsdEluQ29tcG9uZW50RnJhbWUobmFtZSwgc291cmNlLCBvd25lckZuKSB7XG4gIHtcbiAgICBpZiAocHJlZml4ID09PSB1bmRlZmluZWQpIHtcbiAgICAgIC8vIEV4dHJhY3QgdGhlIFZNIHNwZWNpZmljIHByZWZpeCB1c2VkIGJ5IGVhY2ggbGluZS5cbiAgICAgIHRyeSB7XG4gICAgICAgIHRocm93IEVycm9yKCk7XG4gICAgICB9IGNhdGNoICh4KSB7XG4gICAgICAgIHZhciBtYXRjaCA9IHguc3RhY2sudHJpbSgpLm1hdGNoKC9cXG4oICooYXQgKT8pLyk7XG4gICAgICAgIHByZWZpeCA9IG1hdGNoICYmIG1hdGNoWzFdIHx8ICcnO1xuICAgICAgfVxuICAgIH0gLy8gV2UgdXNlIHRoZSBwcmVmaXggdG8gZW5zdXJlIG91ciBzdGFja3MgbGluZSB1cCB3aXRoIG5hdGl2ZSBzdGFjayBmcmFtZXMuXG5cblxuICAgIHJldHVybiAnXFxuJyArIHByZWZpeCArIG5hbWU7XG4gIH1cbn1cbnZhciByZWVudHJ5ID0gZmFsc2U7XG52YXIgY29tcG9uZW50RnJhbWVDYWNoZTtcblxue1xuICB2YXIgUG9zc2libHlXZWFrTWFwID0gdHlwZW9mIFdlYWtNYXAgPT09ICdmdW5jdGlvbicgPyBXZWFrTWFwIDogTWFwO1xuICBjb21wb25lbnRGcmFtZUNhY2hlID0gbmV3IFBvc3NpYmx5V2Vha01hcCgpO1xufVxuXG5mdW5jdGlvbiBkZXNjcmliZU5hdGl2ZUNvbXBvbmVudEZyYW1lKGZuLCBjb25zdHJ1Y3QpIHtcbiAgLy8gSWYgc29tZXRoaW5nIGFza2VkIGZvciBhIHN0YWNrIGluc2lkZSBhIGZha2UgcmVuZGVyLCBpdCBzaG91bGQgZ2V0IGlnbm9yZWQuXG4gIGlmICghZm4gfHwgcmVlbnRyeSkge1xuICAgIHJldHVybiAnJztcbiAgfVxuXG4gIHtcbiAgICB2YXIgZnJhbWUgPSBjb21wb25lbnRGcmFtZUNhY2hlLmdldChmbik7XG5cbiAgICBpZiAoZnJhbWUgIT09IHVuZGVmaW5lZCkge1xuICAgICAgcmV0dXJuIGZyYW1lO1xuICAgIH1cbiAgfVxuXG4gIHZhciBjb250cm9sO1xuICByZWVudHJ5ID0gdHJ1ZTtcbiAgdmFyIHByZXZpb3VzUHJlcGFyZVN0YWNrVHJhY2UgPSBFcnJvci5wcmVwYXJlU3RhY2tUcmFjZTsgLy8gJEZsb3dGaXhNZSBJdCBkb2VzIGFjY2VwdCB1bmRlZmluZWQuXG5cbiAgRXJyb3IucHJlcGFyZVN0YWNrVHJhY2UgPSB1bmRlZmluZWQ7XG4gIHZhciBwcmV2aW91c0Rpc3BhdGNoZXI7XG5cbiAge1xuICAgIHByZXZpb3VzRGlzcGF0Y2hlciA9IFJlYWN0Q3VycmVudERpc3BhdGNoZXIuY3VycmVudDsgLy8gU2V0IHRoZSBkaXNwYXRjaGVyIGluIERFViBiZWNhdXNlIHRoaXMgbWlnaHQgYmUgY2FsbCBpbiB0aGUgcmVuZGVyIGZ1bmN0aW9uXG4gICAgLy8gZm9yIHdhcm5pbmdzLlxuXG4gICAgUmVhY3RDdXJyZW50RGlzcGF0Y2hlci5jdXJyZW50ID0gbnVsbDtcbiAgICBkaXNhYmxlTG9ncygpO1xuICB9XG5cbiAgdHJ5IHtcbiAgICAvLyBUaGlzIHNob3VsZCB0aHJvdy5cbiAgICBpZiAoY29uc3RydWN0KSB7XG4gICAgICAvLyBTb21ldGhpbmcgc2hvdWxkIGJlIHNldHRpbmcgdGhlIHByb3BzIGluIHRoZSBjb25zdHJ1Y3Rvci5cbiAgICAgIHZhciBGYWtlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aHJvdyBFcnJvcigpO1xuICAgICAgfTsgLy8gJEZsb3dGaXhNZVxuXG5cbiAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShGYWtlLnByb3RvdHlwZSwgJ3Byb3BzJywge1xuICAgICAgICBzZXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAvLyBXZSB1c2UgYSB0aHJvd2luZyBzZXR0ZXIgaW5zdGVhZCBvZiBmcm96ZW4gb3Igbm9uLXdyaXRhYmxlIHByb3BzXG4gICAgICAgICAgLy8gYmVjYXVzZSB0aGF0IHdvbid0IHRocm93IGluIGEgbm9uLXN0cmljdCBtb2RlIGZ1bmN0aW9uLlxuICAgICAgICAgIHRocm93IEVycm9yKCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuXG4gICAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09ICdvYmplY3QnICYmIFJlZmxlY3QuY29uc3RydWN0KSB7XG4gICAgICAgIC8vIFdlIGNvbnN0cnVjdCBhIGRpZmZlcmVudCBjb250cm9sIGZvciB0aGlzIGNhc2UgdG8gaW5jbHVkZSBhbnkgZXh0cmFcbiAgICAgICAgLy8gZnJhbWVzIGFkZGVkIGJ5IHRoZSBjb25zdHJ1Y3QgY2FsbC5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICBSZWZsZWN0LmNvbnN0cnVjdChGYWtlLCBbXSk7XG4gICAgICAgIH0gY2F0Y2ggKHgpIHtcbiAgICAgICAgICBjb250cm9sID0geDtcbiAgICAgICAgfVxuXG4gICAgICAgIFJlZmxlY3QuY29uc3RydWN0KGZuLCBbXSwgRmFrZSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIEZha2UuY2FsbCgpO1xuICAgICAgICB9IGNhdGNoICh4KSB7XG4gICAgICAgICAgY29udHJvbCA9IHg7XG4gICAgICAgIH1cblxuICAgICAgICBmbi5jYWxsKEZha2UucHJvdG90eXBlKTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdHJ5IHtcbiAgICAgICAgdGhyb3cgRXJyb3IoKTtcbiAgICAgIH0gY2F0Y2ggKHgpIHtcbiAgICAgICAgY29udHJvbCA9IHg7XG4gICAgICB9XG5cbiAgICAgIGZuKCk7XG4gICAgfVxuICB9IGNhdGNoIChzYW1wbGUpIHtcbiAgICAvLyBUaGlzIGlzIGlubGluZWQgbWFudWFsbHkgYmVjYXVzZSBjbG9zdXJlIGRvZXNuJ3QgZG8gaXQgZm9yIHVzLlxuICAgIGlmIChzYW1wbGUgJiYgY29udHJvbCAmJiB0eXBlb2Ygc2FtcGxlLnN0YWNrID09PSAnc3RyaW5nJykge1xuICAgICAgLy8gVGhpcyBleHRyYWN0cyB0aGUgZmlyc3QgZnJhbWUgZnJvbSB0aGUgc2FtcGxlIHRoYXQgaXNuJ3QgYWxzbyBpbiB0aGUgY29udHJvbC5cbiAgICAgIC8vIFNraXBwaW5nIG9uZSBmcmFtZSB0aGF0IHdlIGFzc3VtZSBpcyB0aGUgZnJhbWUgdGhhdCBjYWxscyB0aGUgdHdvLlxuICAgICAgdmFyIHNhbXBsZUxpbmVzID0gc2FtcGxlLnN0YWNrLnNwbGl0KCdcXG4nKTtcbiAgICAgIHZhciBjb250cm9sTGluZXMgPSBjb250cm9sLnN0YWNrLnNwbGl0KCdcXG4nKTtcbiAgICAgIHZhciBzID0gc2FtcGxlTGluZXMubGVuZ3RoIC0gMTtcbiAgICAgIHZhciBjID0gY29udHJvbExpbmVzLmxlbmd0aCAtIDE7XG5cbiAgICAgIHdoaWxlIChzID49IDEgJiYgYyA+PSAwICYmIHNhbXBsZUxpbmVzW3NdICE9PSBjb250cm9sTGluZXNbY10pIHtcbiAgICAgICAgLy8gV2UgZXhwZWN0IGF0IGxlYXN0IG9uZSBzdGFjayBmcmFtZSB0byBiZSBzaGFyZWQuXG4gICAgICAgIC8vIFR5cGljYWxseSB0aGlzIHdpbGwgYmUgdGhlIHJvb3QgbW9zdCBvbmUuIEhvd2V2ZXIsIHN0YWNrIGZyYW1lcyBtYXkgYmVcbiAgICAgICAgLy8gY3V0IG9mZiBkdWUgdG8gbWF4aW11bSBzdGFjayBsaW1pdHMuIEluIHRoaXMgY2FzZSwgb25lIG1heWJlIGN1dCBvZmZcbiAgICAgICAgLy8gZWFybGllciB0aGFuIHRoZSBvdGhlci4gV2UgYXNzdW1lIHRoYXQgdGhlIHNhbXBsZSBpcyBsb25nZXIgb3IgdGhlIHNhbWVcbiAgICAgICAgLy8gYW5kIHRoZXJlIGZvciBjdXQgb2ZmIGVhcmxpZXIuIFNvIHdlIHNob3VsZCBmaW5kIHRoZSByb290IG1vc3QgZnJhbWUgaW5cbiAgICAgICAgLy8gdGhlIHNhbXBsZSBzb21ld2hlcmUgaW4gdGhlIGNvbnRyb2wuXG4gICAgICAgIGMtLTtcbiAgICAgIH1cblxuICAgICAgZm9yICg7IHMgPj0gMSAmJiBjID49IDA7IHMtLSwgYy0tKSB7XG4gICAgICAgIC8vIE5leHQgd2UgZmluZCB0aGUgZmlyc3Qgb25lIHRoYXQgaXNuJ3QgdGhlIHNhbWUgd2hpY2ggc2hvdWxkIGJlIHRoZVxuICAgICAgICAvLyBmcmFtZSB0aGF0IGNhbGxlZCBvdXIgc2FtcGxlIGZ1bmN0aW9uIGFuZCB0aGUgY29udHJvbC5cbiAgICAgICAgaWYgKHNhbXBsZUxpbmVzW3NdICE9PSBjb250cm9sTGluZXNbY10pIHtcbiAgICAgICAgICAvLyBJbiBWOCwgdGhlIGZpcnN0IGxpbmUgaXMgZGVzY3JpYmluZyB0aGUgbWVzc2FnZSBidXQgb3RoZXIgVk1zIGRvbid0LlxuICAgICAgICAgIC8vIElmIHdlJ3JlIGFib3V0IHRvIHJldHVybiB0aGUgZmlyc3QgbGluZSwgYW5kIHRoZSBjb250cm9sIGlzIGFsc28gb24gdGhlIHNhbWVcbiAgICAgICAgICAvLyBsaW5lLCB0aGF0J3MgYSBwcmV0dHkgZ29vZCBpbmRpY2F0b3IgdGhhdCBvdXIgc2FtcGxlIHRocmV3IGF0IHNhbWUgbGluZSBhc1xuICAgICAgICAgIC8vIHRoZSBjb250cm9sLiBJLmUuIGJlZm9yZSB3ZSBlbnRlcmVkIHRoZSBzYW1wbGUgZnJhbWUuIFNvIHdlIGlnbm9yZSB0aGlzIHJlc3VsdC5cbiAgICAgICAgICAvLyBUaGlzIGNhbiBoYXBwZW4gaWYgeW91IHBhc3NlZCBhIGNsYXNzIHRvIGZ1bmN0aW9uIGNvbXBvbmVudCwgb3Igbm9uLWZ1bmN0aW9uLlxuICAgICAgICAgIGlmIChzICE9PSAxIHx8IGMgIT09IDEpIHtcbiAgICAgICAgICAgIGRvIHtcbiAgICAgICAgICAgICAgcy0tO1xuICAgICAgICAgICAgICBjLS07IC8vIFdlIG1heSBzdGlsbCBoYXZlIHNpbWlsYXIgaW50ZXJtZWRpYXRlIGZyYW1lcyBmcm9tIHRoZSBjb25zdHJ1Y3QgY2FsbC5cbiAgICAgICAgICAgICAgLy8gVGhlIG5leHQgb25lIHRoYXQgaXNuJ3QgdGhlIHNhbWUgc2hvdWxkIGJlIG91ciBtYXRjaCB0aG91Z2guXG5cbiAgICAgICAgICAgICAgaWYgKGMgPCAwIHx8IHNhbXBsZUxpbmVzW3NdICE9PSBjb250cm9sTGluZXNbY10pIHtcbiAgICAgICAgICAgICAgICAvLyBWOCBhZGRzIGEgXCJuZXdcIiBwcmVmaXggZm9yIG5hdGl2ZSBjbGFzc2VzLiBMZXQncyByZW1vdmUgaXQgdG8gbWFrZSBpdCBwcmV0dGllci5cbiAgICAgICAgICAgICAgICB2YXIgX2ZyYW1lID0gJ1xcbicgKyBzYW1wbGVMaW5lc1tzXS5yZXBsYWNlKCcgYXQgbmV3ICcsICcgYXQgJyk7XG5cbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGZuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbXBvbmVudEZyYW1lQ2FjaGUuc2V0KGZuLCBfZnJhbWUpO1xuICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH0gLy8gUmV0dXJuIHRoZSBsaW5lIHdlIGZvdW5kLlxuXG5cbiAgICAgICAgICAgICAgICByZXR1cm4gX2ZyYW1lO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IHdoaWxlIChzID49IDEgJiYgYyA+PSAwKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfSBmaW5hbGx5IHtcbiAgICByZWVudHJ5ID0gZmFsc2U7XG5cbiAgICB7XG4gICAgICBSZWFjdEN1cnJlbnREaXNwYXRjaGVyLmN1cnJlbnQgPSBwcmV2aW91c0Rpc3BhdGNoZXI7XG4gICAgICByZWVuYWJsZUxvZ3MoKTtcbiAgICB9XG5cbiAgICBFcnJvci5wcmVwYXJlU3RhY2tUcmFjZSA9IHByZXZpb3VzUHJlcGFyZVN0YWNrVHJhY2U7XG4gIH0gLy8gRmFsbGJhY2sgdG8ganVzdCB1c2luZyB0aGUgbmFtZSBpZiB3ZSBjb3VsZG4ndCBtYWtlIGl0IHRocm93LlxuXG5cbiAgdmFyIG5hbWUgPSBmbiA/IGZuLmRpc3BsYXlOYW1lIHx8IGZuLm5hbWUgOiAnJztcbiAgdmFyIHN5bnRoZXRpY0ZyYW1lID0gbmFtZSA/IGRlc2NyaWJlQnVpbHRJbkNvbXBvbmVudEZyYW1lKG5hbWUpIDogJyc7XG5cbiAge1xuICAgIGlmICh0eXBlb2YgZm4gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIGNvbXBvbmVudEZyYW1lQ2FjaGUuc2V0KGZuLCBzeW50aGV0aWNGcmFtZSk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHN5bnRoZXRpY0ZyYW1lO1xufVxuZnVuY3Rpb24gZGVzY3JpYmVGdW5jdGlvbkNvbXBvbmVudEZyYW1lKGZuLCBzb3VyY2UsIG93bmVyRm4pIHtcbiAge1xuICAgIHJldHVybiBkZXNjcmliZU5hdGl2ZUNvbXBvbmVudEZyYW1lKGZuLCBmYWxzZSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gc2hvdWxkQ29uc3RydWN0KENvbXBvbmVudCkge1xuICB2YXIgcHJvdG90eXBlID0gQ29tcG9uZW50LnByb3RvdHlwZTtcbiAgcmV0dXJuICEhKHByb3RvdHlwZSAmJiBwcm90b3R5cGUuaXNSZWFjdENvbXBvbmVudCk7XG59XG5cbmZ1bmN0aW9uIGRlc2NyaWJlVW5rbm93bkVsZW1lbnRUeXBlRnJhbWVJbkRFVih0eXBlLCBzb3VyY2UsIG93bmVyRm4pIHtcblxuICBpZiAodHlwZSA9PSBudWxsKSB7XG4gICAgcmV0dXJuICcnO1xuICB9XG5cbiAgaWYgKHR5cGVvZiB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAge1xuICAgICAgcmV0dXJuIGRlc2NyaWJlTmF0aXZlQ29tcG9uZW50RnJhbWUodHlwZSwgc2hvdWxkQ29uc3RydWN0KHR5cGUpKTtcbiAgICB9XG4gIH1cblxuICBpZiAodHlwZW9mIHR5cGUgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIGRlc2NyaWJlQnVpbHRJbkNvbXBvbmVudEZyYW1lKHR5cGUpO1xuICB9XG5cbiAgc3dpdGNoICh0eXBlKSB7XG4gICAgY2FzZSBSRUFDVF9TVVNQRU5TRV9UWVBFOlxuICAgICAgcmV0dXJuIGRlc2NyaWJlQnVpbHRJbkNvbXBvbmVudEZyYW1lKCdTdXNwZW5zZScpO1xuXG4gICAgY2FzZSBSRUFDVF9TVVNQRU5TRV9MSVNUX1RZUEU6XG4gICAgICByZXR1cm4gZGVzY3JpYmVCdWlsdEluQ29tcG9uZW50RnJhbWUoJ1N1c3BlbnNlTGlzdCcpO1xuICB9XG5cbiAgaWYgKHR5cGVvZiB0eXBlID09PSAnb2JqZWN0Jykge1xuICAgIHN3aXRjaCAodHlwZS4kJHR5cGVvZikge1xuICAgICAgY2FzZSBSRUFDVF9GT1JXQVJEX1JFRl9UWVBFOlxuICAgICAgICByZXR1cm4gZGVzY3JpYmVGdW5jdGlvbkNvbXBvbmVudEZyYW1lKHR5cGUucmVuZGVyKTtcblxuICAgICAgY2FzZSBSRUFDVF9NRU1PX1RZUEU6XG4gICAgICAgIC8vIE1lbW8gbWF5IGNvbnRhaW4gYW55IGNvbXBvbmVudCB0eXBlIHNvIHdlIHJlY3Vyc2l2ZWx5IHJlc29sdmUgaXQuXG4gICAgICAgIHJldHVybiBkZXNjcmliZVVua25vd25FbGVtZW50VHlwZUZyYW1lSW5ERVYodHlwZS50eXBlLCBzb3VyY2UsIG93bmVyRm4pO1xuXG4gICAgICBjYXNlIFJFQUNUX0JMT0NLX1RZUEU6XG4gICAgICAgIHJldHVybiBkZXNjcmliZUZ1bmN0aW9uQ29tcG9uZW50RnJhbWUodHlwZS5fcmVuZGVyKTtcblxuICAgICAgY2FzZSBSRUFDVF9MQVpZX1RZUEU6XG4gICAgICAgIHtcbiAgICAgICAgICB2YXIgbGF6eUNvbXBvbmVudCA9IHR5cGU7XG4gICAgICAgICAgdmFyIHBheWxvYWQgPSBsYXp5Q29tcG9uZW50Ll9wYXlsb2FkO1xuICAgICAgICAgIHZhciBpbml0ID0gbGF6eUNvbXBvbmVudC5faW5pdDtcblxuICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBMYXp5IG1heSBjb250YWluIGFueSBjb21wb25lbnQgdHlwZSBzbyB3ZSByZWN1cnNpdmVseSByZXNvbHZlIGl0LlxuICAgICAgICAgICAgcmV0dXJuIGRlc2NyaWJlVW5rbm93bkVsZW1lbnRUeXBlRnJhbWVJbkRFVihpbml0KHBheWxvYWQpLCBzb3VyY2UsIG93bmVyRm4pO1xuICAgICAgICAgIH0gY2F0Y2ggKHgpIHt9XG4gICAgICAgIH1cbiAgICB9XG4gIH1cblxuICByZXR1cm4gJyc7XG59XG5cbnZhciBsb2dnZWRUeXBlRmFpbHVyZXMgPSB7fTtcbnZhciBSZWFjdERlYnVnQ3VycmVudEZyYW1lID0gUmVhY3RTaGFyZWRJbnRlcm5hbHMuUmVhY3REZWJ1Z0N1cnJlbnRGcmFtZTtcblxuZnVuY3Rpb24gc2V0Q3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQoZWxlbWVudCkge1xuICB7XG4gICAgaWYgKGVsZW1lbnQpIHtcbiAgICAgIHZhciBvd25lciA9IGVsZW1lbnQuX293bmVyO1xuICAgICAgdmFyIHN0YWNrID0gZGVzY3JpYmVVbmtub3duRWxlbWVudFR5cGVGcmFtZUluREVWKGVsZW1lbnQudHlwZSwgZWxlbWVudC5fc291cmNlLCBvd25lciA/IG93bmVyLnR5cGUgOiBudWxsKTtcbiAgICAgIFJlYWN0RGVidWdDdXJyZW50RnJhbWUuc2V0RXh0cmFTdGFja0ZyYW1lKHN0YWNrKTtcbiAgICB9IGVsc2Uge1xuICAgICAgUmVhY3REZWJ1Z0N1cnJlbnRGcmFtZS5zZXRFeHRyYVN0YWNrRnJhbWUobnVsbCk7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGNoZWNrUHJvcFR5cGVzKHR5cGVTcGVjcywgdmFsdWVzLCBsb2NhdGlvbiwgY29tcG9uZW50TmFtZSwgZWxlbWVudCkge1xuICB7XG4gICAgLy8gJEZsb3dGaXhNZSBUaGlzIGlzIG9rYXkgYnV0IEZsb3cgZG9lc24ndCBrbm93IGl0LlxuICAgIHZhciBoYXMgPSBGdW5jdGlvbi5jYWxsLmJpbmQoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eSk7XG5cbiAgICBmb3IgKHZhciB0eXBlU3BlY05hbWUgaW4gdHlwZVNwZWNzKSB7XG4gICAgICBpZiAoaGFzKHR5cGVTcGVjcywgdHlwZVNwZWNOYW1lKSkge1xuICAgICAgICB2YXIgZXJyb3IkMSA9IHZvaWQgMDsgLy8gUHJvcCB0eXBlIHZhbGlkYXRpb24gbWF5IHRocm93LiBJbiBjYXNlIHRoZXkgZG8sIHdlIGRvbid0IHdhbnQgdG9cbiAgICAgICAgLy8gZmFpbCB0aGUgcmVuZGVyIHBoYXNlIHdoZXJlIGl0IGRpZG4ndCBmYWlsIGJlZm9yZS4gU28gd2UgbG9nIGl0LlxuICAgICAgICAvLyBBZnRlciB0aGVzZSBoYXZlIGJlZW4gY2xlYW5lZCB1cCwgd2UnbGwgbGV0IHRoZW0gdGhyb3cuXG5cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAvLyBUaGlzIGlzIGludGVudGlvbmFsbHkgYW4gaW52YXJpYW50IHRoYXQgZ2V0cyBjYXVnaHQuIEl0J3MgdGhlIHNhbWVcbiAgICAgICAgICAvLyBiZWhhdmlvciBhcyB3aXRob3V0IHRoaXMgc3RhdGVtZW50IGV4Y2VwdCB3aXRoIGEgYmV0dGVyIG1lc3NhZ2UuXG4gICAgICAgICAgaWYgKHR5cGVvZiB0eXBlU3BlY3NbdHlwZVNwZWNOYW1lXSAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdmFyIGVyciA9IEVycm9yKChjb21wb25lbnROYW1lIHx8ICdSZWFjdCBjbGFzcycpICsgJzogJyArIGxvY2F0aW9uICsgJyB0eXBlIGAnICsgdHlwZVNwZWNOYW1lICsgJ2AgaXMgaW52YWxpZDsgJyArICdpdCBtdXN0IGJlIGEgZnVuY3Rpb24sIHVzdWFsbHkgZnJvbSB0aGUgYHByb3AtdHlwZXNgIHBhY2thZ2UsIGJ1dCByZWNlaXZlZCBgJyArIHR5cGVvZiB0eXBlU3BlY3NbdHlwZVNwZWNOYW1lXSArICdgLicgKyAnVGhpcyBvZnRlbiBoYXBwZW5zIGJlY2F1c2Ugb2YgdHlwb3Mgc3VjaCBhcyBgUHJvcFR5cGVzLmZ1bmN0aW9uYCBpbnN0ZWFkIG9mIGBQcm9wVHlwZXMuZnVuY2AuJyk7XG4gICAgICAgICAgICBlcnIubmFtZSA9ICdJbnZhcmlhbnQgVmlvbGF0aW9uJztcbiAgICAgICAgICAgIHRocm93IGVycjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBlcnJvciQxID0gdHlwZVNwZWNzW3R5cGVTcGVjTmFtZV0odmFsdWVzLCB0eXBlU3BlY05hbWUsIGNvbXBvbmVudE5hbWUsIGxvY2F0aW9uLCBudWxsLCAnU0VDUkVUX0RPX05PVF9QQVNTX1RISVNfT1JfWU9VX1dJTExfQkVfRklSRUQnKTtcbiAgICAgICAgfSBjYXRjaCAoZXgpIHtcbiAgICAgICAgICBlcnJvciQxID0gZXg7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZXJyb3IkMSAmJiAhKGVycm9yJDEgaW5zdGFuY2VvZiBFcnJvcikpIHtcbiAgICAgICAgICBzZXRDdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudChlbGVtZW50KTtcblxuICAgICAgICAgIGVycm9yKCclczogdHlwZSBzcGVjaWZpY2F0aW9uIG9mICVzJyArICcgYCVzYCBpcyBpbnZhbGlkOyB0aGUgdHlwZSBjaGVja2VyICcgKyAnZnVuY3Rpb24gbXVzdCByZXR1cm4gYG51bGxgIG9yIGFuIGBFcnJvcmAgYnV0IHJldHVybmVkIGEgJXMuICcgKyAnWW91IG1heSBoYXZlIGZvcmdvdHRlbiB0byBwYXNzIGFuIGFyZ3VtZW50IHRvIHRoZSB0eXBlIGNoZWNrZXIgJyArICdjcmVhdG9yIChhcnJheU9mLCBpbnN0YW5jZU9mLCBvYmplY3RPZiwgb25lT2YsIG9uZU9mVHlwZSwgYW5kICcgKyAnc2hhcGUgYWxsIHJlcXVpcmUgYW4gYXJndW1lbnQpLicsIGNvbXBvbmVudE5hbWUgfHwgJ1JlYWN0IGNsYXNzJywgbG9jYXRpb24sIHR5cGVTcGVjTmFtZSwgdHlwZW9mIGVycm9yJDEpO1xuXG4gICAgICAgICAgc2V0Q3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQobnVsbCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZXJyb3IkMSBpbnN0YW5jZW9mIEVycm9yICYmICEoZXJyb3IkMS5tZXNzYWdlIGluIGxvZ2dlZFR5cGVGYWlsdXJlcykpIHtcbiAgICAgICAgICAvLyBPbmx5IG1vbml0b3IgdGhpcyBmYWlsdXJlIG9uY2UgYmVjYXVzZSB0aGVyZSB0ZW5kcyB0byBiZSBhIGxvdCBvZiB0aGVcbiAgICAgICAgICAvLyBzYW1lIGVycm9yLlxuICAgICAgICAgIGxvZ2dlZFR5cGVGYWlsdXJlc1tlcnJvciQxLm1lc3NhZ2VdID0gdHJ1ZTtcbiAgICAgICAgICBzZXRDdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudChlbGVtZW50KTtcblxuICAgICAgICAgIGVycm9yKCdGYWlsZWQgJXMgdHlwZTogJXMnLCBsb2NhdGlvbiwgZXJyb3IkMS5tZXNzYWdlKTtcblxuICAgICAgICAgIHNldEN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50KG51bGwpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbnZhciBSZWFjdEN1cnJlbnRPd25lciA9IFJlYWN0U2hhcmVkSW50ZXJuYWxzLlJlYWN0Q3VycmVudE93bmVyO1xudmFyIGhhc093blByb3BlcnR5ID0gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eTtcbnZhciBSRVNFUlZFRF9QUk9QUyA9IHtcbiAga2V5OiB0cnVlLFxuICByZWY6IHRydWUsXG4gIF9fc2VsZjogdHJ1ZSxcbiAgX19zb3VyY2U6IHRydWVcbn07XG52YXIgc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd247XG52YXIgc3BlY2lhbFByb3BSZWZXYXJuaW5nU2hvd247XG52YXIgZGlkV2FybkFib3V0U3RyaW5nUmVmcztcblxue1xuICBkaWRXYXJuQWJvdXRTdHJpbmdSZWZzID0ge307XG59XG5cbmZ1bmN0aW9uIGhhc1ZhbGlkUmVmKGNvbmZpZykge1xuICB7XG4gICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwoY29uZmlnLCAncmVmJykpIHtcbiAgICAgIHZhciBnZXR0ZXIgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGNvbmZpZywgJ3JlZicpLmdldDtcblxuICAgICAgaWYgKGdldHRlciAmJiBnZXR0ZXIuaXNSZWFjdFdhcm5pbmcpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBjb25maWcucmVmICE9PSB1bmRlZmluZWQ7XG59XG5cbmZ1bmN0aW9uIGhhc1ZhbGlkS2V5KGNvbmZpZykge1xuICB7XG4gICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwoY29uZmlnLCAna2V5JykpIHtcbiAgICAgIHZhciBnZXR0ZXIgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGNvbmZpZywgJ2tleScpLmdldDtcblxuICAgICAgaWYgKGdldHRlciAmJiBnZXR0ZXIuaXNSZWFjdFdhcm5pbmcpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiBjb25maWcua2V5ICE9PSB1bmRlZmluZWQ7XG59XG5cbmZ1bmN0aW9uIHdhcm5JZlN0cmluZ1JlZkNhbm5vdEJlQXV0b0NvbnZlcnRlZChjb25maWcsIHNlbGYpIHtcbiAge1xuICAgIGlmICh0eXBlb2YgY29uZmlnLnJlZiA9PT0gJ3N0cmluZycgJiYgUmVhY3RDdXJyZW50T3duZXIuY3VycmVudCAmJiBzZWxmICYmIFJlYWN0Q3VycmVudE93bmVyLmN1cnJlbnQuc3RhdGVOb2RlICE9PSBzZWxmKSB7XG4gICAgICB2YXIgY29tcG9uZW50TmFtZSA9IGdldENvbXBvbmVudE5hbWUoUmVhY3RDdXJyZW50T3duZXIuY3VycmVudC50eXBlKTtcblxuICAgICAgaWYgKCFkaWRXYXJuQWJvdXRTdHJpbmdSZWZzW2NvbXBvbmVudE5hbWVdKSB7XG4gICAgICAgIGVycm9yKCdDb21wb25lbnQgXCIlc1wiIGNvbnRhaW5zIHRoZSBzdHJpbmcgcmVmIFwiJXNcIi4gJyArICdTdXBwb3J0IGZvciBzdHJpbmcgcmVmcyB3aWxsIGJlIHJlbW92ZWQgaW4gYSBmdXR1cmUgbWFqb3IgcmVsZWFzZS4gJyArICdUaGlzIGNhc2UgY2Fubm90IGJlIGF1dG9tYXRpY2FsbHkgY29udmVydGVkIHRvIGFuIGFycm93IGZ1bmN0aW9uLiAnICsgJ1dlIGFzayB5b3UgdG8gbWFudWFsbHkgZml4IHRoaXMgY2FzZSBieSB1c2luZyB1c2VSZWYoKSBvciBjcmVhdGVSZWYoKSBpbnN0ZWFkLiAnICsgJ0xlYXJuIG1vcmUgYWJvdXQgdXNpbmcgcmVmcyBzYWZlbHkgaGVyZTogJyArICdodHRwczovL3JlYWN0anMub3JnL2xpbmsvc3RyaWN0LW1vZGUtc3RyaW5nLXJlZicsIGdldENvbXBvbmVudE5hbWUoUmVhY3RDdXJyZW50T3duZXIuY3VycmVudC50eXBlKSwgY29uZmlnLnJlZik7XG5cbiAgICAgICAgZGlkV2FybkFib3V0U3RyaW5nUmVmc1tjb21wb25lbnROYW1lXSA9IHRydWU7XG4gICAgICB9XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGRlZmluZUtleVByb3BXYXJuaW5nR2V0dGVyKHByb3BzLCBkaXNwbGF5TmFtZSkge1xuICB7XG4gICAgdmFyIHdhcm5BYm91dEFjY2Vzc2luZ0tleSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICghc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd24pIHtcbiAgICAgICAgc3BlY2lhbFByb3BLZXlXYXJuaW5nU2hvd24gPSB0cnVlO1xuXG4gICAgICAgIGVycm9yKCclczogYGtleWAgaXMgbm90IGEgcHJvcC4gVHJ5aW5nIHRvIGFjY2VzcyBpdCB3aWxsIHJlc3VsdCAnICsgJ2luIGB1bmRlZmluZWRgIGJlaW5nIHJldHVybmVkLiBJZiB5b3UgbmVlZCB0byBhY2Nlc3MgdGhlIHNhbWUgJyArICd2YWx1ZSB3aXRoaW4gdGhlIGNoaWxkIGNvbXBvbmVudCwgeW91IHNob3VsZCBwYXNzIGl0IGFzIGEgZGlmZmVyZW50ICcgKyAncHJvcC4gKGh0dHBzOi8vcmVhY3Rqcy5vcmcvbGluay9zcGVjaWFsLXByb3BzKScsIGRpc3BsYXlOYW1lKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgd2FybkFib3V0QWNjZXNzaW5nS2V5LmlzUmVhY3RXYXJuaW5nID0gdHJ1ZTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkocHJvcHMsICdrZXknLCB7XG4gICAgICBnZXQ6IHdhcm5BYm91dEFjY2Vzc2luZ0tleSxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIGRlZmluZVJlZlByb3BXYXJuaW5nR2V0dGVyKHByb3BzLCBkaXNwbGF5TmFtZSkge1xuICB7XG4gICAgdmFyIHdhcm5BYm91dEFjY2Vzc2luZ1JlZiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICghc3BlY2lhbFByb3BSZWZXYXJuaW5nU2hvd24pIHtcbiAgICAgICAgc3BlY2lhbFByb3BSZWZXYXJuaW5nU2hvd24gPSB0cnVlO1xuXG4gICAgICAgIGVycm9yKCclczogYHJlZmAgaXMgbm90IGEgcHJvcC4gVHJ5aW5nIHRvIGFjY2VzcyBpdCB3aWxsIHJlc3VsdCAnICsgJ2luIGB1bmRlZmluZWRgIGJlaW5nIHJldHVybmVkLiBJZiB5b3UgbmVlZCB0byBhY2Nlc3MgdGhlIHNhbWUgJyArICd2YWx1ZSB3aXRoaW4gdGhlIGNoaWxkIGNvbXBvbmVudCwgeW91IHNob3VsZCBwYXNzIGl0IGFzIGEgZGlmZmVyZW50ICcgKyAncHJvcC4gKGh0dHBzOi8vcmVhY3Rqcy5vcmcvbGluay9zcGVjaWFsLXByb3BzKScsIGRpc3BsYXlOYW1lKTtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgd2FybkFib3V0QWNjZXNzaW5nUmVmLmlzUmVhY3RXYXJuaW5nID0gdHJ1ZTtcbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkocHJvcHMsICdyZWYnLCB7XG4gICAgICBnZXQ6IHdhcm5BYm91dEFjY2Vzc2luZ1JlZixcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH0pO1xuICB9XG59XG4vKipcbiAqIEZhY3RvcnkgbWV0aG9kIHRvIGNyZWF0ZSBhIG5ldyBSZWFjdCBlbGVtZW50LiBUaGlzIG5vIGxvbmdlciBhZGhlcmVzIHRvXG4gKiB0aGUgY2xhc3MgcGF0dGVybiwgc28gZG8gbm90IHVzZSBuZXcgdG8gY2FsbCBpdC4gQWxzbywgaW5zdGFuY2VvZiBjaGVja1xuICogd2lsbCBub3Qgd29yay4gSW5zdGVhZCB0ZXN0ICQkdHlwZW9mIGZpZWxkIGFnYWluc3QgU3ltYm9sLmZvcigncmVhY3QuZWxlbWVudCcpIHRvIGNoZWNrXG4gKiBpZiBzb21ldGhpbmcgaXMgYSBSZWFjdCBFbGVtZW50LlxuICpcbiAqIEBwYXJhbSB7Kn0gdHlwZVxuICogQHBhcmFtIHsqfSBwcm9wc1xuICogQHBhcmFtIHsqfSBrZXlcbiAqIEBwYXJhbSB7c3RyaW5nfG9iamVjdH0gcmVmXG4gKiBAcGFyYW0geyp9IG93bmVyXG4gKiBAcGFyYW0geyp9IHNlbGYgQSAqdGVtcG9yYXJ5KiBoZWxwZXIgdG8gZGV0ZWN0IHBsYWNlcyB3aGVyZSBgdGhpc2AgaXNcbiAqIGRpZmZlcmVudCBmcm9tIHRoZSBgb3duZXJgIHdoZW4gUmVhY3QuY3JlYXRlRWxlbWVudCBpcyBjYWxsZWQsIHNvIHRoYXQgd2VcbiAqIGNhbiB3YXJuLiBXZSB3YW50IHRvIGdldCByaWQgb2Ygb3duZXIgYW5kIHJlcGxhY2Ugc3RyaW5nIGByZWZgcyB3aXRoIGFycm93XG4gKiBmdW5jdGlvbnMsIGFuZCBhcyBsb25nIGFzIGB0aGlzYCBhbmQgb3duZXIgYXJlIHRoZSBzYW1lLCB0aGVyZSB3aWxsIGJlIG5vXG4gKiBjaGFuZ2UgaW4gYmVoYXZpb3IuXG4gKiBAcGFyYW0geyp9IHNvdXJjZSBBbiBhbm5vdGF0aW9uIG9iamVjdCAoYWRkZWQgYnkgYSB0cmFuc3BpbGVyIG9yIG90aGVyd2lzZSlcbiAqIGluZGljYXRpbmcgZmlsZW5hbWUsIGxpbmUgbnVtYmVyLCBhbmQvb3Igb3RoZXIgaW5mb3JtYXRpb24uXG4gKiBAaW50ZXJuYWxcbiAqL1xuXG5cbnZhciBSZWFjdEVsZW1lbnQgPSBmdW5jdGlvbiAodHlwZSwga2V5LCByZWYsIHNlbGYsIHNvdXJjZSwgb3duZXIsIHByb3BzKSB7XG4gIHZhciBlbGVtZW50ID0ge1xuICAgIC8vIFRoaXMgdGFnIGFsbG93cyB1cyB0byB1bmlxdWVseSBpZGVudGlmeSB0aGlzIGFzIGEgUmVhY3QgRWxlbWVudFxuICAgICQkdHlwZW9mOiBSRUFDVF9FTEVNRU5UX1RZUEUsXG4gICAgLy8gQnVpbHQtaW4gcHJvcGVydGllcyB0aGF0IGJlbG9uZyBvbiB0aGUgZWxlbWVudFxuICAgIHR5cGU6IHR5cGUsXG4gICAga2V5OiBrZXksXG4gICAgcmVmOiByZWYsXG4gICAgcHJvcHM6IHByb3BzLFxuICAgIC8vIFJlY29yZCB0aGUgY29tcG9uZW50IHJlc3BvbnNpYmxlIGZvciBjcmVhdGluZyB0aGlzIGVsZW1lbnQuXG4gICAgX293bmVyOiBvd25lclxuICB9O1xuXG4gIHtcbiAgICAvLyBUaGUgdmFsaWRhdGlvbiBmbGFnIGlzIGN1cnJlbnRseSBtdXRhdGl2ZS4gV2UgcHV0IGl0IG9uXG4gICAgLy8gYW4gZXh0ZXJuYWwgYmFja2luZyBzdG9yZSBzbyB0aGF0IHdlIGNhbiBmcmVlemUgdGhlIHdob2xlIG9iamVjdC5cbiAgICAvLyBUaGlzIGNhbiBiZSByZXBsYWNlZCB3aXRoIGEgV2Vha01hcCBvbmNlIHRoZXkgYXJlIGltcGxlbWVudGVkIGluXG4gICAgLy8gY29tbW9ubHkgdXNlZCBkZXZlbG9wbWVudCBlbnZpcm9ubWVudHMuXG4gICAgZWxlbWVudC5fc3RvcmUgPSB7fTsgLy8gVG8gbWFrZSBjb21wYXJpbmcgUmVhY3RFbGVtZW50cyBlYXNpZXIgZm9yIHRlc3RpbmcgcHVycG9zZXMsIHdlIG1ha2VcbiAgICAvLyB0aGUgdmFsaWRhdGlvbiBmbGFnIG5vbi1lbnVtZXJhYmxlICh3aGVyZSBwb3NzaWJsZSwgd2hpY2ggc2hvdWxkXG4gICAgLy8gaW5jbHVkZSBldmVyeSBlbnZpcm9ubWVudCB3ZSBydW4gdGVzdHMgaW4pLCBzbyB0aGUgdGVzdCBmcmFtZXdvcmtcbiAgICAvLyBpZ25vcmVzIGl0LlxuXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGVsZW1lbnQuX3N0b3JlLCAndmFsaWRhdGVkJywge1xuICAgICAgY29uZmlndXJhYmxlOiBmYWxzZSxcbiAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgd3JpdGFibGU6IHRydWUsXG4gICAgICB2YWx1ZTogZmFsc2VcbiAgICB9KTsgLy8gc2VsZiBhbmQgc291cmNlIGFyZSBERVYgb25seSBwcm9wZXJ0aWVzLlxuXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGVsZW1lbnQsICdfc2VsZicsIHtcbiAgICAgIGNvbmZpZ3VyYWJsZTogZmFsc2UsXG4gICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgIHdyaXRhYmxlOiBmYWxzZSxcbiAgICAgIHZhbHVlOiBzZWxmXG4gICAgfSk7IC8vIFR3byBlbGVtZW50cyBjcmVhdGVkIGluIHR3byBkaWZmZXJlbnQgcGxhY2VzIHNob3VsZCBiZSBjb25zaWRlcmVkXG4gICAgLy8gZXF1YWwgZm9yIHRlc3RpbmcgcHVycG9zZXMgYW5kIHRoZXJlZm9yZSB3ZSBoaWRlIGl0IGZyb20gZW51bWVyYXRpb24uXG5cbiAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoZWxlbWVudCwgJ19zb3VyY2UnLCB7XG4gICAgICBjb25maWd1cmFibGU6IGZhbHNlLFxuICAgICAgZW51bWVyYWJsZTogZmFsc2UsXG4gICAgICB3cml0YWJsZTogZmFsc2UsXG4gICAgICB2YWx1ZTogc291cmNlXG4gICAgfSk7XG5cbiAgICBpZiAoT2JqZWN0LmZyZWV6ZSkge1xuICAgICAgT2JqZWN0LmZyZWV6ZShlbGVtZW50LnByb3BzKTtcbiAgICAgIE9iamVjdC5mcmVlemUoZWxlbWVudCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGVsZW1lbnQ7XG59O1xuLyoqXG4gKiBodHRwczovL2dpdGh1Yi5jb20vcmVhY3Rqcy9yZmNzL3B1bGwvMTA3XG4gKiBAcGFyYW0geyp9IHR5cGVcbiAqIEBwYXJhbSB7b2JqZWN0fSBwcm9wc1xuICogQHBhcmFtIHtzdHJpbmd9IGtleVxuICovXG5cbmZ1bmN0aW9uIGpzeERFVih0eXBlLCBjb25maWcsIG1heWJlS2V5LCBzb3VyY2UsIHNlbGYpIHtcbiAge1xuICAgIHZhciBwcm9wTmFtZTsgLy8gUmVzZXJ2ZWQgbmFtZXMgYXJlIGV4dHJhY3RlZFxuXG4gICAgdmFyIHByb3BzID0ge307XG4gICAgdmFyIGtleSA9IG51bGw7XG4gICAgdmFyIHJlZiA9IG51bGw7IC8vIEN1cnJlbnRseSwga2V5IGNhbiBiZSBzcHJlYWQgaW4gYXMgYSBwcm9wLiBUaGlzIGNhdXNlcyBhIHBvdGVudGlhbFxuICAgIC8vIGlzc3VlIGlmIGtleSBpcyBhbHNvIGV4cGxpY2l0bHkgZGVjbGFyZWQgKGllLiA8ZGl2IHsuLi5wcm9wc30ga2V5PVwiSGlcIiAvPlxuICAgIC8vIG9yIDxkaXYga2V5PVwiSGlcIiB7Li4ucHJvcHN9IC8+ICkuIFdlIHdhbnQgdG8gZGVwcmVjYXRlIGtleSBzcHJlYWQsXG4gICAgLy8gYnV0IGFzIGFuIGludGVybWVkaWFyeSBzdGVwLCB3ZSB3aWxsIHVzZSBqc3hERVYgZm9yIGV2ZXJ5dGhpbmcgZXhjZXB0XG4gICAgLy8gPGRpdiB7Li4ucHJvcHN9IGtleT1cIkhpXCIgLz4sIGJlY2F1c2Ugd2UgYXJlbid0IGN1cnJlbnRseSBhYmxlIHRvIHRlbGwgaWZcbiAgICAvLyBrZXkgaXMgZXhwbGljaXRseSBkZWNsYXJlZCB0byBiZSB1bmRlZmluZWQgb3Igbm90LlxuXG4gICAgaWYgKG1heWJlS2V5ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIGtleSA9ICcnICsgbWF5YmVLZXk7XG4gICAgfVxuXG4gICAgaWYgKGhhc1ZhbGlkS2V5KGNvbmZpZykpIHtcbiAgICAgIGtleSA9ICcnICsgY29uZmlnLmtleTtcbiAgICB9XG5cbiAgICBpZiAoaGFzVmFsaWRSZWYoY29uZmlnKSkge1xuICAgICAgcmVmID0gY29uZmlnLnJlZjtcbiAgICAgIHdhcm5JZlN0cmluZ1JlZkNhbm5vdEJlQXV0b0NvbnZlcnRlZChjb25maWcsIHNlbGYpO1xuICAgIH0gLy8gUmVtYWluaW5nIHByb3BlcnRpZXMgYXJlIGFkZGVkIHRvIGEgbmV3IHByb3BzIG9iamVjdFxuXG5cbiAgICBmb3IgKHByb3BOYW1lIGluIGNvbmZpZykge1xuICAgICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwoY29uZmlnLCBwcm9wTmFtZSkgJiYgIVJFU0VSVkVEX1BST1BTLmhhc093blByb3BlcnR5KHByb3BOYW1lKSkge1xuICAgICAgICBwcm9wc1twcm9wTmFtZV0gPSBjb25maWdbcHJvcE5hbWVdO1xuICAgICAgfVxuICAgIH0gLy8gUmVzb2x2ZSBkZWZhdWx0IHByb3BzXG5cblxuICAgIGlmICh0eXBlICYmIHR5cGUuZGVmYXVsdFByb3BzKSB7XG4gICAgICB2YXIgZGVmYXVsdFByb3BzID0gdHlwZS5kZWZhdWx0UHJvcHM7XG5cbiAgICAgIGZvciAocHJvcE5hbWUgaW4gZGVmYXVsdFByb3BzKSB7XG4gICAgICAgIGlmIChwcm9wc1twcm9wTmFtZV0gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgIHByb3BzW3Byb3BOYW1lXSA9IGRlZmF1bHRQcm9wc1twcm9wTmFtZV07XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoa2V5IHx8IHJlZikge1xuICAgICAgdmFyIGRpc3BsYXlOYW1lID0gdHlwZW9mIHR5cGUgPT09ICdmdW5jdGlvbicgPyB0eXBlLmRpc3BsYXlOYW1lIHx8IHR5cGUubmFtZSB8fCAnVW5rbm93bicgOiB0eXBlO1xuXG4gICAgICBpZiAoa2V5KSB7XG4gICAgICAgIGRlZmluZUtleVByb3BXYXJuaW5nR2V0dGVyKHByb3BzLCBkaXNwbGF5TmFtZSk7XG4gICAgICB9XG5cbiAgICAgIGlmIChyZWYpIHtcbiAgICAgICAgZGVmaW5lUmVmUHJvcFdhcm5pbmdHZXR0ZXIocHJvcHMsIGRpc3BsYXlOYW1lKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gUmVhY3RFbGVtZW50KHR5cGUsIGtleSwgcmVmLCBzZWxmLCBzb3VyY2UsIFJlYWN0Q3VycmVudE93bmVyLmN1cnJlbnQsIHByb3BzKTtcbiAgfVxufVxuXG52YXIgUmVhY3RDdXJyZW50T3duZXIkMSA9IFJlYWN0U2hhcmVkSW50ZXJuYWxzLlJlYWN0Q3VycmVudE93bmVyO1xudmFyIFJlYWN0RGVidWdDdXJyZW50RnJhbWUkMSA9IFJlYWN0U2hhcmVkSW50ZXJuYWxzLlJlYWN0RGVidWdDdXJyZW50RnJhbWU7XG5cbmZ1bmN0aW9uIHNldEN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50JDEoZWxlbWVudCkge1xuICB7XG4gICAgaWYgKGVsZW1lbnQpIHtcbiAgICAgIHZhciBvd25lciA9IGVsZW1lbnQuX293bmVyO1xuICAgICAgdmFyIHN0YWNrID0gZGVzY3JpYmVVbmtub3duRWxlbWVudFR5cGVGcmFtZUluREVWKGVsZW1lbnQudHlwZSwgZWxlbWVudC5fc291cmNlLCBvd25lciA/IG93bmVyLnR5cGUgOiBudWxsKTtcbiAgICAgIFJlYWN0RGVidWdDdXJyZW50RnJhbWUkMS5zZXRFeHRyYVN0YWNrRnJhbWUoc3RhY2spO1xuICAgIH0gZWxzZSB7XG4gICAgICBSZWFjdERlYnVnQ3VycmVudEZyYW1lJDEuc2V0RXh0cmFTdGFja0ZyYW1lKG51bGwpO1xuICAgIH1cbiAgfVxufVxuXG52YXIgcHJvcFR5cGVzTWlzc3BlbGxXYXJuaW5nU2hvd247XG5cbntcbiAgcHJvcFR5cGVzTWlzc3BlbGxXYXJuaW5nU2hvd24gPSBmYWxzZTtcbn1cbi8qKlxuICogVmVyaWZpZXMgdGhlIG9iamVjdCBpcyBhIFJlYWN0RWxlbWVudC5cbiAqIFNlZSBodHRwczovL3JlYWN0anMub3JnL2RvY3MvcmVhY3QtYXBpLmh0bWwjaXN2YWxpZGVsZW1lbnRcbiAqIEBwYXJhbSB7P29iamVjdH0gb2JqZWN0XG4gKiBAcmV0dXJuIHtib29sZWFufSBUcnVlIGlmIGBvYmplY3RgIGlzIGEgUmVhY3RFbGVtZW50LlxuICogQGZpbmFsXG4gKi9cblxuZnVuY3Rpb24gaXNWYWxpZEVsZW1lbnQob2JqZWN0KSB7XG4gIHtcbiAgICByZXR1cm4gdHlwZW9mIG9iamVjdCA9PT0gJ29iamVjdCcgJiYgb2JqZWN0ICE9PSBudWxsICYmIG9iamVjdC4kJHR5cGVvZiA9PT0gUkVBQ1RfRUxFTUVOVF9UWVBFO1xuICB9XG59XG5cbmZ1bmN0aW9uIGdldERlY2xhcmF0aW9uRXJyb3JBZGRlbmR1bSgpIHtcbiAge1xuICAgIGlmIChSZWFjdEN1cnJlbnRPd25lciQxLmN1cnJlbnQpIHtcbiAgICAgIHZhciBuYW1lID0gZ2V0Q29tcG9uZW50TmFtZShSZWFjdEN1cnJlbnRPd25lciQxLmN1cnJlbnQudHlwZSk7XG5cbiAgICAgIGlmIChuYW1lKSB7XG4gICAgICAgIHJldHVybiAnXFxuXFxuQ2hlY2sgdGhlIHJlbmRlciBtZXRob2Qgb2YgYCcgKyBuYW1lICsgJ2AuJztcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gJyc7XG4gIH1cbn1cblxuZnVuY3Rpb24gZ2V0U291cmNlSW5mb0Vycm9yQWRkZW5kdW0oc291cmNlKSB7XG4gIHtcbiAgICBpZiAoc291cmNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHZhciBmaWxlTmFtZSA9IHNvdXJjZS5maWxlTmFtZS5yZXBsYWNlKC9eLipbXFxcXFxcL10vLCAnJyk7XG4gICAgICB2YXIgbGluZU51bWJlciA9IHNvdXJjZS5saW5lTnVtYmVyO1xuICAgICAgcmV0dXJuICdcXG5cXG5DaGVjayB5b3VyIGNvZGUgYXQgJyArIGZpbGVOYW1lICsgJzonICsgbGluZU51bWJlciArICcuJztcbiAgICB9XG5cbiAgICByZXR1cm4gJyc7XG4gIH1cbn1cbi8qKlxuICogV2FybiBpZiB0aGVyZSdzIG5vIGtleSBleHBsaWNpdGx5IHNldCBvbiBkeW5hbWljIGFycmF5cyBvZiBjaGlsZHJlbiBvclxuICogb2JqZWN0IGtleXMgYXJlIG5vdCB2YWxpZC4gVGhpcyBhbGxvd3MgdXMgdG8ga2VlcCB0cmFjayBvZiBjaGlsZHJlbiBiZXR3ZWVuXG4gKiB1cGRhdGVzLlxuICovXG5cblxudmFyIG93bmVySGFzS2V5VXNlV2FybmluZyA9IHt9O1xuXG5mdW5jdGlvbiBnZXRDdXJyZW50Q29tcG9uZW50RXJyb3JJbmZvKHBhcmVudFR5cGUpIHtcbiAge1xuICAgIHZhciBpbmZvID0gZ2V0RGVjbGFyYXRpb25FcnJvckFkZGVuZHVtKCk7XG5cbiAgICBpZiAoIWluZm8pIHtcbiAgICAgIHZhciBwYXJlbnROYW1lID0gdHlwZW9mIHBhcmVudFR5cGUgPT09ICdzdHJpbmcnID8gcGFyZW50VHlwZSA6IHBhcmVudFR5cGUuZGlzcGxheU5hbWUgfHwgcGFyZW50VHlwZS5uYW1lO1xuXG4gICAgICBpZiAocGFyZW50TmFtZSkge1xuICAgICAgICBpbmZvID0gXCJcXG5cXG5DaGVjayB0aGUgdG9wLWxldmVsIHJlbmRlciBjYWxsIHVzaW5nIDxcIiArIHBhcmVudE5hbWUgKyBcIj4uXCI7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGluZm87XG4gIH1cbn1cbi8qKlxuICogV2FybiBpZiB0aGUgZWxlbWVudCBkb2Vzbid0IGhhdmUgYW4gZXhwbGljaXQga2V5IGFzc2lnbmVkIHRvIGl0LlxuICogVGhpcyBlbGVtZW50IGlzIGluIGFuIGFycmF5LiBUaGUgYXJyYXkgY291bGQgZ3JvdyBhbmQgc2hyaW5rIG9yIGJlXG4gKiByZW9yZGVyZWQuIEFsbCBjaGlsZHJlbiB0aGF0IGhhdmVuJ3QgYWxyZWFkeSBiZWVuIHZhbGlkYXRlZCBhcmUgcmVxdWlyZWQgdG9cbiAqIGhhdmUgYSBcImtleVwiIHByb3BlcnR5IGFzc2lnbmVkIHRvIGl0LiBFcnJvciBzdGF0dXNlcyBhcmUgY2FjaGVkIHNvIGEgd2FybmluZ1xuICogd2lsbCBvbmx5IGJlIHNob3duIG9uY2UuXG4gKlxuICogQGludGVybmFsXG4gKiBAcGFyYW0ge1JlYWN0RWxlbWVudH0gZWxlbWVudCBFbGVtZW50IHRoYXQgcmVxdWlyZXMgYSBrZXkuXG4gKiBAcGFyYW0geyp9IHBhcmVudFR5cGUgZWxlbWVudCdzIHBhcmVudCdzIHR5cGUuXG4gKi9cblxuXG5mdW5jdGlvbiB2YWxpZGF0ZUV4cGxpY2l0S2V5KGVsZW1lbnQsIHBhcmVudFR5cGUpIHtcbiAge1xuICAgIGlmICghZWxlbWVudC5fc3RvcmUgfHwgZWxlbWVudC5fc3RvcmUudmFsaWRhdGVkIHx8IGVsZW1lbnQua2V5ICE9IG51bGwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBlbGVtZW50Ll9zdG9yZS52YWxpZGF0ZWQgPSB0cnVlO1xuICAgIHZhciBjdXJyZW50Q29tcG9uZW50RXJyb3JJbmZvID0gZ2V0Q3VycmVudENvbXBvbmVudEVycm9ySW5mbyhwYXJlbnRUeXBlKTtcblxuICAgIGlmIChvd25lckhhc0tleVVzZVdhcm5pbmdbY3VycmVudENvbXBvbmVudEVycm9ySW5mb10pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBvd25lckhhc0tleVVzZVdhcm5pbmdbY3VycmVudENvbXBvbmVudEVycm9ySW5mb10gPSB0cnVlOyAvLyBVc3VhbGx5IHRoZSBjdXJyZW50IG93bmVyIGlzIHRoZSBvZmZlbmRlciwgYnV0IGlmIGl0IGFjY2VwdHMgY2hpbGRyZW4gYXMgYVxuICAgIC8vIHByb3BlcnR5LCBpdCBtYXkgYmUgdGhlIGNyZWF0b3Igb2YgdGhlIGNoaWxkIHRoYXQncyByZXNwb25zaWJsZSBmb3JcbiAgICAvLyBhc3NpZ25pbmcgaXQgYSBrZXkuXG5cbiAgICB2YXIgY2hpbGRPd25lciA9ICcnO1xuXG4gICAgaWYgKGVsZW1lbnQgJiYgZWxlbWVudC5fb3duZXIgJiYgZWxlbWVudC5fb3duZXIgIT09IFJlYWN0Q3VycmVudE93bmVyJDEuY3VycmVudCkge1xuICAgICAgLy8gR2l2ZSB0aGUgY29tcG9uZW50IHRoYXQgb3JpZ2luYWxseSBjcmVhdGVkIHRoaXMgY2hpbGQuXG4gICAgICBjaGlsZE93bmVyID0gXCIgSXQgd2FzIHBhc3NlZCBhIGNoaWxkIGZyb20gXCIgKyBnZXRDb21wb25lbnROYW1lKGVsZW1lbnQuX293bmVyLnR5cGUpICsgXCIuXCI7XG4gICAgfVxuXG4gICAgc2V0Q3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQkMShlbGVtZW50KTtcblxuICAgIGVycm9yKCdFYWNoIGNoaWxkIGluIGEgbGlzdCBzaG91bGQgaGF2ZSBhIHVuaXF1ZSBcImtleVwiIHByb3AuJyArICclcyVzIFNlZSBodHRwczovL3JlYWN0anMub3JnL2xpbmsvd2FybmluZy1rZXlzIGZvciBtb3JlIGluZm9ybWF0aW9uLicsIGN1cnJlbnRDb21wb25lbnRFcnJvckluZm8sIGNoaWxkT3duZXIpO1xuXG4gICAgc2V0Q3VycmVudGx5VmFsaWRhdGluZ0VsZW1lbnQkMShudWxsKTtcbiAgfVxufVxuLyoqXG4gKiBFbnN1cmUgdGhhdCBldmVyeSBlbGVtZW50IGVpdGhlciBpcyBwYXNzZWQgaW4gYSBzdGF0aWMgbG9jYXRpb24sIGluIGFuXG4gKiBhcnJheSB3aXRoIGFuIGV4cGxpY2l0IGtleXMgcHJvcGVydHkgZGVmaW5lZCwgb3IgaW4gYW4gb2JqZWN0IGxpdGVyYWxcbiAqIHdpdGggdmFsaWQga2V5IHByb3BlcnR5LlxuICpcbiAqIEBpbnRlcm5hbFxuICogQHBhcmFtIHtSZWFjdE5vZGV9IG5vZGUgU3RhdGljYWxseSBwYXNzZWQgY2hpbGQgb2YgYW55IHR5cGUuXG4gKiBAcGFyYW0geyp9IHBhcmVudFR5cGUgbm9kZSdzIHBhcmVudCdzIHR5cGUuXG4gKi9cblxuXG5mdW5jdGlvbiB2YWxpZGF0ZUNoaWxkS2V5cyhub2RlLCBwYXJlbnRUeXBlKSB7XG4gIHtcbiAgICBpZiAodHlwZW9mIG5vZGUgIT09ICdvYmplY3QnKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKEFycmF5LmlzQXJyYXkobm9kZSkpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbm9kZS5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgY2hpbGQgPSBub2RlW2ldO1xuXG4gICAgICAgIGlmIChpc1ZhbGlkRWxlbWVudChjaGlsZCkpIHtcbiAgICAgICAgICB2YWxpZGF0ZUV4cGxpY2l0S2V5KGNoaWxkLCBwYXJlbnRUeXBlKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoaXNWYWxpZEVsZW1lbnQobm9kZSkpIHtcbiAgICAgIC8vIFRoaXMgZWxlbWVudCB3YXMgcGFzc2VkIGluIGEgdmFsaWQgbG9jYXRpb24uXG4gICAgICBpZiAobm9kZS5fc3RvcmUpIHtcbiAgICAgICAgbm9kZS5fc3RvcmUudmFsaWRhdGVkID0gdHJ1ZTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG5vZGUpIHtcbiAgICAgIHZhciBpdGVyYXRvckZuID0gZ2V0SXRlcmF0b3JGbihub2RlKTtcblxuICAgICAgaWYgKHR5cGVvZiBpdGVyYXRvckZuID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIC8vIEVudHJ5IGl0ZXJhdG9ycyB1c2VkIHRvIHByb3ZpZGUgaW1wbGljaXQga2V5cyxcbiAgICAgICAgLy8gYnV0IG5vdyB3ZSBwcmludCBhIHNlcGFyYXRlIHdhcm5pbmcgZm9yIHRoZW0gbGF0ZXIuXG4gICAgICAgIGlmIChpdGVyYXRvckZuICE9PSBub2RlLmVudHJpZXMpIHtcbiAgICAgICAgICB2YXIgaXRlcmF0b3IgPSBpdGVyYXRvckZuLmNhbGwobm9kZSk7XG4gICAgICAgICAgdmFyIHN0ZXA7XG5cbiAgICAgICAgICB3aGlsZSAoIShzdGVwID0gaXRlcmF0b3IubmV4dCgpKS5kb25lKSB7XG4gICAgICAgICAgICBpZiAoaXNWYWxpZEVsZW1lbnQoc3RlcC52YWx1ZSkpIHtcbiAgICAgICAgICAgICAgdmFsaWRhdGVFeHBsaWNpdEtleShzdGVwLnZhbHVlLCBwYXJlbnRUeXBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbi8qKlxuICogR2l2ZW4gYW4gZWxlbWVudCwgdmFsaWRhdGUgdGhhdCBpdHMgcHJvcHMgZm9sbG93IHRoZSBwcm9wVHlwZXMgZGVmaW5pdGlvbixcbiAqIHByb3ZpZGVkIGJ5IHRoZSB0eXBlLlxuICpcbiAqIEBwYXJhbSB7UmVhY3RFbGVtZW50fSBlbGVtZW50XG4gKi9cblxuXG5mdW5jdGlvbiB2YWxpZGF0ZVByb3BUeXBlcyhlbGVtZW50KSB7XG4gIHtcbiAgICB2YXIgdHlwZSA9IGVsZW1lbnQudHlwZTtcblxuICAgIGlmICh0eXBlID09PSBudWxsIHx8IHR5cGUgPT09IHVuZGVmaW5lZCB8fCB0eXBlb2YgdHlwZSA9PT0gJ3N0cmluZycpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgcHJvcFR5cGVzO1xuXG4gICAgaWYgKHR5cGVvZiB0eXBlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBwcm9wVHlwZXMgPSB0eXBlLnByb3BUeXBlcztcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiB0eXBlID09PSAnb2JqZWN0JyAmJiAodHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfRk9SV0FSRF9SRUZfVFlQRSB8fCAvLyBOb3RlOiBNZW1vIG9ubHkgY2hlY2tzIG91dGVyIHByb3BzIGhlcmUuXG4gICAgLy8gSW5uZXIgcHJvcHMgYXJlIGNoZWNrZWQgaW4gdGhlIHJlY29uY2lsZXIuXG4gICAgdHlwZS4kJHR5cGVvZiA9PT0gUkVBQ1RfTUVNT19UWVBFKSkge1xuICAgICAgcHJvcFR5cGVzID0gdHlwZS5wcm9wVHlwZXM7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAocHJvcFR5cGVzKSB7XG4gICAgICAvLyBJbnRlbnRpb25hbGx5IGluc2lkZSB0byBhdm9pZCB0cmlnZ2VyaW5nIGxhenkgaW5pdGlhbGl6ZXJzOlxuICAgICAgdmFyIG5hbWUgPSBnZXRDb21wb25lbnROYW1lKHR5cGUpO1xuICAgICAgY2hlY2tQcm9wVHlwZXMocHJvcFR5cGVzLCBlbGVtZW50LnByb3BzLCAncHJvcCcsIG5hbWUsIGVsZW1lbnQpO1xuICAgIH0gZWxzZSBpZiAodHlwZS5Qcm9wVHlwZXMgIT09IHVuZGVmaW5lZCAmJiAhcHJvcFR5cGVzTWlzc3BlbGxXYXJuaW5nU2hvd24pIHtcbiAgICAgIHByb3BUeXBlc01pc3NwZWxsV2FybmluZ1Nob3duID0gdHJ1ZTsgLy8gSW50ZW50aW9uYWxseSBpbnNpZGUgdG8gYXZvaWQgdHJpZ2dlcmluZyBsYXp5IGluaXRpYWxpemVyczpcblxuICAgICAgdmFyIF9uYW1lID0gZ2V0Q29tcG9uZW50TmFtZSh0eXBlKTtcblxuICAgICAgZXJyb3IoJ0NvbXBvbmVudCAlcyBkZWNsYXJlZCBgUHJvcFR5cGVzYCBpbnN0ZWFkIG9mIGBwcm9wVHlwZXNgLiBEaWQgeW91IG1pc3NwZWxsIHRoZSBwcm9wZXJ0eSBhc3NpZ25tZW50PycsIF9uYW1lIHx8ICdVbmtub3duJyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiB0eXBlLmdldERlZmF1bHRQcm9wcyA9PT0gJ2Z1bmN0aW9uJyAmJiAhdHlwZS5nZXREZWZhdWx0UHJvcHMuaXNSZWFjdENsYXNzQXBwcm92ZWQpIHtcbiAgICAgIGVycm9yKCdnZXREZWZhdWx0UHJvcHMgaXMgb25seSB1c2VkIG9uIGNsYXNzaWMgUmVhY3QuY3JlYXRlQ2xhc3MgJyArICdkZWZpbml0aW9ucy4gVXNlIGEgc3RhdGljIHByb3BlcnR5IG5hbWVkIGBkZWZhdWx0UHJvcHNgIGluc3RlYWQuJyk7XG4gICAgfVxuICB9XG59XG4vKipcbiAqIEdpdmVuIGEgZnJhZ21lbnQsIHZhbGlkYXRlIHRoYXQgaXQgY2FuIG9ubHkgYmUgcHJvdmlkZWQgd2l0aCBmcmFnbWVudCBwcm9wc1xuICogQHBhcmFtIHtSZWFjdEVsZW1lbnR9IGZyYWdtZW50XG4gKi9cblxuXG5mdW5jdGlvbiB2YWxpZGF0ZUZyYWdtZW50UHJvcHMoZnJhZ21lbnQpIHtcbiAge1xuICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoZnJhZ21lbnQucHJvcHMpO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIga2V5ID0ga2V5c1tpXTtcblxuICAgICAgaWYgKGtleSAhPT0gJ2NoaWxkcmVuJyAmJiBrZXkgIT09ICdrZXknKSB7XG4gICAgICAgIHNldEN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50JDEoZnJhZ21lbnQpO1xuXG4gICAgICAgIGVycm9yKCdJbnZhbGlkIHByb3AgYCVzYCBzdXBwbGllZCB0byBgUmVhY3QuRnJhZ21lbnRgLiAnICsgJ1JlYWN0LkZyYWdtZW50IGNhbiBvbmx5IGhhdmUgYGtleWAgYW5kIGBjaGlsZHJlbmAgcHJvcHMuJywga2V5KTtcblxuICAgICAgICBzZXRDdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudCQxKG51bGwpO1xuICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoZnJhZ21lbnQucmVmICE9PSBudWxsKSB7XG4gICAgICBzZXRDdXJyZW50bHlWYWxpZGF0aW5nRWxlbWVudCQxKGZyYWdtZW50KTtcblxuICAgICAgZXJyb3IoJ0ludmFsaWQgYXR0cmlidXRlIGByZWZgIHN1cHBsaWVkIHRvIGBSZWFjdC5GcmFnbWVudGAuJyk7XG5cbiAgICAgIHNldEN1cnJlbnRseVZhbGlkYXRpbmdFbGVtZW50JDEobnVsbCk7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGpzeFdpdGhWYWxpZGF0aW9uKHR5cGUsIHByb3BzLCBrZXksIGlzU3RhdGljQ2hpbGRyZW4sIHNvdXJjZSwgc2VsZikge1xuICB7XG4gICAgdmFyIHZhbGlkVHlwZSA9IGlzVmFsaWRFbGVtZW50VHlwZSh0eXBlKTsgLy8gV2Ugd2FybiBpbiB0aGlzIGNhc2UgYnV0IGRvbid0IHRocm93LiBXZSBleHBlY3QgdGhlIGVsZW1lbnQgY3JlYXRpb24gdG9cbiAgICAvLyBzdWNjZWVkIGFuZCB0aGVyZSB3aWxsIGxpa2VseSBiZSBlcnJvcnMgaW4gcmVuZGVyLlxuXG4gICAgaWYgKCF2YWxpZFR5cGUpIHtcbiAgICAgIHZhciBpbmZvID0gJyc7XG5cbiAgICAgIGlmICh0eXBlID09PSB1bmRlZmluZWQgfHwgdHlwZW9mIHR5cGUgPT09ICdvYmplY3QnICYmIHR5cGUgIT09IG51bGwgJiYgT2JqZWN0LmtleXModHlwZSkubGVuZ3RoID09PSAwKSB7XG4gICAgICAgIGluZm8gKz0gJyBZb3UgbGlrZWx5IGZvcmdvdCB0byBleHBvcnQgeW91ciBjb21wb25lbnQgZnJvbSB0aGUgZmlsZSAnICsgXCJpdCdzIGRlZmluZWQgaW4sIG9yIHlvdSBtaWdodCBoYXZlIG1peGVkIHVwIGRlZmF1bHQgYW5kIG5hbWVkIGltcG9ydHMuXCI7XG4gICAgICB9XG5cbiAgICAgIHZhciBzb3VyY2VJbmZvID0gZ2V0U291cmNlSW5mb0Vycm9yQWRkZW5kdW0oc291cmNlKTtcblxuICAgICAgaWYgKHNvdXJjZUluZm8pIHtcbiAgICAgICAgaW5mbyArPSBzb3VyY2VJbmZvO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaW5mbyArPSBnZXREZWNsYXJhdGlvbkVycm9yQWRkZW5kdW0oKTtcbiAgICAgIH1cblxuICAgICAgdmFyIHR5cGVTdHJpbmc7XG5cbiAgICAgIGlmICh0eXBlID09PSBudWxsKSB7XG4gICAgICAgIHR5cGVTdHJpbmcgPSAnbnVsbCc7XG4gICAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkodHlwZSkpIHtcbiAgICAgICAgdHlwZVN0cmluZyA9ICdhcnJheSc7XG4gICAgICB9IGVsc2UgaWYgKHR5cGUgIT09IHVuZGVmaW5lZCAmJiB0eXBlLiQkdHlwZW9mID09PSBSRUFDVF9FTEVNRU5UX1RZUEUpIHtcbiAgICAgICAgdHlwZVN0cmluZyA9IFwiPFwiICsgKGdldENvbXBvbmVudE5hbWUodHlwZS50eXBlKSB8fCAnVW5rbm93bicpICsgXCIgLz5cIjtcbiAgICAgICAgaW5mbyA9ICcgRGlkIHlvdSBhY2NpZGVudGFsbHkgZXhwb3J0IGEgSlNYIGxpdGVyYWwgaW5zdGVhZCBvZiBhIGNvbXBvbmVudD8nO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdHlwZVN0cmluZyA9IHR5cGVvZiB0eXBlO1xuICAgICAgfVxuXG4gICAgICBlcnJvcignUmVhY3QuanN4OiB0eXBlIGlzIGludmFsaWQgLS0gZXhwZWN0ZWQgYSBzdHJpbmcgKGZvciAnICsgJ2J1aWx0LWluIGNvbXBvbmVudHMpIG9yIGEgY2xhc3MvZnVuY3Rpb24gKGZvciBjb21wb3NpdGUgJyArICdjb21wb25lbnRzKSBidXQgZ290OiAlcy4lcycsIHR5cGVTdHJpbmcsIGluZm8pO1xuICAgIH1cblxuICAgIHZhciBlbGVtZW50ID0ganN4REVWKHR5cGUsIHByb3BzLCBrZXksIHNvdXJjZSwgc2VsZik7IC8vIFRoZSByZXN1bHQgY2FuIGJlIG51bGxpc2ggaWYgYSBtb2NrIG9yIGEgY3VzdG9tIGZ1bmN0aW9uIGlzIHVzZWQuXG4gICAgLy8gVE9ETzogRHJvcCB0aGlzIHdoZW4gdGhlc2UgYXJlIG5vIGxvbmdlciBhbGxvd2VkIGFzIHRoZSB0eXBlIGFyZ3VtZW50LlxuXG4gICAgaWYgKGVsZW1lbnQgPT0gbnVsbCkge1xuICAgICAgcmV0dXJuIGVsZW1lbnQ7XG4gICAgfSAvLyBTa2lwIGtleSB3YXJuaW5nIGlmIHRoZSB0eXBlIGlzbid0IHZhbGlkIHNpbmNlIG91ciBrZXkgdmFsaWRhdGlvbiBsb2dpY1xuICAgIC8vIGRvZXNuJ3QgZXhwZWN0IGEgbm9uLXN0cmluZy9mdW5jdGlvbiB0eXBlIGFuZCBjYW4gdGhyb3cgY29uZnVzaW5nIGVycm9ycy5cbiAgICAvLyBXZSBkb24ndCB3YW50IGV4Y2VwdGlvbiBiZWhhdmlvciB0byBkaWZmZXIgYmV0d2VlbiBkZXYgYW5kIHByb2QuXG4gICAgLy8gKFJlbmRlcmluZyB3aWxsIHRocm93IHdpdGggYSBoZWxwZnVsIG1lc3NhZ2UgYW5kIGFzIHNvb24gYXMgdGhlIHR5cGUgaXNcbiAgICAvLyBmaXhlZCwgdGhlIGtleSB3YXJuaW5ncyB3aWxsIGFwcGVhci4pXG5cblxuICAgIGlmICh2YWxpZFR5cGUpIHtcbiAgICAgIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuO1xuXG4gICAgICBpZiAoY2hpbGRyZW4gIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBpZiAoaXNTdGF0aWNDaGlsZHJlbikge1xuICAgICAgICAgIGlmIChBcnJheS5pc0FycmF5KGNoaWxkcmVuKSkge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjaGlsZHJlbi5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICB2YWxpZGF0ZUNoaWxkS2V5cyhjaGlsZHJlbltpXSwgdHlwZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmIChPYmplY3QuZnJlZXplKSB7XG4gICAgICAgICAgICAgIE9iamVjdC5mcmVlemUoY2hpbGRyZW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBlcnJvcignUmVhY3QuanN4OiBTdGF0aWMgY2hpbGRyZW4gc2hvdWxkIGFsd2F5cyBiZSBhbiBhcnJheS4gJyArICdZb3UgYXJlIGxpa2VseSBleHBsaWNpdGx5IGNhbGxpbmcgUmVhY3QuanN4cyBvciBSZWFjdC5qc3hERVYuICcgKyAnVXNlIHRoZSBCYWJlbCB0cmFuc2Zvcm0gaW5zdGVhZC4nKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdmFsaWRhdGVDaGlsZEtleXMoY2hpbGRyZW4sIHR5cGUpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHR5cGUgPT09IGV4cG9ydHMuRnJhZ21lbnQpIHtcbiAgICAgIHZhbGlkYXRlRnJhZ21lbnRQcm9wcyhlbGVtZW50KTtcbiAgICB9IGVsc2Uge1xuICAgICAgdmFsaWRhdGVQcm9wVHlwZXMoZWxlbWVudCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGVsZW1lbnQ7XG4gIH1cbn0gLy8gVGhlc2UgdHdvIGZ1bmN0aW9ucyBleGlzdCB0byBzdGlsbCBnZXQgY2hpbGQgd2FybmluZ3MgaW4gZGV2XG4vLyBldmVuIHdpdGggdGhlIHByb2QgdHJhbnNmb3JtLiBUaGlzIG1lYW5zIHRoYXQganN4REVWIGlzIHB1cmVseVxuLy8gb3B0LWluIGJlaGF2aW9yIGZvciBiZXR0ZXIgbWVzc2FnZXMgYnV0IHRoYXQgd2Ugd29uJ3Qgc3RvcFxuLy8gZ2l2aW5nIHlvdSB3YXJuaW5ncyBpZiB5b3UgdXNlIHByb2R1Y3Rpb24gYXBpcy5cblxuZnVuY3Rpb24ganN4V2l0aFZhbGlkYXRpb25TdGF0aWModHlwZSwgcHJvcHMsIGtleSkge1xuICB7XG4gICAgcmV0dXJuIGpzeFdpdGhWYWxpZGF0aW9uKHR5cGUsIHByb3BzLCBrZXksIHRydWUpO1xuICB9XG59XG5mdW5jdGlvbiBqc3hXaXRoVmFsaWRhdGlvbkR5bmFtaWModHlwZSwgcHJvcHMsIGtleSkge1xuICB7XG4gICAgcmV0dXJuIGpzeFdpdGhWYWxpZGF0aW9uKHR5cGUsIHByb3BzLCBrZXksIGZhbHNlKTtcbiAgfVxufVxuXG52YXIganN4ID0gIGpzeFdpdGhWYWxpZGF0aW9uRHluYW1pYyA7IC8vIHdlIG1heSB3YW50IHRvIHNwZWNpYWwgY2FzZSBqc3hzIGludGVybmFsbHkgdG8gdGFrZSBhZHZhbnRhZ2Ugb2Ygc3RhdGljIGNoaWxkcmVuLlxuLy8gZm9yIG5vdyB3ZSBjYW4gc2hpcCBpZGVudGljYWwgcHJvZCBmdW5jdGlvbnNcblxudmFyIGpzeHMgPSAganN4V2l0aFZhbGlkYXRpb25TdGF0aWMgO1xuXG5leHBvcnRzLmpzeCA9IGpzeDtcbmV4cG9ydHMuanN4cyA9IGpzeHM7XG4gIH0pKCk7XG59XG4iLCIndXNlIHN0cmljdCc7XG5cbmlmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ3Byb2R1Y3Rpb24nKSB7XG4gIG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9janMvcmVhY3QtanN4LXJ1bnRpbWUucHJvZHVjdGlvbi5taW4uanMnKTtcbn0gZWxzZSB7XG4gIG1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi9janMvcmVhY3QtanN4LXJ1bnRpbWUuZGV2ZWxvcG1lbnQuanMnKTtcbn1cbiIsImV4cG9ydCAqIGZyb20gJ0ByZWR1eC1zYWdhL2NvcmUvZWZmZWN0cyc7XG4iLCJpbXBvcnQgX29iamVjdFNwcmVhZCBmcm9tICdAYmFiZWwvcnVudGltZS9oZWxwZXJzL2VzbS9vYmplY3RTcHJlYWQyJztcblxuLyoqXG4gKiBBZGFwdGVkIGZyb20gUmVhY3Q6IGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWFjdC9ibG9iL21hc3Rlci9wYWNrYWdlcy9zaGFyZWQvZm9ybWF0UHJvZEVycm9yTWVzc2FnZS5qc1xuICpcbiAqIERvIG5vdCByZXF1aXJlIHRoaXMgbW9kdWxlIGRpcmVjdGx5ISBVc2Ugbm9ybWFsIHRocm93IGVycm9yIGNhbGxzLiBUaGVzZSBtZXNzYWdlcyB3aWxsIGJlIHJlcGxhY2VkIHdpdGggZXJyb3IgY29kZXNcbiAqIGR1cmluZyBidWlsZC5cbiAqIEBwYXJhbSB7bnVtYmVyfSBjb2RlXG4gKi9cbmZ1bmN0aW9uIGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoY29kZSkge1xuICByZXR1cm4gXCJNaW5pZmllZCBSZWR1eCBlcnJvciAjXCIgKyBjb2RlICsgXCI7IHZpc2l0IGh0dHBzOi8vcmVkdXguanMub3JnL0Vycm9ycz9jb2RlPVwiICsgY29kZSArIFwiIGZvciB0aGUgZnVsbCBtZXNzYWdlIG9yIFwiICsgJ3VzZSB0aGUgbm9uLW1pbmlmaWVkIGRldiBlbnZpcm9ubWVudCBmb3IgZnVsbCBlcnJvcnMuICc7XG59XG5cbi8vIElubGluZWQgdmVyc2lvbiBvZiB0aGUgYHN5bWJvbC1vYnNlcnZhYmxlYCBwb2x5ZmlsbFxudmFyICQkb2JzZXJ2YWJsZSA9IChmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0eXBlb2YgU3ltYm9sID09PSAnZnVuY3Rpb24nICYmIFN5bWJvbC5vYnNlcnZhYmxlIHx8ICdAQG9ic2VydmFibGUnO1xufSkoKTtcblxuLyoqXG4gKiBUaGVzZSBhcmUgcHJpdmF0ZSBhY3Rpb24gdHlwZXMgcmVzZXJ2ZWQgYnkgUmVkdXguXG4gKiBGb3IgYW55IHVua25vd24gYWN0aW9ucywgeW91IG11c3QgcmV0dXJuIHRoZSBjdXJyZW50IHN0YXRlLlxuICogSWYgdGhlIGN1cnJlbnQgc3RhdGUgaXMgdW5kZWZpbmVkLCB5b3UgbXVzdCByZXR1cm4gdGhlIGluaXRpYWwgc3RhdGUuXG4gKiBEbyBub3QgcmVmZXJlbmNlIHRoZXNlIGFjdGlvbiB0eXBlcyBkaXJlY3RseSBpbiB5b3VyIGNvZGUuXG4gKi9cbnZhciByYW5kb21TdHJpbmcgPSBmdW5jdGlvbiByYW5kb21TdHJpbmcoKSB7XG4gIHJldHVybiBNYXRoLnJhbmRvbSgpLnRvU3RyaW5nKDM2KS5zdWJzdHJpbmcoNykuc3BsaXQoJycpLmpvaW4oJy4nKTtcbn07XG5cbnZhciBBY3Rpb25UeXBlcyA9IHtcbiAgSU5JVDogXCJAQHJlZHV4L0lOSVRcIiArIHJhbmRvbVN0cmluZygpLFxuICBSRVBMQUNFOiBcIkBAcmVkdXgvUkVQTEFDRVwiICsgcmFuZG9tU3RyaW5nKCksXG4gIFBST0JFX1VOS05PV05fQUNUSU9OOiBmdW5jdGlvbiBQUk9CRV9VTktOT1dOX0FDVElPTigpIHtcbiAgICByZXR1cm4gXCJAQHJlZHV4L1BST0JFX1VOS05PV05fQUNUSU9OXCIgKyByYW5kb21TdHJpbmcoKTtcbiAgfVxufTtcblxuLyoqXG4gKiBAcGFyYW0ge2FueX0gb2JqIFRoZSBvYmplY3QgdG8gaW5zcGVjdC5cbiAqIEByZXR1cm5zIHtib29sZWFufSBUcnVlIGlmIHRoZSBhcmd1bWVudCBhcHBlYXJzIHRvIGJlIGEgcGxhaW4gb2JqZWN0LlxuICovXG5mdW5jdGlvbiBpc1BsYWluT2JqZWN0KG9iaikge1xuICBpZiAodHlwZW9mIG9iaiAhPT0gJ29iamVjdCcgfHwgb2JqID09PSBudWxsKSByZXR1cm4gZmFsc2U7XG4gIHZhciBwcm90byA9IG9iajtcblxuICB3aGlsZSAoT2JqZWN0LmdldFByb3RvdHlwZU9mKHByb3RvKSAhPT0gbnVsbCkge1xuICAgIHByb3RvID0gT2JqZWN0LmdldFByb3RvdHlwZU9mKHByb3RvKTtcbiAgfVxuXG4gIHJldHVybiBPYmplY3QuZ2V0UHJvdG90eXBlT2Yob2JqKSA9PT0gcHJvdG87XG59XG5cbi8vIElubGluZWQgLyBzaG9ydGVuZWQgdmVyc2lvbiBvZiBga2luZE9mYCBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9qb25zY2hsaW5rZXJ0L2tpbmQtb2ZcbmZ1bmN0aW9uIG1pbmlLaW5kT2YodmFsKSB7XG4gIGlmICh2YWwgPT09IHZvaWQgMCkgcmV0dXJuICd1bmRlZmluZWQnO1xuICBpZiAodmFsID09PSBudWxsKSByZXR1cm4gJ251bGwnO1xuICB2YXIgdHlwZSA9IHR5cGVvZiB2YWw7XG5cbiAgc3dpdGNoICh0eXBlKSB7XG4gICAgY2FzZSAnYm9vbGVhbic6XG4gICAgY2FzZSAnc3RyaW5nJzpcbiAgICBjYXNlICdudW1iZXInOlxuICAgIGNhc2UgJ3N5bWJvbCc6XG4gICAgY2FzZSAnZnVuY3Rpb24nOlxuICAgICAge1xuICAgICAgICByZXR1cm4gdHlwZTtcbiAgICAgIH1cbiAgfVxuXG4gIGlmIChBcnJheS5pc0FycmF5KHZhbCkpIHJldHVybiAnYXJyYXknO1xuICBpZiAoaXNEYXRlKHZhbCkpIHJldHVybiAnZGF0ZSc7XG4gIGlmIChpc0Vycm9yKHZhbCkpIHJldHVybiAnZXJyb3InO1xuICB2YXIgY29uc3RydWN0b3JOYW1lID0gY3Rvck5hbWUodmFsKTtcblxuICBzd2l0Y2ggKGNvbnN0cnVjdG9yTmFtZSkge1xuICAgIGNhc2UgJ1N5bWJvbCc6XG4gICAgY2FzZSAnUHJvbWlzZSc6XG4gICAgY2FzZSAnV2Vha01hcCc6XG4gICAgY2FzZSAnV2Vha1NldCc6XG4gICAgY2FzZSAnTWFwJzpcbiAgICBjYXNlICdTZXQnOlxuICAgICAgcmV0dXJuIGNvbnN0cnVjdG9yTmFtZTtcbiAgfSAvLyBvdGhlclxuXG5cbiAgcmV0dXJuIHR5cGUuc2xpY2UoOCwgLTEpLnRvTG93ZXJDYXNlKCkucmVwbGFjZSgvXFxzL2csICcnKTtcbn1cblxuZnVuY3Rpb24gY3Rvck5hbWUodmFsKSB7XG4gIHJldHVybiB0eXBlb2YgdmFsLmNvbnN0cnVjdG9yID09PSAnZnVuY3Rpb24nID8gdmFsLmNvbnN0cnVjdG9yLm5hbWUgOiBudWxsO1xufVxuXG5mdW5jdGlvbiBpc0Vycm9yKHZhbCkge1xuICByZXR1cm4gdmFsIGluc3RhbmNlb2YgRXJyb3IgfHwgdHlwZW9mIHZhbC5tZXNzYWdlID09PSAnc3RyaW5nJyAmJiB2YWwuY29uc3RydWN0b3IgJiYgdHlwZW9mIHZhbC5jb25zdHJ1Y3Rvci5zdGFja1RyYWNlTGltaXQgPT09ICdudW1iZXInO1xufVxuXG5mdW5jdGlvbiBpc0RhdGUodmFsKSB7XG4gIGlmICh2YWwgaW5zdGFuY2VvZiBEYXRlKSByZXR1cm4gdHJ1ZTtcbiAgcmV0dXJuIHR5cGVvZiB2YWwudG9EYXRlU3RyaW5nID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiB2YWwuZ2V0RGF0ZSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgdmFsLnNldERhdGUgPT09ICdmdW5jdGlvbic7XG59XG5cbmZ1bmN0aW9uIGtpbmRPZih2YWwpIHtcbiAgdmFyIHR5cGVPZlZhbCA9IHR5cGVvZiB2YWw7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICB0eXBlT2ZWYWwgPSBtaW5pS2luZE9mKHZhbCk7XG4gIH1cblxuICByZXR1cm4gdHlwZU9mVmFsO1xufVxuXG4vKipcbiAqIENyZWF0ZXMgYSBSZWR1eCBzdG9yZSB0aGF0IGhvbGRzIHRoZSBzdGF0ZSB0cmVlLlxuICogVGhlIG9ubHkgd2F5IHRvIGNoYW5nZSB0aGUgZGF0YSBpbiB0aGUgc3RvcmUgaXMgdG8gY2FsbCBgZGlzcGF0Y2goKWAgb24gaXQuXG4gKlxuICogVGhlcmUgc2hvdWxkIG9ubHkgYmUgYSBzaW5nbGUgc3RvcmUgaW4geW91ciBhcHAuIFRvIHNwZWNpZnkgaG93IGRpZmZlcmVudFxuICogcGFydHMgb2YgdGhlIHN0YXRlIHRyZWUgcmVzcG9uZCB0byBhY3Rpb25zLCB5b3UgbWF5IGNvbWJpbmUgc2V2ZXJhbCByZWR1Y2Vyc1xuICogaW50byBhIHNpbmdsZSByZWR1Y2VyIGZ1bmN0aW9uIGJ5IHVzaW5nIGBjb21iaW5lUmVkdWNlcnNgLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IHJlZHVjZXIgQSBmdW5jdGlvbiB0aGF0IHJldHVybnMgdGhlIG5leHQgc3RhdGUgdHJlZSwgZ2l2ZW5cbiAqIHRoZSBjdXJyZW50IHN0YXRlIHRyZWUgYW5kIHRoZSBhY3Rpb24gdG8gaGFuZGxlLlxuICpcbiAqIEBwYXJhbSB7YW55fSBbcHJlbG9hZGVkU3RhdGVdIFRoZSBpbml0aWFsIHN0YXRlLiBZb3UgbWF5IG9wdGlvbmFsbHkgc3BlY2lmeSBpdFxuICogdG8gaHlkcmF0ZSB0aGUgc3RhdGUgZnJvbSB0aGUgc2VydmVyIGluIHVuaXZlcnNhbCBhcHBzLCBvciB0byByZXN0b3JlIGFcbiAqIHByZXZpb3VzbHkgc2VyaWFsaXplZCB1c2VyIHNlc3Npb24uXG4gKiBJZiB5b3UgdXNlIGBjb21iaW5lUmVkdWNlcnNgIHRvIHByb2R1Y2UgdGhlIHJvb3QgcmVkdWNlciBmdW5jdGlvbiwgdGhpcyBtdXN0IGJlXG4gKiBhbiBvYmplY3Qgd2l0aCB0aGUgc2FtZSBzaGFwZSBhcyBgY29tYmluZVJlZHVjZXJzYCBrZXlzLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtlbmhhbmNlcl0gVGhlIHN0b3JlIGVuaGFuY2VyLiBZb3UgbWF5IG9wdGlvbmFsbHkgc3BlY2lmeSBpdFxuICogdG8gZW5oYW5jZSB0aGUgc3RvcmUgd2l0aCB0aGlyZC1wYXJ0eSBjYXBhYmlsaXRpZXMgc3VjaCBhcyBtaWRkbGV3YXJlLFxuICogdGltZSB0cmF2ZWwsIHBlcnNpc3RlbmNlLCBldGMuIFRoZSBvbmx5IHN0b3JlIGVuaGFuY2VyIHRoYXQgc2hpcHMgd2l0aCBSZWR1eFxuICogaXMgYGFwcGx5TWlkZGxld2FyZSgpYC5cbiAqXG4gKiBAcmV0dXJucyB7U3RvcmV9IEEgUmVkdXggc3RvcmUgdGhhdCBsZXRzIHlvdSByZWFkIHRoZSBzdGF0ZSwgZGlzcGF0Y2ggYWN0aW9uc1xuICogYW5kIHN1YnNjcmliZSB0byBjaGFuZ2VzLlxuICovXG5cbmZ1bmN0aW9uIGNyZWF0ZVN0b3JlKHJlZHVjZXIsIHByZWxvYWRlZFN0YXRlLCBlbmhhbmNlcikge1xuICB2YXIgX3JlZjI7XG5cbiAgaWYgKHR5cGVvZiBwcmVsb2FkZWRTdGF0ZSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgZW5oYW5jZXIgPT09ICdmdW5jdGlvbicgfHwgdHlwZW9mIGVuaGFuY2VyID09PSAnZnVuY3Rpb24nICYmIHR5cGVvZiBhcmd1bWVudHNbM10gPT09ICdmdW5jdGlvbicpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSgwKSA6ICdJdCBsb29rcyBsaWtlIHlvdSBhcmUgcGFzc2luZyBzZXZlcmFsIHN0b3JlIGVuaGFuY2VycyB0byAnICsgJ2NyZWF0ZVN0b3JlKCkuIFRoaXMgaXMgbm90IHN1cHBvcnRlZC4gSW5zdGVhZCwgY29tcG9zZSB0aGVtICcgKyAndG9nZXRoZXIgdG8gYSBzaW5nbGUgZnVuY3Rpb24uIFNlZSBodHRwczovL3JlZHV4LmpzLm9yZy90dXRvcmlhbHMvZnVuZGFtZW50YWxzL3BhcnQtNC1zdG9yZSNjcmVhdGluZy1hLXN0b3JlLXdpdGgtZW5oYW5jZXJzIGZvciBhbiBleGFtcGxlLicpO1xuICB9XG5cbiAgaWYgKHR5cGVvZiBwcmVsb2FkZWRTdGF0ZSA9PT0gJ2Z1bmN0aW9uJyAmJiB0eXBlb2YgZW5oYW5jZXIgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgZW5oYW5jZXIgPSBwcmVsb2FkZWRTdGF0ZTtcbiAgICBwcmVsb2FkZWRTdGF0ZSA9IHVuZGVmaW5lZDtcbiAgfVxuXG4gIGlmICh0eXBlb2YgZW5oYW5jZXIgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgaWYgKHR5cGVvZiBlbmhhbmNlciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoMSkgOiBcIkV4cGVjdGVkIHRoZSBlbmhhbmNlciB0byBiZSBhIGZ1bmN0aW9uLiBJbnN0ZWFkLCByZWNlaXZlZDogJ1wiICsga2luZE9mKGVuaGFuY2VyKSArIFwiJ1wiKTtcbiAgICB9XG5cbiAgICByZXR1cm4gZW5oYW5jZXIoY3JlYXRlU3RvcmUpKHJlZHVjZXIsIHByZWxvYWRlZFN0YXRlKTtcbiAgfVxuXG4gIGlmICh0eXBlb2YgcmVkdWNlciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHRocm93IG5ldyBFcnJvcihwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gXCJwcm9kdWN0aW9uXCIgPyBmb3JtYXRQcm9kRXJyb3JNZXNzYWdlKDIpIDogXCJFeHBlY3RlZCB0aGUgcm9vdCByZWR1Y2VyIHRvIGJlIGEgZnVuY3Rpb24uIEluc3RlYWQsIHJlY2VpdmVkOiAnXCIgKyBraW5kT2YocmVkdWNlcikgKyBcIidcIik7XG4gIH1cblxuICB2YXIgY3VycmVudFJlZHVjZXIgPSByZWR1Y2VyO1xuICB2YXIgY3VycmVudFN0YXRlID0gcHJlbG9hZGVkU3RhdGU7XG4gIHZhciBjdXJyZW50TGlzdGVuZXJzID0gW107XG4gIHZhciBuZXh0TGlzdGVuZXJzID0gY3VycmVudExpc3RlbmVycztcbiAgdmFyIGlzRGlzcGF0Y2hpbmcgPSBmYWxzZTtcbiAgLyoqXG4gICAqIFRoaXMgbWFrZXMgYSBzaGFsbG93IGNvcHkgb2YgY3VycmVudExpc3RlbmVycyBzbyB3ZSBjYW4gdXNlXG4gICAqIG5leHRMaXN0ZW5lcnMgYXMgYSB0ZW1wb3JhcnkgbGlzdCB3aGlsZSBkaXNwYXRjaGluZy5cbiAgICpcbiAgICogVGhpcyBwcmV2ZW50cyBhbnkgYnVncyBhcm91bmQgY29uc3VtZXJzIGNhbGxpbmdcbiAgICogc3Vic2NyaWJlL3Vuc3Vic2NyaWJlIGluIHRoZSBtaWRkbGUgb2YgYSBkaXNwYXRjaC5cbiAgICovXG5cbiAgZnVuY3Rpb24gZW5zdXJlQ2FuTXV0YXRlTmV4dExpc3RlbmVycygpIHtcbiAgICBpZiAobmV4dExpc3RlbmVycyA9PT0gY3VycmVudExpc3RlbmVycykge1xuICAgICAgbmV4dExpc3RlbmVycyA9IGN1cnJlbnRMaXN0ZW5lcnMuc2xpY2UoKTtcbiAgICB9XG4gIH1cbiAgLyoqXG4gICAqIFJlYWRzIHRoZSBzdGF0ZSB0cmVlIG1hbmFnZWQgYnkgdGhlIHN0b3JlLlxuICAgKlxuICAgKiBAcmV0dXJucyB7YW55fSBUaGUgY3VycmVudCBzdGF0ZSB0cmVlIG9mIHlvdXIgYXBwbGljYXRpb24uXG4gICAqL1xuXG5cbiAgZnVuY3Rpb24gZ2V0U3RhdGUoKSB7XG4gICAgaWYgKGlzRGlzcGF0Y2hpbmcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gXCJwcm9kdWN0aW9uXCIgPyBmb3JtYXRQcm9kRXJyb3JNZXNzYWdlKDMpIDogJ1lvdSBtYXkgbm90IGNhbGwgc3RvcmUuZ2V0U3RhdGUoKSB3aGlsZSB0aGUgcmVkdWNlciBpcyBleGVjdXRpbmcuICcgKyAnVGhlIHJlZHVjZXIgaGFzIGFscmVhZHkgcmVjZWl2ZWQgdGhlIHN0YXRlIGFzIGFuIGFyZ3VtZW50LiAnICsgJ1Bhc3MgaXQgZG93biBmcm9tIHRoZSB0b3AgcmVkdWNlciBpbnN0ZWFkIG9mIHJlYWRpbmcgaXQgZnJvbSB0aGUgc3RvcmUuJyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGN1cnJlbnRTdGF0ZTtcbiAgfVxuICAvKipcbiAgICogQWRkcyBhIGNoYW5nZSBsaXN0ZW5lci4gSXQgd2lsbCBiZSBjYWxsZWQgYW55IHRpbWUgYW4gYWN0aW9uIGlzIGRpc3BhdGNoZWQsXG4gICAqIGFuZCBzb21lIHBhcnQgb2YgdGhlIHN0YXRlIHRyZWUgbWF5IHBvdGVudGlhbGx5IGhhdmUgY2hhbmdlZC4gWW91IG1heSB0aGVuXG4gICAqIGNhbGwgYGdldFN0YXRlKClgIHRvIHJlYWQgdGhlIGN1cnJlbnQgc3RhdGUgdHJlZSBpbnNpZGUgdGhlIGNhbGxiYWNrLlxuICAgKlxuICAgKiBZb3UgbWF5IGNhbGwgYGRpc3BhdGNoKClgIGZyb20gYSBjaGFuZ2UgbGlzdGVuZXIsIHdpdGggdGhlIGZvbGxvd2luZ1xuICAgKiBjYXZlYXRzOlxuICAgKlxuICAgKiAxLiBUaGUgc3Vic2NyaXB0aW9ucyBhcmUgc25hcHNob3R0ZWQganVzdCBiZWZvcmUgZXZlcnkgYGRpc3BhdGNoKClgIGNhbGwuXG4gICAqIElmIHlvdSBzdWJzY3JpYmUgb3IgdW5zdWJzY3JpYmUgd2hpbGUgdGhlIGxpc3RlbmVycyBhcmUgYmVpbmcgaW52b2tlZCwgdGhpc1xuICAgKiB3aWxsIG5vdCBoYXZlIGFueSBlZmZlY3Qgb24gdGhlIGBkaXNwYXRjaCgpYCB0aGF0IGlzIGN1cnJlbnRseSBpbiBwcm9ncmVzcy5cbiAgICogSG93ZXZlciwgdGhlIG5leHQgYGRpc3BhdGNoKClgIGNhbGwsIHdoZXRoZXIgbmVzdGVkIG9yIG5vdCwgd2lsbCB1c2UgYSBtb3JlXG4gICAqIHJlY2VudCBzbmFwc2hvdCBvZiB0aGUgc3Vic2NyaXB0aW9uIGxpc3QuXG4gICAqXG4gICAqIDIuIFRoZSBsaXN0ZW5lciBzaG91bGQgbm90IGV4cGVjdCB0byBzZWUgYWxsIHN0YXRlIGNoYW5nZXMsIGFzIHRoZSBzdGF0ZVxuICAgKiBtaWdodCBoYXZlIGJlZW4gdXBkYXRlZCBtdWx0aXBsZSB0aW1lcyBkdXJpbmcgYSBuZXN0ZWQgYGRpc3BhdGNoKClgIGJlZm9yZVxuICAgKiB0aGUgbGlzdGVuZXIgaXMgY2FsbGVkLiBJdCBpcywgaG93ZXZlciwgZ3VhcmFudGVlZCB0aGF0IGFsbCBzdWJzY3JpYmVyc1xuICAgKiByZWdpc3RlcmVkIGJlZm9yZSB0aGUgYGRpc3BhdGNoKClgIHN0YXJ0ZWQgd2lsbCBiZSBjYWxsZWQgd2l0aCB0aGUgbGF0ZXN0XG4gICAqIHN0YXRlIGJ5IHRoZSB0aW1lIGl0IGV4aXRzLlxuICAgKlxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBsaXN0ZW5lciBBIGNhbGxiYWNrIHRvIGJlIGludm9rZWQgb24gZXZlcnkgZGlzcGF0Y2guXG4gICAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSBmdW5jdGlvbiB0byByZW1vdmUgdGhpcyBjaGFuZ2UgbGlzdGVuZXIuXG4gICAqL1xuXG5cbiAgZnVuY3Rpb24gc3Vic2NyaWJlKGxpc3RlbmVyKSB7XG4gICAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoNCkgOiBcIkV4cGVjdGVkIHRoZSBsaXN0ZW5lciB0byBiZSBhIGZ1bmN0aW9uLiBJbnN0ZWFkLCByZWNlaXZlZDogJ1wiICsga2luZE9mKGxpc3RlbmVyKSArIFwiJ1wiKTtcbiAgICB9XG5cbiAgICBpZiAoaXNEaXNwYXRjaGluZykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoNSkgOiAnWW91IG1heSBub3QgY2FsbCBzdG9yZS5zdWJzY3JpYmUoKSB3aGlsZSB0aGUgcmVkdWNlciBpcyBleGVjdXRpbmcuICcgKyAnSWYgeW91IHdvdWxkIGxpa2UgdG8gYmUgbm90aWZpZWQgYWZ0ZXIgdGhlIHN0b3JlIGhhcyBiZWVuIHVwZGF0ZWQsIHN1YnNjcmliZSBmcm9tIGEgJyArICdjb21wb25lbnQgYW5kIGludm9rZSBzdG9yZS5nZXRTdGF0ZSgpIGluIHRoZSBjYWxsYmFjayB0byBhY2Nlc3MgdGhlIGxhdGVzdCBzdGF0ZS4gJyArICdTZWUgaHR0cHM6Ly9yZWR1eC5qcy5vcmcvYXBpL3N0b3JlI3N1YnNjcmliZWxpc3RlbmVyIGZvciBtb3JlIGRldGFpbHMuJyk7XG4gICAgfVxuXG4gICAgdmFyIGlzU3Vic2NyaWJlZCA9IHRydWU7XG4gICAgZW5zdXJlQ2FuTXV0YXRlTmV4dExpc3RlbmVycygpO1xuICAgIG5leHRMaXN0ZW5lcnMucHVzaChsaXN0ZW5lcik7XG4gICAgcmV0dXJuIGZ1bmN0aW9uIHVuc3Vic2NyaWJlKCkge1xuICAgICAgaWYgKCFpc1N1YnNjcmliZWQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoaXNEaXNwYXRjaGluZykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSg2KSA6ICdZb3UgbWF5IG5vdCB1bnN1YnNjcmliZSBmcm9tIGEgc3RvcmUgbGlzdGVuZXIgd2hpbGUgdGhlIHJlZHVjZXIgaXMgZXhlY3V0aW5nLiAnICsgJ1NlZSBodHRwczovL3JlZHV4LmpzLm9yZy9hcGkvc3RvcmUjc3Vic2NyaWJlbGlzdGVuZXIgZm9yIG1vcmUgZGV0YWlscy4nKTtcbiAgICAgIH1cblxuICAgICAgaXNTdWJzY3JpYmVkID0gZmFsc2U7XG4gICAgICBlbnN1cmVDYW5NdXRhdGVOZXh0TGlzdGVuZXJzKCk7XG4gICAgICB2YXIgaW5kZXggPSBuZXh0TGlzdGVuZXJzLmluZGV4T2YobGlzdGVuZXIpO1xuICAgICAgbmV4dExpc3RlbmVycy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgICAgY3VycmVudExpc3RlbmVycyA9IG51bGw7XG4gICAgfTtcbiAgfVxuICAvKipcbiAgICogRGlzcGF0Y2hlcyBhbiBhY3Rpb24uIEl0IGlzIHRoZSBvbmx5IHdheSB0byB0cmlnZ2VyIGEgc3RhdGUgY2hhbmdlLlxuICAgKlxuICAgKiBUaGUgYHJlZHVjZXJgIGZ1bmN0aW9uLCB1c2VkIHRvIGNyZWF0ZSB0aGUgc3RvcmUsIHdpbGwgYmUgY2FsbGVkIHdpdGggdGhlXG4gICAqIGN1cnJlbnQgc3RhdGUgdHJlZSBhbmQgdGhlIGdpdmVuIGBhY3Rpb25gLiBJdHMgcmV0dXJuIHZhbHVlIHdpbGxcbiAgICogYmUgY29uc2lkZXJlZCB0aGUgKipuZXh0Kiogc3RhdGUgb2YgdGhlIHRyZWUsIGFuZCB0aGUgY2hhbmdlIGxpc3RlbmVyc1xuICAgKiB3aWxsIGJlIG5vdGlmaWVkLlxuICAgKlxuICAgKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvbmx5IHN1cHBvcnRzIHBsYWluIG9iamVjdCBhY3Rpb25zLiBJZiB5b3Ugd2FudCB0b1xuICAgKiBkaXNwYXRjaCBhIFByb21pc2UsIGFuIE9ic2VydmFibGUsIGEgdGh1bmssIG9yIHNvbWV0aGluZyBlbHNlLCB5b3UgbmVlZCB0b1xuICAgKiB3cmFwIHlvdXIgc3RvcmUgY3JlYXRpbmcgZnVuY3Rpb24gaW50byB0aGUgY29ycmVzcG9uZGluZyBtaWRkbGV3YXJlLiBGb3JcbiAgICogZXhhbXBsZSwgc2VlIHRoZSBkb2N1bWVudGF0aW9uIGZvciB0aGUgYHJlZHV4LXRodW5rYCBwYWNrYWdlLiBFdmVuIHRoZVxuICAgKiBtaWRkbGV3YXJlIHdpbGwgZXZlbnR1YWxseSBkaXNwYXRjaCBwbGFpbiBvYmplY3QgYWN0aW9ucyB1c2luZyB0aGlzIG1ldGhvZC5cbiAgICpcbiAgICogQHBhcmFtIHtPYmplY3R9IGFjdGlvbiBBIHBsYWluIG9iamVjdCByZXByZXNlbnRpbmcg4oCcd2hhdCBjaGFuZ2Vk4oCdLiBJdCBpc1xuICAgKiBhIGdvb2QgaWRlYSB0byBrZWVwIGFjdGlvbnMgc2VyaWFsaXphYmxlIHNvIHlvdSBjYW4gcmVjb3JkIGFuZCByZXBsYXkgdXNlclxuICAgKiBzZXNzaW9ucywgb3IgdXNlIHRoZSB0aW1lIHRyYXZlbGxpbmcgYHJlZHV4LWRldnRvb2xzYC4gQW4gYWN0aW9uIG11c3QgaGF2ZVxuICAgKiBhIGB0eXBlYCBwcm9wZXJ0eSB3aGljaCBtYXkgbm90IGJlIGB1bmRlZmluZWRgLiBJdCBpcyBhIGdvb2QgaWRlYSB0byB1c2VcbiAgICogc3RyaW5nIGNvbnN0YW50cyBmb3IgYWN0aW9uIHR5cGVzLlxuICAgKlxuICAgKiBAcmV0dXJucyB7T2JqZWN0fSBGb3IgY29udmVuaWVuY2UsIHRoZSBzYW1lIGFjdGlvbiBvYmplY3QgeW91IGRpc3BhdGNoZWQuXG4gICAqXG4gICAqIE5vdGUgdGhhdCwgaWYgeW91IHVzZSBhIGN1c3RvbSBtaWRkbGV3YXJlLCBpdCBtYXkgd3JhcCBgZGlzcGF0Y2goKWAgdG9cbiAgICogcmV0dXJuIHNvbWV0aGluZyBlbHNlIChmb3IgZXhhbXBsZSwgYSBQcm9taXNlIHlvdSBjYW4gYXdhaXQpLlxuICAgKi9cblxuXG4gIGZ1bmN0aW9uIGRpc3BhdGNoKGFjdGlvbikge1xuICAgIGlmICghaXNQbGFpbk9iamVjdChhY3Rpb24pKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSg3KSA6IFwiQWN0aW9ucyBtdXN0IGJlIHBsYWluIG9iamVjdHMuIEluc3RlYWQsIHRoZSBhY3R1YWwgdHlwZSB3YXM6ICdcIiArIGtpbmRPZihhY3Rpb24pICsgXCInLiBZb3UgbWF5IG5lZWQgdG8gYWRkIG1pZGRsZXdhcmUgdG8geW91ciBzdG9yZSBzZXR1cCB0byBoYW5kbGUgZGlzcGF0Y2hpbmcgb3RoZXIgdmFsdWVzLCBzdWNoIGFzICdyZWR1eC10aHVuaycgdG8gaGFuZGxlIGRpc3BhdGNoaW5nIGZ1bmN0aW9ucy4gU2VlIGh0dHBzOi8vcmVkdXguanMub3JnL3R1dG9yaWFscy9mdW5kYW1lbnRhbHMvcGFydC00LXN0b3JlI21pZGRsZXdhcmUgYW5kIGh0dHBzOi8vcmVkdXguanMub3JnL3R1dG9yaWFscy9mdW5kYW1lbnRhbHMvcGFydC02LWFzeW5jLWxvZ2ljI3VzaW5nLXRoZS1yZWR1eC10aHVuay1taWRkbGV3YXJlIGZvciBleGFtcGxlcy5cIik7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBhY3Rpb24udHlwZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcihwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gXCJwcm9kdWN0aW9uXCIgPyBmb3JtYXRQcm9kRXJyb3JNZXNzYWdlKDgpIDogJ0FjdGlvbnMgbWF5IG5vdCBoYXZlIGFuIHVuZGVmaW5lZCBcInR5cGVcIiBwcm9wZXJ0eS4gWW91IG1heSBoYXZlIG1pc3NwZWxsZWQgYW4gYWN0aW9uIHR5cGUgc3RyaW5nIGNvbnN0YW50LicpO1xuICAgIH1cblxuICAgIGlmIChpc0Rpc3BhdGNoaW5nKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSg5KSA6ICdSZWR1Y2VycyBtYXkgbm90IGRpc3BhdGNoIGFjdGlvbnMuJyk7XG4gICAgfVxuXG4gICAgdHJ5IHtcbiAgICAgIGlzRGlzcGF0Y2hpbmcgPSB0cnVlO1xuICAgICAgY3VycmVudFN0YXRlID0gY3VycmVudFJlZHVjZXIoY3VycmVudFN0YXRlLCBhY3Rpb24pO1xuICAgIH0gZmluYWxseSB7XG4gICAgICBpc0Rpc3BhdGNoaW5nID0gZmFsc2U7XG4gICAgfVxuXG4gICAgdmFyIGxpc3RlbmVycyA9IGN1cnJlbnRMaXN0ZW5lcnMgPSBuZXh0TGlzdGVuZXJzO1xuXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0ZW5lcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgIHZhciBsaXN0ZW5lciA9IGxpc3RlbmVyc1tpXTtcbiAgICAgIGxpc3RlbmVyKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFjdGlvbjtcbiAgfVxuICAvKipcbiAgICogUmVwbGFjZXMgdGhlIHJlZHVjZXIgY3VycmVudGx5IHVzZWQgYnkgdGhlIHN0b3JlIHRvIGNhbGN1bGF0ZSB0aGUgc3RhdGUuXG4gICAqXG4gICAqIFlvdSBtaWdodCBuZWVkIHRoaXMgaWYgeW91ciBhcHAgaW1wbGVtZW50cyBjb2RlIHNwbGl0dGluZyBhbmQgeW91IHdhbnQgdG9cbiAgICogbG9hZCBzb21lIG9mIHRoZSByZWR1Y2VycyBkeW5hbWljYWxseS4gWW91IG1pZ2h0IGFsc28gbmVlZCB0aGlzIGlmIHlvdVxuICAgKiBpbXBsZW1lbnQgYSBob3QgcmVsb2FkaW5nIG1lY2hhbmlzbSBmb3IgUmVkdXguXG4gICAqXG4gICAqIEBwYXJhbSB7RnVuY3Rpb259IG5leHRSZWR1Y2VyIFRoZSByZWR1Y2VyIGZvciB0aGUgc3RvcmUgdG8gdXNlIGluc3RlYWQuXG4gICAqIEByZXR1cm5zIHt2b2lkfVxuICAgKi9cblxuXG4gIGZ1bmN0aW9uIHJlcGxhY2VSZWR1Y2VyKG5leHRSZWR1Y2VyKSB7XG4gICAgaWYgKHR5cGVvZiBuZXh0UmVkdWNlciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoMTApIDogXCJFeHBlY3RlZCB0aGUgbmV4dFJlZHVjZXIgdG8gYmUgYSBmdW5jdGlvbi4gSW5zdGVhZCwgcmVjZWl2ZWQ6ICdcIiArIGtpbmRPZihuZXh0UmVkdWNlcikpO1xuICAgIH1cblxuICAgIGN1cnJlbnRSZWR1Y2VyID0gbmV4dFJlZHVjZXI7IC8vIFRoaXMgYWN0aW9uIGhhcyBhIHNpbWlsaWFyIGVmZmVjdCB0byBBY3Rpb25UeXBlcy5JTklULlxuICAgIC8vIEFueSByZWR1Y2VycyB0aGF0IGV4aXN0ZWQgaW4gYm90aCB0aGUgbmV3IGFuZCBvbGQgcm9vdFJlZHVjZXJcbiAgICAvLyB3aWxsIHJlY2VpdmUgdGhlIHByZXZpb3VzIHN0YXRlLiBUaGlzIGVmZmVjdGl2ZWx5IHBvcHVsYXRlc1xuICAgIC8vIHRoZSBuZXcgc3RhdGUgdHJlZSB3aXRoIGFueSByZWxldmFudCBkYXRhIGZyb20gdGhlIG9sZCBvbmUuXG5cbiAgICBkaXNwYXRjaCh7XG4gICAgICB0eXBlOiBBY3Rpb25UeXBlcy5SRVBMQUNFXG4gICAgfSk7XG4gIH1cbiAgLyoqXG4gICAqIEludGVyb3BlcmFiaWxpdHkgcG9pbnQgZm9yIG9ic2VydmFibGUvcmVhY3RpdmUgbGlicmFyaWVzLlxuICAgKiBAcmV0dXJucyB7b2JzZXJ2YWJsZX0gQSBtaW5pbWFsIG9ic2VydmFibGUgb2Ygc3RhdGUgY2hhbmdlcy5cbiAgICogRm9yIG1vcmUgaW5mb3JtYXRpb24sIHNlZSB0aGUgb2JzZXJ2YWJsZSBwcm9wb3NhbDpcbiAgICogaHR0cHM6Ly9naXRodWIuY29tL3RjMzkvcHJvcG9zYWwtb2JzZXJ2YWJsZVxuICAgKi9cblxuXG4gIGZ1bmN0aW9uIG9ic2VydmFibGUoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgb3V0ZXJTdWJzY3JpYmUgPSBzdWJzY3JpYmU7XG4gICAgcmV0dXJuIF9yZWYgPSB7XG4gICAgICAvKipcbiAgICAgICAqIFRoZSBtaW5pbWFsIG9ic2VydmFibGUgc3Vic2NyaXB0aW9uIG1ldGhvZC5cbiAgICAgICAqIEBwYXJhbSB7T2JqZWN0fSBvYnNlcnZlciBBbnkgb2JqZWN0IHRoYXQgY2FuIGJlIHVzZWQgYXMgYW4gb2JzZXJ2ZXIuXG4gICAgICAgKiBUaGUgb2JzZXJ2ZXIgb2JqZWN0IHNob3VsZCBoYXZlIGEgYG5leHRgIG1ldGhvZC5cbiAgICAgICAqIEByZXR1cm5zIHtzdWJzY3JpcHRpb259IEFuIG9iamVjdCB3aXRoIGFuIGB1bnN1YnNjcmliZWAgbWV0aG9kIHRoYXQgY2FuXG4gICAgICAgKiBiZSB1c2VkIHRvIHVuc3Vic2NyaWJlIHRoZSBvYnNlcnZhYmxlIGZyb20gdGhlIHN0b3JlLCBhbmQgcHJldmVudCBmdXJ0aGVyXG4gICAgICAgKiBlbWlzc2lvbiBvZiB2YWx1ZXMgZnJvbSB0aGUgb2JzZXJ2YWJsZS5cbiAgICAgICAqL1xuICAgICAgc3Vic2NyaWJlOiBmdW5jdGlvbiBzdWJzY3JpYmUob2JzZXJ2ZXIpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBvYnNlcnZlciAhPT0gJ29iamVjdCcgfHwgb2JzZXJ2ZXIgPT09IG51bGwpIHtcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSgxMSkgOiBcIkV4cGVjdGVkIHRoZSBvYnNlcnZlciB0byBiZSBhbiBvYmplY3QuIEluc3RlYWQsIHJlY2VpdmVkOiAnXCIgKyBraW5kT2Yob2JzZXJ2ZXIpICsgXCInXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gb2JzZXJ2ZVN0YXRlKCkge1xuICAgICAgICAgIGlmIChvYnNlcnZlci5uZXh0KSB7XG4gICAgICAgICAgICBvYnNlcnZlci5uZXh0KGdldFN0YXRlKCkpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIG9ic2VydmVTdGF0ZSgpO1xuICAgICAgICB2YXIgdW5zdWJzY3JpYmUgPSBvdXRlclN1YnNjcmliZShvYnNlcnZlU3RhdGUpO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIHVuc3Vic2NyaWJlOiB1bnN1YnNjcmliZVxuICAgICAgICB9O1xuICAgICAgfVxuICAgIH0sIF9yZWZbJCRvYnNlcnZhYmxlXSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB0aGlzO1xuICAgIH0sIF9yZWY7XG4gIH0gLy8gV2hlbiBhIHN0b3JlIGlzIGNyZWF0ZWQsIGFuIFwiSU5JVFwiIGFjdGlvbiBpcyBkaXNwYXRjaGVkIHNvIHRoYXQgZXZlcnlcbiAgLy8gcmVkdWNlciByZXR1cm5zIHRoZWlyIGluaXRpYWwgc3RhdGUuIFRoaXMgZWZmZWN0aXZlbHkgcG9wdWxhdGVzXG4gIC8vIHRoZSBpbml0aWFsIHN0YXRlIHRyZWUuXG5cblxuICBkaXNwYXRjaCh7XG4gICAgdHlwZTogQWN0aW9uVHlwZXMuSU5JVFxuICB9KTtcbiAgcmV0dXJuIF9yZWYyID0ge1xuICAgIGRpc3BhdGNoOiBkaXNwYXRjaCxcbiAgICBzdWJzY3JpYmU6IHN1YnNjcmliZSxcbiAgICBnZXRTdGF0ZTogZ2V0U3RhdGUsXG4gICAgcmVwbGFjZVJlZHVjZXI6IHJlcGxhY2VSZWR1Y2VyXG4gIH0sIF9yZWYyWyQkb2JzZXJ2YWJsZV0gPSBvYnNlcnZhYmxlLCBfcmVmMjtcbn1cblxuLyoqXG4gKiBQcmludHMgYSB3YXJuaW5nIGluIHRoZSBjb25zb2xlIGlmIGl0IGV4aXN0cy5cbiAqXG4gKiBAcGFyYW0ge1N0cmluZ30gbWVzc2FnZSBUaGUgd2FybmluZyBtZXNzYWdlLlxuICogQHJldHVybnMge3ZvaWR9XG4gKi9cbmZ1bmN0aW9uIHdhcm5pbmcobWVzc2FnZSkge1xuICAvKiBlc2xpbnQtZGlzYWJsZSBuby1jb25zb2xlICovXG4gIGlmICh0eXBlb2YgY29uc29sZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGNvbnNvbGUuZXJyb3IgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjb25zb2xlLmVycm9yKG1lc3NhZ2UpO1xuICB9XG4gIC8qIGVzbGludC1lbmFibGUgbm8tY29uc29sZSAqL1xuXG5cbiAgdHJ5IHtcbiAgICAvLyBUaGlzIGVycm9yIHdhcyB0aHJvd24gYXMgYSBjb252ZW5pZW5jZSBzbyB0aGF0IGlmIHlvdSBlbmFibGVcbiAgICAvLyBcImJyZWFrIG9uIGFsbCBleGNlcHRpb25zXCIgaW4geW91ciBjb25zb2xlLFxuICAgIC8vIGl0IHdvdWxkIHBhdXNlIHRoZSBleGVjdXRpb24gYXQgdGhpcyBsaW5lLlxuICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgfSBjYXRjaCAoZSkge30gLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1lbXB0eVxuXG59XG5cbmZ1bmN0aW9uIGdldFVuZXhwZWN0ZWRTdGF0ZVNoYXBlV2FybmluZ01lc3NhZ2UoaW5wdXRTdGF0ZSwgcmVkdWNlcnMsIGFjdGlvbiwgdW5leHBlY3RlZEtleUNhY2hlKSB7XG4gIHZhciByZWR1Y2VyS2V5cyA9IE9iamVjdC5rZXlzKHJlZHVjZXJzKTtcbiAgdmFyIGFyZ3VtZW50TmFtZSA9IGFjdGlvbiAmJiBhY3Rpb24udHlwZSA9PT0gQWN0aW9uVHlwZXMuSU5JVCA/ICdwcmVsb2FkZWRTdGF0ZSBhcmd1bWVudCBwYXNzZWQgdG8gY3JlYXRlU3RvcmUnIDogJ3ByZXZpb3VzIHN0YXRlIHJlY2VpdmVkIGJ5IHRoZSByZWR1Y2VyJztcblxuICBpZiAocmVkdWNlcktleXMubGVuZ3RoID09PSAwKSB7XG4gICAgcmV0dXJuICdTdG9yZSBkb2VzIG5vdCBoYXZlIGEgdmFsaWQgcmVkdWNlci4gTWFrZSBzdXJlIHRoZSBhcmd1bWVudCBwYXNzZWQgJyArICd0byBjb21iaW5lUmVkdWNlcnMgaXMgYW4gb2JqZWN0IHdob3NlIHZhbHVlcyBhcmUgcmVkdWNlcnMuJztcbiAgfVxuXG4gIGlmICghaXNQbGFpbk9iamVjdChpbnB1dFN0YXRlKSkge1xuICAgIHJldHVybiBcIlRoZSBcIiArIGFyZ3VtZW50TmFtZSArIFwiIGhhcyB1bmV4cGVjdGVkIHR5cGUgb2YgXFxcIlwiICsga2luZE9mKGlucHV0U3RhdGUpICsgXCJcXFwiLiBFeHBlY3RlZCBhcmd1bWVudCB0byBiZSBhbiBvYmplY3Qgd2l0aCB0aGUgZm9sbG93aW5nIFwiICsgKFwia2V5czogXFxcIlwiICsgcmVkdWNlcktleXMuam9pbignXCIsIFwiJykgKyBcIlxcXCJcIik7XG4gIH1cblxuICB2YXIgdW5leHBlY3RlZEtleXMgPSBPYmplY3Qua2V5cyhpbnB1dFN0YXRlKS5maWx0ZXIoZnVuY3Rpb24gKGtleSkge1xuICAgIHJldHVybiAhcmVkdWNlcnMuaGFzT3duUHJvcGVydHkoa2V5KSAmJiAhdW5leHBlY3RlZEtleUNhY2hlW2tleV07XG4gIH0pO1xuICB1bmV4cGVjdGVkS2V5cy5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICB1bmV4cGVjdGVkS2V5Q2FjaGVba2V5XSA9IHRydWU7XG4gIH0pO1xuICBpZiAoYWN0aW9uICYmIGFjdGlvbi50eXBlID09PSBBY3Rpb25UeXBlcy5SRVBMQUNFKSByZXR1cm47XG5cbiAgaWYgKHVuZXhwZWN0ZWRLZXlzLmxlbmd0aCA+IDApIHtcbiAgICByZXR1cm4gXCJVbmV4cGVjdGVkIFwiICsgKHVuZXhwZWN0ZWRLZXlzLmxlbmd0aCA+IDEgPyAna2V5cycgOiAna2V5JykgKyBcIiBcIiArIChcIlxcXCJcIiArIHVuZXhwZWN0ZWRLZXlzLmpvaW4oJ1wiLCBcIicpICsgXCJcXFwiIGZvdW5kIGluIFwiICsgYXJndW1lbnROYW1lICsgXCIuIFwiKSArIFwiRXhwZWN0ZWQgdG8gZmluZCBvbmUgb2YgdGhlIGtub3duIHJlZHVjZXIga2V5cyBpbnN0ZWFkOiBcIiArIChcIlxcXCJcIiArIHJlZHVjZXJLZXlzLmpvaW4oJ1wiLCBcIicpICsgXCJcXFwiLiBVbmV4cGVjdGVkIGtleXMgd2lsbCBiZSBpZ25vcmVkLlwiKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBhc3NlcnRSZWR1Y2VyU2hhcGUocmVkdWNlcnMpIHtcbiAgT2JqZWN0LmtleXMocmVkdWNlcnMpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgIHZhciByZWR1Y2VyID0gcmVkdWNlcnNba2V5XTtcbiAgICB2YXIgaW5pdGlhbFN0YXRlID0gcmVkdWNlcih1bmRlZmluZWQsIHtcbiAgICAgIHR5cGU6IEFjdGlvblR5cGVzLklOSVRcbiAgICB9KTtcblxuICAgIGlmICh0eXBlb2YgaW5pdGlhbFN0YXRlID09PSAndW5kZWZpbmVkJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoMTIpIDogXCJUaGUgc2xpY2UgcmVkdWNlciBmb3Iga2V5IFxcXCJcIiArIGtleSArIFwiXFxcIiByZXR1cm5lZCB1bmRlZmluZWQgZHVyaW5nIGluaXRpYWxpemF0aW9uLiBcIiArIFwiSWYgdGhlIHN0YXRlIHBhc3NlZCB0byB0aGUgcmVkdWNlciBpcyB1bmRlZmluZWQsIHlvdSBtdXN0IFwiICsgXCJleHBsaWNpdGx5IHJldHVybiB0aGUgaW5pdGlhbCBzdGF0ZS4gVGhlIGluaXRpYWwgc3RhdGUgbWF5IFwiICsgXCJub3QgYmUgdW5kZWZpbmVkLiBJZiB5b3UgZG9uJ3Qgd2FudCB0byBzZXQgYSB2YWx1ZSBmb3IgdGhpcyByZWR1Y2VyLCBcIiArIFwieW91IGNhbiB1c2UgbnVsbCBpbnN0ZWFkIG9mIHVuZGVmaW5lZC5cIik7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiByZWR1Y2VyKHVuZGVmaW5lZCwge1xuICAgICAgdHlwZTogQWN0aW9uVHlwZXMuUFJPQkVfVU5LTk9XTl9BQ1RJT04oKVxuICAgIH0pID09PSAndW5kZWZpbmVkJykge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoMTMpIDogXCJUaGUgc2xpY2UgcmVkdWNlciBmb3Iga2V5IFxcXCJcIiArIGtleSArIFwiXFxcIiByZXR1cm5lZCB1bmRlZmluZWQgd2hlbiBwcm9iZWQgd2l0aCBhIHJhbmRvbSB0eXBlLiBcIiArIChcIkRvbid0IHRyeSB0byBoYW5kbGUgJ1wiICsgQWN0aW9uVHlwZXMuSU5JVCArIFwiJyBvciBvdGhlciBhY3Rpb25zIGluIFxcXCJyZWR1eC8qXFxcIiBcIikgKyBcIm5hbWVzcGFjZS4gVGhleSBhcmUgY29uc2lkZXJlZCBwcml2YXRlLiBJbnN0ZWFkLCB5b3UgbXVzdCByZXR1cm4gdGhlIFwiICsgXCJjdXJyZW50IHN0YXRlIGZvciBhbnkgdW5rbm93biBhY3Rpb25zLCB1bmxlc3MgaXQgaXMgdW5kZWZpbmVkLCBcIiArIFwiaW4gd2hpY2ggY2FzZSB5b3UgbXVzdCByZXR1cm4gdGhlIGluaXRpYWwgc3RhdGUsIHJlZ2FyZGxlc3Mgb2YgdGhlIFwiICsgXCJhY3Rpb24gdHlwZS4gVGhlIGluaXRpYWwgc3RhdGUgbWF5IG5vdCBiZSB1bmRlZmluZWQsIGJ1dCBjYW4gYmUgbnVsbC5cIik7XG4gICAgfVxuICB9KTtcbn1cbi8qKlxuICogVHVybnMgYW4gb2JqZWN0IHdob3NlIHZhbHVlcyBhcmUgZGlmZmVyZW50IHJlZHVjZXIgZnVuY3Rpb25zLCBpbnRvIGEgc2luZ2xlXG4gKiByZWR1Y2VyIGZ1bmN0aW9uLiBJdCB3aWxsIGNhbGwgZXZlcnkgY2hpbGQgcmVkdWNlciwgYW5kIGdhdGhlciB0aGVpciByZXN1bHRzXG4gKiBpbnRvIGEgc2luZ2xlIHN0YXRlIG9iamVjdCwgd2hvc2Uga2V5cyBjb3JyZXNwb25kIHRvIHRoZSBrZXlzIG9mIHRoZSBwYXNzZWRcbiAqIHJlZHVjZXIgZnVuY3Rpb25zLlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSByZWR1Y2VycyBBbiBvYmplY3Qgd2hvc2UgdmFsdWVzIGNvcnJlc3BvbmQgdG8gZGlmZmVyZW50XG4gKiByZWR1Y2VyIGZ1bmN0aW9ucyB0aGF0IG5lZWQgdG8gYmUgY29tYmluZWQgaW50byBvbmUuIE9uZSBoYW5keSB3YXkgdG8gb2J0YWluXG4gKiBpdCBpcyB0byB1c2UgRVM2IGBpbXBvcnQgKiBhcyByZWR1Y2Vyc2Agc3ludGF4LiBUaGUgcmVkdWNlcnMgbWF5IG5ldmVyIHJldHVyblxuICogdW5kZWZpbmVkIGZvciBhbnkgYWN0aW9uLiBJbnN0ZWFkLCB0aGV5IHNob3VsZCByZXR1cm4gdGhlaXIgaW5pdGlhbCBzdGF0ZVxuICogaWYgdGhlIHN0YXRlIHBhc3NlZCB0byB0aGVtIHdhcyB1bmRlZmluZWQsIGFuZCB0aGUgY3VycmVudCBzdGF0ZSBmb3IgYW55XG4gKiB1bnJlY29nbml6ZWQgYWN0aW9uLlxuICpcbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSByZWR1Y2VyIGZ1bmN0aW9uIHRoYXQgaW52b2tlcyBldmVyeSByZWR1Y2VyIGluc2lkZSB0aGVcbiAqIHBhc3NlZCBvYmplY3QsIGFuZCBidWlsZHMgYSBzdGF0ZSBvYmplY3Qgd2l0aCB0aGUgc2FtZSBzaGFwZS5cbiAqL1xuXG5cbmZ1bmN0aW9uIGNvbWJpbmVSZWR1Y2VycyhyZWR1Y2Vycykge1xuICB2YXIgcmVkdWNlcktleXMgPSBPYmplY3Qua2V5cyhyZWR1Y2Vycyk7XG4gIHZhciBmaW5hbFJlZHVjZXJzID0ge307XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCByZWR1Y2VyS2V5cy5sZW5ndGg7IGkrKykge1xuICAgIHZhciBrZXkgPSByZWR1Y2VyS2V5c1tpXTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICBpZiAodHlwZW9mIHJlZHVjZXJzW2tleV0gPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHdhcm5pbmcoXCJObyByZWR1Y2VyIHByb3ZpZGVkIGZvciBrZXkgXFxcIlwiICsga2V5ICsgXCJcXFwiXCIpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGlmICh0eXBlb2YgcmVkdWNlcnNba2V5XSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgZmluYWxSZWR1Y2Vyc1trZXldID0gcmVkdWNlcnNba2V5XTtcbiAgICB9XG4gIH1cblxuICB2YXIgZmluYWxSZWR1Y2VyS2V5cyA9IE9iamVjdC5rZXlzKGZpbmFsUmVkdWNlcnMpOyAvLyBUaGlzIGlzIHVzZWQgdG8gbWFrZSBzdXJlIHdlIGRvbid0IHdhcm4gYWJvdXQgdGhlIHNhbWVcbiAgLy8ga2V5cyBtdWx0aXBsZSB0aW1lcy5cblxuICB2YXIgdW5leHBlY3RlZEtleUNhY2hlO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgdW5leHBlY3RlZEtleUNhY2hlID0ge307XG4gIH1cblxuICB2YXIgc2hhcGVBc3NlcnRpb25FcnJvcjtcblxuICB0cnkge1xuICAgIGFzc2VydFJlZHVjZXJTaGFwZShmaW5hbFJlZHVjZXJzKTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHNoYXBlQXNzZXJ0aW9uRXJyb3IgPSBlO1xuICB9XG5cbiAgcmV0dXJuIGZ1bmN0aW9uIGNvbWJpbmF0aW9uKHN0YXRlLCBhY3Rpb24pIHtcbiAgICBpZiAoc3RhdGUgPT09IHZvaWQgMCkge1xuICAgICAgc3RhdGUgPSB7fTtcbiAgICB9XG5cbiAgICBpZiAoc2hhcGVBc3NlcnRpb25FcnJvcikge1xuICAgICAgdGhyb3cgc2hhcGVBc3NlcnRpb25FcnJvcjtcbiAgICB9XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgdmFyIHdhcm5pbmdNZXNzYWdlID0gZ2V0VW5leHBlY3RlZFN0YXRlU2hhcGVXYXJuaW5nTWVzc2FnZShzdGF0ZSwgZmluYWxSZWR1Y2VycywgYWN0aW9uLCB1bmV4cGVjdGVkS2V5Q2FjaGUpO1xuXG4gICAgICBpZiAod2FybmluZ01lc3NhZ2UpIHtcbiAgICAgICAgd2FybmluZyh3YXJuaW5nTWVzc2FnZSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgdmFyIGhhc0NoYW5nZWQgPSBmYWxzZTtcbiAgICB2YXIgbmV4dFN0YXRlID0ge307XG5cbiAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgZmluYWxSZWR1Y2VyS2V5cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgIHZhciBfa2V5ID0gZmluYWxSZWR1Y2VyS2V5c1tfaV07XG4gICAgICB2YXIgcmVkdWNlciA9IGZpbmFsUmVkdWNlcnNbX2tleV07XG4gICAgICB2YXIgcHJldmlvdXNTdGF0ZUZvcktleSA9IHN0YXRlW19rZXldO1xuICAgICAgdmFyIG5leHRTdGF0ZUZvcktleSA9IHJlZHVjZXIocHJldmlvdXNTdGF0ZUZvcktleSwgYWN0aW9uKTtcblxuICAgICAgaWYgKHR5cGVvZiBuZXh0U3RhdGVGb3JLZXkgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIHZhciBhY3Rpb25UeXBlID0gYWN0aW9uICYmIGFjdGlvbi50eXBlO1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09IFwicHJvZHVjdGlvblwiID8gZm9ybWF0UHJvZEVycm9yTWVzc2FnZSgxNCkgOiBcIldoZW4gY2FsbGVkIHdpdGggYW4gYWN0aW9uIG9mIHR5cGUgXCIgKyAoYWN0aW9uVHlwZSA/IFwiXFxcIlwiICsgU3RyaW5nKGFjdGlvblR5cGUpICsgXCJcXFwiXCIgOiAnKHVua25vd24gdHlwZSknKSArIFwiLCB0aGUgc2xpY2UgcmVkdWNlciBmb3Iga2V5IFxcXCJcIiArIF9rZXkgKyBcIlxcXCIgcmV0dXJuZWQgdW5kZWZpbmVkLiBcIiArIFwiVG8gaWdub3JlIGFuIGFjdGlvbiwgeW91IG11c3QgZXhwbGljaXRseSByZXR1cm4gdGhlIHByZXZpb3VzIHN0YXRlLiBcIiArIFwiSWYgeW91IHdhbnQgdGhpcyByZWR1Y2VyIHRvIGhvbGQgbm8gdmFsdWUsIHlvdSBjYW4gcmV0dXJuIG51bGwgaW5zdGVhZCBvZiB1bmRlZmluZWQuXCIpO1xuICAgICAgfVxuXG4gICAgICBuZXh0U3RhdGVbX2tleV0gPSBuZXh0U3RhdGVGb3JLZXk7XG4gICAgICBoYXNDaGFuZ2VkID0gaGFzQ2hhbmdlZCB8fCBuZXh0U3RhdGVGb3JLZXkgIT09IHByZXZpb3VzU3RhdGVGb3JLZXk7XG4gICAgfVxuXG4gICAgaGFzQ2hhbmdlZCA9IGhhc0NoYW5nZWQgfHwgZmluYWxSZWR1Y2VyS2V5cy5sZW5ndGggIT09IE9iamVjdC5rZXlzKHN0YXRlKS5sZW5ndGg7XG4gICAgcmV0dXJuIGhhc0NoYW5nZWQgPyBuZXh0U3RhdGUgOiBzdGF0ZTtcbiAgfTtcbn1cblxuZnVuY3Rpb24gYmluZEFjdGlvbkNyZWF0b3IoYWN0aW9uQ3JlYXRvciwgZGlzcGF0Y2gpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gZGlzcGF0Y2goYWN0aW9uQ3JlYXRvci5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfTtcbn1cbi8qKlxuICogVHVybnMgYW4gb2JqZWN0IHdob3NlIHZhbHVlcyBhcmUgYWN0aW9uIGNyZWF0b3JzLCBpbnRvIGFuIG9iamVjdCB3aXRoIHRoZVxuICogc2FtZSBrZXlzLCBidXQgd2l0aCBldmVyeSBmdW5jdGlvbiB3cmFwcGVkIGludG8gYSBgZGlzcGF0Y2hgIGNhbGwgc28gdGhleVxuICogbWF5IGJlIGludm9rZWQgZGlyZWN0bHkuIFRoaXMgaXMganVzdCBhIGNvbnZlbmllbmNlIG1ldGhvZCwgYXMgeW91IGNhbiBjYWxsXG4gKiBgc3RvcmUuZGlzcGF0Y2goTXlBY3Rpb25DcmVhdG9ycy5kb1NvbWV0aGluZygpKWAgeW91cnNlbGYganVzdCBmaW5lLlxuICpcbiAqIEZvciBjb252ZW5pZW5jZSwgeW91IGNhbiBhbHNvIHBhc3MgYW4gYWN0aW9uIGNyZWF0b3IgYXMgdGhlIGZpcnN0IGFyZ3VtZW50LFxuICogYW5kIGdldCBhIGRpc3BhdGNoIHdyYXBwZWQgZnVuY3Rpb24gaW4gcmV0dXJuLlxuICpcbiAqIEBwYXJhbSB7RnVuY3Rpb258T2JqZWN0fSBhY3Rpb25DcmVhdG9ycyBBbiBvYmplY3Qgd2hvc2UgdmFsdWVzIGFyZSBhY3Rpb25cbiAqIGNyZWF0b3IgZnVuY3Rpb25zLiBPbmUgaGFuZHkgd2F5IHRvIG9idGFpbiBpdCBpcyB0byB1c2UgRVM2IGBpbXBvcnQgKiBhc2BcbiAqIHN5bnRheC4gWW91IG1heSBhbHNvIHBhc3MgYSBzaW5nbGUgZnVuY3Rpb24uXG4gKlxuICogQHBhcmFtIHtGdW5jdGlvbn0gZGlzcGF0Y2ggVGhlIGBkaXNwYXRjaGAgZnVuY3Rpb24gYXZhaWxhYmxlIG9uIHlvdXIgUmVkdXhcbiAqIHN0b3JlLlxuICpcbiAqIEByZXR1cm5zIHtGdW5jdGlvbnxPYmplY3R9IFRoZSBvYmplY3QgbWltaWNraW5nIHRoZSBvcmlnaW5hbCBvYmplY3QsIGJ1dCB3aXRoXG4gKiBldmVyeSBhY3Rpb24gY3JlYXRvciB3cmFwcGVkIGludG8gdGhlIGBkaXNwYXRjaGAgY2FsbC4gSWYgeW91IHBhc3NlZCBhXG4gKiBmdW5jdGlvbiBhcyBgYWN0aW9uQ3JlYXRvcnNgLCB0aGUgcmV0dXJuIHZhbHVlIHdpbGwgYWxzbyBiZSBhIHNpbmdsZVxuICogZnVuY3Rpb24uXG4gKi9cblxuXG5mdW5jdGlvbiBiaW5kQWN0aW9uQ3JlYXRvcnMoYWN0aW9uQ3JlYXRvcnMsIGRpc3BhdGNoKSB7XG4gIGlmICh0eXBlb2YgYWN0aW9uQ3JlYXRvcnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICByZXR1cm4gYmluZEFjdGlvbkNyZWF0b3IoYWN0aW9uQ3JlYXRvcnMsIGRpc3BhdGNoKTtcbiAgfVxuXG4gIGlmICh0eXBlb2YgYWN0aW9uQ3JlYXRvcnMgIT09ICdvYmplY3QnIHx8IGFjdGlvbkNyZWF0b3JzID09PSBudWxsKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIiA/IGZvcm1hdFByb2RFcnJvck1lc3NhZ2UoMTYpIDogXCJiaW5kQWN0aW9uQ3JlYXRvcnMgZXhwZWN0ZWQgYW4gb2JqZWN0IG9yIGEgZnVuY3Rpb24sIGJ1dCBpbnN0ZWFkIHJlY2VpdmVkOiAnXCIgKyBraW5kT2YoYWN0aW9uQ3JlYXRvcnMpICsgXCInLiBcIiArIFwiRGlkIHlvdSB3cml0ZSBcXFwiaW1wb3J0IEFjdGlvbkNyZWF0b3JzIGZyb21cXFwiIGluc3RlYWQgb2YgXFxcImltcG9ydCAqIGFzIEFjdGlvbkNyZWF0b3JzIGZyb21cXFwiP1wiKTtcbiAgfVxuXG4gIHZhciBib3VuZEFjdGlvbkNyZWF0b3JzID0ge307XG5cbiAgZm9yICh2YXIga2V5IGluIGFjdGlvbkNyZWF0b3JzKSB7XG4gICAgdmFyIGFjdGlvbkNyZWF0b3IgPSBhY3Rpb25DcmVhdG9yc1trZXldO1xuXG4gICAgaWYgKHR5cGVvZiBhY3Rpb25DcmVhdG9yID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBib3VuZEFjdGlvbkNyZWF0b3JzW2tleV0gPSBiaW5kQWN0aW9uQ3JlYXRvcihhY3Rpb25DcmVhdG9yLCBkaXNwYXRjaCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIGJvdW5kQWN0aW9uQ3JlYXRvcnM7XG59XG5cbi8qKlxuICogQ29tcG9zZXMgc2luZ2xlLWFyZ3VtZW50IGZ1bmN0aW9ucyBmcm9tIHJpZ2h0IHRvIGxlZnQuIFRoZSByaWdodG1vc3RcbiAqIGZ1bmN0aW9uIGNhbiB0YWtlIG11bHRpcGxlIGFyZ3VtZW50cyBhcyBpdCBwcm92aWRlcyB0aGUgc2lnbmF0dXJlIGZvclxuICogdGhlIHJlc3VsdGluZyBjb21wb3NpdGUgZnVuY3Rpb24uXG4gKlxuICogQHBhcmFtIHsuLi5GdW5jdGlvbn0gZnVuY3MgVGhlIGZ1bmN0aW9ucyB0byBjb21wb3NlLlxuICogQHJldHVybnMge0Z1bmN0aW9ufSBBIGZ1bmN0aW9uIG9idGFpbmVkIGJ5IGNvbXBvc2luZyB0aGUgYXJndW1lbnQgZnVuY3Rpb25zXG4gKiBmcm9tIHJpZ2h0IHRvIGxlZnQuIEZvciBleGFtcGxlLCBjb21wb3NlKGYsIGcsIGgpIGlzIGlkZW50aWNhbCB0byBkb2luZ1xuICogKC4uLmFyZ3MpID0+IGYoZyhoKC4uLmFyZ3MpKSkuXG4gKi9cbmZ1bmN0aW9uIGNvbXBvc2UoKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBmdW5jcyA9IG5ldyBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBmdW5jc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIGlmIChmdW5jcy5sZW5ndGggPT09IDApIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKGFyZykge1xuICAgICAgcmV0dXJuIGFyZztcbiAgICB9O1xuICB9XG5cbiAgaWYgKGZ1bmNzLmxlbmd0aCA9PT0gMSkge1xuICAgIHJldHVybiBmdW5jc1swXTtcbiAgfVxuXG4gIHJldHVybiBmdW5jcy5yZWR1Y2UoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIGEoYi5hcHBseSh2b2lkIDAsIGFyZ3VtZW50cykpO1xuICAgIH07XG4gIH0pO1xufVxuXG4vKipcbiAqIENyZWF0ZXMgYSBzdG9yZSBlbmhhbmNlciB0aGF0IGFwcGxpZXMgbWlkZGxld2FyZSB0byB0aGUgZGlzcGF0Y2ggbWV0aG9kXG4gKiBvZiB0aGUgUmVkdXggc3RvcmUuIFRoaXMgaXMgaGFuZHkgZm9yIGEgdmFyaWV0eSBvZiB0YXNrcywgc3VjaCBhcyBleHByZXNzaW5nXG4gKiBhc3luY2hyb25vdXMgYWN0aW9ucyBpbiBhIGNvbmNpc2UgbWFubmVyLCBvciBsb2dnaW5nIGV2ZXJ5IGFjdGlvbiBwYXlsb2FkLlxuICpcbiAqIFNlZSBgcmVkdXgtdGh1bmtgIHBhY2thZ2UgYXMgYW4gZXhhbXBsZSBvZiB0aGUgUmVkdXggbWlkZGxld2FyZS5cbiAqXG4gKiBCZWNhdXNlIG1pZGRsZXdhcmUgaXMgcG90ZW50aWFsbHkgYXN5bmNocm9ub3VzLCB0aGlzIHNob3VsZCBiZSB0aGUgZmlyc3RcbiAqIHN0b3JlIGVuaGFuY2VyIGluIHRoZSBjb21wb3NpdGlvbiBjaGFpbi5cbiAqXG4gKiBOb3RlIHRoYXQgZWFjaCBtaWRkbGV3YXJlIHdpbGwgYmUgZ2l2ZW4gdGhlIGBkaXNwYXRjaGAgYW5kIGBnZXRTdGF0ZWAgZnVuY3Rpb25zXG4gKiBhcyBuYW1lZCBhcmd1bWVudHMuXG4gKlxuICogQHBhcmFtIHsuLi5GdW5jdGlvbn0gbWlkZGxld2FyZXMgVGhlIG1pZGRsZXdhcmUgY2hhaW4gdG8gYmUgYXBwbGllZC5cbiAqIEByZXR1cm5zIHtGdW5jdGlvbn0gQSBzdG9yZSBlbmhhbmNlciBhcHBseWluZyB0aGUgbWlkZGxld2FyZS5cbiAqL1xuXG5mdW5jdGlvbiBhcHBseU1pZGRsZXdhcmUoKSB7XG4gIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBtaWRkbGV3YXJlcyA9IG5ldyBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICBtaWRkbGV3YXJlc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgfVxuXG4gIHJldHVybiBmdW5jdGlvbiAoY3JlYXRlU3RvcmUpIHtcbiAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIHN0b3JlID0gY3JlYXRlU3RvcmUuYXBwbHkodm9pZCAwLCBhcmd1bWVudHMpO1xuXG4gICAgICB2YXIgX2Rpc3BhdGNoID0gZnVuY3Rpb24gZGlzcGF0Y2goKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gXCJwcm9kdWN0aW9uXCIgPyBmb3JtYXRQcm9kRXJyb3JNZXNzYWdlKDE1KSA6ICdEaXNwYXRjaGluZyB3aGlsZSBjb25zdHJ1Y3RpbmcgeW91ciBtaWRkbGV3YXJlIGlzIG5vdCBhbGxvd2VkLiAnICsgJ090aGVyIG1pZGRsZXdhcmUgd291bGQgbm90IGJlIGFwcGxpZWQgdG8gdGhpcyBkaXNwYXRjaC4nKTtcbiAgICAgIH07XG5cbiAgICAgIHZhciBtaWRkbGV3YXJlQVBJID0ge1xuICAgICAgICBnZXRTdGF0ZTogc3RvcmUuZ2V0U3RhdGUsXG4gICAgICAgIGRpc3BhdGNoOiBmdW5jdGlvbiBkaXNwYXRjaCgpIHtcbiAgICAgICAgICByZXR1cm4gX2Rpc3BhdGNoLmFwcGx5KHZvaWQgMCwgYXJndW1lbnRzKTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICAgIHZhciBjaGFpbiA9IG1pZGRsZXdhcmVzLm1hcChmdW5jdGlvbiAobWlkZGxld2FyZSkge1xuICAgICAgICByZXR1cm4gbWlkZGxld2FyZShtaWRkbGV3YXJlQVBJKTtcbiAgICAgIH0pO1xuICAgICAgX2Rpc3BhdGNoID0gY29tcG9zZS5hcHBseSh2b2lkIDAsIGNoYWluKShzdG9yZS5kaXNwYXRjaCk7XG4gICAgICByZXR1cm4gX29iamVjdFNwcmVhZChfb2JqZWN0U3ByZWFkKHt9LCBzdG9yZSksIHt9LCB7XG4gICAgICAgIGRpc3BhdGNoOiBfZGlzcGF0Y2hcbiAgICAgIH0pO1xuICAgIH07XG4gIH07XG59XG5cbi8qXG4gKiBUaGlzIGlzIGEgZHVtbXkgZnVuY3Rpb24gdG8gY2hlY2sgaWYgdGhlIGZ1bmN0aW9uIG5hbWUgaGFzIGJlZW4gYWx0ZXJlZCBieSBtaW5pZmljYXRpb24uXG4gKiBJZiB0aGUgZnVuY3Rpb24gaGFzIGJlZW4gbWluaWZpZWQgYW5kIE5PREVfRU5WICE9PSAncHJvZHVjdGlvbicsIHdhcm4gdGhlIHVzZXIuXG4gKi9cblxuZnVuY3Rpb24gaXNDcnVzaGVkKCkge31cblxuaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicgJiYgdHlwZW9mIGlzQ3J1c2hlZC5uYW1lID09PSAnc3RyaW5nJyAmJiBpc0NydXNoZWQubmFtZSAhPT0gJ2lzQ3J1c2hlZCcpIHtcbiAgd2FybmluZygnWW91IGFyZSBjdXJyZW50bHkgdXNpbmcgbWluaWZpZWQgY29kZSBvdXRzaWRlIG9mIE5PREVfRU5WID09PSBcInByb2R1Y3Rpb25cIi4gJyArICdUaGlzIG1lYW5zIHRoYXQgeW91IGFyZSBydW5uaW5nIGEgc2xvd2VyIGRldmVsb3BtZW50IGJ1aWxkIG9mIFJlZHV4LiAnICsgJ1lvdSBjYW4gdXNlIGxvb3NlLWVudmlmeSAoaHR0cHM6Ly9naXRodWIuY29tL3plcnRvc2gvbG9vc2UtZW52aWZ5KSBmb3IgYnJvd3NlcmlmeSAnICsgJ29yIHNldHRpbmcgbW9kZSB0byBwcm9kdWN0aW9uIGluIHdlYnBhY2sgKGh0dHBzOi8vd2VicGFjay5qcy5vcmcvY29uY2VwdHMvbW9kZS8pICcgKyAndG8gZW5zdXJlIHlvdSBoYXZlIHRoZSBjb3JyZWN0IGNvZGUgZm9yIHlvdXIgcHJvZHVjdGlvbiBidWlsZC4nKTtcbn1cblxuZXhwb3J0IHsgQWN0aW9uVHlwZXMgYXMgX19ET19OT1RfVVNFX19BY3Rpb25UeXBlcywgYXBwbHlNaWRkbGV3YXJlLCBiaW5kQWN0aW9uQ3JlYXRvcnMsIGNvbWJpbmVSZWR1Y2VycywgY29tcG9zZSwgY3JlYXRlU3RvcmUgfTtcbiIsIi8qKlxuICogQ29weXJpZ2h0IChjKSAyMDE0LXByZXNlbnQsIEZhY2Vib29rLCBJbmMuXG4gKlxuICogVGhpcyBzb3VyY2UgY29kZSBpcyBsaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2UgZm91bmQgaW4gdGhlXG4gKiBMSUNFTlNFIGZpbGUgaW4gdGhlIHJvb3QgZGlyZWN0b3J5IG9mIHRoaXMgc291cmNlIHRyZWUuXG4gKi9cblxudmFyIHJ1bnRpbWUgPSAoZnVuY3Rpb24gKGV4cG9ydHMpIHtcbiAgXCJ1c2Ugc3RyaWN0XCI7XG5cbiAgdmFyIE9wID0gT2JqZWN0LnByb3RvdHlwZTtcbiAgdmFyIGhhc093biA9IE9wLmhhc093blByb3BlcnR5O1xuICB2YXIgdW5kZWZpbmVkOyAvLyBNb3JlIGNvbXByZXNzaWJsZSB0aGFuIHZvaWQgMC5cbiAgdmFyICRTeW1ib2wgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgPyBTeW1ib2wgOiB7fTtcbiAgdmFyIGl0ZXJhdG9yU3ltYm9sID0gJFN5bWJvbC5pdGVyYXRvciB8fCBcIkBAaXRlcmF0b3JcIjtcbiAgdmFyIGFzeW5jSXRlcmF0b3JTeW1ib2wgPSAkU3ltYm9sLmFzeW5jSXRlcmF0b3IgfHwgXCJAQGFzeW5jSXRlcmF0b3JcIjtcbiAgdmFyIHRvU3RyaW5nVGFnU3ltYm9sID0gJFN5bWJvbC50b1N0cmluZ1RhZyB8fCBcIkBAdG9TdHJpbmdUYWdcIjtcblxuICBmdW5jdGlvbiBkZWZpbmUob2JqLCBrZXksIHZhbHVlKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7XG4gICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgd3JpdGFibGU6IHRydWVcbiAgICB9KTtcbiAgICByZXR1cm4gb2JqW2tleV07XG4gIH1cbiAgdHJ5IHtcbiAgICAvLyBJRSA4IGhhcyBhIGJyb2tlbiBPYmplY3QuZGVmaW5lUHJvcGVydHkgdGhhdCBvbmx5IHdvcmtzIG9uIERPTSBvYmplY3RzLlxuICAgIGRlZmluZSh7fSwgXCJcIik7XG4gIH0gY2F0Y2ggKGVycikge1xuICAgIGRlZmluZSA9IGZ1bmN0aW9uKG9iaiwga2V5LCB2YWx1ZSkge1xuICAgICAgcmV0dXJuIG9ialtrZXldID0gdmFsdWU7XG4gICAgfTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHdyYXAoaW5uZXJGbiwgb3V0ZXJGbiwgc2VsZiwgdHJ5TG9jc0xpc3QpIHtcbiAgICAvLyBJZiBvdXRlckZuIHByb3ZpZGVkIGFuZCBvdXRlckZuLnByb3RvdHlwZSBpcyBhIEdlbmVyYXRvciwgdGhlbiBvdXRlckZuLnByb3RvdHlwZSBpbnN0YW5jZW9mIEdlbmVyYXRvci5cbiAgICB2YXIgcHJvdG9HZW5lcmF0b3IgPSBvdXRlckZuICYmIG91dGVyRm4ucHJvdG90eXBlIGluc3RhbmNlb2YgR2VuZXJhdG9yID8gb3V0ZXJGbiA6IEdlbmVyYXRvcjtcbiAgICB2YXIgZ2VuZXJhdG9yID0gT2JqZWN0LmNyZWF0ZShwcm90b0dlbmVyYXRvci5wcm90b3R5cGUpO1xuICAgIHZhciBjb250ZXh0ID0gbmV3IENvbnRleHQodHJ5TG9jc0xpc3QgfHwgW10pO1xuXG4gICAgLy8gVGhlIC5faW52b2tlIG1ldGhvZCB1bmlmaWVzIHRoZSBpbXBsZW1lbnRhdGlvbnMgb2YgdGhlIC5uZXh0LFxuICAgIC8vIC50aHJvdywgYW5kIC5yZXR1cm4gbWV0aG9kcy5cbiAgICBnZW5lcmF0b3IuX2ludm9rZSA9IG1ha2VJbnZva2VNZXRob2QoaW5uZXJGbiwgc2VsZiwgY29udGV4dCk7XG5cbiAgICByZXR1cm4gZ2VuZXJhdG9yO1xuICB9XG4gIGV4cG9ydHMud3JhcCA9IHdyYXA7XG5cbiAgLy8gVHJ5L2NhdGNoIGhlbHBlciB0byBtaW5pbWl6ZSBkZW9wdGltaXphdGlvbnMuIFJldHVybnMgYSBjb21wbGV0aW9uXG4gIC8vIHJlY29yZCBsaWtlIGNvbnRleHQudHJ5RW50cmllc1tpXS5jb21wbGV0aW9uLiBUaGlzIGludGVyZmFjZSBjb3VsZFxuICAvLyBoYXZlIGJlZW4gKGFuZCB3YXMgcHJldmlvdXNseSkgZGVzaWduZWQgdG8gdGFrZSBhIGNsb3N1cmUgdG8gYmVcbiAgLy8gaW52b2tlZCB3aXRob3V0IGFyZ3VtZW50cywgYnV0IGluIGFsbCB0aGUgY2FzZXMgd2UgY2FyZSBhYm91dCB3ZVxuICAvLyBhbHJlYWR5IGhhdmUgYW4gZXhpc3RpbmcgbWV0aG9kIHdlIHdhbnQgdG8gY2FsbCwgc28gdGhlcmUncyBubyBuZWVkXG4gIC8vIHRvIGNyZWF0ZSBhIG5ldyBmdW5jdGlvbiBvYmplY3QuIFdlIGNhbiBldmVuIGdldCBhd2F5IHdpdGggYXNzdW1pbmdcbiAgLy8gdGhlIG1ldGhvZCB0YWtlcyBleGFjdGx5IG9uZSBhcmd1bWVudCwgc2luY2UgdGhhdCBoYXBwZW5zIHRvIGJlIHRydWVcbiAgLy8gaW4gZXZlcnkgY2FzZSwgc28gd2UgZG9uJ3QgaGF2ZSB0byB0b3VjaCB0aGUgYXJndW1lbnRzIG9iamVjdC4gVGhlXG4gIC8vIG9ubHkgYWRkaXRpb25hbCBhbGxvY2F0aW9uIHJlcXVpcmVkIGlzIHRoZSBjb21wbGV0aW9uIHJlY29yZCwgd2hpY2hcbiAgLy8gaGFzIGEgc3RhYmxlIHNoYXBlIGFuZCBzbyBob3BlZnVsbHkgc2hvdWxkIGJlIGNoZWFwIHRvIGFsbG9jYXRlLlxuICBmdW5jdGlvbiB0cnlDYXRjaChmbiwgb2JqLCBhcmcpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIHsgdHlwZTogXCJub3JtYWxcIiwgYXJnOiBmbi5jYWxsKG9iaiwgYXJnKSB9O1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgcmV0dXJuIHsgdHlwZTogXCJ0aHJvd1wiLCBhcmc6IGVyciB9O1xuICAgIH1cbiAgfVxuXG4gIHZhciBHZW5TdGF0ZVN1c3BlbmRlZFN0YXJ0ID0gXCJzdXNwZW5kZWRTdGFydFwiO1xuICB2YXIgR2VuU3RhdGVTdXNwZW5kZWRZaWVsZCA9IFwic3VzcGVuZGVkWWllbGRcIjtcbiAgdmFyIEdlblN0YXRlRXhlY3V0aW5nID0gXCJleGVjdXRpbmdcIjtcbiAgdmFyIEdlblN0YXRlQ29tcGxldGVkID0gXCJjb21wbGV0ZWRcIjtcblxuICAvLyBSZXR1cm5pbmcgdGhpcyBvYmplY3QgZnJvbSB0aGUgaW5uZXJGbiBoYXMgdGhlIHNhbWUgZWZmZWN0IGFzXG4gIC8vIGJyZWFraW5nIG91dCBvZiB0aGUgZGlzcGF0Y2ggc3dpdGNoIHN0YXRlbWVudC5cbiAgdmFyIENvbnRpbnVlU2VudGluZWwgPSB7fTtcblxuICAvLyBEdW1teSBjb25zdHJ1Y3RvciBmdW5jdGlvbnMgdGhhdCB3ZSB1c2UgYXMgdGhlIC5jb25zdHJ1Y3RvciBhbmRcbiAgLy8gLmNvbnN0cnVjdG9yLnByb3RvdHlwZSBwcm9wZXJ0aWVzIGZvciBmdW5jdGlvbnMgdGhhdCByZXR1cm4gR2VuZXJhdG9yXG4gIC8vIG9iamVjdHMuIEZvciBmdWxsIHNwZWMgY29tcGxpYW5jZSwgeW91IG1heSB3aXNoIHRvIGNvbmZpZ3VyZSB5b3VyXG4gIC8vIG1pbmlmaWVyIG5vdCB0byBtYW5nbGUgdGhlIG5hbWVzIG9mIHRoZXNlIHR3byBmdW5jdGlvbnMuXG4gIGZ1bmN0aW9uIEdlbmVyYXRvcigpIHt9XG4gIGZ1bmN0aW9uIEdlbmVyYXRvckZ1bmN0aW9uKCkge31cbiAgZnVuY3Rpb24gR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUoKSB7fVxuXG4gIC8vIFRoaXMgaXMgYSBwb2x5ZmlsbCBmb3IgJUl0ZXJhdG9yUHJvdG90eXBlJSBmb3IgZW52aXJvbm1lbnRzIHRoYXRcbiAgLy8gZG9uJ3QgbmF0aXZlbHkgc3VwcG9ydCBpdC5cbiAgdmFyIEl0ZXJhdG9yUHJvdG90eXBlID0ge307XG4gIGRlZmluZShJdGVyYXRvclByb3RvdHlwZSwgaXRlcmF0b3JTeW1ib2wsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcztcbiAgfSk7XG5cbiAgdmFyIGdldFByb3RvID0gT2JqZWN0LmdldFByb3RvdHlwZU9mO1xuICB2YXIgTmF0aXZlSXRlcmF0b3JQcm90b3R5cGUgPSBnZXRQcm90byAmJiBnZXRQcm90byhnZXRQcm90byh2YWx1ZXMoW10pKSk7XG4gIGlmIChOYXRpdmVJdGVyYXRvclByb3RvdHlwZSAmJlxuICAgICAgTmF0aXZlSXRlcmF0b3JQcm90b3R5cGUgIT09IE9wICYmXG4gICAgICBoYXNPd24uY2FsbChOYXRpdmVJdGVyYXRvclByb3RvdHlwZSwgaXRlcmF0b3JTeW1ib2wpKSB7XG4gICAgLy8gVGhpcyBlbnZpcm9ubWVudCBoYXMgYSBuYXRpdmUgJUl0ZXJhdG9yUHJvdG90eXBlJTsgdXNlIGl0IGluc3RlYWRcbiAgICAvLyBvZiB0aGUgcG9seWZpbGwuXG4gICAgSXRlcmF0b3JQcm90b3R5cGUgPSBOYXRpdmVJdGVyYXRvclByb3RvdHlwZTtcbiAgfVxuXG4gIHZhciBHcCA9IEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlLnByb3RvdHlwZSA9XG4gICAgR2VuZXJhdG9yLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoSXRlcmF0b3JQcm90b3R5cGUpO1xuICBHZW5lcmF0b3JGdW5jdGlvbi5wcm90b3R5cGUgPSBHZW5lcmF0b3JGdW5jdGlvblByb3RvdHlwZTtcbiAgZGVmaW5lKEdwLCBcImNvbnN0cnVjdG9yXCIsIEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlKTtcbiAgZGVmaW5lKEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlLCBcImNvbnN0cnVjdG9yXCIsIEdlbmVyYXRvckZ1bmN0aW9uKTtcbiAgR2VuZXJhdG9yRnVuY3Rpb24uZGlzcGxheU5hbWUgPSBkZWZpbmUoXG4gICAgR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUsXG4gICAgdG9TdHJpbmdUYWdTeW1ib2wsXG4gICAgXCJHZW5lcmF0b3JGdW5jdGlvblwiXG4gICk7XG5cbiAgLy8gSGVscGVyIGZvciBkZWZpbmluZyB0aGUgLm5leHQsIC50aHJvdywgYW5kIC5yZXR1cm4gbWV0aG9kcyBvZiB0aGVcbiAgLy8gSXRlcmF0b3IgaW50ZXJmYWNlIGluIHRlcm1zIG9mIGEgc2luZ2xlIC5faW52b2tlIG1ldGhvZC5cbiAgZnVuY3Rpb24gZGVmaW5lSXRlcmF0b3JNZXRob2RzKHByb3RvdHlwZSkge1xuICAgIFtcIm5leHRcIiwgXCJ0aHJvd1wiLCBcInJldHVyblwiXS5mb3JFYWNoKGZ1bmN0aW9uKG1ldGhvZCkge1xuICAgICAgZGVmaW5lKHByb3RvdHlwZSwgbWV0aG9kLCBmdW5jdGlvbihhcmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ludm9rZShtZXRob2QsIGFyZyk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGV4cG9ydHMuaXNHZW5lcmF0b3JGdW5jdGlvbiA9IGZ1bmN0aW9uKGdlbkZ1bikge1xuICAgIHZhciBjdG9yID0gdHlwZW9mIGdlbkZ1biA9PT0gXCJmdW5jdGlvblwiICYmIGdlbkZ1bi5jb25zdHJ1Y3RvcjtcbiAgICByZXR1cm4gY3RvclxuICAgICAgPyBjdG9yID09PSBHZW5lcmF0b3JGdW5jdGlvbiB8fFxuICAgICAgICAvLyBGb3IgdGhlIG5hdGl2ZSBHZW5lcmF0b3JGdW5jdGlvbiBjb25zdHJ1Y3RvciwgdGhlIGJlc3Qgd2UgY2FuXG4gICAgICAgIC8vIGRvIGlzIHRvIGNoZWNrIGl0cyAubmFtZSBwcm9wZXJ0eS5cbiAgICAgICAgKGN0b3IuZGlzcGxheU5hbWUgfHwgY3Rvci5uYW1lKSA9PT0gXCJHZW5lcmF0b3JGdW5jdGlvblwiXG4gICAgICA6IGZhbHNlO1xuICB9O1xuXG4gIGV4cG9ydHMubWFyayA9IGZ1bmN0aW9uKGdlbkZ1bikge1xuICAgIGlmIChPYmplY3Quc2V0UHJvdG90eXBlT2YpIHtcbiAgICAgIE9iamVjdC5zZXRQcm90b3R5cGVPZihnZW5GdW4sIEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgZ2VuRnVuLl9fcHJvdG9fXyA9IEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlO1xuICAgICAgZGVmaW5lKGdlbkZ1biwgdG9TdHJpbmdUYWdTeW1ib2wsIFwiR2VuZXJhdG9yRnVuY3Rpb25cIik7XG4gICAgfVxuICAgIGdlbkZ1bi5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEdwKTtcbiAgICByZXR1cm4gZ2VuRnVuO1xuICB9O1xuXG4gIC8vIFdpdGhpbiB0aGUgYm9keSBvZiBhbnkgYXN5bmMgZnVuY3Rpb24sIGBhd2FpdCB4YCBpcyB0cmFuc2Zvcm1lZCB0b1xuICAvLyBgeWllbGQgcmVnZW5lcmF0b3JSdW50aW1lLmF3cmFwKHgpYCwgc28gdGhhdCB0aGUgcnVudGltZSBjYW4gdGVzdFxuICAvLyBgaGFzT3duLmNhbGwodmFsdWUsIFwiX19hd2FpdFwiKWAgdG8gZGV0ZXJtaW5lIGlmIHRoZSB5aWVsZGVkIHZhbHVlIGlzXG4gIC8vIG1lYW50IHRvIGJlIGF3YWl0ZWQuXG4gIGV4cG9ydHMuYXdyYXAgPSBmdW5jdGlvbihhcmcpIHtcbiAgICByZXR1cm4geyBfX2F3YWl0OiBhcmcgfTtcbiAgfTtcblxuICBmdW5jdGlvbiBBc3luY0l0ZXJhdG9yKGdlbmVyYXRvciwgUHJvbWlzZUltcGwpIHtcbiAgICBmdW5jdGlvbiBpbnZva2UobWV0aG9kLCBhcmcsIHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgdmFyIHJlY29yZCA9IHRyeUNhdGNoKGdlbmVyYXRvclttZXRob2RdLCBnZW5lcmF0b3IsIGFyZyk7XG4gICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICByZWplY3QocmVjb3JkLmFyZyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgcmVzdWx0ID0gcmVjb3JkLmFyZztcbiAgICAgICAgdmFyIHZhbHVlID0gcmVzdWx0LnZhbHVlO1xuICAgICAgICBpZiAodmFsdWUgJiZcbiAgICAgICAgICAgIHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIiAmJlxuICAgICAgICAgICAgaGFzT3duLmNhbGwodmFsdWUsIFwiX19hd2FpdFwiKSkge1xuICAgICAgICAgIHJldHVybiBQcm9taXNlSW1wbC5yZXNvbHZlKHZhbHVlLl9fYXdhaXQpLnRoZW4oZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIGludm9rZShcIm5leHRcIiwgdmFsdWUsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XG4gICAgICAgICAgICBpbnZva2UoXCJ0aHJvd1wiLCBlcnIsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUHJvbWlzZUltcGwucmVzb2x2ZSh2YWx1ZSkudGhlbihmdW5jdGlvbih1bndyYXBwZWQpIHtcbiAgICAgICAgICAvLyBXaGVuIGEgeWllbGRlZCBQcm9taXNlIGlzIHJlc29sdmVkLCBpdHMgZmluYWwgdmFsdWUgYmVjb21lc1xuICAgICAgICAgIC8vIHRoZSAudmFsdWUgb2YgdGhlIFByb21pc2U8e3ZhbHVlLGRvbmV9PiByZXN1bHQgZm9yIHRoZVxuICAgICAgICAgIC8vIGN1cnJlbnQgaXRlcmF0aW9uLlxuICAgICAgICAgIHJlc3VsdC52YWx1ZSA9IHVud3JhcHBlZDtcbiAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgLy8gSWYgYSByZWplY3RlZCBQcm9taXNlIHdhcyB5aWVsZGVkLCB0aHJvdyB0aGUgcmVqZWN0aW9uIGJhY2tcbiAgICAgICAgICAvLyBpbnRvIHRoZSBhc3luYyBnZW5lcmF0b3IgZnVuY3Rpb24gc28gaXQgY2FuIGJlIGhhbmRsZWQgdGhlcmUuXG4gICAgICAgICAgcmV0dXJuIGludm9rZShcInRocm93XCIsIGVycm9yLCByZXNvbHZlLCByZWplY3QpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgcHJldmlvdXNQcm9taXNlO1xuXG4gICAgZnVuY3Rpb24gZW5xdWV1ZShtZXRob2QsIGFyZykge1xuICAgICAgZnVuY3Rpb24gY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmcoKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZUltcGwoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgaW52b2tlKG1ldGhvZCwgYXJnLCByZXNvbHZlLCByZWplY3QpO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHByZXZpb3VzUHJvbWlzZSA9XG4gICAgICAgIC8vIElmIGVucXVldWUgaGFzIGJlZW4gY2FsbGVkIGJlZm9yZSwgdGhlbiB3ZSB3YW50IHRvIHdhaXQgdW50aWxcbiAgICAgICAgLy8gYWxsIHByZXZpb3VzIFByb21pc2VzIGhhdmUgYmVlbiByZXNvbHZlZCBiZWZvcmUgY2FsbGluZyBpbnZva2UsXG4gICAgICAgIC8vIHNvIHRoYXQgcmVzdWx0cyBhcmUgYWx3YXlzIGRlbGl2ZXJlZCBpbiB0aGUgY29ycmVjdCBvcmRlci4gSWZcbiAgICAgICAgLy8gZW5xdWV1ZSBoYXMgbm90IGJlZW4gY2FsbGVkIGJlZm9yZSwgdGhlbiBpdCBpcyBpbXBvcnRhbnQgdG9cbiAgICAgICAgLy8gY2FsbCBpbnZva2UgaW1tZWRpYXRlbHksIHdpdGhvdXQgd2FpdGluZyBvbiBhIGNhbGxiYWNrIHRvIGZpcmUsXG4gICAgICAgIC8vIHNvIHRoYXQgdGhlIGFzeW5jIGdlbmVyYXRvciBmdW5jdGlvbiBoYXMgdGhlIG9wcG9ydHVuaXR5IHRvIGRvXG4gICAgICAgIC8vIGFueSBuZWNlc3Nhcnkgc2V0dXAgaW4gYSBwcmVkaWN0YWJsZSB3YXkuIFRoaXMgcHJlZGljdGFiaWxpdHlcbiAgICAgICAgLy8gaXMgd2h5IHRoZSBQcm9taXNlIGNvbnN0cnVjdG9yIHN5bmNocm9ub3VzbHkgaW52b2tlcyBpdHNcbiAgICAgICAgLy8gZXhlY3V0b3IgY2FsbGJhY2ssIGFuZCB3aHkgYXN5bmMgZnVuY3Rpb25zIHN5bmNocm9ub3VzbHlcbiAgICAgICAgLy8gZXhlY3V0ZSBjb2RlIGJlZm9yZSB0aGUgZmlyc3QgYXdhaXQuIFNpbmNlIHdlIGltcGxlbWVudCBzaW1wbGVcbiAgICAgICAgLy8gYXN5bmMgZnVuY3Rpb25zIGluIHRlcm1zIG9mIGFzeW5jIGdlbmVyYXRvcnMsIGl0IGlzIGVzcGVjaWFsbHlcbiAgICAgICAgLy8gaW1wb3J0YW50IHRvIGdldCB0aGlzIHJpZ2h0LCBldmVuIHRob3VnaCBpdCByZXF1aXJlcyBjYXJlLlxuICAgICAgICBwcmV2aW91c1Byb21pc2UgPyBwcmV2aW91c1Byb21pc2UudGhlbihcbiAgICAgICAgICBjYWxsSW52b2tlV2l0aE1ldGhvZEFuZEFyZyxcbiAgICAgICAgICAvLyBBdm9pZCBwcm9wYWdhdGluZyBmYWlsdXJlcyB0byBQcm9taXNlcyByZXR1cm5lZCBieSBsYXRlclxuICAgICAgICAgIC8vIGludm9jYXRpb25zIG9mIHRoZSBpdGVyYXRvci5cbiAgICAgICAgICBjYWxsSW52b2tlV2l0aE1ldGhvZEFuZEFyZ1xuICAgICAgICApIDogY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmcoKTtcbiAgICB9XG5cbiAgICAvLyBEZWZpbmUgdGhlIHVuaWZpZWQgaGVscGVyIG1ldGhvZCB0aGF0IGlzIHVzZWQgdG8gaW1wbGVtZW50IC5uZXh0LFxuICAgIC8vIC50aHJvdywgYW5kIC5yZXR1cm4gKHNlZSBkZWZpbmVJdGVyYXRvck1ldGhvZHMpLlxuICAgIHRoaXMuX2ludm9rZSA9IGVucXVldWU7XG4gIH1cblxuICBkZWZpbmVJdGVyYXRvck1ldGhvZHMoQXN5bmNJdGVyYXRvci5wcm90b3R5cGUpO1xuICBkZWZpbmUoQXN5bmNJdGVyYXRvci5wcm90b3R5cGUsIGFzeW5jSXRlcmF0b3JTeW1ib2wsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcztcbiAgfSk7XG4gIGV4cG9ydHMuQXN5bmNJdGVyYXRvciA9IEFzeW5jSXRlcmF0b3I7XG5cbiAgLy8gTm90ZSB0aGF0IHNpbXBsZSBhc3luYyBmdW5jdGlvbnMgYXJlIGltcGxlbWVudGVkIG9uIHRvcCBvZlxuICAvLyBBc3luY0l0ZXJhdG9yIG9iamVjdHM7IHRoZXkganVzdCByZXR1cm4gYSBQcm9taXNlIGZvciB0aGUgdmFsdWUgb2ZcbiAgLy8gdGhlIGZpbmFsIHJlc3VsdCBwcm9kdWNlZCBieSB0aGUgaXRlcmF0b3IuXG4gIGV4cG9ydHMuYXN5bmMgPSBmdW5jdGlvbihpbm5lckZuLCBvdXRlckZuLCBzZWxmLCB0cnlMb2NzTGlzdCwgUHJvbWlzZUltcGwpIHtcbiAgICBpZiAoUHJvbWlzZUltcGwgPT09IHZvaWQgMCkgUHJvbWlzZUltcGwgPSBQcm9taXNlO1xuXG4gICAgdmFyIGl0ZXIgPSBuZXcgQXN5bmNJdGVyYXRvcihcbiAgICAgIHdyYXAoaW5uZXJGbiwgb3V0ZXJGbiwgc2VsZiwgdHJ5TG9jc0xpc3QpLFxuICAgICAgUHJvbWlzZUltcGxcbiAgICApO1xuXG4gICAgcmV0dXJuIGV4cG9ydHMuaXNHZW5lcmF0b3JGdW5jdGlvbihvdXRlckZuKVxuICAgICAgPyBpdGVyIC8vIElmIG91dGVyRm4gaXMgYSBnZW5lcmF0b3IsIHJldHVybiB0aGUgZnVsbCBpdGVyYXRvci5cbiAgICAgIDogaXRlci5uZXh0KCkudGhlbihmdW5jdGlvbihyZXN1bHQpIHtcbiAgICAgICAgICByZXR1cm4gcmVzdWx0LmRvbmUgPyByZXN1bHQudmFsdWUgOiBpdGVyLm5leHQoKTtcbiAgICAgICAgfSk7XG4gIH07XG5cbiAgZnVuY3Rpb24gbWFrZUludm9rZU1ldGhvZChpbm5lckZuLCBzZWxmLCBjb250ZXh0KSB7XG4gICAgdmFyIHN0YXRlID0gR2VuU3RhdGVTdXNwZW5kZWRTdGFydDtcblxuICAgIHJldHVybiBmdW5jdGlvbiBpbnZva2UobWV0aG9kLCBhcmcpIHtcbiAgICAgIGlmIChzdGF0ZSA9PT0gR2VuU3RhdGVFeGVjdXRpbmcpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgcnVubmluZ1wiKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHN0YXRlID09PSBHZW5TdGF0ZUNvbXBsZXRlZCkge1xuICAgICAgICBpZiAobWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgICB0aHJvdyBhcmc7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBCZSBmb3JnaXZpbmcsIHBlciAyNS4zLjMuMy4zIG9mIHRoZSBzcGVjOlxuICAgICAgICAvLyBodHRwczovL3Blb3BsZS5tb3ppbGxhLm9yZy9+am9yZW5kb3JmZi9lczYtZHJhZnQuaHRtbCNzZWMtZ2VuZXJhdG9ycmVzdW1lXG4gICAgICAgIHJldHVybiBkb25lUmVzdWx0KCk7XG4gICAgICB9XG5cbiAgICAgIGNvbnRleHQubWV0aG9kID0gbWV0aG9kO1xuICAgICAgY29udGV4dC5hcmcgPSBhcmc7XG5cbiAgICAgIHdoaWxlICh0cnVlKSB7XG4gICAgICAgIHZhciBkZWxlZ2F0ZSA9IGNvbnRleHQuZGVsZWdhdGU7XG4gICAgICAgIGlmIChkZWxlZ2F0ZSkge1xuICAgICAgICAgIHZhciBkZWxlZ2F0ZVJlc3VsdCA9IG1heWJlSW52b2tlRGVsZWdhdGUoZGVsZWdhdGUsIGNvbnRleHQpO1xuICAgICAgICAgIGlmIChkZWxlZ2F0ZVJlc3VsdCkge1xuICAgICAgICAgICAgaWYgKGRlbGVnYXRlUmVzdWx0ID09PSBDb250aW51ZVNlbnRpbmVsKSBjb250aW51ZTtcbiAgICAgICAgICAgIHJldHVybiBkZWxlZ2F0ZVJlc3VsdDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoY29udGV4dC5tZXRob2QgPT09IFwibmV4dFwiKSB7XG4gICAgICAgICAgLy8gU2V0dGluZyBjb250ZXh0Ll9zZW50IGZvciBsZWdhY3kgc3VwcG9ydCBvZiBCYWJlbCdzXG4gICAgICAgICAgLy8gZnVuY3Rpb24uc2VudCBpbXBsZW1lbnRhdGlvbi5cbiAgICAgICAgICBjb250ZXh0LnNlbnQgPSBjb250ZXh0Ll9zZW50ID0gY29udGV4dC5hcmc7XG5cbiAgICAgICAgfSBlbHNlIGlmIChjb250ZXh0Lm1ldGhvZCA9PT0gXCJ0aHJvd1wiKSB7XG4gICAgICAgICAgaWYgKHN0YXRlID09PSBHZW5TdGF0ZVN1c3BlbmRlZFN0YXJ0KSB7XG4gICAgICAgICAgICBzdGF0ZSA9IEdlblN0YXRlQ29tcGxldGVkO1xuICAgICAgICAgICAgdGhyb3cgY29udGV4dC5hcmc7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgY29udGV4dC5kaXNwYXRjaEV4Y2VwdGlvbihjb250ZXh0LmFyZyk7XG5cbiAgICAgICAgfSBlbHNlIGlmIChjb250ZXh0Lm1ldGhvZCA9PT0gXCJyZXR1cm5cIikge1xuICAgICAgICAgIGNvbnRleHQuYWJydXB0KFwicmV0dXJuXCIsIGNvbnRleHQuYXJnKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHN0YXRlID0gR2VuU3RhdGVFeGVjdXRpbmc7XG5cbiAgICAgICAgdmFyIHJlY29yZCA9IHRyeUNhdGNoKGlubmVyRm4sIHNlbGYsIGNvbnRleHQpO1xuICAgICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwibm9ybWFsXCIpIHtcbiAgICAgICAgICAvLyBJZiBhbiBleGNlcHRpb24gaXMgdGhyb3duIGZyb20gaW5uZXJGbiwgd2UgbGVhdmUgc3RhdGUgPT09XG4gICAgICAgICAgLy8gR2VuU3RhdGVFeGVjdXRpbmcgYW5kIGxvb3AgYmFjayBmb3IgYW5vdGhlciBpbnZvY2F0aW9uLlxuICAgICAgICAgIHN0YXRlID0gY29udGV4dC5kb25lXG4gICAgICAgICAgICA/IEdlblN0YXRlQ29tcGxldGVkXG4gICAgICAgICAgICA6IEdlblN0YXRlU3VzcGVuZGVkWWllbGQ7XG5cbiAgICAgICAgICBpZiAocmVjb3JkLmFyZyA9PT0gQ29udGludWVTZW50aW5lbCkge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIHZhbHVlOiByZWNvcmQuYXJnLFxuICAgICAgICAgICAgZG9uZTogY29udGV4dC5kb25lXG4gICAgICAgICAgfTtcblxuICAgICAgICB9IGVsc2UgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgICAgICBzdGF0ZSA9IEdlblN0YXRlQ29tcGxldGVkO1xuICAgICAgICAgIC8vIERpc3BhdGNoIHRoZSBleGNlcHRpb24gYnkgbG9vcGluZyBiYWNrIGFyb3VuZCB0byB0aGVcbiAgICAgICAgICAvLyBjb250ZXh0LmRpc3BhdGNoRXhjZXB0aW9uKGNvbnRleHQuYXJnKSBjYWxsIGFib3ZlLlxuICAgICAgICAgIGNvbnRleHQubWV0aG9kID0gXCJ0aHJvd1wiO1xuICAgICAgICAgIGNvbnRleHQuYXJnID0gcmVjb3JkLmFyZztcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH07XG4gIH1cblxuICAvLyBDYWxsIGRlbGVnYXRlLml0ZXJhdG9yW2NvbnRleHQubWV0aG9kXShjb250ZXh0LmFyZykgYW5kIGhhbmRsZSB0aGVcbiAgLy8gcmVzdWx0LCBlaXRoZXIgYnkgcmV0dXJuaW5nIGEgeyB2YWx1ZSwgZG9uZSB9IHJlc3VsdCBmcm9tIHRoZVxuICAvLyBkZWxlZ2F0ZSBpdGVyYXRvciwgb3IgYnkgbW9kaWZ5aW5nIGNvbnRleHQubWV0aG9kIGFuZCBjb250ZXh0LmFyZyxcbiAgLy8gc2V0dGluZyBjb250ZXh0LmRlbGVnYXRlIHRvIG51bGwsIGFuZCByZXR1cm5pbmcgdGhlIENvbnRpbnVlU2VudGluZWwuXG4gIGZ1bmN0aW9uIG1heWJlSW52b2tlRGVsZWdhdGUoZGVsZWdhdGUsIGNvbnRleHQpIHtcbiAgICB2YXIgbWV0aG9kID0gZGVsZWdhdGUuaXRlcmF0b3JbY29udGV4dC5tZXRob2RdO1xuICAgIGlmIChtZXRob2QgPT09IHVuZGVmaW5lZCkge1xuICAgICAgLy8gQSAudGhyb3cgb3IgLnJldHVybiB3aGVuIHRoZSBkZWxlZ2F0ZSBpdGVyYXRvciBoYXMgbm8gLnRocm93XG4gICAgICAvLyBtZXRob2QgYWx3YXlzIHRlcm1pbmF0ZXMgdGhlIHlpZWxkKiBsb29wLlxuICAgICAgY29udGV4dC5kZWxlZ2F0ZSA9IG51bGw7XG5cbiAgICAgIGlmIChjb250ZXh0Lm1ldGhvZCA9PT0gXCJ0aHJvd1wiKSB7XG4gICAgICAgIC8vIE5vdGU6IFtcInJldHVyblwiXSBtdXN0IGJlIHVzZWQgZm9yIEVTMyBwYXJzaW5nIGNvbXBhdGliaWxpdHkuXG4gICAgICAgIGlmIChkZWxlZ2F0ZS5pdGVyYXRvcltcInJldHVyblwiXSkge1xuICAgICAgICAgIC8vIElmIHRoZSBkZWxlZ2F0ZSBpdGVyYXRvciBoYXMgYSByZXR1cm4gbWV0aG9kLCBnaXZlIGl0IGFcbiAgICAgICAgICAvLyBjaGFuY2UgdG8gY2xlYW4gdXAuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcInJldHVyblwiO1xuICAgICAgICAgIGNvbnRleHQuYXJnID0gdW5kZWZpbmVkO1xuICAgICAgICAgIG1heWJlSW52b2tlRGVsZWdhdGUoZGVsZWdhdGUsIGNvbnRleHQpO1xuXG4gICAgICAgICAgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgICAgIC8vIElmIG1heWJlSW52b2tlRGVsZWdhdGUoY29udGV4dCkgY2hhbmdlZCBjb250ZXh0Lm1ldGhvZCBmcm9tXG4gICAgICAgICAgICAvLyBcInJldHVyblwiIHRvIFwidGhyb3dcIiwgbGV0IHRoYXQgb3ZlcnJpZGUgdGhlIFR5cGVFcnJvciBiZWxvdy5cbiAgICAgICAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnRleHQubWV0aG9kID0gXCJ0aHJvd1wiO1xuICAgICAgICBjb250ZXh0LmFyZyA9IG5ldyBUeXBlRXJyb3IoXG4gICAgICAgICAgXCJUaGUgaXRlcmF0b3IgZG9lcyBub3QgcHJvdmlkZSBhICd0aHJvdycgbWV0aG9kXCIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gQ29udGludWVTZW50aW5lbDtcbiAgICB9XG5cbiAgICB2YXIgcmVjb3JkID0gdHJ5Q2F0Y2gobWV0aG9kLCBkZWxlZ2F0ZS5pdGVyYXRvciwgY29udGV4dC5hcmcpO1xuXG4gICAgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgIGNvbnRleHQubWV0aG9kID0gXCJ0aHJvd1wiO1xuICAgICAgY29udGV4dC5hcmcgPSByZWNvcmQuYXJnO1xuICAgICAgY29udGV4dC5kZWxlZ2F0ZSA9IG51bGw7XG4gICAgICByZXR1cm4gQ29udGludWVTZW50aW5lbDtcbiAgICB9XG5cbiAgICB2YXIgaW5mbyA9IHJlY29yZC5hcmc7XG5cbiAgICBpZiAoISBpbmZvKSB7XG4gICAgICBjb250ZXh0Lm1ldGhvZCA9IFwidGhyb3dcIjtcbiAgICAgIGNvbnRleHQuYXJnID0gbmV3IFR5cGVFcnJvcihcIml0ZXJhdG9yIHJlc3VsdCBpcyBub3QgYW4gb2JqZWN0XCIpO1xuICAgICAgY29udGV4dC5kZWxlZ2F0ZSA9IG51bGw7XG4gICAgICByZXR1cm4gQ29udGludWVTZW50aW5lbDtcbiAgICB9XG5cbiAgICBpZiAoaW5mby5kb25lKSB7XG4gICAgICAvLyBBc3NpZ24gdGhlIHJlc3VsdCBvZiB0aGUgZmluaXNoZWQgZGVsZWdhdGUgdG8gdGhlIHRlbXBvcmFyeVxuICAgICAgLy8gdmFyaWFibGUgc3BlY2lmaWVkIGJ5IGRlbGVnYXRlLnJlc3VsdE5hbWUgKHNlZSBkZWxlZ2F0ZVlpZWxkKS5cbiAgICAgIGNvbnRleHRbZGVsZWdhdGUucmVzdWx0TmFtZV0gPSBpbmZvLnZhbHVlO1xuXG4gICAgICAvLyBSZXN1bWUgZXhlY3V0aW9uIGF0IHRoZSBkZXNpcmVkIGxvY2F0aW9uIChzZWUgZGVsZWdhdGVZaWVsZCkuXG4gICAgICBjb250ZXh0Lm5leHQgPSBkZWxlZ2F0ZS5uZXh0TG9jO1xuXG4gICAgICAvLyBJZiBjb250ZXh0Lm1ldGhvZCB3YXMgXCJ0aHJvd1wiIGJ1dCB0aGUgZGVsZWdhdGUgaGFuZGxlZCB0aGVcbiAgICAgIC8vIGV4Y2VwdGlvbiwgbGV0IHRoZSBvdXRlciBnZW5lcmF0b3IgcHJvY2VlZCBub3JtYWxseS4gSWZcbiAgICAgIC8vIGNvbnRleHQubWV0aG9kIHdhcyBcIm5leHRcIiwgZm9yZ2V0IGNvbnRleHQuYXJnIHNpbmNlIGl0IGhhcyBiZWVuXG4gICAgICAvLyBcImNvbnN1bWVkXCIgYnkgdGhlIGRlbGVnYXRlIGl0ZXJhdG9yLiBJZiBjb250ZXh0Lm1ldGhvZCB3YXNcbiAgICAgIC8vIFwicmV0dXJuXCIsIGFsbG93IHRoZSBvcmlnaW5hbCAucmV0dXJuIGNhbGwgdG8gY29udGludWUgaW4gdGhlXG4gICAgICAvLyBvdXRlciBnZW5lcmF0b3IuXG4gICAgICBpZiAoY29udGV4dC5tZXRob2QgIT09IFwicmV0dXJuXCIpIHtcbiAgICAgICAgY29udGV4dC5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgY29udGV4dC5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICB9XG5cbiAgICB9IGVsc2Uge1xuICAgICAgLy8gUmUteWllbGQgdGhlIHJlc3VsdCByZXR1cm5lZCBieSB0aGUgZGVsZWdhdGUgbWV0aG9kLlxuICAgICAgcmV0dXJuIGluZm87XG4gICAgfVxuXG4gICAgLy8gVGhlIGRlbGVnYXRlIGl0ZXJhdG9yIGlzIGZpbmlzaGVkLCBzbyBmb3JnZXQgaXQgYW5kIGNvbnRpbnVlIHdpdGhcbiAgICAvLyB0aGUgb3V0ZXIgZ2VuZXJhdG9yLlxuICAgIGNvbnRleHQuZGVsZWdhdGUgPSBudWxsO1xuICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICB9XG5cbiAgLy8gRGVmaW5lIEdlbmVyYXRvci5wcm90b3R5cGUue25leHQsdGhyb3cscmV0dXJufSBpbiB0ZXJtcyBvZiB0aGVcbiAgLy8gdW5pZmllZCAuX2ludm9rZSBoZWxwZXIgbWV0aG9kLlxuICBkZWZpbmVJdGVyYXRvck1ldGhvZHMoR3ApO1xuXG4gIGRlZmluZShHcCwgdG9TdHJpbmdUYWdTeW1ib2wsIFwiR2VuZXJhdG9yXCIpO1xuXG4gIC8vIEEgR2VuZXJhdG9yIHNob3VsZCBhbHdheXMgcmV0dXJuIGl0c2VsZiBhcyB0aGUgaXRlcmF0b3Igb2JqZWN0IHdoZW4gdGhlXG4gIC8vIEBAaXRlcmF0b3IgZnVuY3Rpb24gaXMgY2FsbGVkIG9uIGl0LiBTb21lIGJyb3dzZXJzJyBpbXBsZW1lbnRhdGlvbnMgb2YgdGhlXG4gIC8vIGl0ZXJhdG9yIHByb3RvdHlwZSBjaGFpbiBpbmNvcnJlY3RseSBpbXBsZW1lbnQgdGhpcywgY2F1c2luZyB0aGUgR2VuZXJhdG9yXG4gIC8vIG9iamVjdCB0byBub3QgYmUgcmV0dXJuZWQgZnJvbSB0aGlzIGNhbGwuIFRoaXMgZW5zdXJlcyB0aGF0IGRvZXNuJ3QgaGFwcGVuLlxuICAvLyBTZWUgaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL3JlZ2VuZXJhdG9yL2lzc3Vlcy8yNzQgZm9yIG1vcmUgZGV0YWlscy5cbiAgZGVmaW5lKEdwLCBpdGVyYXRvclN5bWJvbCwgZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHRoaXM7XG4gIH0pO1xuXG4gIGRlZmluZShHcCwgXCJ0b1N0cmluZ1wiLCBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4gXCJbb2JqZWN0IEdlbmVyYXRvcl1cIjtcbiAgfSk7XG5cbiAgZnVuY3Rpb24gcHVzaFRyeUVudHJ5KGxvY3MpIHtcbiAgICB2YXIgZW50cnkgPSB7IHRyeUxvYzogbG9jc1swXSB9O1xuXG4gICAgaWYgKDEgaW4gbG9jcykge1xuICAgICAgZW50cnkuY2F0Y2hMb2MgPSBsb2NzWzFdO1xuICAgIH1cblxuICAgIGlmICgyIGluIGxvY3MpIHtcbiAgICAgIGVudHJ5LmZpbmFsbHlMb2MgPSBsb2NzWzJdO1xuICAgICAgZW50cnkuYWZ0ZXJMb2MgPSBsb2NzWzNdO1xuICAgIH1cblxuICAgIHRoaXMudHJ5RW50cmllcy5wdXNoKGVudHJ5KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlc2V0VHJ5RW50cnkoZW50cnkpIHtcbiAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbiB8fCB7fTtcbiAgICByZWNvcmQudHlwZSA9IFwibm9ybWFsXCI7XG4gICAgZGVsZXRlIHJlY29yZC5hcmc7XG4gICAgZW50cnkuY29tcGxldGlvbiA9IHJlY29yZDtcbiAgfVxuXG4gIGZ1bmN0aW9uIENvbnRleHQodHJ5TG9jc0xpc3QpIHtcbiAgICAvLyBUaGUgcm9vdCBlbnRyeSBvYmplY3QgKGVmZmVjdGl2ZWx5IGEgdHJ5IHN0YXRlbWVudCB3aXRob3V0IGEgY2F0Y2hcbiAgICAvLyBvciBhIGZpbmFsbHkgYmxvY2spIGdpdmVzIHVzIGEgcGxhY2UgdG8gc3RvcmUgdmFsdWVzIHRocm93biBmcm9tXG4gICAgLy8gbG9jYXRpb25zIHdoZXJlIHRoZXJlIGlzIG5vIGVuY2xvc2luZyB0cnkgc3RhdGVtZW50LlxuICAgIHRoaXMudHJ5RW50cmllcyA9IFt7IHRyeUxvYzogXCJyb290XCIgfV07XG4gICAgdHJ5TG9jc0xpc3QuZm9yRWFjaChwdXNoVHJ5RW50cnksIHRoaXMpO1xuICAgIHRoaXMucmVzZXQodHJ1ZSk7XG4gIH1cblxuICBleHBvcnRzLmtleXMgPSBmdW5jdGlvbihvYmplY3QpIHtcbiAgICB2YXIga2V5cyA9IFtdO1xuICAgIGZvciAodmFyIGtleSBpbiBvYmplY3QpIHtcbiAgICAgIGtleXMucHVzaChrZXkpO1xuICAgIH1cbiAgICBrZXlzLnJldmVyc2UoKTtcblxuICAgIC8vIFJhdGhlciB0aGFuIHJldHVybmluZyBhbiBvYmplY3Qgd2l0aCBhIG5leHQgbWV0aG9kLCB3ZSBrZWVwXG4gICAgLy8gdGhpbmdzIHNpbXBsZSBhbmQgcmV0dXJuIHRoZSBuZXh0IGZ1bmN0aW9uIGl0c2VsZi5cbiAgICByZXR1cm4gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgIHdoaWxlIChrZXlzLmxlbmd0aCkge1xuICAgICAgICB2YXIga2V5ID0ga2V5cy5wb3AoKTtcbiAgICAgICAgaWYgKGtleSBpbiBvYmplY3QpIHtcbiAgICAgICAgICBuZXh0LnZhbHVlID0ga2V5O1xuICAgICAgICAgIG5leHQuZG9uZSA9IGZhbHNlO1xuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIFRvIGF2b2lkIGNyZWF0aW5nIGFuIGFkZGl0aW9uYWwgb2JqZWN0LCB3ZSBqdXN0IGhhbmcgdGhlIC52YWx1ZVxuICAgICAgLy8gYW5kIC5kb25lIHByb3BlcnRpZXMgb2ZmIHRoZSBuZXh0IGZ1bmN0aW9uIG9iamVjdCBpdHNlbGYuIFRoaXNcbiAgICAgIC8vIGFsc28gZW5zdXJlcyB0aGF0IHRoZSBtaW5pZmllciB3aWxsIG5vdCBhbm9ueW1pemUgdGhlIGZ1bmN0aW9uLlxuICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcbiAgICAgIHJldHVybiBuZXh0O1xuICAgIH07XG4gIH07XG5cbiAgZnVuY3Rpb24gdmFsdWVzKGl0ZXJhYmxlKSB7XG4gICAgaWYgKGl0ZXJhYmxlKSB7XG4gICAgICB2YXIgaXRlcmF0b3JNZXRob2QgPSBpdGVyYWJsZVtpdGVyYXRvclN5bWJvbF07XG4gICAgICBpZiAoaXRlcmF0b3JNZXRob2QpIHtcbiAgICAgICAgcmV0dXJuIGl0ZXJhdG9yTWV0aG9kLmNhbGwoaXRlcmFibGUpO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIGl0ZXJhYmxlLm5leHQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICByZXR1cm4gaXRlcmFibGU7XG4gICAgICB9XG5cbiAgICAgIGlmICghaXNOYU4oaXRlcmFibGUubGVuZ3RoKSkge1xuICAgICAgICB2YXIgaSA9IC0xLCBuZXh0ID0gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgICAgICB3aGlsZSAoKytpIDwgaXRlcmFibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duLmNhbGwoaXRlcmFibGUsIGkpKSB7XG4gICAgICAgICAgICAgIG5leHQudmFsdWUgPSBpdGVyYWJsZVtpXTtcbiAgICAgICAgICAgICAgbmV4dC5kb25lID0gZmFsc2U7XG4gICAgICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICAgIG5leHQudmFsdWUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcblxuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBuZXh0Lm5leHQgPSBuZXh0O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIFJldHVybiBhbiBpdGVyYXRvciB3aXRoIG5vIHZhbHVlcy5cbiAgICByZXR1cm4geyBuZXh0OiBkb25lUmVzdWx0IH07XG4gIH1cbiAgZXhwb3J0cy52YWx1ZXMgPSB2YWx1ZXM7XG5cbiAgZnVuY3Rpb24gZG9uZVJlc3VsdCgpIHtcbiAgICByZXR1cm4geyB2YWx1ZTogdW5kZWZpbmVkLCBkb25lOiB0cnVlIH07XG4gIH1cblxuICBDb250ZXh0LnByb3RvdHlwZSA9IHtcbiAgICBjb25zdHJ1Y3RvcjogQ29udGV4dCxcblxuICAgIHJlc2V0OiBmdW5jdGlvbihza2lwVGVtcFJlc2V0KSB7XG4gICAgICB0aGlzLnByZXYgPSAwO1xuICAgICAgdGhpcy5uZXh0ID0gMDtcbiAgICAgIC8vIFJlc2V0dGluZyBjb250ZXh0Ll9zZW50IGZvciBsZWdhY3kgc3VwcG9ydCBvZiBCYWJlbCdzXG4gICAgICAvLyBmdW5jdGlvbi5zZW50IGltcGxlbWVudGF0aW9uLlxuICAgICAgdGhpcy5zZW50ID0gdGhpcy5fc2VudCA9IHVuZGVmaW5lZDtcbiAgICAgIHRoaXMuZG9uZSA9IGZhbHNlO1xuICAgICAgdGhpcy5kZWxlZ2F0ZSA9IG51bGw7XG5cbiAgICAgIHRoaXMubWV0aG9kID0gXCJuZXh0XCI7XG4gICAgICB0aGlzLmFyZyA9IHVuZGVmaW5lZDtcblxuICAgICAgdGhpcy50cnlFbnRyaWVzLmZvckVhY2gocmVzZXRUcnlFbnRyeSk7XG5cbiAgICAgIGlmICghc2tpcFRlbXBSZXNldCkge1xuICAgICAgICBmb3IgKHZhciBuYW1lIGluIHRoaXMpIHtcbiAgICAgICAgICAvLyBOb3Qgc3VyZSBhYm91dCB0aGUgb3B0aW1hbCBvcmRlciBvZiB0aGVzZSBjb25kaXRpb25zOlxuICAgICAgICAgIGlmIChuYW1lLmNoYXJBdCgwKSA9PT0gXCJ0XCIgJiZcbiAgICAgICAgICAgICAgaGFzT3duLmNhbGwodGhpcywgbmFtZSkgJiZcbiAgICAgICAgICAgICAgIWlzTmFOKCtuYW1lLnNsaWNlKDEpKSkge1xuICAgICAgICAgICAgdGhpc1tuYW1lXSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgc3RvcDogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmRvbmUgPSB0cnVlO1xuXG4gICAgICB2YXIgcm9vdEVudHJ5ID0gdGhpcy50cnlFbnRyaWVzWzBdO1xuICAgICAgdmFyIHJvb3RSZWNvcmQgPSByb290RW50cnkuY29tcGxldGlvbjtcbiAgICAgIGlmIChyb290UmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICB0aHJvdyByb290UmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMucnZhbDtcbiAgICB9LFxuXG4gICAgZGlzcGF0Y2hFeGNlcHRpb246IGZ1bmN0aW9uKGV4Y2VwdGlvbikge1xuICAgICAgaWYgKHRoaXMuZG9uZSkge1xuICAgICAgICB0aHJvdyBleGNlcHRpb247XG4gICAgICB9XG5cbiAgICAgIHZhciBjb250ZXh0ID0gdGhpcztcbiAgICAgIGZ1bmN0aW9uIGhhbmRsZShsb2MsIGNhdWdodCkge1xuICAgICAgICByZWNvcmQudHlwZSA9IFwidGhyb3dcIjtcbiAgICAgICAgcmVjb3JkLmFyZyA9IGV4Y2VwdGlvbjtcbiAgICAgICAgY29udGV4dC5uZXh0ID0gbG9jO1xuXG4gICAgICAgIGlmIChjYXVnaHQpIHtcbiAgICAgICAgICAvLyBJZiB0aGUgZGlzcGF0Y2hlZCBleGNlcHRpb24gd2FzIGNhdWdodCBieSBhIGNhdGNoIGJsb2NrLFxuICAgICAgICAgIC8vIHRoZW4gbGV0IHRoYXQgY2F0Y2ggYmxvY2sgaGFuZGxlIHRoZSBleGNlcHRpb24gbm9ybWFsbHkuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgICBjb250ZXh0LmFyZyA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAhISBjYXVnaHQ7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcblxuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSBcInJvb3RcIikge1xuICAgICAgICAgIC8vIEV4Y2VwdGlvbiB0aHJvd24gb3V0c2lkZSBvZiBhbnkgdHJ5IGJsb2NrIHRoYXQgY291bGQgaGFuZGxlXG4gICAgICAgICAgLy8gaXQsIHNvIHNldCB0aGUgY29tcGxldGlvbiB2YWx1ZSBvZiB0aGUgZW50aXJlIGZ1bmN0aW9uIHRvXG4gICAgICAgICAgLy8gdGhyb3cgdGhlIGV4Y2VwdGlvbi5cbiAgICAgICAgICByZXR1cm4gaGFuZGxlKFwiZW5kXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYpIHtcbiAgICAgICAgICB2YXIgaGFzQ2F0Y2ggPSBoYXNPd24uY2FsbChlbnRyeSwgXCJjYXRjaExvY1wiKTtcbiAgICAgICAgICB2YXIgaGFzRmluYWxseSA9IGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIik7XG5cbiAgICAgICAgICBpZiAoaGFzQ2F0Y2ggJiYgaGFzRmluYWxseSkge1xuICAgICAgICAgICAgaWYgKHRoaXMucHJldiA8IGVudHJ5LmNhdGNoTG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuY2F0Y2hMb2MsIHRydWUpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0NhdGNoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wcmV2IDwgZW50cnkuY2F0Y2hMb2MpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGhhbmRsZShlbnRyeS5jYXRjaExvYywgdHJ1ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0ZpbmFsbHkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwidHJ5IHN0YXRlbWVudCB3aXRob3V0IGNhdGNoIG9yIGZpbmFsbHlcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcblxuICAgIGFicnVwdDogZnVuY3Rpb24odHlwZSwgYXJnKSB7XG4gICAgICBmb3IgKHZhciBpID0gdGhpcy50cnlFbnRyaWVzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgIHZhciBlbnRyeSA9IHRoaXMudHJ5RW50cmllc1tpXTtcbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYgJiZcbiAgICAgICAgICAgIGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIikgJiZcbiAgICAgICAgICAgIHRoaXMucHJldiA8IGVudHJ5LmZpbmFsbHlMb2MpIHtcbiAgICAgICAgICB2YXIgZmluYWxseUVudHJ5ID0gZW50cnk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGZpbmFsbHlFbnRyeSAmJlxuICAgICAgICAgICh0eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICAgdHlwZSA9PT0gXCJjb250aW51ZVwiKSAmJlxuICAgICAgICAgIGZpbmFsbHlFbnRyeS50cnlMb2MgPD0gYXJnICYmXG4gICAgICAgICAgYXJnIDw9IGZpbmFsbHlFbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgIC8vIElnbm9yZSB0aGUgZmluYWxseSBlbnRyeSBpZiBjb250cm9sIGlzIG5vdCBqdW1waW5nIHRvIGFcbiAgICAgICAgLy8gbG9jYXRpb24gb3V0c2lkZSB0aGUgdHJ5L2NhdGNoIGJsb2NrLlxuICAgICAgICBmaW5hbGx5RW50cnkgPSBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmVjb3JkID0gZmluYWxseUVudHJ5ID8gZmluYWxseUVudHJ5LmNvbXBsZXRpb24gOiB7fTtcbiAgICAgIHJlY29yZC50eXBlID0gdHlwZTtcbiAgICAgIHJlY29yZC5hcmcgPSBhcmc7XG5cbiAgICAgIGlmIChmaW5hbGx5RW50cnkpIHtcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgdGhpcy5uZXh0ID0gZmluYWxseUVudHJ5LmZpbmFsbHlMb2M7XG4gICAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jb21wbGV0ZShyZWNvcmQpO1xuICAgIH0sXG5cbiAgICBjb21wbGV0ZTogZnVuY3Rpb24ocmVjb3JkLCBhZnRlckxvYykge1xuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgICAgdGhyb3cgcmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICByZWNvcmQudHlwZSA9PT0gXCJjb250aW51ZVwiKSB7XG4gICAgICAgIHRoaXMubmV4dCA9IHJlY29yZC5hcmc7XG4gICAgICB9IGVsc2UgaWYgKHJlY29yZC50eXBlID09PSBcInJldHVyblwiKSB7XG4gICAgICAgIHRoaXMucnZhbCA9IHRoaXMuYXJnID0gcmVjb3JkLmFyZztcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcInJldHVyblwiO1xuICAgICAgICB0aGlzLm5leHQgPSBcImVuZFwiO1xuICAgICAgfSBlbHNlIGlmIChyZWNvcmQudHlwZSA9PT0gXCJub3JtYWxcIiAmJiBhZnRlckxvYykge1xuICAgICAgICB0aGlzLm5leHQgPSBhZnRlckxvYztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgfSxcblxuICAgIGZpbmlzaDogZnVuY3Rpb24oZmluYWxseUxvYykge1xuICAgICAgZm9yICh2YXIgaSA9IHRoaXMudHJ5RW50cmllcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICB2YXIgZW50cnkgPSB0aGlzLnRyeUVudHJpZXNbaV07XG4gICAgICAgIGlmIChlbnRyeS5maW5hbGx5TG9jID09PSBmaW5hbGx5TG9jKSB7XG4gICAgICAgICAgdGhpcy5jb21wbGV0ZShlbnRyeS5jb21wbGV0aW9uLCBlbnRyeS5hZnRlckxvYyk7XG4gICAgICAgICAgcmVzZXRUcnlFbnRyeShlbnRyeSk7XG4gICAgICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJjYXRjaFwiOiBmdW5jdGlvbih0cnlMb2MpIHtcbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSB0cnlMb2MpIHtcbiAgICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcbiAgICAgICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgICAgdmFyIHRocm93biA9IHJlY29yZC5hcmc7XG4gICAgICAgICAgICByZXNldFRyeUVudHJ5KGVudHJ5KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRocm93bjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBUaGUgY29udGV4dC5jYXRjaCBtZXRob2QgbXVzdCBvbmx5IGJlIGNhbGxlZCB3aXRoIGEgbG9jYXRpb25cbiAgICAgIC8vIGFyZ3VtZW50IHRoYXQgY29ycmVzcG9uZHMgdG8gYSBrbm93biBjYXRjaCBibG9jay5cbiAgICAgIHRocm93IG5ldyBFcnJvcihcImlsbGVnYWwgY2F0Y2ggYXR0ZW1wdFwiKTtcbiAgICB9LFxuXG4gICAgZGVsZWdhdGVZaWVsZDogZnVuY3Rpb24oaXRlcmFibGUsIHJlc3VsdE5hbWUsIG5leHRMb2MpIHtcbiAgICAgIHRoaXMuZGVsZWdhdGUgPSB7XG4gICAgICAgIGl0ZXJhdG9yOiB2YWx1ZXMoaXRlcmFibGUpLFxuICAgICAgICByZXN1bHROYW1lOiByZXN1bHROYW1lLFxuICAgICAgICBuZXh0TG9jOiBuZXh0TG9jXG4gICAgICB9O1xuXG4gICAgICBpZiAodGhpcy5tZXRob2QgPT09IFwibmV4dFwiKSB7XG4gICAgICAgIC8vIERlbGliZXJhdGVseSBmb3JnZXQgdGhlIGxhc3Qgc2VudCB2YWx1ZSBzbyB0aGF0IHdlIGRvbid0XG4gICAgICAgIC8vIGFjY2lkZW50YWxseSBwYXNzIGl0IG9uIHRvIHRoZSBkZWxlZ2F0ZS5cbiAgICAgICAgdGhpcy5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cbiAgfTtcblxuICAvLyBSZWdhcmRsZXNzIG9mIHdoZXRoZXIgdGhpcyBzY3JpcHQgaXMgZXhlY3V0aW5nIGFzIGEgQ29tbW9uSlMgbW9kdWxlXG4gIC8vIG9yIG5vdCwgcmV0dXJuIHRoZSBydW50aW1lIG9iamVjdCBzbyB0aGF0IHdlIGNhbiBkZWNsYXJlIHRoZSB2YXJpYWJsZVxuICAvLyByZWdlbmVyYXRvclJ1bnRpbWUgaW4gdGhlIG91dGVyIHNjb3BlLCB3aGljaCBhbGxvd3MgdGhpcyBtb2R1bGUgdG8gYmVcbiAgLy8gaW5qZWN0ZWQgZWFzaWx5IGJ5IGBiaW4vcmVnZW5lcmF0b3IgLS1pbmNsdWRlLXJ1bnRpbWUgc2NyaXB0LmpzYC5cbiAgcmV0dXJuIGV4cG9ydHM7XG5cbn0oXG4gIC8vIElmIHRoaXMgc2NyaXB0IGlzIGV4ZWN1dGluZyBhcyBhIENvbW1vbkpTIG1vZHVsZSwgdXNlIG1vZHVsZS5leHBvcnRzXG4gIC8vIGFzIHRoZSByZWdlbmVyYXRvclJ1bnRpbWUgbmFtZXNwYWNlLiBPdGhlcndpc2UgY3JlYXRlIGEgbmV3IGVtcHR5XG4gIC8vIG9iamVjdC4gRWl0aGVyIHdheSwgdGhlIHJlc3VsdGluZyBvYmplY3Qgd2lsbCBiZSB1c2VkIHRvIGluaXRpYWxpemVcbiAgLy8gdGhlIHJlZ2VuZXJhdG9yUnVudGltZSB2YXJpYWJsZSBhdCB0aGUgdG9wIG9mIHRoaXMgZmlsZS5cbiAgdHlwZW9mIG1vZHVsZSA9PT0gXCJvYmplY3RcIiA/IG1vZHVsZS5leHBvcnRzIDoge31cbikpO1xuXG50cnkge1xuICByZWdlbmVyYXRvclJ1bnRpbWUgPSBydW50aW1lO1xufSBjYXRjaCAoYWNjaWRlbnRhbFN0cmljdE1vZGUpIHtcbiAgLy8gVGhpcyBtb2R1bGUgc2hvdWxkIG5vdCBiZSBydW5uaW5nIGluIHN0cmljdCBtb2RlLCBzbyB0aGUgYWJvdmVcbiAgLy8gYXNzaWdubWVudCBzaG91bGQgYWx3YXlzIHdvcmsgdW5sZXNzIHNvbWV0aGluZyBpcyBtaXNjb25maWd1cmVkLiBKdXN0XG4gIC8vIGluIGNhc2UgcnVudGltZS5qcyBhY2NpZGVudGFsbHkgcnVucyBpbiBzdHJpY3QgbW9kZSwgaW4gbW9kZXJuIGVuZ2luZXNcbiAgLy8gd2UgY2FuIGV4cGxpY2l0bHkgYWNjZXNzIGdsb2JhbFRoaXMuIEluIG9sZGVyIGVuZ2luZXMgd2UgY2FuIGVzY2FwZVxuICAvLyBzdHJpY3QgbW9kZSB1c2luZyBhIGdsb2JhbCBGdW5jdGlvbiBjYWxsLiBUaGlzIGNvdWxkIGNvbmNlaXZhYmx5IGZhaWxcbiAgLy8gaWYgYSBDb250ZW50IFNlY3VyaXR5IFBvbGljeSBmb3JiaWRzIHVzaW5nIEZ1bmN0aW9uLCBidXQgaW4gdGhhdCBjYXNlXG4gIC8vIHRoZSBwcm9wZXIgc29sdXRpb24gaXMgdG8gZml4IHRoZSBhY2NpZGVudGFsIHN0cmljdCBtb2RlIHByb2JsZW0uIElmXG4gIC8vIHlvdSd2ZSBtaXNjb25maWd1cmVkIHlvdXIgYnVuZGxlciB0byBmb3JjZSBzdHJpY3QgbW9kZSBhbmQgYXBwbGllZCBhXG4gIC8vIENTUCB0byBmb3JiaWQgRnVuY3Rpb24sIGFuZCB5b3UncmUgbm90IHdpbGxpbmcgdG8gZml4IGVpdGhlciBvZiB0aG9zZVxuICAvLyBwcm9ibGVtcywgcGxlYXNlIGRldGFpbCB5b3VyIHVuaXF1ZSBwcmVkaWNhbWVudCBpbiBhIEdpdEh1YiBpc3N1ZS5cbiAgaWYgKHR5cGVvZiBnbG9iYWxUaGlzID09PSBcIm9iamVjdFwiKSB7XG4gICAgZ2xvYmFsVGhpcy5yZWdlbmVyYXRvclJ1bnRpbWUgPSBydW50aW1lO1xuICB9IGVsc2Uge1xuICAgIEZ1bmN0aW9uKFwiclwiLCBcInJlZ2VuZXJhdG9yUnVudGltZSA9IHJcIikocnVudGltZSk7XG4gIH1cbn1cbiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgaXNPbGRJRSA9IGZ1bmN0aW9uIGlzT2xkSUUoKSB7XG4gIHZhciBtZW1vO1xuICByZXR1cm4gZnVuY3Rpb24gbWVtb3JpemUoKSB7XG4gICAgaWYgKHR5cGVvZiBtZW1vID09PSAndW5kZWZpbmVkJykge1xuICAgICAgLy8gVGVzdCBmb3IgSUUgPD0gOSBhcyBwcm9wb3NlZCBieSBCcm93c2VyaGFja3NcbiAgICAgIC8vIEBzZWUgaHR0cDovL2Jyb3dzZXJoYWNrcy5jb20vI2hhY2stZTcxZDg2OTJmNjUzMzQxNzNmZWU3MTVjMjIyY2I4MDVcbiAgICAgIC8vIFRlc3RzIGZvciBleGlzdGVuY2Ugb2Ygc3RhbmRhcmQgZ2xvYmFscyBpcyB0byBhbGxvdyBzdHlsZS1sb2FkZXJcbiAgICAgIC8vIHRvIG9wZXJhdGUgY29ycmVjdGx5IGludG8gbm9uLXN0YW5kYXJkIGVudmlyb25tZW50c1xuICAgICAgLy8gQHNlZSBodHRwczovL2dpdGh1Yi5jb20vd2VicGFjay1jb250cmliL3N0eWxlLWxvYWRlci9pc3N1ZXMvMTc3XG4gICAgICBtZW1vID0gQm9vbGVhbih3aW5kb3cgJiYgZG9jdW1lbnQgJiYgZG9jdW1lbnQuYWxsICYmICF3aW5kb3cuYXRvYik7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1lbW87XG4gIH07XG59KCk7XG5cbnZhciBnZXRUYXJnZXQgPSBmdW5jdGlvbiBnZXRUYXJnZXQoKSB7XG4gIHZhciBtZW1vID0ge307XG4gIHJldHVybiBmdW5jdGlvbiBtZW1vcml6ZSh0YXJnZXQpIHtcbiAgICBpZiAodHlwZW9mIG1lbW9bdGFyZ2V0XSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHZhciBzdHlsZVRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTsgLy8gU3BlY2lhbCBjYXNlIHRvIHJldHVybiBoZWFkIG9mIGlmcmFtZSBpbnN0ZWFkIG9mIGlmcmFtZSBpdHNlbGZcblxuICAgICAgaWYgKHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCAmJiBzdHlsZVRhcmdldCBpbnN0YW5jZW9mIHdpbmRvdy5IVE1MSUZyYW1lRWxlbWVudCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgIC8vIFRoaXMgd2lsbCB0aHJvdyBhbiBleGNlcHRpb24gaWYgYWNjZXNzIHRvIGlmcmFtZSBpcyBibG9ja2VkXG4gICAgICAgICAgLy8gZHVlIHRvIGNyb3NzLW9yaWdpbiByZXN0cmljdGlvbnNcbiAgICAgICAgICBzdHlsZVRhcmdldCA9IHN0eWxlVGFyZ2V0LmNvbnRlbnREb2N1bWVudC5oZWFkO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgLy8gaXN0YW5idWwgaWdub3JlIG5leHRcbiAgICAgICAgICBzdHlsZVRhcmdldCA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbWVtb1t0YXJnZXRdID0gc3R5bGVUYXJnZXQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1lbW9bdGFyZ2V0XTtcbiAgfTtcbn0oKTtcblxudmFyIHN0eWxlc0luRG9tID0gW107XG5cbmZ1bmN0aW9uIGdldEluZGV4QnlJZGVudGlmaWVyKGlkZW50aWZpZXIpIHtcbiAgdmFyIHJlc3VsdCA9IC0xO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgc3R5bGVzSW5Eb20ubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoc3R5bGVzSW5Eb21baV0uaWRlbnRpZmllciA9PT0gaWRlbnRpZmllcikge1xuICAgICAgcmVzdWx0ID0gaTtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiByZXN1bHQ7XG59XG5cbmZ1bmN0aW9uIG1vZHVsZXNUb0RvbShsaXN0LCBvcHRpb25zKSB7XG4gIHZhciBpZENvdW50TWFwID0ge307XG4gIHZhciBpZGVudGlmaWVycyA9IFtdO1xuXG4gIGZvciAodmFyIGkgPSAwOyBpIDwgbGlzdC5sZW5ndGg7IGkrKykge1xuICAgIHZhciBpdGVtID0gbGlzdFtpXTtcbiAgICB2YXIgaWQgPSBvcHRpb25zLmJhc2UgPyBpdGVtWzBdICsgb3B0aW9ucy5iYXNlIDogaXRlbVswXTtcbiAgICB2YXIgY291bnQgPSBpZENvdW50TWFwW2lkXSB8fCAwO1xuICAgIHZhciBpZGVudGlmaWVyID0gXCJcIi5jb25jYXQoaWQsIFwiIFwiKS5jb25jYXQoY291bnQpO1xuICAgIGlkQ291bnRNYXBbaWRdID0gY291bnQgKyAxO1xuICAgIHZhciBpbmRleCA9IGdldEluZGV4QnlJZGVudGlmaWVyKGlkZW50aWZpZXIpO1xuICAgIHZhciBvYmogPSB7XG4gICAgICBjc3M6IGl0ZW1bMV0sXG4gICAgICBtZWRpYTogaXRlbVsyXSxcbiAgICAgIHNvdXJjZU1hcDogaXRlbVszXVxuICAgIH07XG5cbiAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICBzdHlsZXNJbkRvbVtpbmRleF0ucmVmZXJlbmNlcysrO1xuICAgICAgc3R5bGVzSW5Eb21baW5kZXhdLnVwZGF0ZXIob2JqKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGVzSW5Eb20ucHVzaCh7XG4gICAgICAgIGlkZW50aWZpZXI6IGlkZW50aWZpZXIsXG4gICAgICAgIHVwZGF0ZXI6IGFkZFN0eWxlKG9iaiwgb3B0aW9ucyksXG4gICAgICAgIHJlZmVyZW5jZXM6IDFcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGlkZW50aWZpZXJzLnB1c2goaWRlbnRpZmllcik7XG4gIH1cblxuICByZXR1cm4gaWRlbnRpZmllcnM7XG59XG5cbmZ1bmN0aW9uIGluc2VydFN0eWxlRWxlbWVudChvcHRpb25zKSB7XG4gIHZhciBzdHlsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3N0eWxlJyk7XG4gIHZhciBhdHRyaWJ1dGVzID0gb3B0aW9ucy5hdHRyaWJ1dGVzIHx8IHt9O1xuXG4gIGlmICh0eXBlb2YgYXR0cmlidXRlcy5ub25jZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICB2YXIgbm9uY2UgPSB0eXBlb2YgX193ZWJwYWNrX25vbmNlX18gIT09ICd1bmRlZmluZWQnID8gX193ZWJwYWNrX25vbmNlX18gOiBudWxsO1xuXG4gICAgaWYgKG5vbmNlKSB7XG4gICAgICBhdHRyaWJ1dGVzLm5vbmNlID0gbm9uY2U7XG4gICAgfVxuICB9XG5cbiAgT2JqZWN0LmtleXMoYXR0cmlidXRlcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgc3R5bGUuc2V0QXR0cmlidXRlKGtleSwgYXR0cmlidXRlc1trZXldKTtcbiAgfSk7XG5cbiAgaWYgKHR5cGVvZiBvcHRpb25zLmluc2VydCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIG9wdGlvbnMuaW5zZXJ0KHN0eWxlKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgdGFyZ2V0ID0gZ2V0VGFyZ2V0KG9wdGlvbnMuaW5zZXJ0IHx8ICdoZWFkJyk7XG5cbiAgICBpZiAoIXRhcmdldCkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQ291bGRuJ3QgZmluZCBhIHN0eWxlIHRhcmdldC4gVGhpcyBwcm9iYWJseSBtZWFucyB0aGF0IHRoZSB2YWx1ZSBmb3IgdGhlICdpbnNlcnQnIHBhcmFtZXRlciBpcyBpbnZhbGlkLlwiKTtcbiAgICB9XG5cbiAgICB0YXJnZXQuYXBwZW5kQ2hpbGQoc3R5bGUpO1xuICB9XG5cbiAgcmV0dXJuIHN0eWxlO1xufVxuXG5mdW5jdGlvbiByZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGUpIHtcbiAgLy8gaXN0YW5idWwgaWdub3JlIGlmXG4gIGlmIChzdHlsZS5wYXJlbnROb2RlID09PSBudWxsKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgc3R5bGUucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdHlsZSk7XG59XG4vKiBpc3RhbmJ1bCBpZ25vcmUgbmV4dCAgKi9cblxuXG52YXIgcmVwbGFjZVRleHQgPSBmdW5jdGlvbiByZXBsYWNlVGV4dCgpIHtcbiAgdmFyIHRleHRTdG9yZSA9IFtdO1xuICByZXR1cm4gZnVuY3Rpb24gcmVwbGFjZShpbmRleCwgcmVwbGFjZW1lbnQpIHtcbiAgICB0ZXh0U3RvcmVbaW5kZXhdID0gcmVwbGFjZW1lbnQ7XG4gICAgcmV0dXJuIHRleHRTdG9yZS5maWx0ZXIoQm9vbGVhbikuam9pbignXFxuJyk7XG4gIH07XG59KCk7XG5cbmZ1bmN0aW9uIGFwcGx5VG9TaW5nbGV0b25UYWcoc3R5bGUsIGluZGV4LCByZW1vdmUsIG9iaikge1xuICB2YXIgY3NzID0gcmVtb3ZlID8gJycgOiBvYmoubWVkaWEgPyBcIkBtZWRpYSBcIi5jb25jYXQob2JqLm1lZGlhLCBcIiB7XCIpLmNvbmNhdChvYmouY3NzLCBcIn1cIikgOiBvYmouY3NzOyAvLyBGb3Igb2xkIElFXG5cbiAgLyogaXN0YW5idWwgaWdub3JlIGlmICAqL1xuXG4gIGlmIChzdHlsZS5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gcmVwbGFjZVRleHQoaW5kZXgsIGNzcyk7XG4gIH0gZWxzZSB7XG4gICAgdmFyIGNzc05vZGUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpO1xuICAgIHZhciBjaGlsZE5vZGVzID0gc3R5bGUuY2hpbGROb2RlcztcblxuICAgIGlmIChjaGlsZE5vZGVzW2luZGV4XSkge1xuICAgICAgc3R5bGUucmVtb3ZlQ2hpbGQoY2hpbGROb2Rlc1tpbmRleF0pO1xuICAgIH1cblxuICAgIGlmIChjaGlsZE5vZGVzLmxlbmd0aCkge1xuICAgICAgc3R5bGUuaW5zZXJ0QmVmb3JlKGNzc05vZGUsIGNoaWxkTm9kZXNbaW5kZXhdKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc3R5bGUuYXBwZW5kQ2hpbGQoY3NzTm9kZSk7XG4gICAgfVxuICB9XG59XG5cbmZ1bmN0aW9uIGFwcGx5VG9UYWcoc3R5bGUsIG9wdGlvbnMsIG9iaikge1xuICB2YXIgY3NzID0gb2JqLmNzcztcbiAgdmFyIG1lZGlhID0gb2JqLm1lZGlhO1xuICB2YXIgc291cmNlTWFwID0gb2JqLnNvdXJjZU1hcDtcblxuICBpZiAobWVkaWEpIHtcbiAgICBzdHlsZS5zZXRBdHRyaWJ1dGUoJ21lZGlhJywgbWVkaWEpO1xuICB9IGVsc2Uge1xuICAgIHN0eWxlLnJlbW92ZUF0dHJpYnV0ZSgnbWVkaWEnKTtcbiAgfVxuXG4gIGlmIChzb3VyY2VNYXAgJiYgdHlwZW9mIGJ0b2EgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgY3NzICs9IFwiXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCxcIi5jb25jYXQoYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSwgXCIgKi9cIik7XG4gIH0gLy8gRm9yIG9sZCBJRVxuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAgKi9cblxuXG4gIGlmIChzdHlsZS5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzO1xuICB9IGVsc2Uge1xuICAgIHdoaWxlIChzdHlsZS5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZS5yZW1vdmVDaGlsZChzdHlsZS5maXJzdENoaWxkKTtcbiAgICB9XG5cbiAgICBzdHlsZS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKTtcbiAgfVxufVxuXG52YXIgc2luZ2xldG9uID0gbnVsbDtcbnZhciBzaW5nbGV0b25Db3VudGVyID0gMDtcblxuZnVuY3Rpb24gYWRkU3R5bGUob2JqLCBvcHRpb25zKSB7XG4gIHZhciBzdHlsZTtcbiAgdmFyIHVwZGF0ZTtcbiAgdmFyIHJlbW92ZTtcblxuICBpZiAob3B0aW9ucy5zaW5nbGV0b24pIHtcbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrKztcbiAgICBzdHlsZSA9IHNpbmdsZXRvbiB8fCAoc2luZ2xldG9uID0gaW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMpKTtcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGUsIHN0eWxlSW5kZXgsIGZhbHNlKTtcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGUsIHN0eWxlSW5kZXgsIHRydWUpO1xuICB9IGVsc2Uge1xuICAgIHN0eWxlID0gaW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMpO1xuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZSwgb3B0aW9ucyk7XG5cbiAgICByZW1vdmUgPSBmdW5jdGlvbiByZW1vdmUoKSB7XG4gICAgICByZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGUpO1xuICAgIH07XG4gIH1cblxuICB1cGRhdGUob2JqKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlKG5ld09iaikge1xuICAgIGlmIChuZXdPYmopIHtcbiAgICAgIGlmIChuZXdPYmouY3NzID09PSBvYmouY3NzICYmIG5ld09iai5tZWRpYSA9PT0gb2JqLm1lZGlhICYmIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmVtb3ZlKCk7XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChsaXN0LCBvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9OyAvLyBGb3JjZSBzaW5nbGUtdGFnIHNvbHV0aW9uIG9uIElFNi05LCB3aGljaCBoYXMgYSBoYXJkIGxpbWl0IG9uIHRoZSAjIG9mIDxzdHlsZT5cbiAgLy8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxuXG4gIGlmICghb3B0aW9ucy5zaW5nbGV0b24gJiYgdHlwZW9mIG9wdGlvbnMuc2luZ2xldG9uICE9PSAnYm9vbGVhbicpIHtcbiAgICBvcHRpb25zLnNpbmdsZXRvbiA9IGlzT2xkSUUoKTtcbiAgfVxuXG4gIGxpc3QgPSBsaXN0IHx8IFtdO1xuICB2YXIgbGFzdElkZW50aWZpZXJzID0gbW9kdWxlc1RvRG9tKGxpc3QsIG9wdGlvbnMpO1xuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlKG5ld0xpc3QpIHtcbiAgICBuZXdMaXN0ID0gbmV3TGlzdCB8fCBbXTtcblxuICAgIGlmIChPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobmV3TGlzdCkgIT09ICdbb2JqZWN0IEFycmF5XScpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxhc3RJZGVudGlmaWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGlkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbaV07XG4gICAgICB2YXIgaW5kZXggPSBnZXRJbmRleEJ5SWRlbnRpZmllcihpZGVudGlmaWVyKTtcbiAgICAgIHN0eWxlc0luRG9tW2luZGV4XS5yZWZlcmVuY2VzLS07XG4gICAgfVxuXG4gICAgdmFyIG5ld0xhc3RJZGVudGlmaWVycyA9IG1vZHVsZXNUb0RvbShuZXdMaXN0LCBvcHRpb25zKTtcblxuICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBsYXN0SWRlbnRpZmllcnMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgX2lkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbX2ldO1xuXG4gICAgICB2YXIgX2luZGV4ID0gZ2V0SW5kZXhCeUlkZW50aWZpZXIoX2lkZW50aWZpZXIpO1xuXG4gICAgICBpZiAoc3R5bGVzSW5Eb21bX2luZGV4XS5yZWZlcmVuY2VzID09PSAwKSB7XG4gICAgICAgIHN0eWxlc0luRG9tW19pbmRleF0udXBkYXRlcigpO1xuXG4gICAgICAgIHN0eWxlc0luRG9tLnNwbGljZShfaW5kZXgsIDEpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGxhc3RJZGVudGlmaWVycyA9IG5ld0xhc3RJZGVudGlmaWVycztcbiAgfTtcbn07IiwiZnVuY3Rpb24gYWN0aW9uKHR5cGUsIHBheWxvYWQgPSB7fSkge1xuICByZXR1cm4geyB0eXBlLCBwYXlsb2FkIH07XG59XG5cbmV4cG9ydCBjb25zdCBBVVRIX0FQSV9VTlJFQUNIQUJMRSA9IFwiQVVUSF9BUElfVU5SRUFDSEFCTEVcIjtcbmV4cG9ydCBjb25zdCBBVVRIX0FQSV9SRUFDSEFCTEUgPSBcIkFVVEhfQVBJX1JFQUNIQUJMRVwiO1xuZXhwb3J0IGNvbnN0IFVTRVJfTE9HSU4gPSBcIlVTRVJfTE9HSU5cIjtcbmV4cG9ydCBjb25zdCBVU0VSX0xPR0lOX1dJVEhfQ0hBTkdFUEFTUyA9IFwiVVNFUl9MT0dJTl9XSVRIX0NIQU5HRVBBU1NcIjtcbmV4cG9ydCBjb25zdCBVU0VSX0xPR0lOX1JFU1BPTlNFID0gXCJVU0VSX0xPR0lOX1JFU1BPTlNFXCI7XG5leHBvcnQgY29uc3QgVVNFUl9BVkFJTEFCSUxJVFlfQ0hFQ0sgPSBcIlVTRVJfQVZBSUxBQklMSVRZX0NIRUNLXCI7XG5leHBvcnQgY29uc3QgVVNFUl9BVkFJTEFCSUxJVFlfQ0hFQ0tfUkVTUE9OU0UgPVxuICBcIlVTRVJfQVZBSUxBQklMSVRZX0NIRUNLX1JFU1BPTlNFXCI7XG5leHBvcnQgY29uc3QgVVNFUl9MT0dPVVQgPSBcIlVTRVJfTE9HT1VUXCI7XG5leHBvcnQgY29uc3QgVVNFUl9SRUFVVEggPSBcIlVTRVJfUkVBVVRIXCI7XG5leHBvcnQgY29uc3QgVVNFUl9DUkVBVEUgPSBcIlVTRVJfQ1JFQVRFXCI7XG5leHBvcnQgY29uc3QgVVNFUl9MT0dJTl9DT05GSUcgPSBcIlVTRVJfTE9HSU5fQ09ORklHXCI7XG5leHBvcnQgY29uc3QgVVNFUl9MT0dJTl9DT05GSUdfQ09NUExFVEUgPSBcIlVTRVJfTE9HSU5fQ09ORklHX0NPTVBMRVRFXCI7XG5leHBvcnQgY29uc3QgVVNFUl9SRVFVRVNUX1ZBTElEQVRJT04gPSBcIlVTRVJfUkVRVUVTVF9WQUxJREFUSU9OXCI7XG5leHBvcnQgY29uc3QgVVNFUl9WQUxJREFURSA9IFwiVVNFUl9WQUxJREFURVwiO1xuZXhwb3J0IGNvbnN0IFVTRVJfVkFMSURBVEVfUkVTUE9OU0UgPSBcIlVTRVJfVkFMSURBVEVfUkVTUE9OU0VcIjtcblxuZXhwb3J0IGNvbnN0IHVzZXJuYW1lQXZhaWxhYmxlID0gKHBheWxvYWQpID0+XG4gIGFjdGlvbihVU0VSX0FWQUlMQUJJTElUWV9DSEVDSywgcGF5bG9hZCk7XG5leHBvcnQgY29uc3QgdXNlckxvZ2luQ29uZmlnID0gKHBheWxvYWQpID0+IGFjdGlvbihVU0VSX0xPR0lOX0NPTkZJRywgcGF5bG9hZCk7XG5leHBvcnQgY29uc3QgdXNlckNyZWF0ZSA9IChwYXlsb2FkKSA9PiBhY3Rpb24oVVNFUl9DUkVBVEUsIHBheWxvYWQpO1xuZXhwb3J0IGNvbnN0IHVzZXJMb2dpbiA9IChwYXlsb2FkKSA9PiBhY3Rpb24oVVNFUl9MT0dJTiwgcGF5bG9hZCk7XG5leHBvcnQgY29uc3QgdXNlckxvZ291dCA9ICgpID0+IGFjdGlvbihVU0VSX0xPR09VVCk7XG5leHBvcnQgY29uc3QgYXV0aFVucmVhY2hhYmxlID0gKCkgPT4gYWN0aW9uKEFVVEhfQVBJX1VOUkVBQ0hBQkxFKTtcbmV4cG9ydCBjb25zdCB1c2VyUmVxdWVzdFZhbGlkYXRpb24gPSAocGF5bG9hZCkgPT5cbiAgYWN0aW9uKFVTRVJfUkVRVUVTVF9WQUxJREFUSU9OLCBwYXlsb2FkKTtcbmV4cG9ydCBjb25zdCB1c2VyVmFsaWRhdGUgPSAocGF5bG9hZCkgPT4gYWN0aW9uKFVTRVJfVkFMSURBVEUsIHBheWxvYWQpO1xuZXhwb3J0IGNvbnN0IHVzZXJMb2dpbkFuZENoYW5nZVBhc3N3b3JkID0gKHBheWxvYWQpID0+XG4gIGFjdGlvbihVU0VSX0xPR0lOX1dJVEhfQ0hBTkdFUEFTUywgcGF5bG9hZCk7XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgU2lnbkluQ29udGV4dCB9IGZyb20gXCIuL2luZGV4LmpzXCI7XG5cbmV4cG9ydCBjb25zdCBBY2NvdW50SW52YWxpZCA9ICgpID0+IHtcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoU2lnbkluQ29udGV4dCk7XG4gIGNvbnN0IHsgc3R5bGVzLCBvbkNsb3NlTW9kYWwsIG9uUmV2YWxpZGF0ZSwgb25Hb0JhY2sgfSA9IGNvbnRleHQ7XG5cbiAgcmV0dXJuIChcbiAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuY29udGFpbmVyfT5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy50aXRsZX0+VmFsaWRhdGUgWW91ciBBY2NvdW50PC9kaXY+XG4gICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuZ29CYWNrfSBvbkNsaWNrPXtvbkdvQmFja30+XG4gICAgICAgIE5vdCB5b3VyIGVtYWlsP3tcIiBcIn1cbiAgICAgICAgPHNwYW4gc3R5bGU9e3sgdGV4dERlY29yYXRpb246IFwidW5kZXJsaW5lXCIgfX0+R28gQmFjazwvc3Bhbj4uXG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5zdWJ0aXRsZX0+XG4gICAgICAgIFRvIHVzZSB0aGlzIGFjY291bnQgeW91IG11c3QgdmFsaWRhdGUgaXQgYnkgY2xpY2tpbmcgdGhlIGxpbmsgd2hpY2ggaGFzXG4gICAgICAgIGJlZW4gZW1haWxlZCB0byB5b3UuIElmIHlvdSBoYXZlIG5vdCByZWNlaXZlZCB5b3VyIHZhbGlkYXRpb24gZW1haWwsXG4gICAgICAgIHBsZWFzZSBjaGVjayB5b3VyIHNwYW0gZm9sZGVyLlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IHN0eWxlPXt7IC4uLnN0eWxlcy5yb3csIG1hcmdpblRvcDogXCIxcmVtXCIgfX0+XG4gICAgICAgIDxpbnB1dFxuICAgICAgICAgIHN0eWxlPXtzdHlsZXMuYnV0dG9ufVxuICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgIHZhbHVlPVwiUmUtU2VuZCBWYWxpZGF0aW9uIEVtYWlsXCJcbiAgICAgICAgICBvbkNsaWNrPXtvblJldmFsaWRhdGV9XG4gICAgICAgIC8+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICBzdHlsZT17c3R5bGVzLmJ1dHRvbn1cbiAgICAgICAgICAgIHR5cGU9XCJidXR0b25cIlxuICAgICAgICAgICAgdmFsdWU9XCJPa1wiXG4gICAgICAgICAgICBvbkNsaWNrPXtvbkNsb3NlTW9kYWx9XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbn07XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgRklFTERUWVBFLCBGb3JtRWxlbWVudCB9IGZyb20gXCJAdGhlcGxhY2VsYWIvdWlcIjtcbmltcG9ydCB7IFNpZ25JbkNvbnRleHQgfSBmcm9tIFwiLi9pbmRleC5qc1wiO1xuXG5leHBvcnQgY29uc3QgQXNrRm9yRW1haWwgPSAoKSA9PiB7XG4gIGNvbnN0IGNvbnRleHQgPSB1c2VDb250ZXh0KFNpZ25JbkNvbnRleHQpO1xuICBjb25zdCB7XG4gICAgc3R5bGVzLFxuICAgIG9uQ2hhbmdlLFxuICAgIG9uQ29udGludWUsXG4gICAgZm9ybVZhbHVlcyxcbiAgICBoZWxwTGluayxcbiAgICBpc1dvcmtpbmcsXG4gICAgaGFzQ2xvc2VCdXR0b24sXG4gIH0gPSBjb250ZXh0O1xuXG4gIHJldHVybiAoXG4gICAgPGZvcm0gb25TdWJtaXQ9e29uQ29udGludWV9PlxuICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmNvbnRhaW5lcn0+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy50aXRsZX0+U2lnbiBJbiBUbyBDb250aW51ZTwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuc3VidGl0bGV9PlxuICAgICAgICAgIFNpZ24gaW4gb3IgcmVnaXN0ZXIgd2l0aCB5b3VyIGVtYWlsIGFkZHJlc3NcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PlxuICAgICAgICAgIDxGb3JtRWxlbWVudFxuICAgICAgICAgICAgbGFiZWw9e1wiRW1haWwgQWRkcmVzczpcIn1cbiAgICAgICAgICAgIHN0eWxlPXtzdHlsZXN9XG4gICAgICAgICAgICBpZD17XCJlbWFpbFwifVxuICAgICAgICAgICAgdHlwZT17RklFTERUWVBFLkVNQUlMfVxuICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMucm93fT5cbiAgICAgICAgICA8YnV0dG9uIHN0eWxlPXtzdHlsZXMuYnV0dG9ufSB0eXBlPVwic3VibWl0XCI+XG4gICAgICAgICAgICBDb250aW51ZVxuICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLnJvd30+e2hlbHBMaW5rfTwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9mb3JtPlxuICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwgeyBjcmVhdGVDb250ZXh0LCB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBGSUVMRFRZUEUsIEZvcm1FbGVtZW50IH0gZnJvbSBcIkB0aGVwbGFjZWxhYi91aVwiO1xuaW1wb3J0IHsgU2lnbkluQ29udGV4dCB9IGZyb20gXCIuL2luZGV4LmpzXCI7XG5cbmV4cG9ydCBjb25zdCBDb25maXJtTmV3QWNjb3VudCA9ICgpID0+IHtcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoU2lnbkluQ29udGV4dCk7XG4gIGNvbnN0IHsgb25DbG9zZU1vZGFsIH0gPSBjb250ZXh0O1xuXG4gIHJldHVybiAoXG4gICAgPGRpdiBzdHlsZT17eyBkaXNwbGF5OiBcImZsZXhcIiB9fT5cbiAgICAgIDxkaXYgc3R5bGU9e3sgZGlzcGxheTogXCJmbGV4XCIsIGZsZXhEaXJlY3Rpb246IFwiY29sdW1uXCIsIGhlaWdodDogXCIxMDAlXCIgfX0+XG4gICAgICAgIDxkaXY+WW91ciBhY2NvdW50IGhhcyBiZWVuIGNyZWF0ZWQhPC9kaXY+XG4gICAgICAgIDxkaXY+Q2hlY2sgeW91ciBlbWFpbCB0byBjb21wbGV0ZSByZWdpc3RyYXRpb24uPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGlucHV0IHR5cGU9XCJidXR0b25cIiB2YWx1ZT1cIk9rXCIgb25DbGljaz17b25DbG9zZU1vZGFsfT48L2lucHV0PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwgeyBjcmVhdGVDb250ZXh0LCB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBGSUVMRFRZUEUsIEZvcm1FbGVtZW50IH0gZnJvbSBcIkB0aGVwbGFjZWxhYi91aVwiO1xuaW1wb3J0IHsgU2lnbkluQ29udGV4dCB9IGZyb20gXCIuL2luZGV4LmpzXCI7XG5cbmV4cG9ydCBjb25zdCBOZXdTaWdudXAgPSAoKSA9PiB7XG4gIGNvbnN0IGNvbnRleHQgPSB1c2VDb250ZXh0KFNpZ25JbkNvbnRleHQpO1xuICBjb25zdCB7IHN0eWxlcywgb25DaGFuZ2UsIG9uQ29udGludWUsIGZvcm1WYWx1ZXMsIG9uR29CYWNrIH0gPSBjb250ZXh0O1xuXG4gIHJldHVybiAoXG4gICAgPGZvcm0gb25TdWJtaXQ9e29uQ29udGludWV9PlxuICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmNvbnRhaW5lcn0+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy50aXRsZX0+Q3JlYXRlIHlvdXIgYWNjb3VudDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuZ29CYWNrfSBvbkNsaWNrPXtvbkdvQmFja30+XG4gICAgICAgICAgTm90IHlvdXIgZW1haWw/e1wiIFwifVxuICAgICAgICAgIDxzcGFuIHN0eWxlPXt7IHRleHREZWNvcmF0aW9uOiBcInVuZGVybGluZVwiIH19PkdvIEJhY2s8L3NwYW4+LlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLnJvd30+XG4gICAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmxhYmVsfT5FbWFpbDo8L2Rpdj5cbiAgICAgICAgICA8ZGl2Pntmb3JtVmFsdWVzLnN1Ym1pdHRlZF9lbWFpbH08L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PlxuICAgICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5sYWJlbH0+TmFtZTo8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPEZvcm1FbGVtZW50XG4gICAgICAgICAgICAgIGlkPXtcIm5hbWVcIn1cbiAgICAgICAgICAgICAgdHlwZT17RklFTERUWVBFLlRFWFR9XG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXt7IC4uLnN0eWxlcy5yb3csIG1hcmdpblRvcDogXCIycmVtXCIgfX0+XG4gICAgICAgICAgPGJ1dHRvbiBzdHlsZT17c3R5bGVzLmJ1dHRvbn0gdHlwZT1cInN1Ym1pdFwiPlxuICAgICAgICAgICAgUmVnaXN0ZXJcbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Zvcm0+XG4gICk7XG59O1xuIiwiaW1wb3J0IFJlYWN0LCB7IGNyZWF0ZUNvbnRleHQsIHVzZUNvbnRleHQgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IEZJRUxEVFlQRSwgRm9ybUVsZW1lbnQgfSBmcm9tIFwiQHRoZXBsYWNlbGFiL3VpXCI7XG5pbXBvcnQgeyBTaWduSW5Db250ZXh0IH0gZnJvbSBcIi4vaW5kZXguanNcIjtcblxuZXhwb3J0IGNvbnN0IFJlc2V0UGFzcyA9ICgpID0+IHtcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoU2lnbkluQ29udGV4dCk7XG4gIGNvbnN0IHtcbiAgICBzdHlsZXMsXG4gICAgb25DaGFuZ2UsXG4gICAgb25Db250aW51ZSxcbiAgICBmb3JtVmFsdWVzLFxuICAgIGhlbHBMaW5rLFxuICAgIGJhZFBhc3NMaW1pdCxcbiAgICBwYXNzd29yZFRyaWVzLFxuICAgIGlzV29ya2luZyxcbiAgICBvbkdvQmFjayxcbiAgICBvblJlc2V0LFxuICB9ID0gY29udGV4dDtcblxuICByZXR1cm4gKFxuICAgIDxmb3JtIG9uU3VibWl0PXtvbkNvbnRpbnVlfT5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5jb250YWluZXJ9PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMudGl0bGV9PlxuICAgICAgICAgIFdlbGNvbWUgYmFjayB7Zm9ybVZhbHVlcy5zdWJtaXR0ZWRfZW1haWx9IVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmdvQmFja30gb25DbGljaz17b25Hb0JhY2t9PlxuICAgICAgICAgIE5vdCB5b3VyIGVtYWlsP3tcIiBcIn1cbiAgICAgICAgICA8c3BhbiBzdHlsZT17eyB0ZXh0RGVjb3JhdGlvbjogXCJ1bmRlcmxpbmVcIiB9fT5HbyBCYWNrPC9zcGFuPi5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5zdWJ0aXRsZX0+XG4gICAgICAgICAgUGxlYXNlIHVwZGF0ZSB5b3VyIHBhc3N3b3JkIHRvIGNvbnRpbnVlXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMucm93fT5cbiAgICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuY29sfT5cbiAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgc3R5bGU9e1xuICAgICAgICAgICAgICAgIGJhZFBhc3NMaW1pdCA8IHBhc3N3b3JkVHJpZXMgJiYgIWlzV29ya2luZ1xuICAgICAgICAgICAgICAgICAgPyBzdHlsZXMubGFiZWxfYmFkXG4gICAgICAgICAgICAgICAgICA6IHN0eWxlcy5sYWJlbFxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIEN1cnJlbnQgUGFzc3dvcmQ6XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxGb3JtRWxlbWVudFxuICAgICAgICAgICAgICAgIGlkPXtcInBhc3N3b3JkXCJ9XG4gICAgICAgICAgICAgICAgdHlwZT17RklFTERUWVBFLlBBU1NXT1JEfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPGRpdj5OZXcgUGFzc3dvcmQ6PC9kaXY+XG4gICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICA8Rm9ybUVsZW1lbnRcbiAgICAgICAgICAgICAgICBpZD17XCJuZXdfcGFzc3dvcmRcIn1cbiAgICAgICAgICAgICAgICB0eXBlPXtGSUVMRFRZUEUuUEFTU1dPUkR9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICA8ZGl2Pk5ldyBQYXNzd29yZCBBZ2Fpbjo8L2Rpdj5cbiAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgIDxGb3JtRWxlbWVudFxuICAgICAgICAgICAgICAgIGlkPXtcIm5ld19wYXNzd29yZF9jb25maXJtXCJ9XG4gICAgICAgICAgICAgICAgdHlwZT17RklFTERUWVBFLlBBU1NXT1JEfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBzdHlsZT17eyAuLi5zdHlsZXMucm93LCBtYXJnaW5Ub3A6IFwiMXJlbVwiIH19PlxuICAgICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5sYWJlbH0+UmVtZW1iZXIgTWU6PC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxGb3JtRWxlbWVudFxuICAgICAgICAgICAgICBpZD17XCJleHRlbmRlZFNlc3Npb25cIn1cbiAgICAgICAgICAgICAgdmFsdWU9e2Zvcm1WYWx1ZXNbXCJleHRlbmRlZFNlc3Npb25cIl19XG4gICAgICAgICAgICAgIHR5cGU9e0ZJRUxEVFlQRS5DSEVDS0JPWH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PlxuICAgICAgICAgIDxidXR0b24gc3R5bGU9e3N0eWxlcy5idXR0b259IHR5cGU9XCJzdWJtaXRcIj5cbiAgICAgICAgICAgIFNpZ24gSW5cbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIHtiYWRQYXNzTGltaXQgPCBwYXNzd29yZFRyaWVzICYmIChcbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPGEgb25DbGljaz17b25SZXNldH0+cmVzZXQgYWNjb3VudDwvYT5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKX1cbiAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLnJvd30+e2hlbHBMaW5rfTwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9mb3JtPlxuICApO1xufTtcbiIsImltcG9ydCBSZWFjdCwgeyB1c2VDb250ZXh0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBTaWduSW5Db250ZXh0IH0gZnJvbSBcIi4vaW5kZXguanNcIjtcbmltcG9ydCB7IHVzZURpc3BhdGNoIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgeyB1c2VyTG9nb3V0IH0gZnJvbSBcIi9zcmMvYWN0aW9ucy5qc1wiO1xuXG5leHBvcnQgY29uc3QgU2lnbk91dCA9ICgpID0+IHtcbiAgY29uc3QgY29udGV4dCA9IHVzZUNvbnRleHQoU2lnbkluQ29udGV4dCk7XG4gIGNvbnN0IGRpc3BhdGNoID0gdXNlRGlzcGF0Y2goKTtcbiAgY29uc3QgeyBzdHlsZXMsIG9uQ2xvc2VNb2RhbCwgaGVscExpbmsgfSA9IGNvbnRleHQ7XG5cbiAgcmV0dXJuIChcbiAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuY29udGFpbmVyfT5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy50aXRsZX0+U2lnbiBPdXQ8L2Rpdj5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5zdWJ0aXRsZX0+RG8geW91IHJlYWxseSB3YW50IHRvIHNpZ24gb3V0PzwvZGl2PlxuXG4gICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMucm93fT5cbiAgICAgICAgPGRpdiBzdHlsZT17eyBtYXJnaW5SaWdodDogXCIxcmVtXCIgfX0+XG4gICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgb25DbGljaz17KCkgPT4gZGlzcGF0Y2godXNlckxvZ291dCgpKX1cbiAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMuYnV0dG9ufVxuICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAgU2lnbiBPdXRcbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXtvbkNsb3NlTW9kYWx9IHN0eWxlPXtzdHlsZXMuYnV0dG9ufSB0eXBlPVwic3VibWl0XCI+XG4gICAgICAgICAgICBDYW5jZWxcbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PntoZWxwTGlua308L2Rpdj5cbiAgICA8L2Rpdj5cbiAgKTtcbn07XG4iLCJpbXBvcnQgUmVhY3QsIHsgY3JlYXRlQ29udGV4dCwgdXNlQ29udGV4dCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgRklFTERUWVBFLCBGb3JtRWxlbWVudCB9IGZyb20gXCJAdGhlcGxhY2VsYWIvdWlcIjtcbmltcG9ydCB7IFNpZ25JbkNvbnRleHQgfSBmcm9tIFwiLi9pbmRleC5qc1wiO1xuXG5leHBvcnQgY29uc3QgV2VsY29tZUJhY2sgPSAoKSA9PiB7XG4gIGNvbnN0IGNvbnRleHQgPSB1c2VDb250ZXh0KFNpZ25JbkNvbnRleHQpO1xuICBjb25zdCB7XG4gICAgc3R5bGVzLFxuICAgIG9uQ2hhbmdlLFxuICAgIG9uQ29udGludWUsXG4gICAgZm9ybVZhbHVlcyxcbiAgICBoZWxwTGluayxcbiAgICBiYWRQYXNzTGltaXQsXG4gICAgcGFzc3dvcmRUcmllcyxcbiAgICBpc1dvcmtpbmcsXG4gICAgb25Hb0JhY2ssXG4gICAgb25SZXNldCxcbiAgfSA9IGNvbnRleHQ7XG5cbiAgcmV0dXJuIChcbiAgICA8Zm9ybSBvblN1Ym1pdD17b25Db250aW51ZX0+XG4gICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuY29udGFpbmVyfT5cbiAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLnRpdGxlfT5cbiAgICAgICAgICBXZWxjb21lIGJhY2sge2Zvcm1WYWx1ZXMuc3VibWl0dGVkX2VtYWlsfSFcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5nb0JhY2t9IG9uQ2xpY2s9e29uR29CYWNrfT5cbiAgICAgICAgICBOb3QgeW91ciBlbWFpbD97XCIgXCJ9XG4gICAgICAgICAgPHNwYW4gc3R5bGU9e3sgdGV4dERlY29yYXRpb246IFwidW5kZXJsaW5lXCIgfX0+R28gQmFjazwvc3Bhbj4uXG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMuc3VidGl0bGV9PlxuICAgICAgICAgIEdyZWF0IHRvIHNlZSB5b3UgYWdhaW4sXG4gICAgICAgICAgPGJyIC8+IGVudGVyIHlvdXIgcGFzc3dvcmQgdG8gY29udGludWVcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIHN0eWxlPXtcbiAgICAgICAgICAgICAgYmFkUGFzc0xpbWl0IDwgcGFzc3dvcmRUcmllcyAmJiAhaXNXb3JraW5nXG4gICAgICAgICAgICAgICAgPyBzdHlsZXMubGFiZWxfYmFkXG4gICAgICAgICAgICAgICAgOiBzdHlsZXMubGFiZWxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICA+XG4gICAgICAgICAgICBQYXNzd29yZDpcbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgPEZvcm1FbGVtZW50XG4gICAgICAgICAgICAgIGlkPXtcInBhc3N3b3JkXCJ9XG4gICAgICAgICAgICAgIHR5cGU9e0ZJRUxEVFlQRS5QQVNTV09SRH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdiBzdHlsZT17eyAuLi5zdHlsZXMucm93LCBtYXJnaW5Ub3A6IFwiMXJlbVwiIH19PlxuICAgICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5sYWJlbH0+UmVtZW1iZXIgTWU6PC9kaXY+XG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxGb3JtRWxlbWVudFxuICAgICAgICAgICAgICBpZD17XCJleHRlbmRlZFNlc3Npb25cIn1cbiAgICAgICAgICAgICAgdmFsdWU9e2Zvcm1WYWx1ZXNbXCJleHRlbmRlZFNlc3Npb25cIl19XG4gICAgICAgICAgICAgIHR5cGU9e0ZJRUxEVFlQRS5DSEVDS0JPWH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxkaXYgc3R5bGU9e3N0eWxlcy5yb3d9PlxuICAgICAgICAgIDxidXR0b24gc3R5bGU9e3N0eWxlcy5idXR0b259IHR5cGU9XCJzdWJtaXRcIj5cbiAgICAgICAgICAgIFNpZ24gSW5cbiAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAge2JhZFBhc3NMaW1pdCA8IHBhc3N3b3JkVHJpZXMgJiYgKFxuICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICA8YSBvbkNsaWNrPXtvblJlc2V0fT5yZXNldCBhY2NvdW50PC9hPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgICA8ZGl2IHN0eWxlPXtzdHlsZXMucm93fT57aGVscExpbmt9PC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Zvcm0+XG4gICk7XG59O1xuIiwiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCBjcmVhdGVDb250ZXh0LCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IHVzZVNlbGVjdG9yLCB1c2VEaXNwYXRjaCB9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xuaW1wb3J0IHtcbiAgdXNlcm5hbWVBdmFpbGFibGUsXG4gIHVzZXJMb2dpbixcbiAgdXNlckNyZWF0ZSxcbiAgdXNlclJlcXVlc3RWYWxpZGF0aW9uLFxuICB1c2VyTG9naW5BbmRDaGFuZ2VQYXNzd29yZCxcbn0gZnJvbSBcIi9zcmMvYWN0aW9ucy5qc1wiO1xuaW1wb3J0IHsgU3Bpbm5lciB9IGZyb20gXCIvc3JjL2NvbXBvbmVudHMvU3Bpbm5lclwiO1xuaW1wb3J0IHsgTmV3U2lnbnVwIH0gZnJvbSBcIi4vTmV3U2lnbnVwXCI7XG5pbXBvcnQgeyBBc2tGb3JFbWFpbCB9IGZyb20gXCIuL0Fza0ZvckVtYWlsXCI7XG5pbXBvcnQgeyBTaWduT3V0IH0gZnJvbSBcIi4vU2lnbk91dFwiO1xuaW1wb3J0IHsgV2VsY29tZUJhY2sgfSBmcm9tIFwiLi9XZWxjb21lQmFja1wiO1xuaW1wb3J0IHsgQWNjb3VudEludmFsaWQgfSBmcm9tIFwiLi9BY2NvdW50SW52YWxpZFwiO1xuaW1wb3J0IHsgUmVzZXRQYXNzIH0gZnJvbSBcIi4vUmVzZXRQYXNzXCI7XG5pbXBvcnQgeyBDb25maXJtTmV3QWNjb3VudCB9IGZyb20gXCIuL0NvbmZpcm1OZXdBY2NvdW50XCI7XG5cbmV4cG9ydCBjb25zdCBTaWduSW5Db250ZXh0ID0gY3JlYXRlQ29udGV4dCgpO1xuXG5leHBvcnQgY29uc3QgUGFnZXMgPSAoe1xuICBoZWxwTGluayxcbiAgb25DbG9zZU1vZGFsLFxuICBwcmVmaWxsLFxuICBvblN0YXR1cyxcbiAgc3R5bGUgPSB7fSxcbiAgaXNBdXRoZW50aWNhdGVkLFxuICBoYXNDbG9zZUJ1dHRvbixcbn0pID0+IHtcbiAgY29uc3Qgc3R5bGVzID0ge1xuICAgIG92ZXJsYXk6IHtcbiAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwid2hpdGVcIixcbiAgICAgIGhlaWdodDogXCIxMDAlXCIsXG4gICAgICB3aWR0aDogXCIxMDAlXCIsXG4gICAgICB6SW5kZXg6IDEsXG4gICAgICBvcGFjaXR5OiAxLjAsXG4gICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICB9LFxuICAgIGdvQmFjazoge1xuICAgICAgbWFyZ2luOiBcIjFyZW1cIixcbiAgICAgIG9wYWNpdHk6IFwiMC41XCIsXG4gICAgfSxcbiAgICBjb250YWluZXI6IHtcbiAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcbiAgICAgIHBhZGRpbmc6IFwiMHJlbVwiLFxuICAgICAgbWFyZ2luOiBcImF1dG9cIixcbiAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICBib3R0b206IDAsXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAwLFxuICAgICAgcmlnaHQ6IDAsXG4gICAgICAuLi5zdHlsZS5jb250YWluZXIsXG4gICAgfSxcbiAgICBsYWJlbDoge1xuICAgICAgZm9udFdlaWdodDogXCI5MDBcIixcbiAgICAgIG1hcmdpblJpZ2h0OiBcIi4ycmVtXCIsXG4gICAgICAuLi5zdHlsZS5sYWJlbCxcbiAgICB9LFxuICAgIGxhYmVsX2JhZDoge1xuICAgICAgY29sb3I6IFwicmVkXCIsXG4gICAgICBmb250V2VpZ2h0OiBcIjkwMFwiLFxuICAgICAgLi4uc3R5bGUubGFiZWxfYmFkLFxuICAgIH0sXG4gICAgcm93OiB7XG4gICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgIGZsZXhEaXJlY3Rpb246IFwicm93XCIsXG4gICAgICBhbGlnbkl0ZW1zOiBcImNlbnRlclwiLFxuICAgICAganVzdGlmeUNvbnRlbnQ6IFwiY2VudGVyXCIsXG4gICAgfSxcbiAgICB0aXRsZToge1xuICAgICAgZm9udFNpemU6IFwiMS41cmVtXCIsXG4gICAgICBtYXJnaW46IFwiMCAwIDFyZW0gMFwiLFxuICAgICAgLi4uc3R5bGUudGl0bGUsXG4gICAgfSxcbiAgICBzdWJ0aXRsZToge1xuICAgICAgZm9udFNpemU6IFwiMXJlbVwiLFxuICAgICAgbWFyZ2luOiBcIjAgMCAxcmVtIDBcIixcbiAgICAgIC4uLnN0eWxlLnN1YnRpdGxlLFxuICAgIH0sXG4gICAgYnV0dG9uOiB7XG4gICAgICBoZWlnaHQ6IFwiMnJlbVwiLFxuICAgICAgYm9yZGVyOiBcIjFweCBzb2xpZCBncmV5XCIsXG4gICAgICBib3JkZXJSYWRpdXM6IFwiMC4zcmVtXCIsXG4gICAgICAuLi5zdHlsZS5idXR0b24sXG4gICAgfSxcbiAgfTtcbiAgY29uc3QgZGlzcGF0Y2ggPSB1c2VEaXNwYXRjaCgpO1xuICBjb25zdCBpc1dvcmtpbmcgPSB1c2VTZWxlY3RvcigocmVkdXgpID0+IHJlZHV4LmlzV29ya2luZyk7XG4gIGNvbnN0IGF2YWlsYWJsZVVzZXJuYW1lID0gdXNlU2VsZWN0b3IoKHJlZHV4KSA9PiByZWR1eC5hdmFpbGFibGVVc2VybmFtZSk7XG4gIGNvbnN0IFtmb3JtVmFsdWVzLCBzZXRGb3JtVmFsdWVzXSA9IHVzZVN0YXRlKHtcbiAgICBleHRlbmRlZFNlc3Npb246IHRydWUsXG4gIH0pO1xuXG4gIC8vIEhvdyBtYW55IHBhc3N3b3JkIHRyaWVzIGFsbG93ZWRcbiAgY29uc3QgcGFzc3dvcmRUcmllcyA9IDM7XG4gIGNvbnN0IFtiYWRQYXNzTGltaXQsIHNldEJhZFBhc3NMaW1pdF0gPSB1c2VTdGF0ZShwYXNzd29yZFRyaWVzKTtcbiAgaWYgKGJhZFBhc3NMaW1pdCA9PT0gMCkge1xuICAgIHNldEZvcm1WYWx1ZXMoe30pO1xuICAgIHNldEJhZFBhc3NMaW1pdChwYXNzd29yZFRyaWVzKTtcbiAgfVxuXG4gIC8vIENhbGxlZCBpbmRpcmVjdGx5IHZpYSB1c2VFZmZlY3QgdG8gYXZvaWQgXCJ1cGRhdGUgd2hpbGUgcmVuZGVyXCIgaG9vayBpc3N1ZVxuICBsZXQgc3RhdHVzTWVzc2FnZTtcbiAgY29uc3Qgb25Qb3N0U3RhdHVzID0gKG1lc3NhZ2UpID0+IHtcbiAgICBpZiAodHlwZW9mIG9uU3RhdHVzID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICBvblN0YXR1cyh7XG4gICAgICAgIGljb246IFwiZW52ZWxvcGVcIixcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiBcIiM0YzZkODlcIixcbiAgICAgICAgbWVzc2FnZSxcbiAgICAgIH0pO1xuICB9O1xuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChzdGF0dXNNZXNzYWdlICYmIHR5cGVvZiBvblN0YXR1cyA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICBvblBvc3RTdGF0dXMoc3RhdHVzTWVzc2FnZSk7XG4gICAgICBvbkNsb3NlTW9kYWwoKTtcbiAgICB9XG4gIH0pO1xuXG4gIC8vIEVpdGhlciBwcmVmaWxsIE9SIGxvb2t1cFxuICBjb25zdCBbYWNjb3VudEV4aXN0cywgc2V0QWNjb3VudEV4aXN0c10gPSB1c2VTdGF0ZShmYWxzZSk7XG4gIGNvbnN0IFthY2NvdW50VmFsaWQsIHNldEFjY291bnRWYWxpZF0gPSB1c2VTdGF0ZShmYWxzZSk7XG4gIGNvbnN0IFthY2NvdW50TmVlZHNSZXNldCwgc2V0QWNjb3VudE5lZWRzUmVzZXRdID0gdXNlU3RhdGUoZmFsc2UpO1xuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChhdmFpbGFibGVVc2VybmFtZSkge1xuICAgICAgc2V0QWNjb3VudEV4aXN0cyghYXZhaWxhYmxlVXNlcm5hbWUuYXZhaWxhYmxlKTtcbiAgICAgIHNldEFjY291bnRWYWxpZChhdmFpbGFibGVVc2VybmFtZS52YWxpZCk7XG4gICAgICBzZXRBY2NvdW50TmVlZHNSZXNldChhdmFpbGFibGVVc2VybmFtZS5yZXNldCk7XG4gICAgfVxuICB9LCBbYXZhaWxhYmxlVXNlcm5hbWVdKTtcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBpZiAocHJlZmlsbCkge1xuICAgICAgc2V0Rm9ybVZhbHVlcyh7IGVtYWlsOiBwcmVmaWxsLmVtYWlsLCBzdWJtaXR0ZWRfZW1haWw6IHByZWZpbGwuZW1haWwgfSk7XG4gICAgICBzZXRBY2NvdW50RXhpc3RzKCFwcmVmaWxsLmF2YWlsYWJsZSk7XG4gICAgICBzZXRBY2NvdW50VmFsaWQocHJlZmlsbC52KTtcbiAgICAgIHNldEFjY291bnROZWVkc1Jlc2V0KHByZWZpbGwucik7XG4gICAgfVxuICB9LCBbcHJlZmlsbF0pO1xuXG4gIC8vIEhhbmRsZXMgZWFjaCBwYWdlXG4gIGNvbnN0IFtsb2dpblN0YWdlLCBzZXRMb2dpblN0YWdlXSA9IHVzZVN0YXRlKDApO1xuICBjb25zdCBvbkNvbnRpbnVlID0gKGUpID0+IHtcbiAgICBlPy5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGNvbnN0IG5ld0Zvcm1WYWx1ZXMgPSB7XG4gICAgICAuLi5mb3JtVmFsdWVzLFxuICAgICAgc3VibWl0dGVkX2VtYWlsOiBmb3JtVmFsdWVzW1wiZW1haWxcIl0sXG4gICAgICBzdWJtaXR0ZWRfcGFzc3dvcmQ6IGZvcm1WYWx1ZXNbXCJwYXNzd29yZFwiXSxcbiAgICAgIHN1Ym1pdHRlZF9uYW1lOiBmb3JtVmFsdWVzW1wibmFtZVwiXSxcbiAgICB9O1xuICAgIHNldEZvcm1WYWx1ZXMobmV3Rm9ybVZhbHVlcyk7XG4gICAgaWYgKFxuICAgICAgbmV3Rm9ybVZhbHVlcy5zdWJtaXR0ZWRfZW1haWw/Lmxlbmd0aCA+IDAgJiZcbiAgICAgIG5ld0Zvcm1WYWx1ZXMuc3VibWl0dGVkX3Bhc3N3b3JkPy5sZW5ndGggPiAwXG4gICAgKSB7XG4gICAgICBzZXRCYWRQYXNzTGltaXQoYmFkUGFzc0xpbWl0IC0gMSk7XG4gICAgICBzZXRGb3JtVmFsdWVzKHsgLi4ubmV3Rm9ybVZhbHVlcywgc3VibWl0dGVkX3Bhc3N3b3JkOiBcIlwiIH0pO1xuICAgICAgaWYgKGFjY291bnROZWVkc1Jlc2V0KSB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICBmb3JtVmFsdWVzLm5ld19wYXNzd29yZCAmJlxuICAgICAgICAgIGZvcm1WYWx1ZXMubmV3X3Bhc3N3b3JkX2NvbmZpcm0gPT09IGZvcm1WYWx1ZXMubmV3X3Bhc3N3b3JkXG4gICAgICAgICkge1xuICAgICAgICAgIGRpc3BhdGNoKFxuICAgICAgICAgICAgdXNlckxvZ2luQW5kQ2hhbmdlUGFzc3dvcmQoe1xuICAgICAgICAgICAgICB1c2VybmFtZTogbmV3Rm9ybVZhbHVlcy5zdWJtaXR0ZWRfZW1haWwsXG4gICAgICAgICAgICAgIHBhc3N3b3JkOiBuZXdGb3JtVmFsdWVzLnN1Ym1pdHRlZF9wYXNzd29yZCxcbiAgICAgICAgICAgICAgbmV3cGFzczogZm9ybVZhbHVlcy5uZXdfcGFzc3dvcmQsXG4gICAgICAgICAgICAgIGV4dGVuZGVkU2Vzc2lvbjogbmV3Rm9ybVZhbHVlcy5leHRlbmRlZFNlc3Npb24sXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY29uc29sZS5lcnJvcihcIk5ldyBwYXNzd29yZHMgZG9uJ3QgbWF0Y2hcIik7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGRpc3BhdGNoKFxuICAgICAgICAgIHVzZXJMb2dpbih7XG4gICAgICAgICAgICB1c2VybmFtZTogbmV3Rm9ybVZhbHVlcy5zdWJtaXR0ZWRfZW1haWwsXG4gICAgICAgICAgICBwYXNzd29yZDogbmV3Rm9ybVZhbHVlcy5zdWJtaXR0ZWRfcGFzc3dvcmQsXG4gICAgICAgICAgICBleHRlbmRlZFNlc3Npb246IG5ld0Zvcm1WYWx1ZXMuZXh0ZW5kZWRTZXNzaW9uLFxuICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChcbiAgICAgICFhY2NvdW50RXhpc3RzICYmXG4gICAgICBuZXdGb3JtVmFsdWVzLnN1Ym1pdHRlZF9lbWFpbCAmJlxuICAgICAgbmV3Rm9ybVZhbHVlcy5zdWJtaXR0ZWRfbmFtZVxuICAgICkge1xuICAgICAgZGlzcGF0Y2goXG4gICAgICAgIHVzZXJDcmVhdGUoe1xuICAgICAgICAgIHVzZXJuYW1lOiBuZXdGb3JtVmFsdWVzLnN1Ym1pdHRlZF9lbWFpbCxcbiAgICAgICAgICBuYW1lOiBuZXdGb3JtVmFsdWVzLnN1Ym1pdHRlZF9uYW1lLFxuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9IGVsc2UgaWYgKG5ld0Zvcm1WYWx1ZXMuc3VibWl0dGVkX2VtYWlsPy5sZW5ndGggPiAwKSB7XG4gICAgICBkaXNwYXRjaCh1c2VybmFtZUF2YWlsYWJsZSh7IHVzZXJuYW1lOiBuZXdGb3JtVmFsdWVzLnN1Ym1pdHRlZF9lbWFpbCB9KSk7XG4gICAgfVxuICAgIHNldExvZ2luU3RhZ2UobG9naW5TdGFnZSArIDEpO1xuICB9O1xuXG4gIGNvbnN0IG9uQ2hhbmdlID0gKHBheWxvYWQpID0+IHtcbiAgICBzZXRGb3JtVmFsdWVzKHsgLi4uZm9ybVZhbHVlcywgW3BheWxvYWQuaWRdOiBwYXlsb2FkLnZhbHVlIH0pO1xuICB9O1xuXG4gIGNvbnN0IG9uUmV2YWxpZGF0ZSA9IChwYXlsb2FkKSA9PiB7XG4gICAgb25DbG9zZU1vZGFsKCk7XG4gICAgZGlzcGF0Y2godXNlclJlcXVlc3RWYWxpZGF0aW9uKHsgdXNlcm5hbWU6IGZvcm1WYWx1ZXMuc3VibWl0dGVkX2VtYWlsIH0pKTtcbiAgICBvblBvc3RTdGF0dXMoXG4gICAgICBcIlZhbGlkYXRpb24gcmUtc2VudCwgY2hlY2sgeW91ciBpbmJveCB0byBjb21wbGV0ZSByZWdpc3RyYXRpb24hXCJcbiAgICApO1xuICB9O1xuXG4gIGNvbnN0IG9uUmVzZXQgPSAocGF5bG9hZCkgPT4ge1xuICAgIG9uQ2xvc2VNb2RhbCgpO1xuICAgIGRpc3BhdGNoKHVzZXJSZXF1ZXN0VmFsaWRhdGlvbih7IHVzZXJuYW1lOiBmb3JtVmFsdWVzLnN1Ym1pdHRlZF9lbWFpbCB9KSk7XG4gICAgb25Qb3N0U3RhdHVzKFwiQWNjb3VudCByZXNldCwgY2hlY2sgeW91ciBpbmJveCB0byBsb2cgYmFjayBpblwiKTtcbiAgfTtcblxuICBsZXQgcGFnZUNvbnRlbnQgPSBpc0F1dGhlbnRpY2F0ZWQgPyA8U2lnbk91dCAvPiA6IDxBc2tGb3JFbWFpbCAvPjtcbiAgaWYgKFxuICAgICFhY2NvdW50RXhpc3RzICYmXG4gICAgZm9ybVZhbHVlcy5zdWJtaXR0ZWRfZW1haWwgJiZcbiAgICBmb3JtVmFsdWVzLnN1Ym1pdHRlZF9uYW1lXG4gICkge1xuICAgIHBhZ2VDb250ZW50ID0gPENvbmZpcm1OZXdBY2NvdW50IC8+O1xuICAgIHN0YXR1c01lc3NhZ2UgPVxuICAgICAgXCJBY2NvdW50IGNyZWF0ZWQsIGNoZWNrIHlvdXIgaW5ib3ggdG8gY29tcGxldGUgcmVnaXN0cmF0aW9uIVwiO1xuICB9IGVsc2UgaWYgKGZvcm1WYWx1ZXMuc3VibWl0dGVkX2VtYWlsICYmICFpc1dvcmtpbmcpIHtcbiAgICBpZiAoYWNjb3VudEV4aXN0cykge1xuICAgICAgaWYgKCFhY2NvdW50VmFsaWQpIHtcbiAgICAgICAgcGFnZUNvbnRlbnQgPSA8QWNjb3VudEludmFsaWQgLz47XG4gICAgICB9IGVsc2UgaWYgKGFjY291bnROZWVkc1Jlc2V0KSB7XG4gICAgICAgIHBhZ2VDb250ZW50ID0gPFJlc2V0UGFzcyAvPjtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHBhZ2VDb250ZW50ID0gPFdlbGNvbWVCYWNrIC8+O1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBwYWdlQ29udGVudCA9IDxOZXdTaWdudXAgLz47XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIChcbiAgICA8U2lnbkluQ29udGV4dC5Qcm92aWRlclxuICAgICAgdmFsdWU9e3tcbiAgICAgICAgc3R5bGVzLFxuICAgICAgICBvblJlc2V0LFxuICAgICAgICBvbkNoYW5nZSxcbiAgICAgICAgb25Db250aW51ZSxcbiAgICAgICAgb25SZXZhbGlkYXRlLFxuICAgICAgICBvbkdvQmFjazogKCkgPT4gc2V0Rm9ybVZhbHVlcyh7fSksXG4gICAgICAgIGZvcm1WYWx1ZXMsXG4gICAgICAgIGhlbHBMaW5rLFxuICAgICAgICBiYWRQYXNzTGltaXQsXG4gICAgICAgIHBhc3N3b3JkVHJpZXMsXG4gICAgICAgIG9uQ2xvc2VNb2RhbCxcbiAgICAgICAgaXNXb3JraW5nLFxuICAgICAgICBvblN0YXR1cyxcbiAgICAgICAgaGFzQ2xvc2VCdXR0b24sXG4gICAgICB9fVxuICAgID5cbiAgICAgIDxTcGlubmVyPntwYWdlQ29udGVudH08L1NwaW5uZXI+XG4gICAgPC9TaWduSW5Db250ZXh0LlByb3ZpZGVyPlxuICApO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgUGFnZXM7XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgdXNlU2VsZWN0b3IsIHVzZURpc3BhdGNoIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgeyB1c2VyTG9naW5Db25maWcgfSBmcm9tIFwiL3NyYy9hY3Rpb25zLmpzXCI7XG5pbXBvcnQgUGFnZXMgZnJvbSBcIi4vUGFnZXNcIjtcbmltcG9ydCB7IE1vZGFsIH0gZnJvbSBcIkB0aGVwbGFjZWxhYi91aVwiO1xuaW1wb3J0IHsgUmluZyB9IGZyb20gXCJyZWFjdC1zcGlubmVycy1jc3NcIjtcblxuY29uc3QgU2lnbkluID0gKHtcbiAgYXV0aFNlcnZpY2UsXG4gIGF1dGhIZWxwZXIsXG4gIG9uTG9naW4sXG4gIG9uTG9nb3V0LFxuICBvblN0YXR1cyxcbiAgb25BdmF0YXJDbGljayxcbiAgaGVscExpbmssXG4gIHN0eWxlLFxuICBwcmVmaWxsLFxuICBzaG93VXNlcm5hbWUgPSBmYWxzZSxcbiAgc2l6ZVRocmVzaG9sZCA9IDkwMCxcbiAgaGFzQ2xvc2VCdXR0b24gPSB0cnVlLFxuICBsb2dpblRleHQgPSBcIlNpZ24gSW5cIixcbiAgbG9nb3V0VGV4dCA9IFwiU2lnbiBPdXRcIixcbn0pID0+IHtcbiAgY29uc3Qgd2luZG93U2l6ZSA9ICgpID0+IHtcbiAgICBjb25zdCBbd2luZG93U2l6ZSwgc2V0V2luZG93U2l6ZV0gPSB1c2VTdGF0ZSh7XG4gICAgICB3aWR0aDogdW5kZWZpbmVkLFxuICAgICAgaGVpZ2h0OiB1bmRlZmluZWQsXG4gICAgICBzaXplVGhyZXNob2xkOiBmYWxzZSxcbiAgICB9KTtcbiAgICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgICAgZnVuY3Rpb24gaGFuZGxlUmVzaXplKCkge1xuICAgICAgICBzZXRXaW5kb3dTaXplKHtcbiAgICAgICAgICB3aWR0aDogd2luZG93LmlubmVyV2lkdGgsXG4gICAgICAgICAgaGVpZ2h0OiB3aW5kb3cuaW5uZXJIZWlnaHQsXG4gICAgICAgICAgdGhyZXNob2xkOiB3aW5kb3cuaW5uZXJXaWR0aCA8IHNpemVUaHJlc2hvbGQsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJyZXNpemVcIiwgaGFuZGxlUmVzaXplKTtcbiAgICAgIGhhbmRsZVJlc2l6ZSgpO1xuICAgICAgcmV0dXJuICgpID0+IHdpbmRvdy5yZW1vdmVFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGhhbmRsZVJlc2l6ZSk7XG4gICAgfSwgW10pO1xuICAgIHJldHVybiB3aW5kb3dTaXplO1xuICB9O1xuICBjb25zdCBzaXplID0gd2luZG93U2l6ZSgpO1xuICBjb25zdCBhdXRoU2VydmljZVJlYWNoYWJsZSA9IHVzZVNlbGVjdG9yKFxuICAgIChyZWR1eCkgPT4gcmVkdXguYXV0aFNlcnZpY2VSZWFjaGFibGVcbiAgKTtcbiAgY29uc3QgaXNBdXRoZW50aWNhdGVkID0gdXNlU2VsZWN0b3IoKHJlZHV4KSA9PiByZWR1eC5pc0F1dGhlbnRpY2F0ZWQpO1xuICBjb25zdCBpc0luaXRpYWxpemluZyA9IHVzZVNlbGVjdG9yKChyZWR1eCkgPT4gcmVkdXguaXNJbml0aWFsaXppbmcpO1xuICBjb25zdCBbbW9kYWxWaXNpYmxlLCBzZXRNb2RhbFZpc2libGVdID0gdXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBkaXNwYXRjaCA9IHVzZURpc3BhdGNoKCk7XG4gIGNvbnN0IGVtYWlsID0gdXNlU2VsZWN0b3IoKHJlZHV4KSA9PiByZWR1eC5jbGFpbXM/LmVtYWlsKTtcblxuICAvLyBDb25maWd1cmUgc2VydmljZXNcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBkaXNwYXRjaChcbiAgICAgIHVzZXJMb2dpbkNvbmZpZyh7XG4gICAgICAgIGF1dGhTZXJ2aWNlLFxuICAgICAgICBhdXRoSGVscGVyLFxuICAgICAgICBvbkxvZ2luLFxuICAgICAgICBvbkxvZ291dCxcbiAgICAgICAgb25BdmF0YXJDbGljayxcbiAgICAgIH0pXG4gICAgKTtcbiAgfSwgW2F1dGhTZXJ2aWNlLCBhdXRoSGVscGVyLCBvbkxvZ2luLCBvbkxvZ291dF0pO1xuXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgc2V0TW9kYWxWaXNpYmxlKHByZWZpbGwgPyB0cnVlIDogZmFsc2UpO1xuICB9LCBbaXNBdXRoZW50aWNhdGVkXSk7XG5cbiAgLy8gUHJldmVudHMgRk9VQyBiZWZvcmUgd2Ugc3RhYmlsaXplXG4gIGlmIChpc0luaXRpYWxpemluZykgcmV0dXJuIDxSaW5nIGNvbG9yPVwid2hpdGVcIiBzaXplPXsyMH0gLz47XG4gIGNvbnN0IHN0eWxlcyA9IHtcbiAgICAuLi5zdHlsZSxcbiAgICBjb250YWluZXI6IHtcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBtYXJnaW46IDAsXG4gICAgICAuLi5zdHlsZT8uY29udGFpbmVyLFxuICAgIH0sXG4gICAgdGl0bGU6IHtcbiAgICAgIG1hcmdpbjogMCxcbiAgICAgIC4uLnN0eWxlPy50aXRsZSxcbiAgICB9LFxuICAgIHN1YnRpdGxlOiB7XG4gICAgICBtYXJnaW46IFwiMXJlbVwiLFxuICAgICAgLi4uc3R5bGU/LnN1YnRpdGxlLFxuICAgIH0sXG4gICAgZm9vdGVyOiB7XG4gICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgIHRleHRBbGlnbjogXCJjZW50ZXJcIixcbiAgICAgIGJhY2tncm91bmQ6IFwibGlnaHRHcmV5XCIsXG4gICAgICB3aWR0aDogXCIxMDAlXCIsXG4gICAgICBoZWlnaHQ6IFwiNHJlbVwiLFxuICAgICAgcG9zaXRpb246IFwiYWJzb2x1dGVcIixcbiAgICAgIGJvdHRvbTogMCxcbiAgICAgIC4uLnN0eWxlPy5mb290ZXIsXG4gICAgfSxcbiAgICBtb2RhbDogc2l6ZS50aHJlc2hvbGRcbiAgICAgID8ge1xuICAgICAgICAgIGhlaWdodDogXCIyNXJlbVwiLFxuICAgICAgICAgIHdpZHRoOiBcIjI1cmVtXCIsXG4gICAgICAgICAgbWF4V2lkdGg6IFwiMTAwdmhcIixcbiAgICAgICAgICBtYXhIZWlnaHQ6IFwiMTAwdmhcIixcbiAgICAgICAgICBtaW5XaWR0aDogXCIxMDB2d1wiLFxuICAgICAgICAgIG1pbkhlaWdodDogXCIxMDB2aFwiLFxuICAgICAgICB9XG4gICAgICA6IHtcbiAgICAgICAgICBwYWRkaW5nOiAwLFxuICAgICAgICAgIGRpc3BsYXk6IFwiZmxleFwiLFxuICAgICAgICAgIGhlaWdodDogXCIyNXJlbVwiLFxuICAgICAgICAgIHdpZHRoOiBcIjI1cmVtXCIsXG4gICAgICAgICAgbWFyZ2luVG9wOiBcIjEwcmVtXCIsXG4gICAgICAgICAgbWF4V2lkdGg6IFwiNDByZW1cIixcbiAgICAgICAgICBtYXhIZWlnaHQ6IFwiMzByZW1cIixcbiAgICAgICAgfSxcbiAgfTtcblxuICBpZiAoIWF1dGhTZXJ2aWNlUmVhY2hhYmxlKSB7XG4gICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXtcImZhcyBmYS1leGNsYW1hdGlvbi10cmlhbmdsZVwifSAvPjtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gKFxuICAgICAgPFJlYWN0LkZyYWdtZW50PlxuICAgICAgICB7KCFpc0F1dGhlbnRpY2F0ZWQgJiYgKFxuICAgICAgICAgIDxkaXYgb25DbGljaz17KCkgPT4gc2V0TW9kYWxWaXNpYmxlKHRydWUpfT57bG9naW5UZXh0fTwvZGl2PlxuICAgICAgICApKSB8fCAoXG4gICAgICAgICAgPGRpdiBvbkNsaWNrPXsoKSA9PiBzZXRNb2RhbFZpc2libGUodHJ1ZSl9PlxuICAgICAgICAgICAge2xvZ291dFRleHR9IHtzaG93VXNlcm5hbWUgPyBgKCR7ZW1haWx9KWAgOiBcIlwifVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgICB7bW9kYWxWaXNpYmxlICYmIChcbiAgICAgICAgICA8TW9kYWxcbiAgICAgICAgICAgIG9uT3V0c2lkZUNsaWNrPXsoKSA9PiBzZXRNb2RhbFZpc2libGUoZmFsc2UpfVxuICAgICAgICAgICAgc3R5bGU9e3N0eWxlcy5tb2RhbH1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8UGFnZXNcbiAgICAgICAgICAgICAgaXNBdXRoZW50aWNhdGVkPXtpc0F1dGhlbnRpY2F0ZWR9XG4gICAgICAgICAgICAgIHsuLi57XG4gICAgICAgICAgICAgICAgaGFzQ2xvc2VCdXR0b24sXG4gICAgICAgICAgICAgICAgb25DbG9zZU1vZGFsOiAoKSA9PiBzZXRNb2RhbFZpc2libGUoZmFsc2UpLFxuICAgICAgICAgICAgICAgIGhlbHBMaW5rLFxuICAgICAgICAgICAgICAgIHN0eWxlLFxuICAgICAgICAgICAgICAgIHByZWZpbGwsXG4gICAgICAgICAgICAgICAgb25TdGF0dXMsXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAge2hhc0Nsb3NlQnV0dG9uICYmIChcbiAgICAgICAgICAgICAgPGRpdiBzdHlsZT17c3R5bGVzLmZvb3Rlcn0+XG4gICAgICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBtYXJnaW46IFwiYXV0b1wiIH19PlxuICAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgIHN0eWxlPXtzdHlsZXMuYnV0dG9ufVxuICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU9e1wiQ2FuY2VsXCJ9XG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHNldE1vZGFsVmlzaWJsZShmYWxzZSl9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgPC9Nb2RhbD5cbiAgICAgICAgKX1cbiAgICAgIDwvUmVhY3QuRnJhZ21lbnQ+XG4gICAgKTtcbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgU2lnbkluO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgUmluZyB9IGZyb20gXCJyZWFjdC1zcGlubmVycy1jc3NcIjtcbmltcG9ydCB7IHVzZVNlbGVjdG9yIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5leHBvcnQgY29uc3QgU3Bpbm5lciA9ICh7IGNoaWxkcmVuIH0pID0+IHtcbiAgY29uc3QgaXNXb3JraW5nID0gdXNlU2VsZWN0b3IoKHJlZHV4KSA9PiByZWR1eC5pc1dvcmtpbmcpO1xuICByZXR1cm4gKFxuICAgIDxSZWFjdC5GcmFnbWVudD5cbiAgICAgIHtpc1dvcmtpbmcgJiYgKFxuICAgICAgICA8ZGl2XG4gICAgICAgICAgc3R5bGU9e3tcbiAgICAgICAgICAgIHBvc2l0aW9uOiBcImFic29sdXRlXCIsXG4gICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwid2hpdGVcIixcbiAgICAgICAgICAgIGhlaWdodDogXCIxMDAlXCIsXG4gICAgICAgICAgICB3aWR0aDogXCIxMDAlXCIsXG4gICAgICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgICAgICBvcGFjaXR5OiAxLjAsXG4gICAgICAgICAgICBkaXNwbGF5OiBcImZsZXhcIixcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPGRpdiBzdHlsZT17eyB3aWR0aDogXCIxMDAlXCIsIG1hcmdpbjogXCJhdXRvXCIsIHRleHRBbGlnbjogXCJjZW50ZXJcIiB9fT5cbiAgICAgICAgICAgIDxSaW5nIGNvbG9yPVwiIzdhNzY3NlwiIHNpemU9ezMyfSAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgICl9XG4gICAgICB7Y2hpbGRyZW59XG4gICAgPC9SZWFjdC5GcmFnbWVudD5cbiAgKTtcbn07XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0LCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgdXNlU2VsZWN0b3IsIHVzZURpc3BhdGNoIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgeyB1c2VyVmFsaWRhdGUgfSBmcm9tIFwiL3NyYy9hY3Rpb25zLmpzXCI7XG5pbXBvcnQgeyBSaW5nIH0gZnJvbSBcInJlYWN0LXNwaW5uZXJzLWNzc1wiO1xuaW1wb3J0IHsgdXNlckxvZ2luQ29uZmlnIH0gZnJvbSBcIi9zcmMvYWN0aW9ucy5qc1wiO1xuXG5jb25zdCBWYWxpZGF0ZSA9ICh7XG4gIHRva2VuLFxuICBzdHlsZSxcbiAgb25Db21wbGV0ZSxcbiAgYXV0aFNlcnZpY2UsXG4gIGF1dGhIZWxwZXIsXG4gIG9uTG9naW4sXG4gIG9uTG9nb3V0LFxuICBvbkF2YXRhckNsaWNrLFxufSkgPT4ge1xuICBjb25zdCBkaXNwYXRjaCA9IHVzZURpc3BhdGNoKCk7XG4gIGNvbnN0IGlzV29ya2luZyA9IHVzZVNlbGVjdG9yKChyZWR1eCkgPT4gcmVkdXguaXNXb3JraW5nKTtcbiAgY29uc3QgcmVkdXggPSB1c2VTZWxlY3RvcigocmVkdXgpID0+IHJlZHV4KTtcbiAgY29uc29sZS5sb2cocmVkdXgpO1xuICBjb25zdCBhdXRoU2VydmljZVJlYWNoYWJsZSA9IHVzZVNlbGVjdG9yKFxuICAgIChyZWR1eCkgPT4gcmVkdXguYXV0aFNlcnZpY2VSZWFjaGFibGVcbiAgKTtcbiAgY29uc3QgYWNjb3VudFN0YXR1cyA9IHVzZVNlbGVjdG9yKChyZWR1eCkgPT4gcmVkdXgudmFsaWRhdGVkQWNjb3VudCk7XG5cbiAgLy8gQ29uZmlndXJlIHNlcnZpY2VzXG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgZGlzcGF0Y2goXG4gICAgICB1c2VyTG9naW5Db25maWcoe1xuICAgICAgICBhdXRoU2VydmljZSxcbiAgICAgICAgYXV0aEhlbHBlcixcbiAgICAgICAgb25Mb2dpbixcbiAgICAgICAgb25Mb2dvdXQsXG4gICAgICAgIG9uQXZhdGFyQ2xpY2ssXG4gICAgICB9KVxuICAgICk7XG4gIH0sIFthdXRoU2VydmljZSwgYXV0aEhlbHBlciwgb25Mb2dpbiwgb25Mb2dvdXRdKTtcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChhdXRoU2VydmljZVJlYWNoYWJsZSkgZGlzcGF0Y2godXNlclZhbGlkYXRlKHRva2VuKSk7XG4gIH0sIFthdXRoU2VydmljZVJlYWNoYWJsZSwgdG9rZW5dKTtcblxuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmICghaXNXb3JraW5nICYmIGFjY291bnRTdGF0dXM/LnYgJiYgdHlwZW9mIG9uQ29tcGxldGUgPT09IFwiZnVuY3Rpb25cIilcbiAgICAgIG9uQ29tcGxldGUoYWNjb3VudFN0YXR1cyk7XG4gIH0sIFtpc1dvcmtpbmcsIGFjY291bnRTdGF0dXNdKTtcblxuICBpZiAoIWF1dGhTZXJ2aWNlUmVhY2hhYmxlKSByZXR1cm4gbnVsbDtcbiAgY29uc3Qgc3R5bGVzID0ge1xuICAgIG1vZGFsU3R5bGU6IHtcbiAgICAgIGhlaWdodDogXCIyNXJlbVwiLFxuICAgICAgd2lkdGg6IFwiMjVyZW1cIixcbiAgICAgIG1hcmdpblRvcDogXCIxMHJlbVwiLFxuICAgIH0sXG4gICAgLi4uc3R5bGUsXG4gIH07XG4gIGlmIChpc1dvcmtpbmcpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBzdHlsZT17eyB0ZXh0QWxpZ246IFwiY2VudGVyXCIgfX0+XG4gICAgICAgIFZhbGlkYXRpbmcuLi5cbiAgICAgICAgPGJyIC8+IDxSaW5nIGNvbG9yPVwiIzdhNzY3NlwiIHNpemU9ezMyfSAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdj5cbiAgICAgICAgeyhhY2NvdW50U3RhdHVzPy52ICYmIChcbiAgICAgICAgICA8ZGl2PkFjY291bnQgVmFsaWRhdGVkISBQbGVhc2Ugc2lnbiBpbiB0byBjb250aW51ZTwvZGl2PlxuICAgICAgICApKSB8fCAoXG4gICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxkaXY+VmFsaWRhdGlvbiBsaW5rIGlzIGludmFsaWQgb3IgZXhwaXJlZDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICApfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufTtcbmV4cG9ydCBkZWZhdWx0IFZhbGlkYXRlO1xuIiwiaW1wb3J0IFZhbGlkYXRlQ29tcG9uZW50IGZyb20gXCIuL2NvbXBvbmVudHMvVmFsaWRhdGVcIjtcbmltcG9ydCBTaWduSW5Db21wb25lbnQgZnJvbSBcIi4vY29tcG9uZW50cy9TaWduSW5cIjtcbmltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgc3RvcmUgZnJvbSBcIi4vc3RvcmUuanNcIjtcblxuY29uc3QgVmFsaWRhdGUgPSAocHJvcHMpID0+IHtcbiAgcmV0dXJuIChcbiAgICA8UHJvdmlkZXIgc3RvcmU9e3N0b3JlfT5cbiAgICAgIDxWYWxpZGF0ZUNvbXBvbmVudCB7Li4ucHJvcHN9IC8+XG4gICAgPC9Qcm92aWRlcj5cbiAgKTtcbn07XG5cbmNvbnN0IFNpZ25JbiA9IChwcm9wcykgPT4ge1xuICByZXR1cm4gKFxuICAgIDxQcm92aWRlciBzdG9yZT17c3RvcmV9PlxuICAgICAgPFNpZ25JbkNvbXBvbmVudCB7Li4ucHJvcHN9IC8+XG4gICAgPC9Qcm92aWRlcj5cbiAgKTtcbn07XG5cbmV4cG9ydCB7IFNpZ25JbiwgVmFsaWRhdGUgfTtcbiIsImltcG9ydCAqIGFzIGFjdGlvbnMgZnJvbSBcIi4uL2FjdGlvbnNcIjtcblxuY29uc3QgdXNlciA9IChcbiAgc3RhdGUgPSB7XG4gICAgYXV0aFNlcnZpY2VSZWFjaGFibGU6IGZhbHNlLFxuICAgIGlzSW5pdGlhbGl6aW5nOiB0cnVlLFxuICAgIGlzQXV0aGVudGljYXRlZDogZmFsc2UsXG4gICAgdG9rZW46IFwiXCIsXG4gICAgYXZhaWxhYmxlVXNlcm5hbWVzOiBbXSxcbiAgICB2YWxpZGF0ZWRBY2NvdW50czogW10sXG4gIH0sXG4gIGFjdGlvblxuKSA9PiB7XG4gIHN3aXRjaCAoYWN0aW9uLnR5cGUpIHtcbiAgICBjYXNlIGFjdGlvbnMuVVNFUl9BVkFJTEFCSUxJVFlfQ0hFQ0s6XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfTE9HSU46XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfVkFMSURBVEU6XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfTE9HSU5fV0lUSF9DSEFOR0VQQVNTOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGlzV29ya2luZzogdHJ1ZSxcbiAgICAgIH07XG4gICAgY2FzZSBhY3Rpb25zLkFVVEhfQVBJX1VOUkVBQ0hBQkxFOlxuICAgICAgcmV0dXJuIHsgLi4uc3RhdGUsIGlzSW5pdGlhbGl6aW5nOiBmYWxzZSwgYXV0aFNlcnZpY2VSZWFjaGFibGU6IGZhbHNlIH07XG4gICAgY2FzZSBhY3Rpb25zLkFVVEhfQVBJX1JFQUNIQUJMRTpcbiAgICAgIHJldHVybiB7IC4uLnN0YXRlLCBpc0luaXRpYWxpemluZzogZmFsc2UsIGF1dGhTZXJ2aWNlUmVhY2hhYmxlOiB0cnVlIH07XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfTE9HSU5fQ09ORklHX0NPTVBMRVRFOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGF1dGhTZXJ2aWNlUmVhY2hhYmxlOiB0cnVlLFxuICAgICAgICAuLi5hY3Rpb24ucGF5bG9hZCxcbiAgICAgIH07XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfQVZBSUxBQklMSVRZX0NIRUNLX1JFU1BPTlNFOlxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4uc3RhdGUsXG4gICAgICAgIGF2YWlsYWJsZVVzZXJuYW1lOiBhY3Rpb24ucGF5bG9hZCxcbiAgICAgICAgaXNXb3JraW5nOiBmYWxzZSxcbiAgICAgIH07XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfTE9HSU5fUkVTUE9OU0U6XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5zdGF0ZSxcbiAgICAgICAgLi4uYWN0aW9uLnBheWxvYWQsXG4gICAgICAgIGlzV29ya2luZzogZmFsc2UsXG4gICAgICAgIGlzSW5pdGlhbGl6aW5nOiBmYWxzZSxcbiAgICAgIH07XG4gICAgY2FzZSBhY3Rpb25zLlVTRVJfVkFMSURBVEVfUkVTUE9OU0U6XG4gICAgICBjb25zb2xlLmxvZyhhY3Rpb24ucGF5bG9hZCk7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5zdGF0ZSxcbiAgICAgICAgdmFsaWRhdGVkQWNjb3VudDogYWN0aW9uLnBheWxvYWQsXG4gICAgICAgIGlzV29ya2luZzogZmFsc2UsXG4gICAgICB9O1xuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gc3RhdGU7XG4gIH1cbn07XG5cbmV4cG9ydCBkZWZhdWx0IHVzZXI7XG4iLCJpbXBvcnQgeyBwdXQsIGNhbGwsIHNlbGVjdCwgZGVsYXkgfSBmcm9tIFwicmVkdXgtc2FnYS9lZmZlY3RzXCI7XG5pbXBvcnQgKiBhcyBhY3Rpb25zIGZyb20gXCIuL2FjdGlvbnMuanNcIjtcblxuY29uc3QgZGVjb2RlVG9rZW4gPSAodG9rZW4pID0+IHtcbiAgdHJ5IHtcbiAgICB2YXIgYmFzZTY0VXJsID0gdG9rZW4uc3BsaXQoXCIuXCIpWzFdO1xuICAgIHZhciBiYXNlNjQgPSBiYXNlNjRVcmwucmVwbGFjZSgvLS9nLCBcIitcIikucmVwbGFjZSgvXy9nLCBcIi9cIik7XG4gICAgcmV0dXJuIEpTT04ucGFyc2Uod2luZG93LmF0b2IoYmFzZTY0KSk7XG4gIH0gY2F0Y2gge1xuICAgIHJldHVybiBudWxsO1xuICB9XG59O1xuXG5leHBvcnQgY29uc3QgcGVyaW9kaWNhbGx5UmVhdXRob3JpemUgPSBmdW5jdGlvbiogKCkge1xuICBjb25zdCBjdXJyZW50VG9rZW4gPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInJlZnJlc2hUb2tlblwiKTtcbiAgaWYgKGN1cnJlbnRUb2tlbikgeWllbGQgY2FsbChyZWF1dGhvcml6ZSk7XG4gIHdoaWxlICh0cnVlKSB7XG4gICAgY29uc3QgcmVkdXhTdGF0ZSA9IHlpZWxkIHNlbGVjdCgpO1xuICAgIGlmIChyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlUmVhY2hhYmxlKVxuICAgICAgaWYgKHJlZHV4U3RhdGUuaXNBdXRoZW50aWNhdGVkKSB7XG4gICAgICAgIGxldCBub3cgPSBEYXRlLm5vdygpIC8gMTAwMDtcbiAgICAgICAgbGV0IHNlY29uZHNVbnRpbEV4cGlyZSA9IHJlZHV4U3RhdGUuY2xhaW1zLmV4cCAtIG5vdztcbiAgICAgICAgaWYgKHNlY29uZHNVbnRpbEV4cGlyZSA8PSAxMjApIHtcbiAgICAgICAgICB5aWVsZCBjYWxsKHJlYXV0aG9yaXplKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgaWYgKGN1cnJlbnRUb2tlbikgeWllbGQgY2FsbChyZWF1dGhvcml6ZSk7XG4gICAgICB9XG4gICAgeWllbGQgZGVsYXkoNjAwMDApO1xuICB9XG59O1xuXG5leHBvcnQgY29uc3QgcmVhdXRob3JpemUgPSBmdW5jdGlvbiogKGFjdGlvbikge1xuICBjb25zdCByZWR1eFN0YXRlID0geWllbGQgc2VsZWN0KCk7XG4gIGNvbnN0IHJlZnJlc2hUb2tlbiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmVmcmVzaFRva2VuXCIpO1xuICBpZiAocmVkdXhTdGF0ZS5hdXRoU2VydmljZVJlYWNoYWJsZSkge1xuICAgIGlmIChyZWZyZXNoVG9rZW4pIHtcbiAgICAgIGNvbnN0IHJlc3BvbnNlID0geWllbGQgX21ha2VSZXF1ZXN0KHtcbiAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgdXJsOiBgJHtyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlfS9ycGMvcmVhdXRob3JpemVgLFxuICAgICAgICBib2R5OiB7IHRva2VuOiByZWZyZXNoVG9rZW4gfSxcbiAgICAgIH0pO1xuICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLm9rKSB7XG4gICAgICAgIGNvbnN0IHJlcyA9IHlpZWxkIHJlc3BvbnNlLmpzb24oKTtcbiAgICAgICAgY29uc3QgYXV0aFRva2VuID0gcmVzLmF1dGg7XG4gICAgICAgIGNvbnN0IHJlZnJlc2hUb2tlbiA9IHJlcy5yZWZyZXNoO1xuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJlZnJlc2hUb2tlblwiLCByZWZyZXNoVG9rZW4pO1xuICAgICAgICBjb25zdCBwYXlsb2FkID0ge1xuICAgICAgICAgIC4uLnJlcyxcbiAgICAgICAgICBpc0F1dGhlbnRpY2F0ZWQ6IHRydWUsXG4gICAgICAgICAgY2xhaW1zOiBkZWNvZGVUb2tlbihhdXRoVG9rZW4pLFxuICAgICAgICB9O1xuICAgICAgICB5aWVsZCBwdXQoe1xuICAgICAgICAgIHR5cGU6IGFjdGlvbnMuVVNFUl9MT0dJTl9SRVNQT05TRSxcbiAgICAgICAgICBwYXlsb2FkLFxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKHR5cGVvZiByZWR1eFN0YXRlLm9uTG9naW4gPT09IFwiZnVuY3Rpb25cIilcbiAgICAgICAgICByZWR1eFN0YXRlLm9uTG9naW4ocGF5bG9hZCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcInJlZnJlc2hUb2tlblwiKTtcbiAgICAgICAgY29uc29sZS53YXJuKFwiUmVhdXRob3JpemF0aW9uIEZhaWxlZFwiKTtcbiAgICAgICAgeWllbGQgY2FsbChsb2dvdXQpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICB5aWVsZCBsb2dvdXQoKTtcbiAgICB9XG4gIH1cbn07XG5cbmV4cG9ydCBjb25zdCBjaGFuZ2VQYXNzQW5kTG9naW4gPSBmdW5jdGlvbiogKGFjdGlvbikge1xuICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcInJlZnJlc2hUb2tlblwiKTtcbiAgY29uc3QgcmVkdXhTdGF0ZSA9IHlpZWxkIHNlbGVjdCgpO1xuICBpZiAocmVkdXhTdGF0ZS5hdXRoU2VydmljZSkge1xuICAgIGNvbnN0IHJlc3BvbnNlID0geWllbGQgX21ha2VSZXF1ZXN0KHtcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICB1cmw6IGAke3JlZHV4U3RhdGUuYXV0aFNlcnZpY2V9L3JwYy9jaGFuZ2VwYXNzYCxcbiAgICAgIGJvZHk6IHtcbiAgICAgICAgZW1haWw6IGFjdGlvbi5wYXlsb2FkLnVzZXJuYW1lLFxuICAgICAgICBwYXNzOiBhY3Rpb24ucGF5bG9hZC5wYXNzd29yZCxcbiAgICAgICAgbmV3cGFzczogYWN0aW9uLnBheWxvYWQubmV3cGFzcyxcbiAgICAgICAgZXh0ZW5kOiBhY3Rpb24ucGF5bG9hZC5leHRlbmRlZFNlc3Npb25cbiAgICAgICAgICA/IGFjdGlvbi5wYXlsb2FkLmV4dGVuZGVkU2Vzc2lvblxuICAgICAgICAgIDogZmFsc2UsXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLm9rKSB7XG4gICAgICBjb25zdCByZXMgPSB5aWVsZCByZXNwb25zZS5qc29uKCk7XG4gICAgICBjb25zdCBhdXRoVG9rZW4gPSByZXMuYXV0aDtcbiAgICAgIGNvbnN0IHJlZnJlc2hUb2tlbiA9IHJlcy5yZWZyZXNoO1xuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJyZWZyZXNoVG9rZW5cIiwgcmVmcmVzaFRva2VuKTtcbiAgICAgIGNvbnN0IHBheWxvYWQgPSB7XG4gICAgICAgIGlzQXV0aGVudGljYXRlZDogdHJ1ZSxcbiAgICAgICAgYXV0aFRva2VuLFxuICAgICAgICBjbGFpbXM6IGRlY29kZVRva2VuKGF1dGhUb2tlbiksXG4gICAgICB9O1xuICAgICAgeWllbGQgcHV0KHtcbiAgICAgICAgdHlwZTogYWN0aW9ucy5VU0VSX0xPR0lOX1JFU1BPTlNFLFxuICAgICAgICBwYXlsb2FkLFxuICAgICAgfSk7XG4gICAgICBpZiAodHlwZW9mIHJlZHV4U3RhdGUub25Mb2dpbiA9PT0gXCJmdW5jdGlvblwiKVxuICAgICAgICB5aWVsZCByZWR1eFN0YXRlLm9uTG9naW4ocGF5bG9hZCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHlpZWxkIGNhbGwobG9nb3V0KTtcbiAgICB9XG4gIH1cbn07XG5cbmV4cG9ydCBjb25zdCBsb2dpbiA9IGZ1bmN0aW9uKiAoYWN0aW9uKSB7XG4gIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwicmVmcmVzaFRva2VuXCIpO1xuICBjb25zdCByZWR1eFN0YXRlID0geWllbGQgc2VsZWN0KCk7XG5cbiAgaWYgKHJlZHV4U3RhdGUuYXV0aFNlcnZpY2UpIHtcbiAgICBjb25zdCByZXNwb25zZSA9IHlpZWxkIF9tYWtlUmVxdWVzdCh7XG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgdXJsOiBgJHtyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlfS9ycGMvbG9naW5gLFxuICAgICAgYm9keToge1xuICAgICAgICBlbWFpbDogYWN0aW9uLnBheWxvYWQudXNlcm5hbWUsXG4gICAgICAgIHBhc3M6IGFjdGlvbi5wYXlsb2FkLnBhc3N3b3JkLFxuICAgICAgICBleHRlbmQ6IGFjdGlvbi5wYXlsb2FkLmV4dGVuZGVkU2Vzc2lvblxuICAgICAgICAgID8gYWN0aW9uLnBheWxvYWQuZXh0ZW5kZWRTZXNzaW9uXG4gICAgICAgICAgOiBmYWxzZSxcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uub2spIHtcbiAgICAgIGNvbnN0IHJlcyA9IHlpZWxkIHJlc3BvbnNlLmpzb24oKTtcbiAgICAgIGNvbnN0IGF1dGhUb2tlbiA9IHJlcy5hdXRoO1xuICAgICAgY29uc3QgcmVmcmVzaFRva2VuID0gcmVzLnJlZnJlc2g7XG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJlZnJlc2hUb2tlblwiLCByZWZyZXNoVG9rZW4pO1xuICAgICAgY29uc3QgcGF5bG9hZCA9IHtcbiAgICAgICAgLi4ucmVzLFxuICAgICAgICBpc0F1dGhlbnRpY2F0ZWQ6IHRydWUsXG4gICAgICAgIGNsYWltczogZGVjb2RlVG9rZW4oYXV0aFRva2VuKSxcbiAgICAgIH07XG4gICAgICB5aWVsZCBwdXQoe1xuICAgICAgICB0eXBlOiBhY3Rpb25zLlVTRVJfTE9HSU5fUkVTUE9OU0UsXG4gICAgICAgIHBheWxvYWQsXG4gICAgICB9KTtcbiAgICAgIGlmICh0eXBlb2YgcmVkdXhTdGF0ZS5vbkxvZ2luID09PSBcImZ1bmN0aW9uXCIpXG4gICAgICAgIHlpZWxkIHJlZHV4U3RhdGUub25Mb2dpbihwYXlsb2FkKTtcbiAgICB9IGVsc2Uge1xuICAgICAgeWllbGQgY2FsbChsb2dvdXQpO1xuICAgIH1cbiAgfVxufTtcblxuZXhwb3J0IGNvbnN0IGxvZ291dCA9IGZ1bmN0aW9uKiAoYWN0aW9uKSB7XG4gIGNvbnN0IHJlZHV4U3RhdGUgPSB5aWVsZCBzZWxlY3QoKTtcbiAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJyZWZyZXNoVG9rZW5cIik7XG4gIGNvbnN0IHBheWxvYWQgPSB7XG4gICAgaXNBdXRoZW50aWNhdGVkOiBmYWxzZSxcbiAgICBhdXRoVG9rZW46IFwiXCIsXG4gICAgY2xhaW1zOiB7fSxcbiAgfTtcbiAgeWllbGQgcHV0KHtcbiAgICB0eXBlOiBhY3Rpb25zLlVTRVJfTE9HSU5fUkVTUE9OU0UsXG4gICAgcGF5bG9hZCxcbiAgfSk7XG4gIGlmICh0eXBlb2YgcmVkdXhTdGF0ZS5vbkxvZ291dCA9PT0gXCJmdW5jdGlvblwiKSByZWR1eFN0YXRlLm9uTG9nb3V0KHBheWxvYWQpO1xufTtcblxuZXhwb3J0IGNvbnN0IHVzZXJWYWxpZGF0ZSA9IGZ1bmN0aW9uKiAoYWN0aW9uKSB7XG4gIGNvbnN0IHRva2VuID0gYWN0aW9uLnBheWxvYWQ7XG4gIGNvbnN0IHJlZHV4U3RhdGUgPSB5aWVsZCBzZWxlY3QoKTtcbiAgeWllbGQgY29uc29sZS5sb2cocmVkdXhTdGF0ZSk7XG4gIGlmIChyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlUmVhY2hhYmxlKSB7XG4gICAgY29uc3QgZGVjb2RlZFRva2VuID0gZGVjb2RlVG9rZW4odG9rZW4pO1xuICAgIHlpZWxkIGNvbnNvbGUubG9nKGRlY29kZWRUb2tlbik7XG4gICAgaWYgKGRlY29kZWRUb2tlbj8uZW1haWwpIHtcbiAgICAgIGNvbnN0IGNoZWNrVG9rZW4gPSB5aWVsZCBfbWFrZVJlcXVlc3Qoe1xuICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICB1cmw6IGAke3JlZHV4U3RhdGUuYXV0aEhlbHBlcn0vdXNlci92YWxpZGF0ZWAsXG4gICAgICAgIGJvZHk6IHsgdG9rZW4gfSxcbiAgICAgIH0pO1xuICAgICAgeWllbGQgY29uc29sZS5sb2coY2hlY2tUb2tlbik7XG4gICAgICBpZiAoIWNoZWNrVG9rZW4ub2spIHtcbiAgICAgICAgeWllbGQgcHV0KHtcbiAgICAgICAgICB0eXBlOiBhY3Rpb25zLlVTRVJfVkFMSURBVEVfUkVTUE9OU0UsXG4gICAgICAgICAgcGF5bG9hZDogbnVsbCxcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBjaGVja1VzZXIgPSB5aWVsZCBfbWFrZVJlcXVlc3Qoe1xuICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgdXJsOiBgJHtyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlfS9ycGMvY2hlY2t1c2VybmFtZWAsXG4gICAgICAgICAgYm9keTogeyB1c2VybmFtZTogZGVjb2RlZFRva2VuLmVtYWlsIH0sXG4gICAgICAgIH0pO1xuICAgICAgICBjb25zdCB1c2VyU3RhdGUgPSB5aWVsZCBjaGVja1VzZXIuanNvbigpO1xuICAgICAgICB5aWVsZCBwdXQoe1xuICAgICAgICAgIHR5cGU6IGFjdGlvbnMuVVNFUl9WQUxJREFURV9SRVNQT05TRSxcbiAgICAgICAgICBwYXlsb2FkOiB7IC4uLnVzZXJTdGF0ZSwgZW1haWw6IGRlY29kZWRUb2tlbi5lbWFpbCB9LFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgeWllbGQgcHV0KHtcbiAgICAgICAgdHlwZTogYWN0aW9ucy5VU0VSX1ZBTElEQVRFX1JFU1BPTlNFLFxuICAgICAgICBwYXlsb2FkOiBudWxsLFxuICAgICAgfSk7XG4gICAgfVxuICB9XG59O1xuXG5leHBvcnQgY29uc3QgdXNlclJlcXVlc3RWYWxpZGF0aW9uID0gZnVuY3Rpb24qIChhY3Rpb24pIHtcbiAgY29uc3QgcmVkdXhTdGF0ZSA9IHlpZWxkIHNlbGVjdCgpO1xuICBpZiAocmVkdXhTdGF0ZS5hdXRoU2VydmljZVJlYWNoYWJsZSkge1xuICAgIGNvbnN0IHJlc3BvbnNlID0geWllbGQgX21ha2VSZXF1ZXN0KHtcbiAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICB1cmw6IGAke3JlZHV4U3RhdGUuYXV0aEhlbHBlcn0vdXNlci92YWxpZGF0ZS9yZXNldGAsXG4gICAgICBib2R5OiB7IGVtYWlsOiBhY3Rpb24ucGF5bG9hZC51c2VybmFtZSB9LFxuICAgIH0pO1xuICB9XG59O1xuXG5leHBvcnQgY29uc3QgdXNlcm5hbWVBdmFpbGFibGUgPSBmdW5jdGlvbiogKGFjdGlvbikge1xuICBjb25zdCByZWR1eFN0YXRlID0geWllbGQgc2VsZWN0KCk7XG4gIGlmIChyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlUmVhY2hhYmxlKSB7XG4gICAgY29uc3QgcmVzcG9uc2UgPSB5aWVsZCBfbWFrZVJlcXVlc3Qoe1xuICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgIHVybDogYCR7cmVkdXhTdGF0ZS5hdXRoU2VydmljZX0vcnBjL2NoZWNrdXNlcm5hbWVgLFxuICAgICAgYm9keTogeyB1c2VybmFtZTogYWN0aW9uLnBheWxvYWQudXNlcm5hbWUgfSxcbiAgICB9KTtcblxuICAgIGxldCBwYXlsb2FkO1xuICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5vaykge1xuICAgICAgY29uc3QgcmVzID0geWllbGQgcmVzcG9uc2UuanNvbigpO1xuICAgICAgcGF5bG9hZCA9IHtcbiAgICAgICAgdXNlcm5hbWU6IGFjdGlvbi5wYXlsb2FkLnVzZXJuYW1lLFxuICAgICAgICBhdmFpbGFibGU6IHJlcy5hdmFpbGFibGUsXG4gICAgICAgIHZhbGlkOiByZXMudixcbiAgICAgICAgcmVzZXQ6IHJlcy5yLFxuICAgICAgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgcGF5bG9hZCA9IHtcbiAgICAgICAgdXNlcm5hbWU6IGFjdGlvbi5wYXlsb2FkLnVzZXJuYW1lLFxuICAgICAgICBhdmFpbGFibGU6IGZhbHNlLFxuICAgICAgICB2YWxpZDogcmVzLnYsXG4gICAgICAgIHJlc2V0OiByZXMucixcbiAgICAgIH07XG4gICAgfVxuICAgIHlpZWxkIHB1dCh7XG4gICAgICB0eXBlOiBhY3Rpb25zLlVTRVJfQVZBSUxBQklMSVRZX0NIRUNLX1JFU1BPTlNFLFxuICAgICAgcGF5bG9hZCxcbiAgICB9KTtcbiAgfVxufTtcblxuZXhwb3J0IGNvbnN0IHVzZXJDcmVhdGUgPSBmdW5jdGlvbiogKGFjdGlvbikge1xuICBjb25zdCByZWR1eFN0YXRlID0geWllbGQgc2VsZWN0KCk7XG4gIGlmIChyZWR1eFN0YXRlLmF1dGhTZXJ2aWNlUmVhY2hhYmxlKVxuICAgIHlpZWxkIF9tYWtlUmVxdWVzdCh7XG4gICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgdXJsOiBgJHtyZWR1eFN0YXRlLmF1dGhIZWxwZXJ9L3VzZXJgLFxuICAgICAgYm9keTogeyBlbWFpbDogYWN0aW9uLnBheWxvYWQudXNlcm5hbWUsIG5hbWU6IGFjdGlvbi5wYXlsb2FkLm5hbWUgfSxcbiAgICB9KTtcbn07XG5cbmV4cG9ydCBjb25zdCB2YWxpZGF0ZUNvbmZpZyA9IGZ1bmN0aW9uKiAoYWN0aW9uKSB7XG4gIGlmICghYWN0aW9uLnBheWxvYWQuYXV0aFNlcnZpY2UgfHwgIWFjdGlvbi5wYXlsb2FkLmF1dGhIZWxwZXIpIHtcbiAgICBjb25zb2xlLmVycm9yKFxuICAgICAgXCJAdGhlcGxhY2VsYWIvdXNlcjogTWlzc2luZyBjb25maWcuIFlvdSBtdXN0IHByb3ZpZGUgYm90aCBhdXRoU2VydmljZSBhbmQgYXV0aEhlbHBlciB2aWEgPExvZ2luLz5cIlxuICAgICk7XG4gICAgeWllbGQgcHV0KHsgdHlwZTogYWN0aW9ucy5BVVRIX0FQSV9VTlJFQUNIQUJMRSB9KTtcbiAgfSBlbHNlIHtcbiAgICBjb25zdCBjaGVja0F1dGhTZXJ2aWNlID0geWllbGQgX21ha2VSZXF1ZXN0KHtcbiAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgIHVybDogYWN0aW9uLnBheWxvYWQuYXV0aFNlcnZpY2UsXG4gICAgfSk7XG4gICAgY29uc3QgY2hlY2tBdXRoSGVscGVyID0geWllbGQgX21ha2VSZXF1ZXN0KHtcbiAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgIHVybDogYWN0aW9uLnBheWxvYWQuYXV0aEhlbHBlcixcbiAgICB9KTtcbiAgICBpZiAoY2hlY2tBdXRoU2VydmljZS5vayAmJiBjaGVja0F1dGhIZWxwZXIub2spXG4gICAgICB5aWVsZCBwdXQoe1xuICAgICAgICB0eXBlOiBhY3Rpb25zLlVTRVJfTE9HSU5fQ09ORklHX0NPTVBMRVRFLFxuICAgICAgICBwYXlsb2FkOiBhY3Rpb24ucGF5bG9hZCxcbiAgICAgIH0pO1xuICAgIHlpZWxkIHB1dCh7XG4gICAgICB0eXBlOiBhY3Rpb25zLlVTRVJfUkVBVVRILFxuICAgIH0pO1xuICB9XG59O1xuXG5jb25zdCBfbWFrZVJlcXVlc3QgPSBmdW5jdGlvbiogKHsgbWV0aG9kLCB1cmwsIGJvZHkgfSkge1xuICB0cnkge1xuICAgIGxldCByZXF1ZXN0ID0ge1xuICAgICAgbWV0aG9kOiBtZXRob2QsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogXCJhcHBsaWNhdGlvbi9qc29uLCBhcHBsaWNhdGlvbi94bWwsIHRleHQvcGxhaW4sIHRleHQvaHRtbCwgKi4qXCIsXG4gICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvbjsgY2hhcnNldD11dGYtOFwiLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IGJvZHlcbiAgICAgICAgPyB0eXBlb2YgYm9keSA9PT0gXCJzdHJpbmdcIlxuICAgICAgICAgID8gYm9keVxuICAgICAgICAgIDogSlNPTi5zdHJpbmdpZnkoYm9keSkucmVwbGFjZSgvXFxcXHUwMDAwL2csIFwiXCIpXG4gICAgICAgIDogbnVsbCxcbiAgICB9O1xuICAgIGlmICghYm9keSB8fCBtZXRob2QgPT09IFwiR0VUXCIpIGRlbGV0ZSByZXF1ZXN0LmJvZHk7XG4gICAgcmV0dXJuIHlpZWxkIGZldGNoKHVybCwgcmVxdWVzdCk7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICB5aWVsZCBwdXQoeyB0eXBlOiBhY3Rpb25zLkFVVEhfQVBJX1VOUkVBQ0hBQkxFIH0pO1xuICAgIGNvbnNvbGUuZXJyb3IoXCJGQVRBTDogQ2Fubm90IHJlYWNoIGF1dGggQVBJIGVuZHBvaW50KHMpIVwiKTtcbiAgICByZXR1cm4gZTtcbiAgfVxufTtcbiIsImltcG9ydCB7IHRha2VMYXRlc3QsIGFsbCwgY2FsbCB9IGZyb20gXCJyZWR1eC1zYWdhL2VmZmVjdHNcIjtcbmltcG9ydCAqIGFzIGFjdGlvbnMgZnJvbSBcIi4vYWN0aW9uc1wiO1xuaW1wb3J0IHtcbiAgcGVyaW9kaWNhbGx5UmVhdXRob3JpemUsXG4gIGxvZ2luLFxuICBsb2dvdXQsXG4gIHJlYXV0aG9yaXplLFxuICB1c2VybmFtZUF2YWlsYWJsZSxcbiAgdXNlckNyZWF0ZSxcbiAgdmFsaWRhdGVDb25maWcsXG4gIHVzZXJSZXF1ZXN0VmFsaWRhdGlvbixcbiAgdXNlclZhbGlkYXRlLFxuICBjaGFuZ2VQYXNzQW5kTG9naW4sXG59IGZyb20gXCIuL3NhZ2FcIjtcbmltcG9ydCBjcmVhdGVTYWdhTWlkZGxld2FyZSBmcm9tIFwicmVkdXgtc2FnYVwiO1xuaW1wb3J0IHJvb3RSZWR1Y2VyIGZyb20gXCIuL3JlZHVjZXJcIjtcbmltcG9ydCB7IGNyZWF0ZVN0b3JlLCBhcHBseU1pZGRsZXdhcmUgfSBmcm9tIFwicmVkdXhcIjtcblxuZnVuY3Rpb24qIHJvb3RTYWdhKCkge1xuICB5aWVsZCBhbGwoW1xuICAgIHBlcmlvZGljYWxseVJlYXV0aG9yaXplKCksXG4gICAgdGFrZUxhdGVzdChhY3Rpb25zLlVTRVJfTE9HSU4sIGxvZ2luKSxcbiAgICB0YWtlTGF0ZXN0KGFjdGlvbnMuVVNFUl9MT0dJTl9XSVRIX0NIQU5HRVBBU1MsIGNoYW5nZVBhc3NBbmRMb2dpbiksXG4gICAgdGFrZUxhdGVzdChhY3Rpb25zLlVTRVJfTE9HT1VULCBsb2dvdXQpLFxuICAgIHRha2VMYXRlc3QoYWN0aW9ucy5VU0VSX1JFQVVUSCwgcmVhdXRob3JpemUpLFxuICAgIHRha2VMYXRlc3QoYWN0aW9ucy5VU0VSX0FWQUlMQUJJTElUWV9DSEVDSywgdXNlcm5hbWVBdmFpbGFibGUpLFxuICAgIHRha2VMYXRlc3QoYWN0aW9ucy5VU0VSX0NSRUFURSwgdXNlckNyZWF0ZSksXG4gICAgdGFrZUxhdGVzdChhY3Rpb25zLlVTRVJfTE9HSU5fQ09ORklHLCB2YWxpZGF0ZUNvbmZpZyksXG4gICAgdGFrZUxhdGVzdChhY3Rpb25zLlVTRVJfUkVRVUVTVF9WQUxJREFUSU9OLCB1c2VyUmVxdWVzdFZhbGlkYXRpb24pLFxuICAgIHRha2VMYXRlc3QoYWN0aW9ucy5VU0VSX1ZBTElEQVRFLCB1c2VyVmFsaWRhdGUpLFxuICBdKTtcbn1cblxuY29uc3Qgc2FnYU1pZGRsZXdhcmUgPSBjcmVhdGVTYWdhTWlkZGxld2FyZSgpO1xuY29uc3Qgc3RvcmUgPSAoKSA9PiB7XG4gIGxldCBzdG9yZSA9IGNyZWF0ZVN0b3JlKHJvb3RSZWR1Y2VyLCBhcHBseU1pZGRsZXdhcmUoc2FnYU1pZGRsZXdhcmUpKTtcbiAgc2FnYU1pZGRsZXdhcmUucnVuKHJvb3RTYWdhKTtcbiAgcmV0dXJuIHN0b3JlO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgc3RvcmUoKTtcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkB0aGVwbGFjZWxhYi91aVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1yZWR1eFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWR1eC1zYWdhXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=