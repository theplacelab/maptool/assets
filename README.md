# Asset Service

**v. ALPHA**  
Support for asset management. Coordinates S3, CloudFlare and a registry for metadata database.
`.gitlab-ci.yml` file builds and packages these for Docker/K8, sample Docker configs included.
Front-end (React) [available here](https://gitlab.com/theplacelab/packages/assets).

[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/auth/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/auth/-/pipelines/latest)

# AWS Config

The `AWS_` vars MUST be named the way they are as they are used indirectly to configure the s3 client. In particular, even if `AWS_ACCESS_KEY_ID` does not appear to be used in the code, it is picked up by the s3 client from the environment.

# Cloudflare

Cloudflare API is used to clear the CDN cache after files are deleted.

- Get the Cloudflare CLOUDFLARE_TOKEN [here](https://dash.cloudflare.com/profile/api-tokens)

- Then get the zone identifier (CLOUDFLARE_ZONE / :identifier) for your domain with the following query:

```
curl -X GET "https://api.cloudflare.com/client/v4/zones" \
    -H "X-Auth-Email: CLOUDFLARE_EMAIL" \
    -H "X-Auth-Key: CLOUDFLARE_TOKEN" \
    -H "Content-Type: application/json"
```


# KNEX
yarn knex seed:make 01_assets
knex migrate:make create_users_table
```
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.alterTable('assets', (table) => {
    table.string('metabutt', 128);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.alterTable('assets', (table) => {
    table.dropColumn('metabutt');
  });
};
```